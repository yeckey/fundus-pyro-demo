# Pyro - CINCO Products for the Web

Pyro enables an alternative generation target - the web.
The generated product defined by CINCO can be used simultaneously by multiple users.

Pyro manages your projects and team members, all accessible by any browser.
View, edit and show your models with each device.

# Pyro developer Guide

* [Set up Meta-Pyro](https://gitlab.com/scce/pyro/wikis/meta-pyro)

# Installation Guide

Use the following guidelines to get started with your Pyro Product:

*  [Use Pyro for your CINCO Product](https://gitlab.com/scce/pyro/wikis/usage)
*  [Prepare your local DyWA](https://gitlab.com/scce/pyro/wikis/Prepare-the-DyWA)
*  [Prepare the Pyro App](https://gitlab.com/scce/pyro/wikis/prepare-pyro-app)

# First Steps

The following instructions will guide you to the first steps in the generated Pyro App:

*  [User registration](https://gitlab.com/scce/pyro/wikis/user-register)
*  [Manage projects](https://gitlab.com/scce/pyro/wikis/project-management)

# Pyro Feature

Pyro supports almost all features of CINCO.
A detailed overview of all supported and not (yet) supported features is listed below:

*  [MGL basics](https://gitlab.com/scce/pyro/wikis/mgl-basic-feature)
*  [MGL annotations](https://gitlab.com/scce/pyro/wikis/mgl-annotations-feature)  
*  [CPD annotations](https://gitlab.com/scce/pyro/wikis/cpd-annotations-feature)
*  [Style](https://gitlab.com/scce/pyro/wikis/msl-feature)