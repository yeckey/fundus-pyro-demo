package info.scce.pyro.trans;

/**
 * Author zweihoff
 */

public class BendingPoint
{

    private int x;
    private int y;

    public int getX() {
        return this.x;
    }
    
    public int getY() {
        return this.y;
    }
    
    public BendingPoint(int x,int y) {
    	this.x = x;
    	this.y = y;
    }

}
