package info.scce.pyro.core;

import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroUserController;
import info.scce.pyro.core.rest.types.FindPyroUser;
import info.scce.pyro.core.rest.types.PyroUser;
import info.scce.pyro.core.rest.types.PyroUserSearch;

import java.util.stream.Collectors;

@javax.transaction.Transactional
@javax.ws.rs.Path("/users")
public class UsersController {
	
	@javax.inject.Inject
	private de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroUserController subjectController;

	@javax.inject.Inject
	private info.scce.pyro.rest.ObjectCache objectCache;
	
	/** Get all users. */
	@javax.ws.rs.GET
	@javax.ws.rs.Path("/")
	@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@org.jboss.resteasy.annotations.GZIP
	public javax.ws.rs.core.Response  getUsers() {
		final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser subject = subjectController.read((Long)org.apache.shiro.SecurityUtils.getSubject().getPrincipal());
	
		if(subject!=null && isAdmin(subject)) {
			
			final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser searchObject = subjectController.createSearchObject(null);
			
			final java.util.List<de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser> result = subjectController.findByProperties(searchObject);
			
			final java.util.List<PyroUser> users = new java.util.ArrayList<>();
			for (de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser user : result) {
				users.add(PyroUser.fromDywaEntity(user, objectCache));
			}
 
			return javax.ws.rs.core.Response.ok(users).build();
		}

		return javax.ws.rs.core.Response.status(
				javax.ws.rs.core.Response.Status.FORBIDDEN).build();
	}
	
	/** Get a user by its username or email. */
	@javax.ws.rs.POST
	@javax.ws.rs.Path("/search")
	@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@org.jboss.resteasy.annotations.GZIP
	public javax.ws.rs.core.Response searchUser(PyroUserSearch search) {
		final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser subject = subjectController.read((Long)org.apache.shiro.SecurityUtils.getSubject().getPrincipal());
		
		if(subject!=null) {
			final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser searchObject = subjectController.createSearchObject(null);
			searchObject.setusername(search.getusernameOrEmail());
			
			final java.util.List<de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser> resultByUsername = subjectController.findByProperties(searchObject);
			if (resultByUsername.size() == 1) {
				return javax.ws.rs.core.Response.ok(PyroUser.fromDywaEntity(resultByUsername.get(0), objectCache)).build();
			}
			
			searchObject.setemail(search.getusernameOrEmail());
			
			final java.util.List<de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser> resultByEmail = subjectController.findByProperties(searchObject);
			if (resultByEmail.size() == 1) {
				return javax.ws.rs.core.Response.ok(PyroUser.fromDywaEntity(resultByEmail.get(0), objectCache)).build();
			}
			
			return javax.ws.rs.core.Response.status(
					javax.ws.rs.core.Response.Status.NOT_FOUND).build();
		}
		
		return javax.ws.rs.core.Response.status(
				javax.ws.rs.core.Response.Status.FORBIDDEN).build();
	}
	
	@javax.ws.rs.DELETE
	@javax.ws.rs.Path("/{userId}")
	@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@org.jboss.resteasy.annotations.GZIP
	public javax.ws.rs.core.Response delete(@javax.ws.rs.PathParam("userId") final long userId) {
		final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser subject = subjectController.read((Long)org.apache.shiro.SecurityUtils.getSubject().getPrincipal());
		if(subject!=null && isAdmin(subject)) {
			final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser userToDelete = subjectController.read(userId);
			if (subject.equals(userToDelete)) { // an admin should not delete himself
				return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.BAD_REQUEST).build(); 
			}	
			
			// TODO delete all access right vectors
			// TODO delete user from all organizations
			// TODO delete user as owner from all projects
			// TODO delete user 
			
			return javax.ws.rs.core.Response.ok().build();
		}
		return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.FORBIDDEN).build();
	}
	
	@javax.ws.rs.POST
	@javax.ws.rs.Path("/{userId}/roles/addAdmin")
	@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@org.jboss.resteasy.annotations.GZIP
	public javax.ws.rs.core.Response makeAdmin(@javax.ws.rs.PathParam("userId") final long userId) {
		final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser subject = subjectController.read((Long)org.apache.shiro.SecurityUtils.getSubject().getPrincipal());
		
		if(subject!=null && isAdmin(subject)) {
			final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser user = subjectController.read(userId);
			if (user == null) {
				return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.NOT_FOUND).build();
			}
			
			if (!user.getsystemRoles_PyroSystemRole().contains(de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroSystemRole.ADMIN)) {
				user.getsystemRoles_PyroSystemRole().add(de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroSystemRole.ADMIN);
				user.getsystemRoles_PyroSystemRole().add(de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroSystemRole.ORGANIZATION_MANAGER);
			}
			
			return javax.ws.rs.core.Response.ok(PyroUser.fromDywaEntity(user,objectCache)).build();
		}
		
		return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.FORBIDDEN).build();
	}
	
	@javax.ws.rs.POST
	@javax.ws.rs.Path("/{userId}/roles/removeAdmin")
	@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@org.jboss.resteasy.annotations.GZIP
	public javax.ws.rs.core.Response makeUser(@javax.ws.rs.PathParam("userId") final long userId) {
		final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser subject = subjectController.read((Long)org.apache.shiro.SecurityUtils.getSubject().getPrincipal());
		
		if(subject!=null && isAdmin(subject)) {
			final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser user = subjectController.read(userId);
			if (user == null) {
				return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.NOT_FOUND).build();
			}
						
			// an admin should not remove his own admin rights
			if (isAdmin(user) && user.getDywaId() == subject.getDywaId()) {
				return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.BAD_REQUEST).build();
			}
			
			user.getsystemRoles_PyroSystemRole().remove(de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroSystemRole.ADMIN);
			return javax.ws.rs.core.Response.ok(PyroUser.fromDywaEntity(user,objectCache)).build();
		}
		
		return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.FORBIDDEN).build();
	}
	
	@javax.ws.rs.POST
	@javax.ws.rs.Path("/{userId}/roles/addOrgManager")
	@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@org.jboss.resteasy.annotations.GZIP
	public javax.ws.rs.core.Response addOrgManager(@javax.ws.rs.PathParam("userId") final long userId) {
		final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser subject = subjectController.read((Long)org.apache.shiro.SecurityUtils.getSubject().getPrincipal());
		
		if(subject!=null && isAdmin(subject)) {
			final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser user = subjectController.read(userId);
			if (user == null) {
				return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.NOT_FOUND).build();
			}
			
			if (!user.getsystemRoles_PyroSystemRole().contains(de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroSystemRole.ORGANIZATION_MANAGER)) {
				user.getsystemRoles_PyroSystemRole().add(de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroSystemRole.ORGANIZATION_MANAGER);
			}
			
			return javax.ws.rs.core.Response.ok(PyroUser.fromDywaEntity(user,objectCache)).build();
		}
		
		return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.FORBIDDEN).build();
	}
	
	@javax.ws.rs.POST
	@javax.ws.rs.Path("/{userId}/roles/removeOrgManager")
	@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@org.jboss.resteasy.annotations.GZIP
	public javax.ws.rs.core.Response removeOrgManager(@javax.ws.rs.PathParam("userId") final long userId) {
		final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser subject = subjectController.read((Long)org.apache.shiro.SecurityUtils.getSubject().getPrincipal());
		
		if(subject!=null && isAdmin(subject)) {
			final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser user = subjectController.read(userId);
			if (user == null) {
				return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.NOT_FOUND).build();
			}
												
			user.getsystemRoles_PyroSystemRole().remove(de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroSystemRole.ORGANIZATION_MANAGER);
			return javax.ws.rs.core.Response.ok(PyroUser.fromDywaEntity(user,objectCache)).build();
		}
		
		return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.FORBIDDEN).build();
	}
			
	private boolean isAdmin(de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser user) {
		return user.getsystemRoles_PyroSystemRole().contains(de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroSystemRole.ADMIN);
	}
}
