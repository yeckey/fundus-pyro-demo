package info.scce.pyro.api;

import de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroFile;
import de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroFolder;
import de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject;
import de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser;
import graphmodel.GraphModel;
import info.scce.pyro.core.MainControllerBundle;
import info.scce.pyro.core.command.CommandExecuter;
import info.scce.pyro.message.MessageDialog;

import javax.persistence.FlushModeType;

import org.eclipse.xtext.xbase.lib.Extension;

/**
 * Author zweihoff
 */
public abstract class PyroControl {

    private MessageDialog messageDialog;

    private CommandExecuter cmdExecuter;
    
    @org.eclipse.xtext.xbase.lib.Extension
    protected de.jabc.cinco.meta.runtime.xapi.GraphModelExtension _graphModelExtension = new de.jabc.cinco.meta.runtime.xapi.GraphModelExtension();


    public final void init(CommandExecuter cmdExecuter) {
        this.cmdExecuter = cmdExecuter;
        messageDialog = new MessageDialog(cmdExecuter, cmdExecuter.getGraphModelWebSocket());
    }
    
    public final MainControllerBundle getMainControllerBundle() {
        return this.cmdExecuter.mainContollerBundle;
    }

    public final CommandExecuter commandExecuter() {
        return cmdExecuter;
    }
    
    public final PyroUser getCurrentUser() {
        return this.cmdExecuter.getBatch().getUser();
    }

    public final MessageDialog messageDialog() {
        return messageDialog;
    }

    public final PyroProject currentProject() {
        return cmdExecuter.pyroProject;
    }
    
    protected final void openFile(GraphModel g) {
        cmdExecuter.openFile(g.getDelegate());
    }
    protected final void openFile(PyroFile g) {
        cmdExecuter.openFile(g);
    }

    protected final void setFlushModeAUTO() {
        cmdExecuter.getControllerBundle().getEntityManager().setFlushMode(FlushModeType.AUTO);
    }
    protected final void setFlushModeACOMMIT() {
        cmdExecuter.getControllerBundle().getEntityManager().setFlushMode(FlushModeType.COMMIT);
    }

    protected final PyroFolder getParentFolder(graphmodel.GraphModel g) {
        PyroFolder current = this.commandExecuter().pyroProject;
        return getParentFolder(current,g);
    }

    protected final PyroProject getProject() {
        return this.commandExecuter().pyroProject;
    }

    private PyroFolder getParentFolder(PyroFolder current,graphmodel.GraphModel g) {
        if(current.getfiles_PyroFile().stream().filter(n->n.getDywaId()==g.getDelegate().getDywaId()).findFirst().isPresent()) {
            return current;
        }
        for(PyroFolder pf:current.getinnerFolders_PyroFolder()) {
            PyroFolder result = getParentFolder(pf,g);
            if(result != null) {
                return result;
            }
        }
        return null;
    }
}

