package info.scce.pyro.core.graphmodel;

/**
 * Author zweihoff
 */

@com.fasterxml.jackson.annotation.JsonFilter("DIME_Selective_Filter")
@com.fasterxml.jackson.annotation.JsonIdentityInfo(generator = com.voodoodyne.jackson.jsog.JSOGGenerator.class)
@com.fasterxml.jackson.annotation.JsonTypeInfo(use = com.fasterxml.jackson.annotation.JsonTypeInfo.Id.CLASS, property = info.scce.pyro.util.Constants.DYWA_RUNTIME_TYPE)
public interface Edge extends ModelElement
{


    @com.fasterxml.jackson.annotation.JsonProperty("a_sourceElement")
    public Node getsourceElement();

    @com.fasterxml.jackson.annotation.JsonProperty("sourceElement")
    public void setsourceElement(final Node sourceElement);
    
    @com.fasterxml.jackson.annotation.JsonProperty("a_targetElement")
    public Node gettargetElement();

    @com.fasterxml.jackson.annotation.JsonProperty("targetElement")
    public void settargetElement(final Node targetElement);
    
    @com.fasterxml.jackson.annotation.JsonProperty("a_bendingPoints")
    public java.util.List<BendingPoint> getbendingPoints();

    @com.fasterxml.jackson.annotation.JsonProperty("bendingPoints")
    public void setbendingPoints(final java.util.List<BendingPoint> bendingPoints);
}
