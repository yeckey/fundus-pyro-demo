package info.scce.pyro.core.rest.types;

import java.util.List;

/**
 * Author zweihoff
 */

@com.fasterxml.jackson.annotation.JsonFilter("DIME_Selective_Filter")
@com.fasterxml.jackson.annotation.JsonIdentityInfo(generator = com.voodoodyne.jackson.jsog.JSOGGenerator.class)
public class ProjectServiceList
{
    private List<String> active = new java.util.LinkedList<>();
    private List<String> disabled = new java.util.LinkedList<>();

    @com.fasterxml.jackson.annotation.JsonProperty("active")
    public List<String> getActive() {
        return this.active;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("active")
    public void setActive(final List<String> active) {
        this.active = active;
    }
    
    @com.fasterxml.jackson.annotation.JsonProperty("disabled")
    public List<String> getDisabled() {
        return this.disabled;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("disabled")
    public void setDisabled(final List<String> disabled) {
        this.disabled = disabled;
    }
    
}
