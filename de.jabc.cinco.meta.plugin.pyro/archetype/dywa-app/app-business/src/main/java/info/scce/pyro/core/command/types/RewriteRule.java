package info.scce.pyro.core.command.types;

/**
 * Author zweihoff
 */
@com.fasterxml.jackson.annotation.JsonFilter("DIME_Selective_Filter")
@com.fasterxml.jackson.annotation.JsonIdentityInfo(generator = com.voodoodyne.jackson.jsog.JSOGGenerator.class)
@com.fasterxml.jackson.annotation.JsonTypeInfo(use = com.fasterxml.jackson.annotation.JsonTypeInfo.Id.CLASS, property = info.scce.pyro.util.Constants.DYWA_RUNTIME_TYPE)
public class RewriteRule {
    @com.fasterxml.jackson.annotation.JsonProperty("newId")
    long newId;
    @com.fasterxml.jackson.annotation.JsonProperty("oldId")
    long oldId;
    
    public RewriteRule(long oldId, long newId) {
    	this.oldId = oldId;
    	this.newId = newId;
    }
    
}
