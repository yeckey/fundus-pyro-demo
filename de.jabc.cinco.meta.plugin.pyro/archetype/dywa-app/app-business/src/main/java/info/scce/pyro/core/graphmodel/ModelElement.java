package info.scce.pyro.core.graphmodel;

/**
 * Author zweihoff
 */

@com.fasterxml.jackson.annotation.JsonFilter("DIME_Selective_Filter")
@com.fasterxml.jackson.annotation.JsonIdentityInfo(generator = com.voodoodyne.jackson.jsog.JSOGGenerator.class)
@com.fasterxml.jackson.annotation.JsonTypeInfo(use = com.fasterxml.jackson.annotation.JsonTypeInfo.Id.CLASS, property = info.scce.pyro.util.Constants.DYWA_RUNTIME_TYPE)
public interface ModelElement extends IdentifiableElement
{

    @com.fasterxml.jackson.annotation.JsonProperty("a_container")
    public IdentifiableElement getcontainer();

    @com.fasterxml.jackson.annotation.JsonProperty("container")
    public void setcontainer(final IdentifiableElement container);

}
