package info.scce.pyro.core.rest.types;

/**
 * Author zweihoff
 */

@com.fasterxml.jackson.annotation.JsonFilter("DIME_Selective_Filter")
@com.fasterxml.jackson.annotation.JsonIdentityInfo(generator = com.voodoodyne.jackson.jsog.JSOGGenerator.class)
public class PyroFolder extends info.scce.pyro.rest.RESTBaseImpl implements info.scce.pyro.rest.RESTBaseType
{


    private String name;

    @com.fasterxml.jackson.annotation.JsonProperty("name")
    public String getname() {
        return this.name;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("name")
    public void setname(final String name) {
        this.name = name;
    }


    private java.util.List<PyroFolder> innerFolders = new java.util.LinkedList<>();

    @com.fasterxml.jackson.annotation.JsonProperty("innerFolders")
    public java.util.List<PyroFolder> getinnerFolders() {
        return this.innerFolders;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("innerFolders")
    public void setinnerFolders(final java.util.List<PyroFolder> innerFolders) {
        this.innerFolders = innerFolders;
    }


    private java.util.List<IPyroFile> files = new java.util.LinkedList<>();

    @com.fasterxml.jackson.annotation.JsonProperty("files")
    public java.util.List<IPyroFile> getfiles() {
        return this.files;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("files")
    public void setfiles(final java.util.List<IPyroFile> files) {
        this.files = files;
    }





    public static PyroFolder fromDywaEntity(final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroFolder entity, info.scce.pyro.rest.ObjectCache objectCache) {

        if(objectCache.containsRestTo(entity)){
            return objectCache.getRestTo(entity);
        }
        final PyroFolder result;
        result = new PyroFolder();
        result.setDywaId(entity.getDywaId());
        result.setDywaName(entity.getDywaName());
        result.setDywaVersion(entity.getDywaVersion());

        result.setname(entity.getname());

        for(de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroFolder p:entity.getinnerFolders_PyroFolder()){
            result.getinnerFolders().add(PyroFolder.fromDywaEntity(p,objectCache));
        }

        for(de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroFile p:entity.getfiles_PyroFile()){
        	if(p instanceof de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroModelFile) {
        		result.getfiles().add((IPyroFile)PyroModelFile.fromDywaEntity((de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroModelFile)p,objectCache));        		
        	}
        	if(p instanceof de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroBinaryFile) {
        		result.getfiles().add(PyroBinaryFile.fromDywaEntity((de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroBinaryFile)p,objectCache));        		
        	}
        	if(p instanceof de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroTextualFile) {
        		result.getfiles().add(PyroTextualFile.fromDywaEntity((de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroTextualFile)p,objectCache));        		
        	}
        	if(p instanceof de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroURLFile) {
        		result.getfiles().add(PyroURLFile.fromDywaEntity((de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroURLFile)p,objectCache));        		
        	}
        }

        objectCache.putRestTo(entity, result);

        return result;
    }
}
