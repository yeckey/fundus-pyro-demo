package info.scce.pyro.core;

import de.ls5.dywa.generated.controller.info.scce.pyro.core.*;
import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroFileController;
import de.ls5.dywa.generated.entity.info.scce.pyro.core.*;
import de.ls5.dywa.generated.util.DomainFileController;
import de.ls5.dywa.generated.util.FileReference;
import info.scce.pyro.core.rest.types.CreatePyroFolder;
import info.scce.pyro.core.rest.types.GraphModelProperty;
import info.scce.pyro.core.rest.types.PyroProjectStructure;
import info.scce.pyro.core.rest.types.UpdatePyroFolder;
import info.scce.pyro.sync.GraphModelWebSocket;
import info.scce.pyro.sync.ProjectWebSocket;
import info.scce.pyro.sync.WebSocketMessage;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import java.util.LinkedList;

@javax.transaction.Transactional
@javax.ws.rs.Path("/graph")
public class GraphModelController {

    @javax.inject.Inject
    private IdentifiableElementController identifiableElementController;

    @javax.inject.Inject
    private DomainFileController domainFileController;

    @javax.inject.Inject
    private BendingPointController bendingPointController;

    @javax.inject.Inject
    private de.ls5.dywa.generated.controller.info.scce.pyro.core.GraphModelController graphModelController;

	@javax.inject.Inject
	private PyroUserController subjectController;

    @javax.inject.Inject
    private PyroFolderController folderController;
    
    @javax.inject.Inject
    private PyroFileController fileController;

	@javax.inject.Inject
	private PyroProjectController projectController;

	@javax.inject.Inject
	private info.scce.pyro.rest.ObjectCache objectCache;

    @javax.inject.Inject
    private ProjectWebSocket projectWebSocket;

    @javax.inject.Inject
    private GraphModelWebSocket graphModelWebSocket;

	@javax.ws.rs.POST
	@javax.ws.rs.Path("create/folder/private")
	@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@javax.ws.rs.Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@org.jboss.resteasy.annotations.GZIP
	public Response createFolder(CreatePyroFolder newFolder) {

        //find parent
        final PyroFolder pf = folderController.read(newFolder.getparentId());
        checkPermission(pf);
        if(pf==null){
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        final PyroFolder newPF = folderController.create("PyroFolder_"+newFolder.getname());
        newPF.setname(newFolder.getname());
        pf.getinnerFolders_PyroFolder().add(newPF);
        sendProjectUpdate(pf);

		return Response.ok(info.scce.pyro.core.rest.types.PyroFolder.fromDywaEntity(newPF,objectCache)).build();
		
	}
	
	
	
	
	@javax.ws.rs.POST
    @javax.ws.rs.Path("move/folder/{id}/{targetId}/private")
    @javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    @org.jboss.resteasy.annotations.GZIP
    public Response moveFolder(@javax.ws.rs.PathParam("id") final long id, 
    					       @javax.ws.rs.PathParam("targetId") final long targetId) {
		
		final PyroFolder sourceFolder = folderController.read(id);
		if (sourceFolder == null) return Response.status(Response.Status.NOT_FOUND).build();
        checkPermission(sourceFolder);
        
        PyroFolder targetFolder = folderController.read(targetId);
    	if (targetFolder == null) {
    		targetFolder = projectController.read(targetId);
    		if (targetFolder == null) return Response.status(Response.Status.NOT_FOUND).build();
    	}
    	checkPermission(targetFolder);
    	
    	if (id == targetId) {
    		return Response.status(Response.Status.BAD_REQUEST).entity("Cannot move folder to itself").build();
    	}
    	    	    	
    	for (final PyroFolder f: targetFolder.getinnerFolders_PyroFolder()) {
    		if (sourceFolder.getname().equals(f.getname())) 
				return Response.status(Response.Status.BAD_REQUEST).entity("Name already exists").build();
    	}
    	
    	if (isAscendantFolderOf(sourceFolder, targetFolder)) {
    		return Response.status(Response.Status.BAD_REQUEST).entity("Cannot move folder to ascendant").build();
    	}
    	    	
    	final PyroFolder parentFolder = getParent(sourceFolder);
    	parentFolder.getinnerFolders_PyroFolder().remove(sourceFolder);
    	targetFolder.getinnerFolders_PyroFolder().add(sourceFolder);
    	
    	sendProjectUpdate(targetFolder);
    	sendProjectUpdate(parentFolder);
		
    	return Response.ok().build();
    }
	
	private boolean isAscendantFolderOf(PyroFolder parent, PyroFolder possibleAcendant) {
		final java.util.Queue<PyroFolder> queue = new java.util.ArrayDeque<>();
		queue.offer(parent);
		while (!queue.isEmpty()) {
			final PyroFolder p = queue.poll();
			for (PyroFolder f: p.getinnerFolders_PyroFolder()) {
				if (f.equals(possibleAcendant)) return true;
				queue.offer(f);
			}
		}
		return false;
	}
	
	private void sendProjectUpdate(PyroFolder folder){
        final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser subject = subjectController.read((Long)org.apache.shiro.SecurityUtils.getSubject().getPrincipal());

        final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject parent = getProject(folder);
        projectWebSocket.send(parent.getDywaId(), WebSocketMessage.fromDywaEntity(subject.getDywaId(),PyroProjectStructure.fromDywaEntity(parent,objectCache)));

    }

    void checkPermission(PyroFolder folder) {
        final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser user = subjectController.read((Long)org.apache.shiro.SecurityUtils.getSubject().getPrincipal());

        de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject project = getProject(folder);
        if(project.getorganization().getowners_PyroUser().contains(user) 
        		|| project.getorganization().getmembers_PyroUser().contains(user)){
            return;
        }
        throw new WebApplicationException(Response.Status.FORBIDDEN);
    }

    de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject getProject(de.ls5.dywa.generated.entity.info.scce.pyro.core.GraphModel graph){
        PyroFolder so = folderController.createSearchObject("search_folder");
        so.setfiles_PyroFile(new LinkedList<>());
        so.getfiles_PyroFile().add(graph);
        java.util.List<PyroFolder> parents = folderController.findByProperties(so);
        if(parents.isEmpty()){
            throw new IllegalStateException("Graph without parent detected");
        }
        return getProject(parents.get(0));
    }

    de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject getProject(PyroFolder folder){
	    if(folder instanceof de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject){
	        return (de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject) folder;
        }
        return getProject(getParent(folder));
    }
    
    PyroFolder getParent(PyroFolder folder) {
    	PyroFolder so = folderController.createSearchObject("search_project");
        so.setinnerFolders_PyroFolder(new LinkedList<>());
        so.getinnerFolders_PyroFolder().add(folder);
        java.util.List<PyroFolder> parents = folderController.findByProperties(so);
        if(parents.isEmpty()){
            throw new IllegalStateException("Folder without parent detected");
        }
        return parents.get(0);
    }

    @javax.ws.rs.POST
    @javax.ws.rs.Path("update/folder/private")
    @javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    @javax.ws.rs.Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    @org.jboss.resteasy.annotations.GZIP
    public Response updateFolder(UpdatePyroFolder folder) {

        //find folder
        final PyroFolder pf = folderController.read(folder.getdywaId());
        checkPermission(pf);
        if(pf==null){
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        pf.setname(folder.getname());
        sendProjectUpdate(pf);
        return Response.ok(info.scce.pyro.core.rest.types.PyroFolder.fromDywaEntity(pf,objectCache)).build();
    }

    @javax.ws.rs.POST
    @javax.ws.rs.Path("update/graphmodel/private")
    @javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    @javax.ws.rs.Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    @org.jboss.resteasy.annotations.GZIP
    public Response updateGraphModel(info.scce.pyro.core.graphmodel.GraphModel graphModel) {

        //find graphmodel
        final GraphModel gm = graphModelController.read(graphModel.getDywaId());

        PyroFolder pf = folderController.createSearchObject("search_parent_folder");
        pf.setfiles_PyroFile(new LinkedList<>());
        pf.getfiles_PyroFile().add(gm);
        PyroFolder parentFolder = folderController.findByProperties(pf).get(0);

        checkPermission(parentFolder);

        de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject project = getProject(parentFolder);

        if(gm==null){
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        gm.setfilename(graphModel.getfilename());
        if(graphModel.getscale()!=null){
            gm.setscale(graphModel.getscale());
        }
        if(graphModel.getheight()!=null){
            gm.setheight(graphModel.getheight());
        }
        if(graphModel.getwidth()!=null){
            gm.setwidth(graphModel.getwidth());
        }
        if(graphModel.getconnector()!=null){
            gm.setconnector(graphModel.getconnector());
        }
        gm.setrouter(graphModel.getrouter());

        final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser subject = subjectController.read((Long)org.apache.shiro.SecurityUtils.getSubject().getPrincipal());
        graphModelWebSocket.send(gm.getDywaId(),WebSocketMessage.fromDywaEntity(subject.getDywaId(), GraphModelProperty.fromDywaEntity(gm)));

        projectWebSocket.send(project.getDywaId(), WebSocketMessage.fromDywaEntity(subject.getDywaId(),PyroProjectStructure.fromDywaEntity(project,objectCache)));
        
        return Response.ok(GraphModelProperty.fromDywaEntity(gm)).build();
    }
    
    @javax.ws.rs.POST
    @javax.ws.rs.Path("update/graphmodel/shared/private")
    @javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    @javax.ws.rs.Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    @org.jboss.resteasy.annotations.GZIP
    public Response updateGraphModelSharing(info.scce.pyro.core.rest.types.GraphModelShared graphModel) {

        //find graphmodel
        final GraphModel gm = graphModelController.read(graphModel.getDywaId());

        PyroFolder pf = folderController.createSearchObject("search_parent_folder");
        pf.setfiles_PyroFile(new LinkedList<>());
        pf.getfiles_PyroFile().add(gm);
        PyroFolder parentFolder = folderController.findByProperties(pf).get(0);

        checkPermission(parentFolder);

        if(gm==null){
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        gm.setisPublic(graphModel.getisPublic());

        return Response.ok(graphModel).build();
    }

    @javax.ws.rs.GET
    @javax.ws.rs.Path("remove/folder/{id}/{parentId}/private")
    @javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    @org.jboss.resteasy.annotations.GZIP
    public Response removeFolder(@javax.ws.rs.PathParam("id") final long id,@javax.ws.rs.PathParam("parentId") final long parentId) {

        //find parent
        final PyroFolder pf = folderController.read(id);

        checkPermission(pf);

        final PyroFolder parent = folderController.read(parentId);
        if(pf==null||parent==null){
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        //cascade remove
        if(parent.getinnerFolders_PyroFolder().contains(pf)){
            removeFolder(pf,parent);
            sendProjectUpdate(parent);
            return Response.ok("OK").build();
        }
        return Response.status(Response.Status.BAD_REQUEST).build();
    }


    void removeFolder(PyroFolder folder,PyroFolder parent) {

        java.util.List<PyroFolder> innerFolder = new LinkedList<>(folder.getinnerFolders_PyroFolder());
        innerFolder.forEach(n->removeFolder(n,folder));

        java.util.List<PyroFile> files = new LinkedList<>(folder.getfiles_PyroFile());
        folder.getfiles_PyroFile().clear();
        files.forEach(this::removeFile);
        if(parent!=null){
            parent.getinnerFolders_PyroFolder().remove(folder);
        }
        files.forEach(n->fileController.deleteWithIncomingReferences(n));
        folderController.delete(folder);
    }

    private void removeContainer(ModelElementContainer container) {
        container.getmodelElements_ModelElement().forEach((n)->{
            if(n instanceof Container){
                removeContainer((Container) n);
                removeNode((Container)n);
            }
            if(n instanceof Node){
                removeNode((Node) n);
            }
        });
        container.getmodelElements_ModelElement().forEach((n)->{
            if(n instanceof Edge){
                removeEdge((Edge) n);
            }
        });
        container.getmodelElements_ModelElement().clear();
        if(container instanceof Node) {
            removeNode((Node)container);
        }
    }

    private void removeFile(PyroFile pyroFile) {
        if(pyroFile instanceof PyroBinaryFile) {
            FileReference fileReference = ((PyroBinaryFile) pyroFile).getfile();
            ((PyroBinaryFile) pyroFile).setfile(null);
            domainFileController.deleteFile(fileReference);
        }
        if(pyroFile instanceof PyroModelFile) {
            if(pyroFile instanceof de.ls5.dywa.generated.entity.info.scce.pyro.core.GraphModel) {
                removeContainer((de.ls5.dywa.generated.entity.info.scce.pyro.core.GraphModel)pyroFile);
            }
        }
    }


    private void removeNode(Node node){
        node.getincoming_Edge().forEach(e->e.settargetElement(null));
        node.getincoming_Edge().clear();
        node.getoutgoing_Edge().forEach(e->e.setsourceElement(null));
        node.getoutgoing_Edge().clear();
        node.setcontainer(null);
        identifiableElementController.delete(node);

    }

    private void removeEdge(Edge edge){
        edge.setsourceElement(null);
        edge.settargetElement(null);
        edge.setcontainer(null);
        edge.getbendingPoints_BendingPoint().forEach(n->bendingPointController.delete(n));
        edge.getbendingPoints_BendingPoint().clear();
        identifiableElementController.delete(edge);
    }



}
