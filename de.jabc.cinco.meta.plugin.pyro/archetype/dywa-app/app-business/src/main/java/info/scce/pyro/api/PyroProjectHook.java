package info.scce.pyro.api;

import de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject;

/**
 * Author zweihoff
 */
public abstract class PyroProjectHook extends PyroHook {

    public abstract void execute(PyroProject project);
    
    public boolean canExecute(PyroProject project) { return true; }
}

