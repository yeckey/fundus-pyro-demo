package info.scce.pyro.core.command.types;


/**
 * Author zweihoff
 */
@com.fasterxml.jackson.annotation.JsonFilter("DIME_Selective_Filter")
@com.fasterxml.jackson.annotation.JsonIdentityInfo(generator = com.voodoodyne.jackson.jsog.JSOGGenerator.class)
@com.fasterxml.jackson.annotation.JsonTypeInfo(use = com.fasterxml.jackson.annotation.JsonTypeInfo.Id.CLASS, property = info.scce.pyro.util.Constants.DYWA_RUNTIME_TYPE)
public class CheckResultCommand extends Command {

    private java.util.List<CheckResult> results = new java.util.LinkedList<>();

    @com.fasterxml.jackson.annotation.JsonProperty("results")
    public java.util.List<CheckResult> getResults() {
        return results;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("message")
    public void setMessage(java.util.List<CheckResult> results) {
        this.results = results;
    }
    
    public static CheckResultCommand fromElement(graphmodel.IdentifiableElement element) {
        CheckResultCommand crc = new CheckResultCommand();
        crc.setDelegateId(element.getDelegate().getDywaId());
        crc.setDywaName(element.getClass().getSimpleName());
        crc.setDywaVersion(element.getDelegate().getDywaVersion());
        return crc;
    }
    
    public void addResult(String msg, String type) {
    	CheckResult cr = new CheckResult();
    	cr.setMessage(msg);
    	cr.setType(type);
    	results.add(cr);
    }

    @Override
    protected void rewrite(long oldId, long newId) {
        
    }
}
