package info.scce.pyro.core.rest.types;

/**
 * Author zweihoff
 */

@com.fasterxml.jackson.annotation.JsonFilter("DIME_Selective_Filter")
@com.fasterxml.jackson.annotation.JsonIdentityInfo(generator = com.voodoodyne.jackson.jsog.JSOGGenerator.class)
public class PyroTextualFile extends PyroFile
{

    private String content;

    @com.fasterxml.jackson.annotation.JsonProperty("content")
    public String getcontent() {
        return this.content;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("content")
    public void setcontent(final String content) {
        this.content = content;
    }

    




    public static PyroTextualFile fromDywaEntity(final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroTextualFile entity, info.scce.pyro.rest.ObjectCache objectCache) {

        if(objectCache.containsRestTo(entity)){
            return objectCache.getRestTo(entity);
        }
        final PyroTextualFile result;
        result = new PyroTextualFile();
        result.setDywaId(entity.getDywaId());
        result.setDywaName(entity.getDywaName());
        result.setDywaVersion(entity.getDywaVersion());
        result.set__type(entity.getClass().getSimpleName());

        result.setfilename(entity.getfilename());
        result.setextension(entity.getextension());
        result.setcontent(entity.getcontent());

        objectCache.putRestTo(entity, result);

        return result;
    }
}
