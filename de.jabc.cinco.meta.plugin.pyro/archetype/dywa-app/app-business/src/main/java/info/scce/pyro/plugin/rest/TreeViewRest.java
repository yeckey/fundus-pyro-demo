package info.scce.pyro.plugin.rest;

import de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroElement;

import java.util.List;

/**
 * Author zweihoff
 */

@com.fasterxml.jackson.annotation.JsonFilter("DIME_Selective_Filter")
@com.fasterxml.jackson.annotation.JsonIdentityInfo(generator = com.voodoodyne.jackson.jsog.JSOGGenerator.class)
public class TreeViewRest
{

    private List<TreeViewNodeRest> layer;

    @com.fasterxml.jackson.annotation.JsonProperty("layer")
    public List<TreeViewNodeRest> getlayer() {
        return this.layer;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("layer")
    public void setlayer(final List<TreeViewNodeRest> layer) {
        this.layer = layer;
    }

}
