package info.scce.pyro.core.rest.types;

@com.fasterxml.jackson.annotation.JsonFilter("DIME_Selective_Filter")
@com.fasterxml.jackson.annotation.JsonIdentityInfo(generator = com.voodoodyne.jackson.jsog.JSOGGenerator.class)
public class PyroSettings extends info.scce.pyro.rest.RESTBaseImpl implements info.scce.pyro.rest.RESTBaseType {
   
    private PyroStyle style;

    @com.fasterxml.jackson.annotation.JsonProperty("style")
    public PyroStyle getstyle() {
        return this.style;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("style")
    public void setstyle(final PyroStyle style) {
        this.style = style;
    }
    
    
    
    private boolean globallyCreateOrganizations;
    
    @com.fasterxml.jackson.annotation.JsonProperty("globallyCreateOrganizations")
    public boolean getgloballyCreateOrganizations() {
        return this.globallyCreateOrganizations;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("globallyCreateOrganizations")
    public void setgloballyCreateOrganizations(final boolean globallyCreateOrganizations) {
        this.globallyCreateOrganizations = globallyCreateOrganizations;
    }
    
    

    public static PyroSettings fromDywaEntity(
    		final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroSettings entity, 
    		final info.scce.pyro.rest.ObjectCache objectCache) {

        if(objectCache.containsRestTo(entity)){
            return objectCache.getRestTo(entity);
        }
        
        final PyroSettings result;
        result = new PyroSettings();
        result.setDywaId(entity.getDywaId());
        result.setDywaName(entity.getDywaName());
        result.setDywaVersion(entity.getDywaVersion());
        result.setstyle(PyroStyle.fromDywaEntity(entity.getstyle(), objectCache));
        result.setgloballyCreateOrganizations(entity.getgloballyCreateOrganizations());
        
        objectCache.putRestTo(entity, result);
                
        return result;
    }
}