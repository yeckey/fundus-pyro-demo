package info.scce.pyro.core.rest.types;

/**
 * Author zweihoff
 */

@com.fasterxml.jackson.annotation.JsonFilter("DIME_Selective_Filter")
@com.fasterxml.jackson.annotation.JsonIdentityInfo(generator = com.voodoodyne.jackson.jsog.JSOGGenerator.class)
public class PyroProject extends info.scce.pyro.rest.RESTBaseImpl implements info.scce.pyro.rest.RESTBaseType
{

    private PyroUser owner;

    @com.fasterxml.jackson.annotation.JsonProperty("owner")
    public PyroUser geowner() {
        return this.owner;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("owner")
    public void setowner(final PyroUser owner) {
        this.owner = owner;
    }
    
    
    
    private PyroOrganization organization;

    @com.fasterxml.jackson.annotation.JsonProperty("organization")
    public PyroOrganization getorganization() {
        return this.organization;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("organization")
    public void setorganization(final PyroOrganization organization) {
        this.organization = organization;
    }
    

   
    private java.lang.String name;

    @com.fasterxml.jackson.annotation.JsonProperty("name")
    public java.lang.String getname() {
        return this.name;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("name")
    public void setname(final java.lang.String name) {
        this.name = name;
    }

    
    
    private java.lang.String description;

    @com.fasterxml.jackson.annotation.JsonProperty("description")
    public java.lang.String getdescription() {
        return this.description;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("description")
    public void setdescription(final java.lang.String description) {
        this.description = description;
    }

    

    public static PyroProject fromDywaEntity(final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject entity, info.scce.pyro.rest.ObjectCache objectCache) {

        if(objectCache.containsRestTo(entity)){
            return objectCache.getRestTo(entity);
        }
        final PyroProject result;
        result = new PyroProject();
        result.setDywaId(entity.getDywaId());
        result.setDywaName(entity.getDywaName());
        result.setDywaVersion(entity.getDywaVersion());

        result.setname(entity.getname());
        result.setdescription(entity.getdescription());

        objectCache.putRestTo(entity, result);

        if(entity.getorganization() != null) {
            result.setorganization(PyroOrganization.fromDywaEntity(entity.getorganization(),objectCache));
        }

        if(entity.getowner() != null) {
            result.setowner(PyroUser.fromDywaEntity(entity.getowner(),objectCache));
        }

        return result;
    }
}
