package info.scce.pyro.core.command;
import de.ls5.dywa.generated.entity.info.scce.pyro.core.*;

import style.Appearance;
import style.BooleanEnum;
import style.LineStyle;
import info.scce.pyro.core.command.types.*;
import info.scce.pyro.sync.GraphModelWebSocket;

import java.util.*;
import java.util.stream.Collectors;


/**
 * Author zweihoff
 */
abstract public class CommandExecuter {

    final GraphModelWebSocket graphModelWebSocket;
    final ControllerBundle controllerBundle;
    protected final BatchExecution batch;
    public final PyroProject pyroProject;
    public final info.scce.pyro.core.MainControllerBundle mainContollerBundle;
    
    public final ControllerBundle getControllerBundle() { return controllerBundle; }
    
    public final info.scce.pyro.core.MainControllerBundle getMainControllerBundle() { return mainContollerBundle; }
    

    public List<HighlightCommand> getHighlightings() {
        return highlightings;
    }

    public void setHighlightings(List<HighlightCommand> highlightings) {
        this.highlightings = highlightings;
    }
    
    public void openFile(PyroFile file) {
        openFileCommand = new OpenFileCommand();
        openFileCommand.setDywaId(file.getDywaId());
    }
    
    public OpenFileCommand getOpenFileCommand() {
        return openFileCommand;
    }

    protected List<HighlightCommand> highlightings;
    protected OpenFileCommand openFileCommand;
    CommandExecuter(ControllerBundle controllerBundle,BatchExecution batch,GraphModelWebSocket graphModelWebSocket,List<HighlightCommand> highlightings,PyroProject pyroProject,info.scce.pyro.core.MainControllerBundle mainContollerBundle) {
        this.controllerBundle = controllerBundle;
        this.batch = batch;
        this.graphModelWebSocket = graphModelWebSocket;
        this.highlightings = highlightings;
        this.pyroProject = pyroProject;
        this.mainContollerBundle = mainContollerBundle;
    }
    
    public GraphModelWebSocket getGraphModelWebSocket() {
        return graphModelWebSocket;
    }

    protected void createNode(String type,Node node, ModelElementContainer modelElementContainer,long x, long y,long width, long height,info.scce.pyro.core.graphmodel.Node prev){
        createNode(type,node, modelElementContainer,x, y,width, height,null,prev);
    }


    protected void createNode(String type, Node node, ModelElementContainer modelElementContainer, long x, long y, long width, long height, info.scce.pyro.core.graphmodel.PyroElement primeElement, info.scce.pyro.core.graphmodel.Node prev){

        info.scce.pyro.core.command.types.CreateNodeCommand cmd = new CreateNodeCommand();
        cmd.setDelegateId(node.getDywaId());
        cmd.setDywaVersion(node.getDywaVersion());
        cmd.setDywaName(node.getDywaName());
        cmd.setContainerId(modelElementContainer.getDywaId());
        cmd.setWidth(width);
        cmd.setHeight(height);
        cmd.setX(x);
        cmd.setY(y);
        cmd.setType(type);
        if(primeElement!=null){
            cmd.setPrimeId(primeElement.getDywaId());
            cmd.setPrimeElement(primeElement);
        }
        cmd.setElement(prev);
        batch.add(cmd);

        node.setcontainer(modelElementContainer);
        modelElementContainer.getmodelElements_ModelElement().add(node);
        node.setx(x);
        node.sety(y);
        node.setwidth(width);
        node.setheight(height);
    }

    protected void moveNode(String type,Node node, ModelElementContainer modelElementContainer, long x, long y){

        info.scce.pyro.core.command.types.MoveNodeCommand cmd = new MoveNodeCommand();
        cmd.setDelegateId(node.getDywaId());
        cmd.setDywaVersion(node.getDywaVersion());
        cmd.setDywaName(node.getDywaName());
        cmd.setOldContainerId(node.getcontainer().getDywaId());
        cmd.setContainerId(modelElementContainer.getDywaId());
        cmd.setOldX(node.getx());
        cmd.setOldY(node.gety());
        cmd.setX(x);
        cmd.setY(y);
        cmd.setType(type);
        batch.add(cmd);

        node.getcontainer().getmodelElements_ModelElement().remove(node);
        node.setcontainer(modelElementContainer);
        modelElementContainer.getmodelElements_ModelElement().add(node);
        node.setx(x);
        node.sety(y);
    }

    protected void resizeNode(String type,Node node, long width, long height){

        info.scce.pyro.core.command.types.ResizeNodeCommand cmd = new ResizeNodeCommand();
        cmd.setDelegateId(node.getDywaId());
        cmd.setDywaVersion(node.getDywaVersion());
        cmd.setDywaName(node.getDywaName());
        cmd.setOldHeight(node.getheight());
        cmd.setOldWidth(node.getwidth());
        cmd.setWidth(width);
        cmd.setHeight(height);
        cmd.setType(type);
        batch.add(cmd);

        node.setwidth(width);
        node.setheight(height);
    }

    protected void rotateNode(String type,Node node,long angle){
        info.scce.pyro.core.command.types.RotateNodeCommand cmd = new RotateNodeCommand();
        cmd.setDelegateId(node.getDywaId());
        cmd.setDywaVersion(node.getDywaVersion());
        cmd.setDywaName(node.getDywaName());
        cmd.setOldAngle(node.getangle());
        cmd.setAngle(angle);
        cmd.setType(type);
        batch.add(cmd);

        node.setangle(angle);
    }

    protected void removeNode(String type,Node node,info.scce.pyro.core.graphmodel.PyroElement primeNode,info.scce.pyro.core.graphmodel.Node prev){
        info.scce.pyro.core.command.types.RemoveNodeCommand cmd = new RemoveNodeCommand();
        cmd.setDelegateId(node.getDywaId());
        cmd.setDywaVersion(node.getDywaVersion());
        cmd.setDywaName(node.getDywaName());
        cmd.setContainerId(node.getcontainer().getDywaId());
        cmd.setWidth(node.getwidth());
        cmd.setHeight(node.getheight());
        cmd.setX(node.getx());
        cmd.setY(node.gety());
        cmd.setType(type);
        if(primeNode != null){
            cmd.setPrimeId(primeNode.getDywaId());
            cmd.setPrimeElement(primeNode);
        }
        cmd.setElement(prev);

        //remove highlighting
        Optional<HighlightCommand> he = getHighlightings().stream().filter(n -> n.getDywaId() == node.getDywaId()).findAny();
        if(he.isPresent()) {
            getHighlightings().remove(he.get());
        }
        
        batch.add(cmd);
        controllerBundle.nodeController.deleteWithIncomingReferences(node);



    }

    protected GraphModel getRootModel(IdentifiableElement modelElement){
        if(modelElement instanceof GraphModel){
            return (GraphModel) modelElement;
        }
        if(modelElement instanceof ModelElement){
            if((((ModelElement) modelElement).getcontainer())!=null){
                return getRootModel((((ModelElement) modelElement).getcontainer()));
            }
        }
        return null;
    }

    protected void createEdge(String type,Edge edge, Node source, Node target,List<BendingPoint> positions,info.scce.pyro.core.graphmodel.Edge prev){

        GraphModel graphModel = getRootModel(source);

        info.scce.pyro.core.command.types.CreateEdgeCommand cmd = new CreateEdgeCommand();
        cmd.setDelegateId(edge.getDywaId());
        cmd.setDywaVersion(edge.getDywaVersion());
        cmd.setDywaName(edge.getDywaName());
        cmd.setSourceId(source.getDywaId());
        cmd.setTargetId(target.getDywaId());
        cmd.setPositions(positions.stream().map(info.scce.pyro.core.graphmodel.BendingPoint::fromDywaEntity).collect(Collectors.toList()));
        cmd.setType(type);
        cmd.setElement(prev);
        batch.add(cmd);

        edge.setcontainer(graphModel);
        graphModel.getmodelElements_ModelElement().add(edge);
        edge.settargetElement(target);
        if(!edge.getbendingPoints_BendingPoint().equals(positions)){
            edge.setbendingPoints_BendingPoint(positions);
        }
        edge.setsourceElement(source);
        source.getoutgoing_Edge().add(edge);
        target.getincoming_Edge().add(edge);
    }

    protected void reconnectEdge(String type,Edge edge, Node source, Node target){

        info.scce.pyro.core.command.types.ReconnectEdgeCommand cmd = new ReconnectEdgeCommand();
        cmd.setDelegateId(edge.getDywaId());
        cmd.setDywaVersion(edge.getDywaVersion());
        cmd.setDywaName(edge.getDywaName());
        cmd.setOldSourceId(edge.getsourceElement().getDywaId());
        cmd.setSourceId(source.getDywaId());
        cmd.setOldTargetId(edge.gettargetElement().getDywaId());
        cmd.setTargetId(target.getDywaId());
        cmd.setType(type);
        batch.add(cmd);

        edge.getsourceElement().getoutgoing_Edge().remove(edge);
        source.getoutgoing_Edge().add(edge);
        edge.setsourceElement(source);

        edge.gettargetElement().getincoming_Edge().remove(edge);
        target.getincoming_Edge().add(edge);
        edge.settargetElement(target);
    }

    protected void updateBendingPoints(String type,Edge edge, List<info.scce.pyro.core.graphmodel.BendingPoint> points){

        info.scce.pyro.core.command.types.UpdateBendPointCommand cmd = new UpdateBendPointCommand();
        cmd.setDelegateId(edge.getDywaId());
        cmd.setDywaVersion(edge.getDywaVersion());
        cmd.setDywaName(edge.getDywaName());
        cmd.setOldPositions(edge.getbendingPoints_BendingPoint().stream().map(info.scce.pyro.core.graphmodel.BendingPoint::fromDywaEntity).collect(Collectors.toList()));
        cmd.setPositions(points);
        cmd.setType(type);
        batch.add(cmd);


        List<BendingPoint> cpPoints = new LinkedList<>(edge.getbendingPoints_BendingPoint());
        edge.getbendingPoints_BendingPoint().clear();
        cpPoints.forEach(b->controllerBundle.bendingPointController.delete(b));
        points.forEach(b->{
            BendingPoint bp = controllerBundle.bendingPointController.create("BendingPoint");
            bp.setx(b.getx());
            bp.sety(b.gety());
            edge.getbendingPoints_BendingPoint().add(bp);
        });
    }

    protected void removeEdge(String type,Edge edge,info.scce.pyro.core.graphmodel.Edge prev){

        info.scce.pyro.core.command.types.RemoveEdgeCommand cmd = new RemoveEdgeCommand();
        cmd.setDelegateId(edge.getDywaId());
        cmd.setDywaVersion(edge.getDywaVersion());
        cmd.setDywaName(edge.getDywaName());
        cmd.setSourceId(edge.getsourceElement().getDywaId());
        cmd.setTargetId(edge.gettargetElement().getDywaId());
        cmd.setPositions(edge.getbendingPoints_BendingPoint().stream().map(info.scce.pyro.core.graphmodel.BendingPoint::fromDywaEntity).collect(Collectors.toList()));
        cmd.setType(type);
        cmd.setElement(prev);
        batch.add(cmd);

        //remove highlighting
        Optional<HighlightCommand> he = getHighlightings().stream().filter(n -> n.getDywaId() == edge.getDywaId()).findAny();
        if(he.isPresent()) {
            getHighlightings().remove(he.get());
        }

        edge.getcontainer().getmodelElements_ModelElement().remove(edge);
        edge.setcontainer(null);
        List<BendingPoint> points = new LinkedList<>(edge.getbendingPoints_BendingPoint());
        edge.getbendingPoints_BendingPoint().clear();
        points.forEach((b)->controllerBundle.bendingPointController.delete(b));
        controllerBundle.edgeController.deleteWithIncomingReferences(edge);
    }
    
    protected void updatePropertiesReNew(String type, info.scce.pyro.core.graphmodel.IdentifiableElement element,info.scce.pyro.core.graphmodel.IdentifiableElement prevElement) {
        Optional<UpdateCommand> uc = batch
                .getCommands()
                .stream()
                .filter(n->n instanceof UpdateCommand)
                .map(n->(UpdateCommand)n)
                .filter(n->n.getElement().getDywaId()==element.getDywaId())
                .findFirst();
        if(uc.isPresent()){
            batch.getCommands().remove(uc.get());
        }
        updateProperties(type,element,prevElement);
    }
    
    protected void updateProperties(String type, info.scce.pyro.core.graphmodel.IdentifiableElement element,info.scce.pyro.core.graphmodel.IdentifiableElement prevElement){
        info.scce.pyro.core.command.types.UpdateCommand cmd = new UpdateCommand();
        cmd.setDelegateId(element.getDywaId());
        cmd.setDywaVersion(element.getDywaVersion());
        cmd.setDywaName(element.getDywaName());
        cmd.setType(type);
        cmd.setElement(element);
        cmd.setPrevElement(prevElement);
        batch.add(cmd);
    }

    protected Appearance mergeAppearance(Appearance defaultAppearance, Appearance calculated){
        if(calculated.getForeground()!=null){
            defaultAppearance.setForeground(calculated.getForeground());
        }
        if(calculated.getBackground()!=null){
            defaultAppearance.setBackground(calculated.getBackground());
        }
        if(calculated.getLineStyle()!= LineStyle.UNSPECIFIED) {
            defaultAppearance.setLineStyle(calculated.getLineStyle());
        }
        if(calculated.getLineWidth()!= -1) {
            defaultAppearance.setLineWidth(calculated.getLineWidth());
        }
        if(calculated.getLineInVisible()!= null) {
            defaultAppearance.setLineInVisible(calculated.getLineInVisible());
        }
        if(calculated.getTransparency()!= -1.0) {
            defaultAppearance.setTransparency(calculated.getTransparency());
        }
        if(calculated.getAngle()!= -1.0F) {
            defaultAppearance.setAngle(calculated.getAngle());
        }
        if(calculated.getFont()!= null) {
            if(calculated.getFont().getFontName()!=null) {
                defaultAppearance.getFont().setFontName(calculated.getFont().getFontName());
            }
            if(calculated.getFont().getSize()>0) {
                defaultAppearance.getFont().setSize(calculated.getFont().getSize());
            }
            defaultAppearance.getFont().setIsBold(calculated.getFont().isIsBold());
            defaultAppearance.getFont().setIsItalic(calculated.getFont().isIsItalic());
        }
        if(calculated.getFilled()!= BooleanEnum.UNDEF) {
            defaultAppearance.setFilled(calculated.getFilled());
        }
        if(calculated.getImagePath()!= null) {
            defaultAppearance.setImagePath(calculated.getImagePath());
        }
        return defaultAppearance;
        
    }

    protected void updateAppearance(String type, graphmodel.IdentifiableElement element, Appearance appearance){
        final AppearanceCommand result = new AppearanceCommand();
        result.setType(type);
        result.setDelegateId(element.getDelegate().getDywaId());
        result.setDywaVersion(element.getDelegate().getDywaVersion());
        result.setDywaName(element.getDelegate().getDywaName());
        result.setAppearance(info.scce.pyro.core.graphmodel.Appearance.fromAppearance(appearance));
        batch.add(result);
    }

    public BatchExecution getBatch(){
        return batch;
    }

    public List<ModelElement> getAllModelElements() {
        return getAllModelElements(getBatch().getGraphModel());
    }

    private List<ModelElement> getAllModelElements(ModelElementContainer mec) {
        List<ModelElement> result = new LinkedList<>();
        result.addAll(mec.getmodelElements_ModelElement());
        mec.getmodelElements_ModelElement().stream().filter(n->n instanceof ModelElementContainer).forEach(n->result.addAll(getAllModelElements((ModelElementContainer) n)));
        return result;
    }

    public void updateAppearance() {}
}
