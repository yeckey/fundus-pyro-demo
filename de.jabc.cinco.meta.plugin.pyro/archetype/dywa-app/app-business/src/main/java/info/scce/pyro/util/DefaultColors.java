package info.scce.pyro.util;

public class DefaultColors {

	public static final String NAV_BG_COLOR = "525252";

	public static final String NAV_TEXT_COLOR = "afafaf";

	public static final String BODY_BG_COLOR = "313131";
	
	public static final String BODY_TEXT_COLOR = "ffffff";

	public static final String PRIMARY_BG_COLOR = "007bff";
	
	public static final String PRIMARY_TEXT_COLOR = "ffffff";
}
