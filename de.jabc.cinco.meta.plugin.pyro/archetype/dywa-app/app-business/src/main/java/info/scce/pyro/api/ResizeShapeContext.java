package info.scce.pyro.api;

public enum ResizeShapeContext {
    DIRECTION_EAST, DIRECTION_NORTH, DIRECTION_NORTH_WEST, DIRECTION_UNSPECIFIED
}