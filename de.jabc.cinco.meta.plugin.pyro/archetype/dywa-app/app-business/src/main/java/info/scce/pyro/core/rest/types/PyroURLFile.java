package info.scce.pyro.core.rest.types;

/**
 * Author zweihoff
 */

@com.fasterxml.jackson.annotation.JsonFilter("DIME_Selective_Filter")
@com.fasterxml.jackson.annotation.JsonIdentityInfo(generator = com.voodoodyne.jackson.jsog.JSOGGenerator.class)
public class PyroURLFile extends PyroFile
{

    private String url;

    @com.fasterxml.jackson.annotation.JsonProperty("url")
    public String geturl() {
        return this.url;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("url")
    public void seturl(final String url) {
        this.url = url;
    }

    




    public static PyroURLFile fromDywaEntity(final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroURLFile entity, info.scce.pyro.rest.ObjectCache objectCache) {

        if(objectCache.containsRestTo(entity)){
            return objectCache.getRestTo(entity);
        }
        final PyroURLFile result;
        result = new PyroURLFile();
        result.setDywaId(entity.getDywaId());
        result.setDywaName(entity.getDywaName());
        result.setDywaVersion(entity.getDywaVersion());
        result.set__type(entity.getClass().getSimpleName());

        result.setfilename(entity.getfilename());
        result.setextension(entity.getextension());
        result.seturl(entity.geturl());

        objectCache.putRestTo(entity, result);

        return result;
    }
}
