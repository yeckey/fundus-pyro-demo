package info.scce.pyro.util;

public class UserUtils {

	public static String createEmailHash(String email) {
		try {
			final java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
		    md.update(email.getBytes());
		    final byte[] digest = md.digest();
		    return javax.xml.bind.DatatypeConverter.printHexBinary(digest).toLowerCase();
		} catch (java.security.NoSuchAlgorithmException e) {
			return "3f26135581c551d622fc1a727c17681f"; // md5 hash for "pyro"
		}
	}
}
