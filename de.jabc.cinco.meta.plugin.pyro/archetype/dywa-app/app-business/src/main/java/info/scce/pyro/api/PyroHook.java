package info.scce.pyro.api;

import info.scce.pyro.IPyroController;
import info.scce.pyro.core.command.ControllerBundle;

import java.util.Set;

/**
 * Author zweihoff
 */
abstract class PyroHook {

    private Set<IPyroController> bundles;


    public final void init(Set<IPyroController> bundles) {
        this.bundles = bundles;
    }

    public Set<IPyroController> getBundles() {
        return this.bundles;
    }

}

