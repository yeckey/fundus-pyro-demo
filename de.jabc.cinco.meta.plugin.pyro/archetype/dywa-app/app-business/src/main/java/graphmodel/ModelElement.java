package graphmodel;

public interface ModelElement extends IdentifiableElement {
	de.ls5.dywa.generated.entity.info.scce.pyro.core.ModelElement getDelegate();
	ModelElementContainer getContainer();
	GraphModel getRootElement();
}
