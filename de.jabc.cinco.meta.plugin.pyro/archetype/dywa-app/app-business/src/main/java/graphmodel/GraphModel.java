package graphmodel;

public interface GraphModel extends ModelElementContainer {
	de.ls5.dywa.generated.entity.info.scce.pyro.core.GraphModel getDelegate();
	public void deleteModelElement(ModelElement cme);
}
