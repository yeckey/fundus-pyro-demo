package graphmodel;

public interface Edge extends ModelElement {
	de.ls5.dywa.generated.entity.info.scce.pyro.core.Edge getDelegate();
	public void delete();
	public Node getSourceElement();
	public Node getTargetElement();
	public void reconnectSource(Node node);
	public void reconnectTarget(Node node);
	public void addBendingPoint(int x,int y);
}
