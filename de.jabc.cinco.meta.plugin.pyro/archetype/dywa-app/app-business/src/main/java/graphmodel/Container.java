package graphmodel;

public interface Container extends Node, ModelElementContainer {
	de.ls5.dywa.generated.entity.info.scce.pyro.core.Container getDelegate();
}
