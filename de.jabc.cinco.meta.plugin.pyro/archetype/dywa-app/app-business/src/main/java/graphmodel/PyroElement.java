package graphmodel;

public interface PyroElement {
	public String getId();
	public de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroElement getDelegate();
}
