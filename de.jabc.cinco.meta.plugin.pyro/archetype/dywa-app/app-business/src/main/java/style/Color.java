package style;

public interface Color {

    int getR();

    void setR(int value);


    int getG();

    void setG(int value);


    int getB();

    void setB(int value);

} // Color

