package org.eclipse.emf.ecore;

public class EStructuralFeature 
{
	private String name;

	public String getName() {
		return name;
	}
	
	public void setName(String value) {
		this.name = value;
	}

}
