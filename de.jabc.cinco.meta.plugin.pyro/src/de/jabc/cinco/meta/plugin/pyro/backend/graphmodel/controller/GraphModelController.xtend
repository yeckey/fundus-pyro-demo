package de.jabc.cinco.meta.plugin.pyro.backend.graphmodel.controller

import de.jabc.cinco.meta.plugin.pyro.backend.graphmodel.command.GraphModelCommandExecuter
import de.jabc.cinco.meta.plugin.pyro.util.Generatable
import de.jabc.cinco.meta.plugin.pyro.util.GeneratorCompound
import de.jabc.cinco.meta.plugin.pyro.util.MGLExtension
import java.io.File
import java.util.List
import java.util.Map
import mgl.GraphModel
import mgl.ModelElement
import mgl.Node
import mgl.NodeContainer
import style.NodeStyle
import style.Styles

class GraphModelController extends Generatable {
	
	new(GeneratorCompound gc) {
		super(gc)
	}
	
	def filename(GraphModel g)'''«g.name.fuEscapeJava»Controller.java'''
	
	
	
	def content(GraphModel g,Styles styles,Map<String,Iterable<File>> staticGenerationFiles) {
		val hasAppearanceProviders = g.hasAppearanceProvider(styles)
		val hasChecks = g.hasChecks
	'''
	package info.scce.pyro.core;
	
	import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroFolderController;
	import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroUserController;
	import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroBinaryFileController;
	import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroURLFileController;
	import de.ls5.dywa.generated.entity.info.scce.pyro.core.*;
	import de.ls5.dywa.generated.util.DomainFileController;
	import info.scce.pyro.core.command.types.*;
	import info.scce.pyro.core.rest.types.*;
	import de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroFolder;
	import de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject;
	import de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser;
	import info.scce.pyro.core.command.«g.name.fuEscapeJava»CommandExecuter;
	import info.scce.pyro.core.command.«g.name.fuEscapeJava»ControllerBundle;
	import info.scce.pyro.«g.name.lowEscapeJava».rest.«g.name.fuEscapeJava»;
	import info.scce.pyro.sync.GraphModelWebSocket;
	import info.scce.pyro.sync.ProjectWebSocket;
	import info.scce.pyro.sync.WebSocketMessage;
	
	import javax.ws.rs.core.Response;
	import java.io.IOException;
	
	@javax.transaction.Transactional
	@javax.ws.rs.Path("/«g.name.lowEscapeDart»")
	public class «g.name.fuEscapeJava»Controller extends MainGraphModelController {
		
		«FOR e:g.elementsAndTypesAndEnums»
			@javax.inject.Inject
			private «g.dywaControllerFQN».«e.name.fuEscapeJava»Controller «e.name.escapeJava»Controller;
		«ENDFOR»
		
		«FOR pr:g.importetPrimeTypes.filter[!equals(g)]»
			@javax.inject.Inject
			«pr.type.graphModel.dywaControllerFQN».«pr.type.name.fuEscapeJava»Controller prime«pr.type.graphModel.name.escapeJava»«pr.type.name.escapeJava»Controller;
		«ENDFOR»
		
		«FOR gpr:g.importetPrimeTypes.map[n|n.type.graphModel].filter(GraphModel).filter[gr|!gr.equals(g)].groupBy[name].entrySet.map[value.get(0)]»
			@javax.inject.Inject
			info.scce.pyro.core.«gpr.name.fuEscapeJava»Controller primeGraph«gpr.name.fuEscapeJava»Controller;
		«ENDFOR»
		
		«FOR gm:gc.graphMopdels.filter[!equals(g)]»
		@javax.inject.Inject
		info.scce.pyro.core.«gm.name.fuEscapeJava»Controller main«gm.name.fuEscapeJava»;
		«ENDFOR»
	
	
		@javax.ws.rs.POST
		@javax.ws.rs.Path("create/private")
		@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
		@javax.ws.rs.Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
		@org.jboss.resteasy.annotations.GZIP
		public Response createGraphModel(CreateGraphModel graph) {
	
			final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser subject = subjectController
							.read((Long) org.apache.shiro.SecurityUtils.getSubject()
									.getPrincipal());
	
	        final PyroFolder folder = folderController.read(graph.getparentId());
	        
	        if(folder==null||subject==null){
	            return Response.status(Response.Status.BAD_REQUEST).build();
	        } 
	        
	        PyroProject pp = graphModelController.getProject(folder);
	        if (!canCreateGraphModel(subject, pp)) {
	        	return Response.status(Response.Status.FORBIDDEN).build();
	        }
	
		    final de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.fuEscapeJava».«g.name.fuEscapeJava» newGraph =  create«g.name.fuEscapeJava»(
		    	folder,graph.getfilename(),subject
		    );
	        
			return Response.ok(«g.name.fuEscapeJava».fromDywaEntity(newGraph,new info.scce.pyro.rest.ObjectCache())).build();
		}
		
		public de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.fuEscapeJava».«g.name.fuEscapeJava» create«g.name.fuEscapeJava»(PyroFolder folder, String filename,de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser subject) {
			final de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.fuEscapeJava».«g.name.fuEscapeJava» newGraph =  «g.name.escapeJava»Controller.create("«g.name.fuEscapeJava»_"+filename);
			PyroProject pp = graphModelController.getProject(folder);
			 newGraph.setfilename(filename);
			«g.name.escapeJava»CommandExecuter executer = buildExecuter(newGraph,pp);
			info.scce.pyro.core.highlight.HighlightFactory.eINSTANCE.warmup(executer);
			«new GraphModelCommandExecuter(gc).setDefault('''newGraph''',g,g,true)»
					    
			newGraph.setextension("«g.fileExtension»");
			folder.getfiles_PyroFile().add(newGraph);
			
			
			if(subject != null) {
				projectWebSocket.send(pp.getDywaId(), WebSocketMessage.fromDywaEntity(subject.getDywaId(), info.scce.pyro.core.rest.types.PyroProjectStructure.fromDywaEntity(pp,objectCache)));
			}
				        
	        «IF g.hasPostCreateHook»
	        	factoryWarmup(pp,subject,executer);
		        «g.apiFQN».«g.name.fuEscapeJava» ce = new «g.apiImplFQN».«g.name.fuEscapeJava»Impl(newGraph,executer);
		        «g.postCreateHook» ca = new «g.postCreateHook»();
		        ca.init(executer);
		        ca.postCreate(ce);
			«ENDIF»
			
			return newGraph;
		}
		
		@javax.ws.rs.GET
		@javax.ws.rs.Path("read/{id}/private")
		@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
		@org.jboss.resteasy.annotations.GZIP
		public Response load(@javax.ws.rs.PathParam("id") long id) {
			
			final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser subject = subjectController
										.read((Long) org.apache.shiro.SecurityUtils.getSubject()
												.getPrincipal());
													
			final de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«g.name.escapeJava» graph = «g.name.escapeJava»Controller.read(id);
			if (graph == null) {
				return Response.status(Response.Status.BAD_REQUEST).build();
			}
			
			PyroProject project = graphModelController.getProject(graph);
			if (!canReadGraphModel(subject, project)) {
	        	return Response.status(Response.Status.FORBIDDEN).build();
	        }
			
			return Response.ok(«g.name.escapeJava».fromDywaEntity(graph, objectCache))
					.build();
	
		}
		«FOR n:g.nodes.filter[prime].filter[hasJumpToAnnotation]»
		@javax.ws.rs.GET
		@javax.ws.rs.Path("jumpto/{id}/«n.name.lowEscapeDart»/{elementid}/private")
		@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
		@org.jboss.resteasy.annotations.GZIP
		public Response load«n.name.escapeJava»(@javax.ws.rs.PathParam("id") long id,@javax.ws.rs.PathParam("elementid") long elementId) {
			
			final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser subject = subjectController
										.read((Long) org.apache.shiro.SecurityUtils.getSubject()
												.getPrincipal());
													
			final de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«g.name.escapeJava» graph = «g.name.escapeJava»Controller.read(id);
			if (graph == null) {
				return Response.status(Response.Status.BAD_REQUEST).build();
			}
			
			final de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«n.name.escapeJava» node = «n.name.escapeJava»Controller.read(elementId);
			if(node == null) {
				return Response.status(Response.Status.BAD_REQUEST).build();
			}
			
			PyroProject project = graphModelController.getProject(graph);
			if (!canReadGraphModel(subject, project)) {
				return Response.status(Response.Status.FORBIDDEN).build();
			}
			«g.name.escapeJava»CommandExecuter executer = buildExecuter(node,project);
			final «g.apiFQN».«n.name.fuEscapeJava» apiNode = new «g.apiImplFQN».«n.name.fuEscapeJava»Impl(node,executer);
			
			
	        «(n as Node).primeReference.type.graphModel.apiFQN».«(n as Node).primeReference.type.name.fuEscapeJava» primeNode = apiNode.get«n.primeReference.name.fuEscapeJava»();
			String primeElementlId = primeNode.getId();
	        «IF (n as Node).primeReference.type instanceof GraphModel»
	        String primeGraphModelId = primeElementlId;
	        «ELSE»
	        	graphmodel.ModelElementContainer mec = primeNode.getContainer();
	        	while(!(mec instanceof graphmodel.GraphModel)) {
	        		mec = ((graphmodel.Container)mec).getContainer();
	        	}
	        	String primeGraphModelId = mec.getId();
	        «ENDIF»
			info.scce.pyro.message.JumpToPrimeAnswer resp = new info.scce.pyro.message.JumpToPrimeAnswer();
			resp.setGraphModelId(primeGraphModelId);
			resp.setElementId(primeElementlId);
			resp.setGraphModelType("«(n as Node).primeReference.type.graphModel.name.fuEscapeDart»");
			resp.setElementType("«(n as Node).primeReference.type.name.fuEscapeDart»");
			return Response.ok(resp).build();
		}
		«ENDFOR»
		
		«IF hasChecks»
				@javax.ws.rs.GET
				@javax.ws.rs.Path("checks/{id}/private")
				@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
				@org.jboss.resteasy.annotations.GZIP
				public Response check(@javax.ws.rs.PathParam("id") long id) {
			
					final de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«g.name.escapeJava» graph = «g.name.escapeJava»Controller
						.read(id);
					final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser user = subjectController
						.read((Long) org.apache.shiro.SecurityUtils.getSubject()
							.getPrincipal());
					if (graph == null || user == null) {
						return Response.status(Response.Status.BAD_REQUEST).build();
					}
					
					PyroProject pyroProject = graphModelController.getProject(graph);
					«g.name.escapeJava»CommandExecuter executer = buildExecuter(graph,pyroProject);
					info.scce.pyro.core.highlight.HighlightFactory.eINSTANCE.warmup(executer);
					factoryWarmup(pyroProject,user,executer);
					«g.apiFQN».«g.name.escapeJava» cgraph = new «g.apiImplFQN».«g.name.escapeJava»Impl(graph,executer);
					
					final java.util.Map<graphmodel.IdentifiableElement,info.scce.pyro.core.command.types.CheckResultCommand> results = new java.util.HashMap<>();
					//do check
					final «g.package».mcam.cli.«g.name.fuEscapeJava»Execution exec = new «g.package».mcam.cli.«g.name.fuEscapeJava»Execution();
					final «g.package».mcam.adapter.«g.name.fuEscapeJava»Adapter adapter = exec.initApiAdapter(cgraph);
					exec.getCheckModuleRegistry().forEach((n)->{
						n.init();
						n.execute(adapter).forEach((e,i)->{
							graphmodel.IdentifiableElement element = (graphmodel.IdentifiableElement)e;
							info.scce.pyro.core.command.types.CheckResultCommand crc = info.scce.pyro.core.command.types.CheckResultCommand.fromElement(element);
							if(results.containsKey(element)) {
								crc = results.get(element);
							}
							info.scce.pyro.core.command.types.CheckResultCommand crcFinal = crc;
							«g.package».mcam.adapter.«g.name.fuEscapeJava»Id adapterId = («g.package».mcam.adapter.«g.name.fuEscapeJava»Id)i;
							adapterId.getErrors().forEach(m->crcFinal.addResult(m,"error"));
							adapterId.getWarnings().forEach(m->crcFinal.addResult(m,"warning"));
							adapterId.getInfos().forEach(m->crcFinal.addResult(m,"info"));
							if(!crcFinal.getResults().isEmpty()){
								results.put(element,crcFinal);
							}
						});
					});
					
					return Response.ok(results.values()).build();
				}
		«ENDIF»
		
		«IF hasAppearanceProviders»
		@javax.ws.rs.GET
		@javax.ws.rs.Path("appearance/{id}/private")
		@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
		@org.jboss.resteasy.annotations.GZIP
		public Response appearance(@javax.ws.rs.PathParam("id") long id) {
	
			final de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«g.name.escapeJava» graph = «g.name.escapeJava»Controller
				.read(id);
			final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser user = subjectController
				.read((Long) org.apache.shiro.SecurityUtils.getSubject()
					.getPrincipal());
			if (graph == null || user == null) {
				return Response.status(Response.Status.BAD_REQUEST).build();
			}
			
			PyroProject project = graphModelController.getProject(graph);
			if (!canReadGraphModel(user, project)) {
	        	return Response.status(Response.Status.FORBIDDEN).build();
	        }
			
			//setup batch execution
			«g.name.escapeJava»CommandExecuter executer = buildExecuter(graph,project);
			info.scce.pyro.core.highlight.HighlightFactory.eINSTANCE.warmup(executer);
			factoryWarmup(project,user,executer);
			//update appearance
			executer.updateAppearance();
			// propagate
			return createResponse("basic_valid_answer", executer,
					user.getDywaId(), graph.getDywaId(), java.util.Collections.emptyList());
		}
		«ENDIF»
		«IF g.generating»
		@javax.ws.rs.GET
		@javax.ws.rs.Path("generate/{id}/{generatorId}/private")
		@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
		@org.jboss.resteasy.annotations.GZIP
		public Response generate(@javax.ws.rs.PathParam("id") long id,@javax.ws.rs.PathParam("generatorId") String generatorId) {
		
			final de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«g.name.escapeJava» graph = «g.name.escapeJava»Controller
				.read(id);
			final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser user = subjectController
				.read((Long) org.apache.shiro.SecurityUtils.getSubject()
					.getPrincipal());
			if (graph == null || user == null) {
				return Response.status(Response.Status.BAD_REQUEST).build();
			}
			
			PyroProject pyroProject = graphModelController.getProject(graph);
			if (!canUpdateGraphModel(user, pyroProject)) {
				return Response.status(Response.Status.FORBIDDEN).build();
	        }
			
			//setup batch execution
			«g.name.escapeJava»CommandExecuter executer = buildExecuter(graph,pyroProject);
			info.scce.pyro.core.highlight.HighlightFactory.eINSTANCE.warmup(executer);
			
			factoryWarmup(pyroProject,user,executer);
			«g.apiFQN».«g.name.escapeJava» cgraph = new «g.apiImplFQN».«g.name.escapeJava»Impl(graph,executer);
			«FOR gen:g.generators»
				«IF gen.value.length >= 3»
					if(generatorId != null && generatorId.equals("«gen.value.get(0)»")) {
				«ELSE»
					if(generatorId == null || generatorId.equals("null")) {
				«ENDIF»
				try {
					
					java.util.Map<String,String[]> staticResourceURLs = new java.util.HashMap<>();
					«FOR f:staticGenerationFiles.entrySet»
					staticResourceURLs.put("«f.key»",new String[]{
						«FOR file:f.value SEPARATOR ","»
						"«file.absolutePath.replace("\\","/").suffix(f.key)»"
						«ENDFOR»
						});
					«ENDFOR»
					«gen.value.get(0)» generator = new «gen.value.get(0)»();
					generator.generateFiles(
						cgraph,
						pyroProject,
						"«IF gen.value.size>1»«gen.value.get(1)»«ENDIF»",
						folderController,
						fileController,
						binaryFileController,
						urlFileController,
						domainFileController,
						pyroElementController,
						"asset/static/«g.name.lowEscapeJava»",
						staticResourceURLs
					);
					projectWebSocket.send(pyroProject.getDywaId(), WebSocketMessage.fromDywaEntity(user.getDywaId(), info.scce.pyro.core.rest.types.PyroProjectStructure.fromDywaEntity(pyroProject,objectCache)));
					return javax.ws.rs.core.Response.ok(info.scce.pyro.core.rest.types.PyroProjectStructure.fromDywaEntity(pyroProject,objectCache)).build();
				
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			«ENDFOR»
			return Response.status(Response.Status.EXPECTATION_FAILED).build();
		}
		«ENDIF»
		
		«IF g.interpreting»
		@javax.ws.rs.GET
		@javax.ws.rs.Path("interpreter/{id}/private")
		@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
		@org.jboss.resteasy.annotations.GZIP
		public Response interpreter(@javax.ws.rs.PathParam("id") long id) {
		
			final de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«g.name.escapeJava» graph = «g.name.escapeJava»Controller
				.read(id);
			final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser user = subjectController
				.read((Long) org.apache.shiro.SecurityUtils.getSubject()
					.getPrincipal());
			if (graph == null || user == null) {
				return Response.status(Response.Status.BAD_REQUEST).build();
			}
			
			PyroProject pyroProject = graphModelController.getProject(graph);
			if (!canUpdateGraphModel(user, pyroProject)) {
				return Response.status(Response.Status.FORBIDDEN).build();
	        }
			
			//setup batch execution
			«g.name.escapeJava»CommandExecuter executer = buildExecuter(graph,pyroProject);
			info.scce.pyro.core.highlight.HighlightFactory.eINSTANCE.warmup(executer);
			
			factoryWarmup(pyroProject,user,executer);
			«g.apiFQN».«g.name.escapeJava» g = new «g.apiImplFQN».«g.name.escapeJava»Impl(graph,executer);

			«FOR gen:g.interperters»
				«gen.value.get(0)» interpreter = new «gen.value.get(0)»();
				interpreter.init(executer);
				interpreter.runInterpreter(g);
			«ENDFOR»
				
			projectWebSocket.send(pyroProject.getDywaId(), WebSocketMessage.fromDywaEntity(user.getDywaId(), info.scce.pyro.core.rest.types.PyroProjectStructure.fromDywaEntity(pyroProject,objectCache)));
			return javax.ws.rs.core.Response.ok(info.scce.pyro.core.rest.types.PyroProjectStructure.fromDywaEntity(pyroProject,objectCache)).build();
		}
		«ENDIF»
		
		@javax.ws.rs.GET
		@javax.ws.rs.Path("{id}/customaction/{elementId}/fetch/private")
		@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
		@org.jboss.resteasy.annotations.GZIP
		public Response fetchCustomActions(@javax.ws.rs.core.Context javax.ws.rs.core.UriInfo uriInfo,@javax.ws.rs.PathParam("id") long id,@javax.ws.rs.PathParam("elementId") long elementId) {
			
			final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser user = subjectController
						.read((Long) org.apache.shiro.SecurityUtils.getSubject()
							.getPrincipal());
			
			final de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«g.name.escapeJava» graph = «g.name.escapeJava»Controller.read(id);
			if (graph == null) {
				return Response.status(Response.Status.BAD_REQUEST).build();
			}
			
			PyroProject pyroProject = graphModelController.getProject(graph);
			if (!canUpdateGraphModel(user, pyroProject)) {
				return Response.status(Response.Status.FORBIDDEN).build();
	        }
			
			java.util.Map<String,String> map = new java.util.HashMap<>();
			«IF !g.elementsAndGraphmodel.filter[hasCustomAction].empty»
			final de.ls5.dywa.generated.entity.info.scce.pyro.core.IdentifiableElement elem = identifiableElementController.read(elementId);
			
			«g.name.escapeJava»CommandExecuter executer = buildExecuter(graph,pyroProject);
			info.scce.pyro.core.highlight.HighlightFactory.eINSTANCE.warmup(executer);
			factoryWarmup(pyroProject,user,executer);
			«FOR e:(g.elements.filter[!isIsAbstract]+#[g]).filter[hasCustomAction] SEPARATOR " else "»
			if(elem instanceof de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«e.name.fuEscapeJava») {
				de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«e.name.fuEscapeJava» e = (de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«e.name.fuEscapeJava»)elem;
				«g.apiFQN».«e.name.fuEscapeJava» ce = new «g.apiImplFQN».«e.name.fuEscapeJava»Impl(e,executer);
				«FOR anno:e.customAction»
				{
					«anno.value.get(0)» ca = new «anno.value.get(0)»();
					ca.init(executer,uriInfo);
					if(ca.canExecute(ce)){
						map.put("«anno.value.get(0)»",ca.getName());
					}
				}
				«ENDFOR»
			}
			«ENDFOR»
			«ENDIF»
			return Response.ok(map)
					.build();
			
		}
		
		«FOR ai:g.editorButtons.indexed»
		«{
			val a = ai.value
			'''
			@javax.ws.rs.POST
			@javax.ws.rs.Path("{id}/button/«a.value.get(1).escapeJava»/{elementId}/trigger/private")
			@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
			@org.jboss.resteasy.annotations.GZIP
			public Response triggerButtonActions«ai.key»(@javax.ws.rs.PathParam("id") long id,@javax.ws.rs.PathParam("elementId") long elementId, info.scce.pyro.core.command.types.Action action) {
				
				final de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«g.name.escapeJava» graph = «g.name.escapeJava»Controller.read(id);
				if (graph == null) {
					return Response.status(Response.Status.BAD_REQUEST).build();
				}
				final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser user = subjectController
					.read((Long) org.apache.shiro.SecurityUtils.getSubject()
						.getPrincipal());
						
				PyroProject pyroProject = graphModelController.getProject(graph);
				if (!canUpdateGraphModel(user, pyroProject)) {
					return Response.status(Response.Status.FORBIDDEN).build();
		        }
				
				«g.name.escapeJava»CommandExecuter executer = buildExecuter(graph,pyroProject);
							
				final de.ls5.dywa.generated.entity.info.scce.pyro.core.IdentifiableElement elem = identifiableElementController.read(elementId);
				info.scce.pyro.core.highlight.HighlightFactory.eINSTANCE.warmup(executer);
				factoryWarmup(pyroProject,user,executer);
				if(elem instanceof de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«g.name.fuEscapeJava») {
					de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«g.name.fuEscapeJava» e = (de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«g.name.fuEscapeJava»)elem;
					«g.apiFQN».«g.name.fuEscapeJava» ce = new «g.apiImplFQN».«g.name.fuEscapeJava»Impl(e,executer);
					«a.value.get(0)» ca = new «a.value.get(0)»();
					ca.init(executer);
					ca.execute(ce);
				}
				
				«IF hasAppearanceProviders»
				executer.updateAppearance();
				«ENDIF»
				
				//propagate
				Response response = createResponse("basic_valid_answer",executer,user.getDywaId(),graph.getDywaId(), java.util.Collections.emptyList());
				graphModelWebSocket.send(id,WebSocketMessage.fromDywaEntity(user.getDywaId(),response.getEntity()));
				return response;
			}
			'''
		}»
		
		«ENDFOR»
		
		@javax.ws.rs.POST
		@javax.ws.rs.Path("{id}/customaction/{elementId}/trigger/private")
		@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
		@org.jboss.resteasy.annotations.GZIP
		public Response triggerCustomActions(@javax.ws.rs.core.Context javax.ws.rs.core.UriInfo uriInfo,@javax.ws.rs.PathParam("id") long id,@javax.ws.rs.PathParam("elementId") long elementId,info.scce.pyro.core.command.types.Action action) {
			
			final de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«g.name.escapeJava» graph = «g.name.escapeJava»Controller.read(id);
			if (graph == null) {
				return Response.status(Response.Status.BAD_REQUEST).build();
			}
			final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser user = subjectController
				.read((Long) org.apache.shiro.SecurityUtils.getSubject()
					.getPrincipal());
					
			PyroProject pyroProject = graphModelController.getProject(graph);
			if (!canUpdateGraphModel(user, pyroProject)) {
				return Response.status(Response.Status.FORBIDDEN).build();
	        }
			
			«g.name.escapeJava»CommandExecuter executer = buildExecuter(graph,pyroProject);
			«IF g.elementsAndGraphmodel.exists[hasCustomAction]»
						
			final de.ls5.dywa.generated.entity.info.scce.pyro.core.IdentifiableElement elem = identifiableElementController.read(elementId);
			info.scce.pyro.core.highlight.HighlightFactory.eINSTANCE.warmup(executer);
			factoryWarmup(pyroProject,user,executer);
			«FOR e:(g.elements.filter[!isIsAbstract]+#[g]).filter[hasCustomAction] SEPARATOR " else "»
			if(elem instanceof de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«e.name.fuEscapeJava») {
				de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«e.name.fuEscapeJava» e = (de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«e.name.fuEscapeJava»)elem;
				«g.apiFQN».«e.name.fuEscapeJava» ce = new «g.apiImplFQN».«e.name.fuEscapeJava»Impl(e,executer);
				«FOR anno:e.customAction»
				if(action.getFqn().equals("«anno.value.get(0)»")) {
					«anno.value.get(0)» ca = new «anno.value.get(0)»();
					ca.init(executer,uriInfo);
					ca.execute(ce);
				}
				«ENDFOR»
			}
			«ENDFOR»
			
			«IF hasAppearanceProviders»
			executer.updateAppearance();
			«ENDIF»
			
			//propagate
			Response response = createResponse("basic_valid_answer",executer,user.getDywaId(),graph.getDywaId(), java.util.Collections.emptyList());
			graphModelWebSocket.send(id,WebSocketMessage.fromDywaEntity(user.getDywaId(),response.getEntity()));
			«ELSE»
			Response response = createResponse("basic_valid_answer",executer,user.getDywaId(),graph.getDywaId(), java.util.Collections.emptyList());
			«ENDIF»
			return response;
		}
		
		@javax.ws.rs.POST
		@javax.ws.rs.Path("{id}/psaction/{elementId}/trigger/private")
		@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
		@org.jboss.resteasy.annotations.GZIP
		public Response triggerPostSelectActions(@javax.ws.rs.PathParam("id") long id,@javax.ws.rs.PathParam("elementId") long elementId,info.scce.pyro.core.command.types.Action action) {
			
			final de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«g.name.escapeJava» graph = «g.name.escapeJava»Controller.read(id);
			if (graph == null) {
				return Response.status(Response.Status.BAD_REQUEST).build();
			}
			final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser user = subjectController
					.read((Long) org.apache.shiro.SecurityUtils.getSubject()
							.getPrincipal());
							
			PyroProject pyroProject = graphModelController.getProject(graph);
			if (!canUpdateGraphModel(user, pyroProject)) {
				return Response.status(Response.Status.FORBIDDEN).build();
		    }
			
			«g.name.escapeJava»CommandExecuter executer = buildExecuter(graph,pyroProject);
		
			«IF g.elementsAndGraphmodel.exists[hasPostSelect]»
				info.scce.pyro.core.highlight.HighlightFactory.eINSTANCE.warmup(executer);
				factoryWarmup(pyroProject,user,executer);
				
				final de.ls5.dywa.generated.entity.info.scce.pyro.core.IdentifiableElement elem = identifiableElementController.read(elementId);
				boolean hasExecuted = false;
				«FOR e:(g.elements.filter[!isIsAbstract]+#[g]).filter[hasPostSelect] SEPARATOR " else "»
				if("«g.name.lowEscapeDart».«e.name.fuEscapeDart»".equals(action.getFqn())) {
					de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«e.name.fuEscapeJava» e = (de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«e.name.fuEscapeJava»)elem;
					«g.apiFQN».«e.name.fuEscapeJava» ce = new «g.apiImplFQN».«e.name.fuEscapeJava»Impl(e,executer);
					{
						«e.postSelectHook» ca = new «e.postSelectHook»();
						ca.init(executer);
						ca.postSelect(ce);
					}
				}
				«ENDFOR»
				
				«IF hasAppearanceProviders»
				executer.updateAppearance();
				«ENDIF»
				
				Response response = createResponse("basic_valid_answer",executer,user.getDywaId(),graph.getDywaId(), java.util.Collections.emptyList());
				//propagate
				graphModelWebSocket.send(id,WebSocketMessage.fromDywaEntity(user.getDywaId(),response.getEntity()));
			«ELSE»
				Response response = createResponse("basic_valid_answer",executer,user.getDywaId(),graph.getDywaId(), java.util.Collections.emptyList());
			«ENDIF»
			return response;
		}
		
		@javax.ws.rs.POST
		@javax.ws.rs.Path("{id}/dbaction/{elementId}/trigger/private")
		@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
		@org.jboss.resteasy.annotations.GZIP
		public Response triggerDoubleClickActions(@javax.ws.rs.core.Context javax.ws.rs.core.UriInfo uriInfo,@javax.ws.rs.PathParam("id") long id,@javax.ws.rs.PathParam("elementId") long elementId,info.scce.pyro.core.command.types.Action action) {
			
			final de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«g.name.escapeJava» graph = «g.name.escapeJava»Controller.read(id);
			if (graph == null) {
				return Response.status(Response.Status.BAD_REQUEST).build();
			}
			final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser user = subjectController
					.read((Long) org.apache.shiro.SecurityUtils.getSubject()
							.getPrincipal());
							
			PyroProject pyroProject = graphModelController.getProject(graph);
			if (!canUpdateGraphModel(user, pyroProject)) {
				return Response.status(Response.Status.FORBIDDEN).build();
	        }
			
			«g.name.escapeJava»CommandExecuter executer = buildExecuter(graph,pyroProject);

			«IF g.elementsAndGraphmodel.exists[hasDoubleClickAction]»
				info.scce.pyro.core.highlight.HighlightFactory.eINSTANCE.warmup(executer);
				factoryWarmup(pyroProject,user,executer);
				final de.ls5.dywa.generated.entity.info.scce.pyro.core.IdentifiableElement elem = identifiableElementController.read(elementId);
				boolean hasExecuted = false;
				«FOR e:(g.elements.filter[!isIsAbstract]+#[g]).filter[hasDoubleClickAction] SEPARATOR " else "»
				if(elem instanceof de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«e.name.fuEscapeJava») {
					de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«e.name.fuEscapeJava» e = (de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«e.name.fuEscapeJava»)elem;
					«g.apiFQN».«e.name.fuEscapeJava» ce = new «g.apiImplFQN».«e.name.fuEscapeJava»Impl(e,executer);
					«FOR anno:e.doubleClickAction»
					{
						«anno.value.get(0)» ca = new «anno.value.get(0)»();
						ca.init(executer,uriInfo);
						if(ca.canExecute(ce)){
							ca.execute(ce);
							hasExecuted = true;
						}
					}
					«ENDFOR»
				}
				«ENDFOR»
				
				«IF hasAppearanceProviders»
				executer.updateAppearance();
				«ENDIF»
				
				Response response = createResponse("basic_valid_answer",executer,user.getDywaId(),graph.getDywaId(), java.util.Collections.emptyList());
				if(hasExecuted){
					//propagate
					graphModelWebSocket.send(id,WebSocketMessage.fromDywaEntity(user.getDywaId(),response.getEntity()));
				}
			«ELSE»
				Response response = createResponse("basic_valid_answer",executer,user.getDywaId(),graph.getDywaId(), java.util.Collections.emptyList());
			«ENDIF»
			return response;
		}
		
		@javax.ws.rs.GET
		@javax.ws.rs.Path("remove/{id}/{parentId}/private")
		@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
		@org.jboss.resteasy.annotations.GZIP
		public Response removeGraphModel(@javax.ws.rs.PathParam("id") final long id,@javax.ws.rs.PathParam("parentId") final long parentId) {
			final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser subject = subjectController
					.read((Long) org.apache.shiro.SecurityUtils.getSubject()
							.getPrincipal());
			//find parent
			final de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«g.name.escapeJava» gm = «g.name.escapeJava»Controller.read(id);
			final PyroFolder parent = folderController.read(parentId);
			if(gm==null||parent==null){
				return Response.status(Response.Status.NOT_FOUND).build();
			}
			
			PyroProject pyroProject = graphModelController.getProject(gm);
			if (!canDeleteGraphModel(subject, pyroProject)) {
				return Response.status(Response.Status.FORBIDDEN).build();
	        }
			
			//cascade remove
			if(parent.getfiles_PyroFile().contains(gm)){
				parent.getfiles_PyroFile().remove(gm);
				«g.name.escapeJava»CommandExecuter executer = buildExecuter(gm,pyroProject);
				info.scce.pyro.core.highlight.HighlightFactory.eINSTANCE.warmup(executer);
				factoryWarmup(pyroProject,subject,executer);
				removeContainer(gm,executer);
				
				PyroProject pp = graphModelController.getProject(parent);
				projectWebSocket.send(pp.getDywaId(), WebSocketMessage.fromDywaEntity(subject.getDywaId(), info.scce.pyro.core.rest.types.PyroProjectStructure.fromDywaEntity(pp,objectCache)));
				
				return Response.ok("OK").build();
			}
			return Response.status(Response.Status.BAD_REQUEST).build();
	
		}
	
		private void removeContainer(ModelElementContainer container,«g.name.escapeJava»CommandExecuter executer) {
			java.util.List<ModelElement> modelElements = new java.util.LinkedList<>(container.getmodelElements_ModelElement());
			modelElements.forEach((n)->{
				«FOR e:g.edgesTopologically.filter[!isIsAbstract] SEPARATOR " else "»
				if(n instanceof de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«e.name.escapeJava»){
					executer.remove«e.name.escapeJava»((de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«e.name.escapeJava») n);
				}
				«ENDFOR»
			});
			modelElements.forEach((n)->{
				«FOR e:g.nodesTopologically.filter[!isIsAbstract] SEPARATOR " else "»
				if(n instanceof «g.dywaFQN».«e.name.escapeJava»){
					«IF e instanceof NodeContainer»
					removeContainer((Container) n,executer);
					«ELSE»
					executer.remove«e.name.escapeJava»((«g.dywaFQN».«e.name.escapeJava») n
					«IF e.prime»
					,((«g.dywaFQN».«e.name.escapeJava») n).get«e.primeReference.name»()
					«ENDIF»);
					«ENDIF»
				}
				«ENDFOR»
			});
			«FOR e:g.nodesTopologically.filter(NodeContainer).filter[!isIsAbstract] + #[g] SEPARATOR " else "»
			if(container instanceof «g.dywaFQN».«e.name.escapeJava»){
				executer.remove«e.name.escapeJava»(
				(«g.dywaFQN».«e.name.escapeJava») container
				«IF e instanceof Node»«IF e.prime»,((«g.dywaFQN».«e.name.escapeJava») container).get«e.primeReference.name»()«ENDIF»«ENDIF»
				);
			}
			«ENDFOR»
		}
		
		@javax.ws.rs.POST
	    @javax.ws.rs.Path("message/{graphModelId}/private")
	    @javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	    @javax.ws.rs.Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	    @org.jboss.resteasy.annotations.GZIP
	    public Response receiveMessage(@javax.ws.rs.PathParam("graphModelId") long graphModelId, Message m) {
	
	        final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser subject = subjectController.read((Long)org.apache.shiro.SecurityUtils.getSubject().getPrincipal());
	        final de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«g.name.escapeJava» graph = «g.name.escapeJava»Controller.read(graphModelId);
	        if(subject==null||graph==null){
	            return Response.status(Response.Status.BAD_REQUEST).build();
	        }
	        
	        PyroProject pyroProject = graphModelController.getProject(graph);
			if (!canUpdateGraphModel(subject, pyroProject)) {
				return Response.status(Response.Status.FORBIDDEN).build();
	        }
	        
	        

	        if(m instanceof CompoundCommandMessage){
	            Response response = executeCommand((CompoundCommandMessage) m, subject, graph,pyroProject);
            	if(response.getStatus()==200){
            		graphModelWebSocket.send(graphModelId,WebSocketMessage.fromDywaEntity(subject.getDywaId(),response.getEntity()));
            	}
            	return response;
	        }
	        else if(m instanceof GraphPropertyMessage){
	            final GraphPropertyMessage gpm = (GraphPropertyMessage) m;
	            graph.setconnector(gpm.getGraph().getconnector());
	            graph.setrouter(gpm.getGraph().getrouter());
	            graph.setwidth(gpm.getGraph().getwidth());
	            graph.setheight(gpm.getGraph().getheight());
	            graph.setscale(gpm.getGraph().getscale());
	            //propagate
	            graphModelWebSocket.send(graphModelId,WebSocketMessage.fromDywaEntity(subject.getDywaId(), m));
	            return Response.ok("OK").build();
	        }
	        else if (m instanceof PropertyMessage) {
				Response response = executePropertyUpdate((PropertyMessage) m, subject,graph,pyroProject);
				if(response.getStatus()==200){
					graphModelWebSocket.send(graphModelId,WebSocketMessage.fromDywaEntity(subject.getDywaId(),response.getEntity()));
				}
				return response;
			} else if (m instanceof ProjectMessage) {
				return Response.ok("OK").build();
			}
	
	        return Response.status(Response.Status.BAD_REQUEST).build();
	    }
		    
		private Response executePropertyUpdate(PropertyMessage pm,PyroUser user,de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.fuEscapeJava».«g.name.fuEscapeJava» graph, PyroProject pyroProject) {
		    «g.name.escapeJava»CommandExecuter executer = buildExecuter(graph,pyroProject);
		    info.scce.pyro.core.highlight.HighlightFactory.eINSTANCE.warmup(executer);
		    factoryWarmup(graphModelController.getProject(graph),user,executer);
		    «IF g.hasPreSave»
	         {
		         «g.apiImplFQN».«g.name.fuEscapeJava»Impl element = new «g.apiImplFQN».«g.name.fuEscapeJava»Impl(graph,executer);
		         «g.getPreChange» hook = new «g.getPreChange»();
		         hook.init(executer);
		         hook.preSave(element);
	         }
	         «ENDIF»
		    «FOR e:g.elements.filter[!isIsAbstract] + #[g] SEPARATOR "else "
		    »if (pm.getDelegate() instanceof info.scce.pyro.«g.name.lowEscapeJava».rest.«e.name.escapeJava»){
		         de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«e.name.escapeJava» target = «e.name.escapeJava»Controller.read(pm.getDelegate().getDywaId());
		         executer.update«e.name.escapeJava»(target, (info.scce.pyro.«g.name.lowEscapeJava».rest.«e.name.escapeJava») pm.getDelegate());
		         «IF e.hasPostAttributeValueChange»
	     				//property change hook
	     				«g.apiImplFQN».«e.name.fuEscapeJava»Impl element = new «g.apiImplFQN».«e.name.fuEscapeJava»Impl(target,executer);
	     				«e.postAttributeValueChange» hook = new «e.postAttributeValueChange»();
	     				«FOR attr:e.attributesExtended»
	     				{
	     					org.eclipse.emf.ecore.EStructuralFeature esf = new org.eclipse.emf.ecore.EStructuralFeature();
	     					esf.setName("«attr.name»");
	     					hook.init(executer);
	     					if(hook.canHandleChange(element,esf)) {
	     						hook.handleChange(element,esf);
	     					}
	     				}
	     				«ENDFOR»
	     			«ENDIF»
		         
		    }
		    «ENDFOR»
		    CompoundCommandMessage response = new CompoundCommandMessage();
			response.setType("basic_valid_answer");
			CompoundCommand cc = new CompoundCommand();
			cc.setQueue(executer.getBatch().getCommands());
			response.setCmd(cc);
			response.setGraphModelId(graph.getDywaId());
			response.setSenderDywaId(user.getDywaId());
			response.setHighlightings(executer.getHighlightings());
			«IF hasAppearanceProviders»
			executer.updateAppearance();
			«ENDIF»
			return Response.ok(response).build();
		}
		
		«g.name.escapeJava»ControllerBundle buildBundle(){
		        return new «g.name.escapeJava»ControllerBundle(
		                nodeController,
		                edgeController,
		                bendingPointController,
		                domainFileController,
		                entityManager,
		                «g.name.escapeJava»Controller
		                «FOR e:g.elementsAndTypesAndEnums BEFORE "," SEPARATOR ","»
		                «e.name.escapeJava»Controller
		                «ENDFOR»
		                «FOR pr:g.importetPrimeTypes BEFORE "," SEPARATOR ","»
                    	prime«pr.type.graphModel.name.escapeJava»«pr.type.name.escapeJava»Controller
                    	«ENDFOR»
                    	«FOR gpr:g.importetPrimeTypes.map[n|n.type.graphModel].filter(GraphModel).filter[gr|!gr.equals(g)].toSet BEFORE "," SEPARATOR ","»
            			primeGraph«gpr.name.fuEscapeJava»Controller
            			«ENDFOR»
		        );
		}
		
		MainControllerBundle mainBundle() {
			MainControllerBundle bundle = new MainControllerBundle();
			bundle.set«g.name.fuEscapeJava»Controller(this);
			«FOR gm:gc.graphMopdels.filter[!equals(g)]»
			bundle.set«gm.name.fuEscapeJava»Controller(main«gm.name.fuEscapeJava»);
			«ENDFOR»
			return bundle;
		}
		
		void factoryWarmup(PyroProject pp,PyroUser user,«g.name.escapeJava»CommandExecuter executor) {
			MainControllerBundle bundle = mainBundle();
			«g.factoryFQN».«g.name.toCamelCase.fuEscapeJava»Factory.eINSTANCE.warmup(pp,user,bundle,executor);
			«FOR gm:gc.graphMopdels.filter[!equals(g)]»
			«gm.factoryFQN».«gm.name.toCamelCase.fuEscapeJava»Factory.eINSTANCE.warmup(pp,user,bundle,null);
			«ENDFOR»
		}
		
		public «g.name.escapeJava»CommandExecuter buildExecuter(de.ls5.dywa.generated.entity.info.scce.pyro.core.IdentifiableElement ie, PyroProject pyroProject){
			de.ls5.dywa.generated.entity.info.scce.pyro.core.ModelElementContainer mec = null;
			if(ie instanceof de.ls5.dywa.generated.entity.info.scce.pyro.core.ModelElementContainer) {
				mec = (de.ls5.dywa.generated.entity.info.scce.pyro.core.ModelElementContainer)ie;
			} else if(ie instanceof de.ls5.dywa.generated.entity.info.scce.pyro.core.ModelElement) {
				mec = ((de.ls5.dywa.generated.entity.info.scce.pyro.core.ModelElement)ie).getcontainer();
			}
			while (mec!=null) {
				if(mec instanceof de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«g.name.fuEscapeJava») {
					«g.name.fuEscapeJava»ControllerBundle bundle = buildBundle();
					de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser user = null;
					if(org.apache.shiro.SecurityUtils.getSubject().getPrincipal() != null ) {
						user = subjectController
							.read((Long) org.apache.shiro.SecurityUtils.getSubject()
									.getPrincipal());
					}
					return new «g.name.escapeJava»CommandExecuter(bundle,user,objectCache,graphModelWebSocket,(de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«g.name.fuEscapeDart»)mec,new java.util.LinkedList<>(),pyroProject,mainBundle());
				}
				else if(mec instanceof de.ls5.dywa.generated.entity.info.scce.pyro.core.Container) {
					mec = ((de.ls5.dywa.generated.entity.info.scce.pyro.core.Container) mec).getcontainer();
				}
				else {
					break;
				}
				
			}
			throw new IllegalStateException("Graphmodel could not be found");
		}
		
		private void createNode(String type,ModelElementContainer mec, long x, long y, PyroElement pe, «g.name.escapeJava»CommandExecuter executer) {
			«FOR node:g.nodesTopologically.filter(NodeContainer).filter[!isIsAbstract] + #[g] SEPARATOR " else "»
			if(mec instanceof de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«node.name.fuEscapeJava») {
				«g.apiFQN».«node.name.fuEscapeJava» n = new
				«g.apiImplFQN».«node.name.fuEscapeJava»Impl((de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«node.name.fuEscapeJava»)mec,executer);
				«FOR n:node.possibleEmbeddingTypes(g).filter[!isIsAbstract] SEPARATOR " else "»
				if(type.equals("«g.name.lowEscapeDart».«n.name.fuEscapeDart»")) {
					n.new«n.name.fuEscapeJava»(
					«IF n.isPrime»
						«IF (n as Node).primeReference.type.graphModel instanceof GraphModel»
							new «(n as Node).primeReference.type.graphModel.apiImplFQN».«(n as Node).primeReference.type.name.fuEscapeJava»Impl(
								(«(n as Node).primeReference.type.graphModel.dywaFQN».«(n as Node).primeReference.type.name.fuEscapeJava»)pe,
							«IF (n as Node).primeReference.type.graphModel.equals(g)»executer«ELSE»primeGraph«((n as Node).primeReference.type.graphModel as GraphModel).name.fuEscapeJava»Controller.buildExecuter((«(n as Node).primeReference.type.graphModel.dywaFQN».«(n as Node).primeReference.type.name.fuEscapeJava»)pe,executer.pyroProject)«ENDIF»),
						«ELSE»
							new «(n as Node).primeReference.type.graphModel.apiFQN».impl.«(n as Node).primeReference.type.name.fuEscapeJava»Impl((«(n as Node).primeReference.type.graphModel.dywaFQN».«(n as Node).primeReference.type.name.fuEscapeJava»)pe),
						«ENDIF»
					«ENDIF»
					java.lang.Math.toIntExact(x),
					java.lang.Math.toIntExact(y));
				}
				«ENDFOR»
			}
			«ENDFOR»
		}
		
		private void addBendingPoints(de.ls5.dywa.generated.entity.info.scce.pyro.core.Edge delegate, java.util.List<info.scce.pyro.core.graphmodel.BendingPoint> positions) {
			positions.forEach(p->{
			    de.ls5.dywa.generated.entity.info.scce.pyro.core.BendingPoint bp = bendingPointController.create("BendingPoint");
			    bp.setx(p.getx());
			    bp.sety(p.gety());
			    delegate.getbendingPoints_BendingPoint().add(bp);
			});
		}
		
		private void createEdge(String type, Node source, Node target, java.util.List<info.scce.pyro.core.graphmodel.BendingPoint> positions,«g.name.escapeJava»CommandExecuter executer) {
			«FOR source:g.nodesTopologically.filter[!isIsAbstract].filter[!possibleOutgoing(g).empty] SEPARATOR " else "»
			if(source instanceof de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«source.name.fuEscapeJava») {
				«g.apiFQN».«source.name.fuEscapeJava» sn = new
				«g.apiImplFQN».«source.name.fuEscapeJava»Impl((de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«source.name.fuEscapeJava»)source,executer);
				«FOR edge:source.possibleOutgoing(g).filter[!isIsAbstract] SEPARATOR " else "»
				if(type.equals("«g.name.lowEscapeDart».«edge.name.fuEscapeDart»")) {
					«FOR target:edge.possibleTargets(g).filter[!isIsAbstract] SEPARATOR " else "»
					if(target instanceof de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«target.name.fuEscapeJava») {
						«g.apiFQN».«edge.name.fuEscapeJava» edge = sn.new«edge.name.fuEscapeJava»(new «g.apiImplFQN».«target.name.fuEscapeJava»Impl((de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«target.name.fuEscapeJava»)target,executer));
						addBendingPoints(edge.getDelegate(),positions);
					}
					«ENDFOR»
				}
				«ENDFOR»
			}
			«ENDFOR»
		}
	
	    private Response executeCommand(CompoundCommandMessage ccm, PyroUser user, de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«g.name.escapeJava» graph,PyroProject pyroProject) {
	        //setup batch execution
	        «g.name.escapeJava»CommandExecuter executer = buildExecuter(graph,pyroProject);
	        info.scce.pyro.core.highlight.HighlightFactory.eINSTANCE.warmup(executer);
	        factoryWarmup(pyroProject,user,executer);
	        //execute command
	        try{
	        	boolean isReOrUndo = ccm.getType().contains("redo")||ccm.getType().contains("undo");
        		«IF g.hasPreSave»
		         {
			         «g.apiImplFQN».«g.name.fuEscapeJava»Impl element = new «g.apiImplFQN».«g.name.fuEscapeJava»Impl(graph,executer);
			         «g.getPreChange» hook = new «g.getPreChange»();
			         hook.init(executer);
			         hook.preSave(element);
		         }
		         «ENDIF»
		        for(Command c:ccm.getCmd().getQueue()){
		            if(c instanceof CreateNodeCommand){
		                CreateNodeCommand cm = (CreateNodeCommand) c;
		                ModelElementContainer mec = modelElementContainerController.read(cm.getContainerId());
		                if(isReOrUndo) {
			                «g.nodesTopologically.filter[!isIsAbstract].map['''
			                if(cm.getDelegateId()!=0){
			                     Node n = executer.create«name.escapeJava»(cm.getX(),cm.getY(),mec,(info.scce.pyro.«g.name.lowEscapeJava».rest.«name.fuEscapeJava»)cm.getElement()«IF prime»,cm.getPrimeId()«ENDIF»);
			                	 ccm.rewriteId(cm.getDelegateId(),n.getDywaId());
			                } else {
			                	executer.create«name.escapeJava»(cm.getX(),cm.getY(),«{
			                					val nodeStyle = styling(it,styles) as NodeStyle
			                					val size = nodeStyle.mainShape.size
			                					 '''
			                						  	 «IF size!=null»
			                						  	 «size.width»,
			                						  	 «size.height»
			                						  	 «ELSE»
			                						  	 «MGLExtension.DEFAULT_WIDTH»,
			                						  	 «MGLExtension.DEFAULT_HEIGHT»
			                						  	 «ENDIF»
			                						  	 '''
			                				}»,mec,(info.scce.pyro.«g.name.lowEscapeJava».rest.«name.fuEscapeJava»)cm.getElement()«IF prime»,cm.getPrimeId()«ENDIF»);
			                }
			                '''.checkType(it,g)].join»
		                } else {
		                	if(cm.getPrimeId()!=0) {
				                PyroElement pe = pyroElementController.read(cm.getPrimeId());
			                	createNode(c.getType(),mec,cm.getX(),cm.getY(),pe,executer);
		                	} else {
		                		createNode(c.getType(),mec,cm.getX(),cm.getY(),null,executer);
		                	}
		                }
		            }
		            else if(c instanceof MoveNodeCommand){
		                MoveNodeCommand cm = (MoveNodeCommand) c;
		                Node node = nodeController.read(cm.getDelegateId());
		                ModelElementContainer mec = modelElementContainerController.read(cm.getContainerId());
		                graphmodel.ModelElementContainer cmec = null;
		                «FOR mec:g.nodesTopologically.filter[!isIsAbstract].filter(NodeContainer)+#[g] SEPARATOR " else "»
		                if(mec instanceof «mec.cast(g)»){
		                	cmec = new «g.apiImplFQN».«mec.name.fuEscapeJava»Impl(
		                		(«mec.cast(g)») mec,
		                		executer
		                	);
		                }
		                «ENDFOR»
		                «g.nodesTopologically.filter[!isIsAbstract].map['''
		                if(isReOrUndo) {
		                	executer.move«name.escapeJava»((«it.cast(g)»)node,cm.getX(),cm.getY(),mec);		                	
		                } else {
		                	«g.apiFQN».«name.fuEscapeJava» cn =
		                	new «g.apiImplFQN».«name.fuEscapeJava»Impl((«it.cast(g)»)node,executer);
		                	cn.moveTo(cmec,java.lang.Math.toIntExact(cm.getX()),java.lang.Math.toIntExact(cm.getY()));
		                }
		                '''.checkType(it,g)].join»
		            }
		            else if(c instanceof ResizeNodeCommand){
		                ResizeNodeCommand cm = (ResizeNodeCommand) c;
		                Node node = nodeController.read(cm.getDelegateId());
		                «g.nodesTopologically.filter[!isIsAbstract].map['''
		                if(isReOrUndo) {
		                	executer.resize«name.escapeJava»((«it.cast(g)»)node,cm.getWidth(),cm.getHeight());
		                } else {
		                	«g.apiFQN».«name.fuEscapeJava» cn =
		                	new «g.apiImplFQN».«name.fuEscapeJava»Impl((«it.cast(g)»)node,executer);
		                	cn.resize(java.lang.Math.toIntExact(cm.getWidth()),java.lang.Math.toIntExact(cm.getHeight()));
		                }
		                '''.checkType(it,g)].join»
		            }
«««		            else if(c instanceof RotateNodeCommand){
«««		                RotateNodeCommand cm = (RotateNodeCommand) c;
«««		                Node node = nodeController.read(cm.getDelegateId());
«««		                «g.nodes.map['''executer.rotate«name.escapeJava»(«it.cast(g)»node,cm.getAngle());'''.checkType(it,g)].join»
«««		            }
		            else if(c instanceof RemoveNodeCommand){
		                RemoveNodeCommand cm = (RemoveNodeCommand) c;
		                Node node = nodeController.read(cm.getDelegateId());
		                «g.nodesTopologically.filter[!isIsAbstract].map['''
		                if(isReOrUndo) {
		                	executer.remove«name.escapeJava»((«it.cast(g)»)node«IF prime»,((«it.cast(g)»)node).get«primeReference.name»()«ENDIF»);
		                } else {
		                	«g.apiFQN».«name.fuEscapeJava» cn =
		                	new «g.apiImplFQN».«name.fuEscapeJava»Impl((«it.cast(g)»)node,executer);
		                	cn.delete();
		                }
		                '''.checkType(it,g)].join»
		            }
		            else if(c instanceof CreateEdgeCommand){
		                CreateEdgeCommand cm = (CreateEdgeCommand) c;
		                Node source = nodeController.read(cm.getSourceId());
		                Node target = nodeController.read(cm.getTargetId());
		                «g.edgesTopologically.filter[!isIsAbstract].map['''
		                if(isReOrUndo) {
			                if(cm.getDelegateId()!=0){
	                            Edge e = executer.create«name.escapeJava»(source,target,cm.getPositions(),(info.scce.pyro.«g.name.lowEscapeJava».rest.«name.fuEscapeJava»)cm.getElement());
	                            ccm.rewriteId(cm.getDelegateId(),e.getDywaId());
	                        } else {
				                executer.create«name.escapeJava»(source,target,cm.getPositions(),(info.scce.pyro.«g.name.lowEscapeJava».rest.«name.fuEscapeJava»)cm.getElement());
	                        }
                        } else {
                        	createEdge(c.getType(),source,target,cm.getPositions(),executer);
                        }
		                '''.checkType(it,g)].join»
		            }
		            else if(c instanceof ReconnectEdgeCommand){
		                ReconnectEdgeCommand cm = (ReconnectEdgeCommand) c;
		                Edge edge = edgeController.read(cm.getDelegateId());
		                Node source = nodeController.read(cm.getSourceId());
		                Node target = nodeController.read(cm.getTargetId());
		                «g.edgesTopologically.filter[!isIsAbstract].map['''
		                if(isReOrUndo) {
		                	executer.reconnect«name.escapeJava»((«it.cast(g)»)edge,source,target);
		                } else {
		                	«g.apiFQN».«name.fuEscapeJava» ce =
		                	new «g.apiImplFQN».«name.fuEscapeJava»Impl((«it.cast(g)»)edge,executer);
		                	if(!edge.gettargetElement().equals(target)) {
		                		//target changed
		                		«FOR target:possibleTargets(it,g).filter[!isIsAbstract] SEPARATOR " else "»
		                		if(target instanceof «target.cast(g)») {
		                			ce.reconnectTarget(new «g.apiImplFQN».«target.name.fuEscapeJava»Impl(
		                				(«target.cast(g)»)target,
		                				executer));
		                		}
		                		«ENDFOR»
		                	} else {
		                		«FOR source:possibleSources(it,g).filter[!isIsAbstract] SEPARATOR " else "»
		                		if(source instanceof «source.cast(g)») {
		                			ce.reconnectSource(new «g.apiImplFQN».«source.name.fuEscapeJava»Impl(
		                			(«source.cast(g)»)source,
		                			executer));
		                		}
		                		«ENDFOR»
		                	}
		                }
		                '''.checkType(it,g)].join»
		            }
		            else if(c instanceof RemoveEdgeCommand){
		                RemoveEdgeCommand cm = (RemoveEdgeCommand) c;
		                Edge edge = edgeController.read(cm.getDelegateId());
		                «g.edgesTopologically.filter[!isIsAbstract].map['''
		                if(isReOrUndo) {
		                	executer.remove«name.escapeJava»((«it.cast(g)»)edge);
		                } else {
		                	«g.apiFQN».«name.fuEscapeJava» ce =
		                	new «g.apiImplFQN».«name.fuEscapeJava»Impl((«it.cast(g)»)edge,executer);
		                	ce.delete();
		                }
		                '''.checkType(it,g)].join»
		            }
		            else if(c instanceof UpdateBendPointCommand){
		                UpdateBendPointCommand cm = (UpdateBendPointCommand) c;
		                Edge edge = edgeController.read(cm.getDelegateId());
		                «g.edgesTopologically.filter[!isIsAbstract].map['''
		                executer.update«name.escapeJava»((«it.cast(g)»)edge,cm.getPositions());
		                '''.checkType(it,g)].join»
		            }
		            else if(c instanceof UpdateCommand && isReOrUndo){
    					UpdateCommand cm = (UpdateCommand) c;
    					IdentifiableElement element = identifiableElementController.read(cm.getDelegateId());
    					«g.elementsAndGraphmodel.filter[!isIsAbstract].map['''
		                executer.update«name.fuEscapeJava»((«it.cast(g)»)element,(info.scce.pyro.«g.name.lowEscapeJava».rest.«name.fuEscapeJava»)cm.getElement());
		                '''.checkType(it,g)].join»
    					
    
    				}
		            else {
						return Response.status(Response.Status.BAD_REQUEST).build();
		            }
		        }
		        «IF hasAppearanceProviders»
				executer.updateAppearance();
				«ENDIF»
	        } catch(Exception e) {
	        	//send rollback
	        	e.printStackTrace();
	        	java.util.List<Command> reversed = new java.util.LinkedList<>(ccm.getCmd().getQueue());
				java.util.Collections.reverse(reversed);
	        	ccm.getCmd().setQueue(reversed);
	        	if(ccm.getType().equals("basic")){
					ccm.setType("basic_invalid_answer");
				} else if(ccm.getType().equals("undo")){
					ccm.setType("undo_invalid_answer");
				} else if(ccm.getType().equals("redo")){
					ccm.setType("redo_invalid_answer");
				}
	        	return Response.ok(ccm).build();
	        }
			String type = "";
			if(ccm.getType().equals("basic")){
				type = "basic_valid_answer";
			} else if(ccm.getType().equals("undo")){
				type = "undo_valid_answer";
			} else if(ccm.getType().equals("redo")){
				type = "redo_valid_answer";
			}
			return createResponse(type,executer,user.getDywaId(),graph.getDywaId(),ccm.getRewriteRule());
	    }
	    
	    private Response createResponse(String type,«g.name.fuEscapeJava»CommandExecuter executer,long userId,long graphId,java.util.List<RewriteRule> rewriteRuleList) {
	       	CompoundCommandMessage response = new CompoundCommandMessage();
	   		response.setType(type);
	   		response.setRewriteRule(rewriteRuleList);
	   		CompoundCommand cc = new CompoundCommand();
	   		cc.setQueue(executer.getBatch().getCommands());
	   		response.setCmd(cc);
	   		response.setOpenFile(executer.getOpenFileCommand());
	   		response.setGraphModelId(graphId);
	   		response.setSenderDywaId(userId);
	   		response.setHighlightings(executer.getHighlightings());
	   		return Response.ok(response).build();
	    }
	    
	    private boolean canCreateGraphModel(
	    	de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser user,
	    	de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject project) {
	    	return canPerformOperation(user, project, de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroCrudOperation.CREATE);
	    }

		private boolean canReadGraphModel(
	    	de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser user,
	    	de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject project) {
	    	return canPerformOperation(user, project, de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroCrudOperation.READ);
	    }
	    
	    private boolean canUpdateGraphModel(
	    	de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser user,
	    	de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject project) {
	    	return canPerformOperation(user, project, de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroCrudOperation.UPDATE);
	    }
	    	    
	    private boolean canDeleteGraphModel(
	    	de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser user,
	    	de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject project) {
	    	return canPerformOperation(user, project, de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroCrudOperation.DELETE);
	    }
	    
	    private boolean canPerformOperation(
	    	de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser user,
	    	de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject project,
	    	de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroCrudOperation operation	    	
	    ) {
			final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroGraphModelPermissionVector searchObject = permissionController.createSearchObject(null);
			searchObject.setuser(user);
			searchObject.setproject(project);
			searchObject.setgraphModelType(de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroGraphModelType.«g.name.toUnderScoreCase»);
			
			final java.util.List<de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroGraphModelPermissionVector> result = permissionController.findByProperties(searchObject);
			return result.size() != 1 ? false : result.get(0).getpermissions_PyroCrudOperation().contains(operation);
	    }
	}
	
	'''
	}
	
	def suffix(String absolutPath, String resourceFolder) {
		absolutPath.substring(absolutPath.lastIndexOf(resourceFolder)+resourceFolder.length+1)
	}
	
	def cast(ModelElement e,GraphModel g)'''de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«e.name.escapeJava»'''
	
	def checkType(CharSequence s,ModelElement e,GraphModel g)
	'''
	if(c.getType().equals("«g.name.lowEscapeDart».«e.name.fuEscapeDart»")) {
		«s»
	}
	'''
}