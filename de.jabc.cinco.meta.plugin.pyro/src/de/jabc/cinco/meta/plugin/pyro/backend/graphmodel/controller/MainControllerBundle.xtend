package de.jabc.cinco.meta.plugin.pyro.backend.graphmodel.controller

import de.jabc.cinco.meta.plugin.pyro.util.Generatable
import de.jabc.cinco.meta.plugin.pyro.util.GeneratorCompound

class MainControllerBundle extends Generatable {
	
	new(GeneratorCompound gc) {
		super(gc)
	}
	
	def filename()'''MainControllerBundle.java'''
	
	def content()
	'''
	package info.scce.pyro.core;
	«FOR g:gc.graphMopdels»
	import info.scce.pyro.core.«g.name.fuEscapeJava»Controller;
	«ENDFOR»
	
	
	
	public class MainControllerBundle {
		«FOR g:gc.graphMopdels»
		private «g.name.fuEscapeJava»Controller «g.name.escapeJava»Controller;
		
		public «g.name.fuEscapeJava»Controller get«g.name.fuEscapeJava»Controller() { return «g.name.escapeJava»Controller; }
		public void set«g.name.fuEscapeJava»Controller(«g.name.fuEscapeJava»Controller ctrl) { «g.name.escapeJava»Controller = ctrl; }
		«ENDFOR»
	}
	'''
	
}