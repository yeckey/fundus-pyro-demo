package de.jabc.cinco.meta.plugin.pyro

import de.jabc.cinco.meta.core.pluginregistry.validation.IMetaPluginValidator
import org.eclipse.emf.ecore.EObject
import productDefinition.Annotation

import static de.jabc.cinco.meta.core.pluginregistry.validation.ValidationResult.newError
import de.jabc.cinco.meta.core.pluginregistry.validation.ValidationResult
import org.eclipse.emf.ecore.EStructuralFeature
import org.eclipse.jdt.core.IType
import org.eclipse.core.resources.ResourcesPlugin
import org.eclipse.jdt.core.JavaCore
import org.eclipse.jdt.core.IJavaProject
import productDefinition.CincoProduct
import org.eclipse.core.resources.IFolder
import org.eclipse.core.resources.IFile

class CPDValidator implements IMetaPluginValidator {
	var pyroAnnotationExists = false

	/**
	 * Checks if for each known annotation the requirements are fulfilled.
	 */
	override checkAll(EObject eObject) {
		pyroAnnotationExists(eObject)
		if (eObject instanceof Annotation) {
			val annotation = eObject as Annotation
			switch annotation.name {
				case "pyro": checkPyroAnnotation(annotation)
				case "pyroProjectPostCreate": checkPyroProjectPostCreate(annotation)
				case "pyroOrganizationPostCreate": checkPyroOrganizationPostCreate(annotation)
				case "pyroEditorLayout": checkPyroEditorLayout(annotation)
				case "pyroRootPostCreate": checkPyroRootPostCreate(annotation)
				case "pyroInitialOrganizations": checkPyroInitialOrganizations(annotation)
				case "pyroOrganizationPerUser": checkPyroOrganizationPerUser(annotation)
				case "pyroProjectPerUser": checkPyroProjectPerUser(annotation)
				case "pyroOAuth": checkPyroOAuth(annotation)
				case "pyroIncludeSources": checkPyroIncludeSources(annotation)
				case "pyroProjectService": checkPyroProjectService(annotation)
				case "pyroProjectAction": checkPyroProjectAction(annotation)
				case "pyroTransientAPI": checkPyroTransientAPI(annotation)
				case "pyroImpressum": checkPyroImpressum(annotation)
				case "pyroClosedRegistration": checkPyroClosedRegistration(annotation)
			}
		}
	}
	
	/**
	 * Checks if an URL to the impressum is provided
	 */
	def checkPyroClosedRegistration(Annotation annotation) {
		if (!pyroAnnotationExists) {
			return newError("@pyro is missing: This annotation has no effect",
				annotation.eClass.getEStructuralFeature("name"))
		} else {
			for (name : annotation.value) {
				if (name == "") {
					return newError("Given name has to be non-empty",
						annotation.eClass.getEStructuralFeature("name"))
				}
			}
		}
	}

	/**
	 * Checks if an URL to the impressum is provided
	 */
	def checkPyroImpressum(Annotation annotation) {
		if (!pyroAnnotationExists) {
			return newError("@pyro is missing: This annotation has no effect",
				annotation.eClass.getEStructuralFeature("name"))
		} else {
			if (annotation.getValue().size() != 1) {
				return newError("A URL to the Impressum is required", annotation.eClass.getEStructuralFeature("name"))
			}else{
				if(annotation.value.head == ""){
					return newError("Given link has to be non-empty",
							annotation.eClass.getEStructuralFeature("name"))
				}
			}
		}
	}

	/**
	 * Checks if an valid path to an mgl exists.
	 */
	def checkPyroTransientAPI(Annotation annotation) {
		if (!pyroAnnotationExists) {
			return newError("@pyro is missing: This annotation has no effect",
				annotation.eClass.getEStructuralFeature("name"))
		} else {
			if (annotation.getValue().size() != 1) {
				return newError("At least a path to an MGL is needed", annotation.eClass.getEStructuralFeature("name"))
			} else {
				var firstParam = annotation.value.head
				isValidMGLPath(firstParam, annotation)
			}
		}
	}

	/**
	 * Checks if two arguments(fqn, button name) exists, if button name is non-empty, and if fqn is valid.
	 */
	def checkPyroProjectAction(Annotation annotation) {
		if (!pyroAnnotationExists) {
			return newError("@pyro is missing: This annotation has no effect",
				annotation.eClass.getEStructuralFeature("name"))
		} else {
			if (annotation.getValue().size() != 2) {
				return newError("At least an FQN and a name for the button are needed",
					annotation.eClass.getEStructuralFeature("name"))
			} else {
				var secondParam = annotation.value.get(1)
				if (secondParam.isEmpty) {
					return newError('''The second argument has to be non-empty.''',
						annotation.eClass.getEStructuralFeature("value"))
				} else {
					var firstParam = annotation.value.head
					isValidFQN(firstParam, annotation)
				}
			}
		}
	}

	/**
	 * Checks if at least two arguments (fqn, button name) exists, if all arguments are non-empty, and if given fqn is valid.
	 */
	def checkPyroProjectService(Annotation annotation) {
		if (!pyroAnnotationExists) {
			return newError("@pyro is missing: This annotation has no effect",
				annotation.eClass.getEStructuralFeature("name"))
		} else {
			if (annotation.getValue().size() < 2) {
				return newError("At least an FQN and a name for the button are needed",
					annotation.eClass.getEStructuralFeature("name"))
			} else {
				var firstParam = annotation.value.head
				if (firstParam != "") {
					val correctFile = findClass(firstParam) // find the corresponding java class file
					if (correctFile !== null) {
						if (!correctFile.exists) { // checks if class exists 
							return newError("Java Class does not exists.",
								annotation.eClass.getEStructuralFeature("value"))
						}
					} else {
						return newError("Java Class does not exists.", annotation.eClass.getEStructuralFeature("value"))
					}
				}
				// checks if arguments are non-empty
				for (value : annotation.value) {
					if (value.isEmpty) {
						var index = annotation.value.indexOf(value) + 1
						return newError('''The «index». argument has to be non-empty.''',
							annotation.eClass.getEStructuralFeature("name"))
					}
				}
			}
		}
	}

	/**
	 * Returns true if a @pyro anntation exists
	 */
	def pyroAnnotationExists(EObject object) {
		if (object instanceof CincoProduct) {
			var annotations = (object as CincoProduct).annotations
			for (annot : annotations) {
				if (annot.name.equals("pyro")) {
					pyroAnnotationExists = true
					return pyroAnnotationExists
				}
			}
		}
		return pyroAnnotationExists
	}

	/**
	 * Checks if @pyroIncludeSources has at least one non-empty file path
	 */
	def ValidationResult<String, EStructuralFeature> checkPyroIncludeSources(Annotation annotation) {
		if (!pyroAnnotationExists) {
			return newError("@pyro is missing: This annotation has no effect",
				annotation.eClass.getEStructuralFeature("name"))
		} else {
			if (annotation.getValue().size() == 0) {
				return newError("At least one path to the file, which should be included, is needed",
					annotation.eClass.getEStructuralFeature("name"))
			} else {
				for (value1 : annotation.value) {
					if (value1.empty) {
						return newError("Argument has to be non-empty.",
							annotation.eClass.getEStructuralFeature("value"))
					}
				}
			}
		}
	}

	/**
	 * Checks if a correct path to a properties files is given
	 */
	def ValidationResult<String, EStructuralFeature> checkPyroOAuth(Annotation annotation) {
		if (!pyroAnnotationExists) {
			return newError("@pyro is missing: This annotation has no effect",
				annotation.eClass.getEStructuralFeature("name"))
		} else {
			if (annotation.getValue().size() != 1) {
				return newError("Path to properties files is missing", annotation.eClass.getEStructuralFeature("name"))
			} else {
				// are given paths correct?
				var parameter = annotation.value.head
				isValidFQN(parameter, annotation)
			}
		}

	}

	/**
	 * Checks if @pyroProjectPerUser has no arguments
	 */
	def ValidationResult<String, EStructuralFeature> checkPyroProjectPerUser(Annotation annotation) {
		if (!pyroAnnotationExists) {
			return newError("@pyro is missing: This annotation has no effect",
				annotation.eClass.getEStructuralFeature("name"))
		} else {
			if (annotation.getValue().size() != 0) {
				return newError("No arguments allowed", annotation.eClass.getEStructuralFeature("name"))
			}
		}
	}

	/**
	 * Checks if @pyroOrganizationPerUser has no arguments
	 */
	def ValidationResult<String, EStructuralFeature> checkPyroOrganizationPerUser(Annotation annotation) {
		if (!pyroAnnotationExists) {
			return newError("@pyro is missing: This annotation has no effect",
				annotation.eClass.getEStructuralFeature("name"))
		} else {
			if (annotation.getValue().size() != 0) {
				return newError("No arguments allowed", annotation.eClass.getEStructuralFeature("name"))
			}
		}
	}

	/**
	 * Checks if @pyroInitialorganizations has at least one non-empty argument
	 */
	def ValidationResult<String, EStructuralFeature> checkPyroInitialOrganizations(Annotation annotation) {
		if (!pyroAnnotationExists) {
			return newError("@pyro is missing: This annotation has no effect",
				annotation.eClass.getEStructuralFeature("name"))
		} else {
			if (annotation.getValue().size() == 0) {
				return newError("At least one argument (organization name) is needed",
					annotation.eClass.getEStructuralFeature("name"))
			} else {
				for (name : annotation.value) {
					if (name == "") {
						return newError("Given organization name has to be non-empty",
							annotation.eClass.getEStructuralFeature("name"))
					}
				}

			}
		}
	}

	/**
	 *  Checks if a correct fully-qualified name is given
	 */
	def ValidationResult<String, EStructuralFeature> checkPyroRootPostCreate(Annotation annotation) {
		if (!pyroAnnotationExists) {
			return newError("@pyro is missing: This annotation has no effect",
				annotation.eClass.getEStructuralFeature("name"))
		} else {
			if (annotation.getValue().size() != 1) {
				return newError("One argument (FQN) is needed", annotation.eClass.getEStructuralFeature("name"))
			} else {
				// is given path correct?
				var parameter = annotation.value.head
				isValidFQN(parameter, annotation)
			}
		}
	}

	/**
	 *  Checks if a correct fully-qualified name is given
	 */
	def ValidationResult<String, EStructuralFeature> checkPyroEditorLayout(Annotation annotation) {
		if (!pyroAnnotationExists) {
			return newError("@pyro is missing: This annotation has no effect",
				annotation.eClass.getEStructuralFeature("name"))
		} else {
			if (annotation.getValue().size() != 1) {
				return newError("One argument (FQN) is needed", annotation.eClass.getEStructuralFeature("name"))
			} else {
				// is given path correct?
				var parameter = annotation.value.head
				isValidFQN(parameter, annotation)
			}
		}
	}

	/**
	 *  Checks if a correct fully-qualified name is given
	 */
	def ValidationResult<String, EStructuralFeature> checkPyroOrganizationPostCreate(Annotation annotation) {
		if (!pyroAnnotationExists) {
			return newError("@pyro is missing: This annotation has no effect",
				annotation.eClass.getEStructuralFeature("name"))
		} else {
			if (annotation.getValue().size() != 1) {
				return newError("One argument (FQN) is needed", annotation.eClass.getEStructuralFeature("name"))
			} else {
				// is given path correct?
				var parameter = annotation.value.head
				isValidFQN(parameter, annotation)
			}
		}
	}

	/**
	 *  Checks if a correct fully-qualified name is given
	 */
	def ValidationResult<String, EStructuralFeature> checkPyroProjectPostCreate(Annotation annotation) {
		if (!pyroAnnotationExists) {
			return newError("@pyro is missing: This annotation has no effect",
				annotation.eClass.getEStructuralFeature("name"))
		} else {
			if (annotation.getValue().size() != 1) {
				return newError("At least one argument (FQN) is needed",
					annotation.eClass.getEStructuralFeature("name"))
			} else {
				// is given path correct?
				var parameter = annotation.value.head
				isValidFQN(parameter, annotation)
			}
		}
	}

	/**
	 * Checks if the value of @pyro is empty
	 */
	def ValidationResult<String, EStructuralFeature> checkPyroAnnotation(Annotation annotation) {
		if (annotation.getValue().size() != 0) {
			return newError("No arguments allowed", annotation.eClass.getEStructuralFeature("name"))
		}

	}

	// ---------Helper methods-----------------------------
	/**
	 * Checks if given FQN is valid: non-empty, class exists
	 */
	private def ValidationResult<String, EStructuralFeature> isValidFQN(String parameter, Annotation annotation) {
		if (parameter != "") {
			val correctFile = findClass(parameter) // find the corresponding java class file
			if (correctFile !== null) {
				if (!correctFile.exists) { // checks if class exists 
					return newError("Java Class does not exists.", annotation.eClass.getEStructuralFeature("value"))
				}
			} else {
				return newError("Java Class does not exists.", annotation.eClass.getEStructuralFeature("value"))
			}
		} else {
			return newError("Argument has to be non-empty.", annotation.eClass.getEStructuralFeature("value"))
		}
	}

	/**
	 * Checks if given MGL path is valid: non-empty, mgl file exists.
	 */
	private def ValidationResult<String, EStructuralFeature> isValidMGLPath(String parameter, Annotation annotation) {
		if (parameter != "") {
			val correctFile = findMGL(parameter) // find the corresponding java class file
			if (correctFile !== null) {
				if (!correctFile.exists) { // checks if class exists 
					return newError("MGL does not exists.", annotation.eClass.getEStructuralFeature("value"))
				}
			} else {
				return newError("MGL does not exists.", annotation.eClass.getEStructuralFeature("value"))
			}
		} else {
			return newError("Argument has to be non-empty.", annotation.eClass.getEStructuralFeature("value"))
		}
	}

	/**
	 * Help method to find the given class
	 */
	private def findClass(String parameter) {
		var IType javaClass = null
		val root = ResourcesPlugin.workspace.root
		val projects = root.projects
		for (project : projects) {
			var jproject = JavaCore.create(project) as IJavaProject
			if (jproject.exists) {
				try {
					javaClass = jproject.findType(parameter)
					if (javaClass !== null) {
						return javaClass
					}
				} catch (Exception e) {
					// nothing to do here (?)
				}
			}
		}
		return javaClass
	}

	/**
	 * Helper method to find the given mgl
	 */
	private def findMGL(String parameter) {
		var IFile mgl = null
		var mglName = parameter.substring(parameter.lastIndexOf("/"))
		var projectName = parameter.substring(0, parameter.indexOf("/"))
		val root = ResourcesPlugin.workspace.root
		val projects = root.projects
		for (project : projects) {
			if (project.name.equals(projectName)) {
				var IFolder modelFolder = project.getFolder("model")
				if (modelFolder.exists) {
					var IFile mglFile = modelFolder.getFile(mglName)
					if (mglFile.exists) {
						mgl = mglFile
						return mgl
					}
				}
			}
		}
		return mgl
	}

}
