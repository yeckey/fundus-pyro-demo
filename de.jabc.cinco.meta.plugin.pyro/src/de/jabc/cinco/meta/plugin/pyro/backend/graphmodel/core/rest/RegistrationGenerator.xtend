package de.jabc.cinco.meta.plugin.pyro.backend.graphmodel.core.rest

import de.jabc.cinco.meta.plugin.pyro.util.Generatable
import de.jabc.cinco.meta.plugin.pyro.util.GeneratorCompound
import mgl.GraphModel
import productDefinition.CincoProduct

class RegistrationGenerator extends Generatable {
	
	new(GeneratorCompound gc) {
		super(gc)
	}
	
	def filename(GraphModel g)'''RegistrationController.java'''
	
	def content(GraphModel g)
	'''
	package info.scce.pyro.core.rest;
	
	import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroUserController;
	import info.scce.pyro.core.rest.types.FindPyroUser;
	import info.scce.pyro.core.OrganizationController;
	import info.scce.pyro.core.rest.types.PyroUser;
	import info.scce.pyro.core.rest.types.PyroUserRegistration;
	import info.scce.pyro.util.UserUtils;
	
	@javax.transaction.Transactional
	@javax.ws.rs.Path("/register/")
	public class RegistrationController {
		
		private static final int MIN_PASSWORD_LENGTH = 5;
	
		@javax.inject.Inject
		private PyroUserController subjectController;
		
		@javax.inject.Inject
		private OrganizationController organizationController;
	
		@javax.inject.Inject
		private info.scce.pyro.rest.ObjectCache objectCache;
		
		
		«IF gc.cpd.hasClosedRegistration»
		@javax.ws.rs.POST
		@javax.ws.rs.Path("new/private")
		@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
		@org.jboss.resteasy.annotations.GZIP
		public javax.ws.rs.core.Response  createUser(PyroUserRegistration pyroUserRegistration) {
			if(
							pyroUserRegistration.getusername()==null
					) {
				return javax.ws.rs.core.Response.status(
						javax.ws.rs.core.Response.Status.FORBIDDEN).build();
			}
			if(
							pyroUserRegistration.getusername().isEmpty()
					) {
				return javax.ws.rs.core.Response.status(
						javax.ws.rs.core.Response.Status.FORBIDDEN).build();
			}
			final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser searchObject = subjectController.createSearchObject(null);
			searchObject.setusername(pyroUserRegistration.getusername());
			final java.util.List<de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser> users = subjectController.findByProperties(searchObject);
			if(users.isEmpty()) {
				de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser user = subjectController.create(pyroUserRegistration.getusername());
				user.setisActivated(false);
				user.setusername(pyroUserRegistration.getusername());
				return javax.ws.rs.core.Response.ok(PyroUser.fromDywaEntity(user,objectCache)).build();
			}
			return javax.ws.rs.core.Response.status(
					javax.ws.rs.core.Response.Status.BAD_REQUEST).build();
		}
		«ENDIF»
		
		@javax.ws.rs.POST
		@javax.ws.rs.Path("new/public")
		@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
		@org.jboss.resteasy.annotations.GZIP
		public javax.ws.rs.core.Response  registerUser(PyroUserRegistration pyroUserRegistration) {
	
			if(
							pyroUserRegistration.getemail()==null||
							pyroUserRegistration.getusername()==null||
							pyroUserRegistration.getname()==null||
							pyroUserRegistration.getpassword()==null
					) {
				return javax.ws.rs.core.Response.status(
						javax.ws.rs.core.Response.Status.FORBIDDEN).build();
			}
			
			if(
					pyroUserRegistration.getemail().isEmpty()||
							pyroUserRegistration.getusername().isEmpty()||
							pyroUserRegistration.getname().isEmpty()||
							pyroUserRegistration.getpassword().isEmpty()
					) {
				return javax.ws.rs.core.Response.status(
						javax.ws.rs.core.Response.Status.FORBIDDEN).build();
			}
	
			if(pyroUserRegistration.getpassword().length() < MIN_PASSWORD_LENGTH){
				return javax.ws.rs.core.Response.status(
						javax.ws.rs.core.Response.Status.FORBIDDEN).build();
			}
			
	
			final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser searchObject = subjectController.createSearchObject(null);
			«IF gc.cpd.hasClosedRegistration»
			final boolean userShouldBeAdmin = «gc.cpd.adminUsers.map['''pyroUserRegistration.getusername().equals("«it»")'''].join("||")»;
			«ELSE»
			final boolean userShouldBeAdmin = subjectController.findByProperties(searchObject).isEmpty();
			«ENDIF»
			searchObject.setusername(pyroUserRegistration.getusername());
			«IF gc.cpd.hasClosedRegistration»
			searchObject.setisActivated(false);
			«ENDIF»
			final java.util.List<de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser> users = subjectController.findByProperties(searchObject);
			if(«IF gc.cpd.hasClosedRegistration»!«ENDIF»users.isEmpty()){
				«IF gc.cpd.hasClosedRegistration»
				de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser user = users.get(0);
				«ELSE»
				de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser user = subjectController.create(pyroUserRegistration.getusername());
				«ENDIF»
				if (userShouldBeAdmin) {
					user.getsystemRoles_PyroSystemRole().add(de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroSystemRole.ADMIN);
					user.getsystemRoles_PyroSystemRole().add(de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroSystemRole.ORGANIZATION_MANAGER);
					«FOR a:gc.initialOrganizations»
						organizationController.addOrganizationOwner(user,organizationController.getOrganization("«a»"));
					«ENDFOR»
				}«IF !gc.initialOrganizations.empty» else {
					«FOR a:gc.initialOrganizations»
						organizationController.addOrganizationMember(user,organizationController.getOrganization("«a»"));
					«ENDFOR»
				}«ENDIF»
				user.setemail(pyroUserRegistration.getemail());
				user.setemailHash(UserUtils.createEmailHash(pyroUserRegistration.getemail()));
				user.setpassword(pyroUserRegistration.getpassword());
				user.setusername(pyroUserRegistration.getusername());
				user.setisActivated(true);
				«IF gc.organizationPerUser»
				//organization per user enabled
				de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganization o = organizationController.createOrganization(user.getusername()+" Organization","",user);
				«ENDIF»
							
	
				return javax.ws.rs.core.Response.ok("Activation mail send").build();
			}
	
			return javax.ws.rs.core.Response.status(
					javax.ws.rs.core.Response.Status.FORBIDDEN).build();
		}
		
	}

	'''
	

	
}