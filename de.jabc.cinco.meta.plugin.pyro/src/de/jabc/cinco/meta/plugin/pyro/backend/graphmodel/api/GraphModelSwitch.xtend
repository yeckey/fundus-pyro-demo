  package de.jabc.cinco.meta.plugin.pyro.backend.graphmodel.api

import de.jabc.cinco.meta.plugin.pyro.util.Generatable
import de.jabc.cinco.meta.plugin.pyro.util.GeneratorCompound
import de.jabc.cinco.meta.plugin.pyro.util.MGLExtension
import mgl.Attribute
import mgl.ContainingElement
import mgl.Edge
import mgl.GraphModel
import mgl.GraphicalModelElement
import mgl.ModelElement
import mgl.Node
import mgl.NodeContainer
import mgl.UserDefinedType
import style.NodeStyle
import style.Styles

class GraphModelSwitch extends Generatable {
	
	protected extension GraphModelElementHook = new GraphModelElementHook
	
	new(GeneratorCompound gc) {
		super(gc)
	}
	
	def filename(GraphModel g)'''«g.name.toCamelCase.fuEscapeJava»Switch.java'''
	
	
	def createSwitchCase(String type)'''
	if(element instanceof graphmodel.«type») {
		result = case«type»((graphmodel.«type»)element);
		if(result != null) {
			return result;
		}
	}
	'''
	
	def createSwitchMethod(String type)'''
	protected T case«type»(graphmodel.«type» element) {
		return null;
	}
	'''
	
	def content(GraphModel g)
	{
	'''
	package «g.apiFQN».util;
	
	public class «g.name.toCamelCase.fuEscapeJava»Switch<T> {
		
			protected T doSwitch(graphmodel.IdentifiableElement element) {
				T result = null;
				«FOR e:g.elementsAndGraphmodel»
				if(element instanceof «g.apiFQN».«e.name.escapeJava») {
					result = case«e.name.escapeJava»((«g.apiFQN».«e.name.escapeJava»)element);
					if(result != null) {
						return result;
					}
				}
				«ENDFOR»
				«"GraphModel".createSwitchCase»
				«"Container".createSwitchCase»
				«"Node".createSwitchCase»
				«"Edge".createSwitchCase»
				«"ModelElementContainer".createSwitchCase»
				«"ModelElement".createSwitchCase»
				«"IdentifiableElement".createSwitchCase»
				return defaultCase(element);
			}
			
			«FOR e:g.elementsAndGraphmodel»
			protected T case«e.name.escapeJava»(«g.apiFQN».«e.name.escapeJava» element) {
				return null;
			}
			«ENDFOR»
			
			«"GraphModel".createSwitchMethod»
			«"Container".createSwitchMethod»
			«"Node".createSwitchMethod»
			«"Edge".createSwitchMethod»
			«"ModelElementContainer".createSwitchMethod»
			«"ModelElement".createSwitchMethod»
			«"IdentifiableElement".createSwitchMethod»

			protected T defaultCase(graphmodel.IdentifiableElement object) {
				return null;
			}
	}
	'''
	}
	
	
}