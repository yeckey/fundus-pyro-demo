package de.jabc.cinco.meta.plugin.pyro.backend.graphmodel.rest

import de.jabc.cinco.meta.plugin.pyro.util.Generatable
import de.jabc.cinco.meta.plugin.pyro.util.GeneratorCompound
import org.eclipse.emf.ecore.ENamedElement
import org.eclipse.emf.ecore.EPackage
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.EEnum

class EcoreRestTO extends Generatable{
	
	new(GeneratorCompound gc) {
		super(gc)
	}
	
	def filenamePackage(EPackage p)'''«p.name.fuEscapeJava».java'''
	
	def filenameStructural(ENamedElement p)'''«p.name.fuEscapeJava».java'''
	
	def filenameEcore(EPackage p)'''«p.name.fuEscapeJava»List.java'''
	
	def contentPackage(EPackage p)
	'''
	package info.scce.pyro.«p.name.lowEscapeJava».rest;
	
	/**
	 * Author zweihoff
	 */
	
	@com.fasterxml.jackson.annotation.JsonFilter("DIME_Selective_Filter")
	@com.fasterxml.jackson.annotation.JsonIdentityInfo(generator = com.voodoodyne.jackson.jsog.JSOGGenerator.class)
	@com.fasterxml.jackson.annotation.JsonTypeInfo(use = com.fasterxml.jackson.annotation.JsonTypeInfo.Id.CLASS, property = info.scce.pyro.util.Constants.DYWA_RUNTIME_TYPE)
	public class «p.name.fuEscapeJava» implements info.scce.pyro.core.graphmodel.PyroElement, info.scce.pyro.core.rest.types.IPyroFile
	{
		private String __type;
				
	    @com.fasterxml.jackson.annotation.JsonProperty("__type")
	    public String get__type() {
	        return this.__type;
	    }
	
	    @com.fasterxml.jackson.annotation.JsonProperty("__type")
	    public void set__type(final String __type) {
	        this.__type = __type;
	    }
	    
	    @com.fasterxml.jackson.annotation.JsonProperty("dywaId")
	    private long dywaId;
	    
	    @Override
    	public long getDywaId() {
    		return dywaId;
    	}
    
    	@Override
    	public void setDywaId(long id) {
    		dywaId = id;
    	}
    	
    	@com.fasterxml.jackson.annotation.JsonProperty("dywaVersion")
    	private long dywaVersion;
	    
    	@Override
    	public long getDywaVersion() {
    		return dywaVersion;
    	}
	    
    	@Override
    	public void setDywaVersion(long version) {
    		dywaVersion = version;
    	}
    	
    	@com.fasterxml.jackson.annotation.JsonProperty("dywaName")
    	private String dywaName;
    		    
	    
    	@Override
    	public String getDywaName() {
    		return dywaName;
    	}
			    
    	@Override
    	public void setDywaName(String name) {
    		dywaName = name;
    	}
		    	
		 private String filename;
		
	    @com.fasterxml.jackson.annotation.JsonProperty("filename")
	    public String getfilename() {
	        return this.filename;
	    }
	
	    @com.fasterxml.jackson.annotation.JsonProperty("filename")
	    public void setfilename(final String filename) {
	        this.filename = filename;
	    }
	
	    private String extension;
	
	    @com.fasterxml.jackson.annotation.JsonProperty("extension")
	    public String getextension() {
	        return this.extension;
	    }
	
	    @com.fasterxml.jackson.annotation.JsonProperty("extension")
	    public void setextension(final String extension) {
	        this.extension = extension;
	    }
		
		«FOR c:p.elements»
			private java.util.List<info.scce.pyro.«p.name.lowEscapeJava».rest.«c.name.fuEscapeJava»> «c.name.escapeJava» = new java.util.LinkedList<>();
						
			@com.fasterxml.jackson.annotation.JsonProperty("«c.name.escapeJava»")
			public java.util.List<info.scce.pyro.«p.name.lowEscapeJava».rest.«c.name.fuEscapeJava»> get«c.name.escapeJava»() {
			    return this.«c.name.escapeJava»;
			}
			
			@com.fasterxml.jackson.annotation.JsonProperty("«c.name.escapeJava»")
			public void set«c.name.escapeJava»(final java.util.List<info.scce.pyro.«p.name.lowEscapeJava».rest.«c.name.fuEscapeJava»> «c.name.escapeJava») {
			    this.«c.name.escapeJava» = «c.name.escapeJava»;
			}
		«ENDFOR»
		
	
	    public static «p.name.fuEscapeJava» fromDywaEntity(final de.ls5.dywa.generated.entity.«p.name.escapeJava».«p.name.fuEscapeJava» entity, info.scce.pyro.rest.ObjectCache objectCache) {
			if(objectCache != null && objectCache.containsRestTo(entity)){
				return objectCache.getRestTo(entity);
			}
			final «p.name.fuEscapeJava» result;
			result = new «p.name.fuEscapeJava»();
			if(objectCache != null) {
				objectCache.putRestTo(entity, result);
			}
			result.setDywaId(entity.getDywaId());
			result.setDywaName(entity.getDywaName());
			result.setDywaVersion(entity.getDywaVersion());
			result.set__type("«p.name.lowEscapeDart».«p.name.fuEscapeDart»");
			result.setfilename(entity.getfilename());
			result.setextension(entity.getextension());
			«FOR c:p.elements»
			result.set«c.name.escapeJava»(
				entity.get«c.name.lowEscapeJava»s_«c.name.escapeJava»().stream().map((n)->info.scce.pyro.«p.name.lowEscapeJava».rest.«c.name.fuEscapeJava».fromDywaEntity(n,objectCache)).collect(java.util.stream.Collectors.toList())
			);
			«ENDFOR»
			return result;
	    }
	}
	'''
	
	def contentEcore(EPackage p)
	'''
	package info.scce.pyro.«p.name.lowEscapeJava».rest;
	
	/**
	 * Author zweihoff
	 */
	
	@com.fasterxml.jackson.annotation.JsonIdentityInfo(generator = com.voodoodyne.jackson.jsog.JSOGGenerator.class)
	public class «p.name.fuEscapeJava»List
	{
			private java.util.List<info.scce.pyro.«p.name.lowEscapeJava».rest.«p.name.fuEscapeJava»> list = new java.util.LinkedList<>();
						
			@com.fasterxml.jackson.annotation.JsonProperty("list")
			public java.util.List<info.scce.pyro.«p.name.lowEscapeJava».rest.«p.name.fuEscapeJava»> getlist() {
			    return this.list;
			}
			
			@com.fasterxml.jackson.annotation.JsonProperty("list")
			public void setlist(final java.util.List<info.scce.pyro.«p.name.lowEscapeJava».rest.«p.name.fuEscapeJava»> list) {
			    this.list = list;
			}
		
	
	    public static «p.name.fuEscapeJava»List fromDywaEntity(final java.util.Collection<de.ls5.dywa.generated.entity.«p.name.escapeJava».«p.name.fuEscapeJava»> entity, info.scce.pyro.rest.ObjectCache objectCache) {
			«p.name.fuEscapeJava»List result = new «p.name.fuEscapeJava»List();
			result.setlist(
				entity.stream().map((n)->info.scce.pyro.«p.name.lowEscapeJava».rest.«p.name.fuEscapeJava».fromDywaEntity(n,objectCache)).collect(java.util.stream.Collectors.toList())
			);
			return result;
	    }
	}
	'''
	
	def contentStructural(ENamedElement p,EPackage pack)
	'''
	package info.scce.pyro.«pack.name.lowEscapeJava».rest;
	
	/**
	 * Author zweihoff
	 */
	
	@com.fasterxml.jackson.annotation.JsonFilter("DIME_Selective_Filter")
	@com.fasterxml.jackson.annotation.JsonIdentityInfo(generator = com.voodoodyne.jackson.jsog.JSOGGenerator.class)
	@com.fasterxml.jackson.annotation.JsonTypeInfo(use = com.fasterxml.jackson.annotation.JsonTypeInfo.Id.CLASS, property = info.scce.pyro.util.Constants.DYWA_RUNTIME_TYPE)
	public class «p.name.fuEscapeJava» implements info.scce.pyro.core.graphmodel.PyroElement
	{
		private String __type;
						
	    @com.fasterxml.jackson.annotation.JsonProperty("__type")
	    public String get__type() {
	        return this.__type;
	    }
	
	    @com.fasterxml.jackson.annotation.JsonProperty("__type")
	    public void set__type(final String __type) {
	        this.__type = __type;
	    }
	    
	    @com.fasterxml.jackson.annotation.JsonProperty("dywaId")
	    private long dywaId;
	    
	    @Override
    	public long getDywaId() {
    		return dywaId;
    	}
    
    	@Override
    	public void setDywaId(long id) {
    		dywaId = id;
    	}
    	
    	@com.fasterxml.jackson.annotation.JsonProperty("dywaVersion")
    	private long dywaVersion;
	    
    	@Override
    	public long getDywaVersion() {
    		return dywaVersion;
    	}
	    
    	@Override
    	public void setDywaVersion(long version) {
    		dywaVersion = version;
    	}
    	
    	@com.fasterxml.jackson.annotation.JsonProperty("dywaName")
    	private String dywaName;
    		    
	    
    	@Override
    	public String getDywaName() {
    		return dywaName;
    	}
			    
    	@Override
    	public void setDywaName(String name) {
    		dywaName = name;
    	}
		«IF p instanceof EClass»
		«FOR attr:p.EAllAttributes»
		private «attr.primitiveJavaType(pack)» «attr.name.escapeJava»«IF attr.list» = new java.util.LinkedList<>()«ENDIF»;
								
		@com.fasterxml.jackson.annotation.JsonProperty("«attr.name.escapeJava»")
		public «attr.primitiveJavaType(pack)» get«attr.name.escapeJava»() {
		    return this.«attr.name.escapeJava»;
		}
		
		@com.fasterxml.jackson.annotation.JsonProperty("«attr.name.escapeJava»")
		public void set«attr.name.escapeJava»(final «attr.primitiveJavaType(pack)» «attr.name.escapeJava») {
		    this.«attr.name.escapeJava» = «attr.name.escapeJava»;
		}
		«ENDFOR»
		«FOR attr:p.EAllReferences»
		private «attr.complexJavaType(pack)» «attr.name.escapeJava»«IF attr.list» = new java.util.LinkedList<>()«ENDIF»;
										
		@com.fasterxml.jackson.annotation.JsonProperty("«attr.name.escapeJava»")
		public «attr.complexJavaType(pack)» get«attr.name.escapeJava»() {
		    return this.«attr.name.escapeJava»;
		}
		
		@com.fasterxml.jackson.annotation.JsonProperty("«attr.name.escapeJava»")
		public void set«attr.name.escapeJava»(final «attr.complexJavaType(pack)» «attr.name.escapeJava») {
		    this.«attr.name.escapeJava» = «attr.name.escapeJava»;
		}
		«ENDFOR»
		«ENDIF»
	
		public static «p.name.fuEscapeJava» fromDywaEntityProperties(final de.ls5.dywa.generated.entity.«pack.name.escapeJava».«p.name.fuEscapeJava» entity, info.scce.pyro.rest.ObjectCache objectCache) {
			return fromDywaEntity(entity,objectCache);
		}
	
	    public static «p.name.fuEscapeJava» fromDywaEntity(final de.ls5.dywa.generated.entity.«pack.name.escapeJava».«p.name.fuEscapeJava» entity, info.scce.pyro.rest.ObjectCache objectCache) {
			
			if(objectCache!=null&&objectCache.containsRestTo(entity)){
				return objectCache.getRestTo(entity);
			}
			final «p.name.fuEscapeJava» result;
			result = new «p.name.fuEscapeJava»();
			if(objectCache != null) {
				objectCache.putRestTo(entity, result);
			}
			result.setDywaId(entity.getDywaId());
			result.setDywaName(entity.getDywaName());
			result.setDywaVersion(entity.getDywaVersion());
			result.set__type("«pack.name.lowEscapeDart».«p.name.fuEscapeDart»");
			«IF p instanceof EClass»
				«FOR attr:p.EAllAttributes»
					«IF attr.list»
						«IF attr.EType instanceof EEnum»
						result.set«attr.name.escapeJava»(
							entity.get«attr.name.escapeJava»().stream().map((n)->
							info.scce.pyro.«pack.name.lowEscapeJava».rest.«attr.EType.name.fuEscapeJava».fromDywaEntity(
								n
								objectCache
							)).collect(java.util.stream.Collectors.toList())
						);
						«ELSE»
						result.set«attr.name.escapeJava»(entity.get«attr.name.escapeJava»());
						«ENDIF»
					«ELSE»
						«IF attr.EType instanceof EEnum»
						result.set«attr.name.escapeJava»(
							info.scce.pyro.«pack.name.lowEscapeJava».rest.«attr.EType.name.fuEscapeJava».fromDywaEntity(
								entity.get«attr.name.escapeJava»(),
								objectCache
							)
						);
						«ELSE»
						result.set«attr.name.escapeJava»(entity.get«attr.name.escapeJava»());
						«ENDIF»
					«ENDIF»
				«ENDFOR»
				«FOR attr:p.EReferences»
					«IF attr.list»
					result.set«attr.name.escapeJava»(
						entity.get«attr.name.escapeJava»_«attr.EType.name.fuEscapeJava»().stream().map((n)->
						info.scce.pyro.«pack.name.lowEscapeJava».rest.«attr.EType.name.fuEscapeJava».fromDywaEntity(
						n,
						objectCache
						)).collect(java.util.stream.Collectors.toList())
					);
					«ELSE»
					if(entity.get«attr.name.escapeJava»() != null) {
						result.set«attr.name.escapeJava»(
							info.scce.pyro.«pack.name.lowEscapeJava».rest.«attr.EType.name.fuEscapeJava».fromDywaEntity(
							entity.get«attr.name.escapeJava»(),
							objectCache
							)
						);
					}
					«ENDIF»
				«ENDFOR»
			«ENDIF»
			return result;
	    }
	}
	'''
	
	
}