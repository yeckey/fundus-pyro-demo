package de.jabc.cinco.meta.plugin.pyro.frontend.pages.editor.plugin

import de.jabc.cinco.meta.plugin.pyro.util.EditorViewPlugin
import de.jabc.cinco.meta.plugin.pyro.util.GeneratorCompound
import de.jabc.cinco.meta.plugin.pyro.util.PluginComponent
import de.jabc.cinco.meta.plugin.pyro.util.EditorViewPluginRestController
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.EReference

class EcoreModelView extends EditorViewPlugin {
	
	private PluginComponent pc
	
	new(GeneratorCompound gc) {
		super(gc)
		pc = new PluginComponent
		pc.tab = "Ecore"
		pc.key = "plugin_ecore"
		pc.fetchURL = "ecoreview/read/'+project.dywaId.toString()+'/private"	
	}
	
	override getPluginComponent() {
		pc
	}
	
	override getRestController(){
		
	val rc = new EditorViewPluginRestController()
	rc.filename="EcoreRestController.java"
	rc.content = '''
	package info.scce.pyro.plugin.controller;
	«FOR lib:gc.ecores»
	import info.scce.pyro.«lib.name.lowEscapeJava».rest.«lib.name.fuEscapeJava»;
	«ENDFOR»
	import info.scce.pyro.plugin.rest.TreeViewNodeRest;
	import info.scce.pyro.plugin.rest.TreeViewRest;
	import javax.ws.rs.core.Response;
	import java.util.Collections;
	import java.util.LinkedList;
	import java.util.List;
	import java.util.stream.Collectors;
	
	@javax.transaction.Transactional
	@javax.ws.rs.Path("/ecoreview")
	public class EcoreRestController {
	
	    @javax.inject.Inject
	    private info.scce.pyro.rest.ObjectCache objectCache;
	    
	    @javax.inject.Inject
	    private de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroProjectController projectController;
	    		
		«FOR lib:gc.ecores»
	    @javax.inject.Inject
	    private «lib.dywaControllerFQN».«lib.name.fuEscapeJava»Controller «lib.name.escapeJava»Controller;
	    
	    @javax.inject.Inject
	    private info.scce.pyro.core.«lib.name.fuEscapeJava»Controller «lib.name.escapeJava»RestController;
		«ENDFOR»
	    @javax.ws.rs.GET
	    @javax.ws.rs.Path("read/{id}/private")
	    @javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	    @org.jboss.resteasy.annotations.GZIP
	    public Response load(@javax.ws.rs.PathParam("id") final long id) {
	    	final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject project = projectController.read(id);
			
			if(project==null){
			    return Response.status(Response.Status.BAD_REQUEST).build();
			}
	    	TreeViewRest tvr = new TreeViewRest();
	    	tvr.setlayer(new LinkedList<>());
			«FOR lib:gc.ecores»
	        final java.util.Set<«lib.dywaFQN».«lib.name.fuEscapeJava»> list«lib.name.escapeJava» = «lib.name.escapeJava»RestController
	                .collectProjectFiles(project);
	        tvr.getlayer().addAll(buildResponse(list«lib.name.escapeJava»));
	        «ENDFOR»
	        return Response.ok(tvr).build();
	    }
	
		«FOR lib:gc.ecores»
	    private List<TreeViewNodeRest> buildResponse(java.util.Set<de.ls5.dywa.generated.entity.«lib.name.escapeJava».«lib.name.fuEscapeJava»> list) {
	    	java.util.Map<de.ls5.dywa.generated.util.Identifiable,TreeViewNodeRest> cache = new java.util.HashMap<>();
	    	return list.stream().map(n->buildResponse«lib.name.fuEscapeJava»(n,cache)).collect(Collectors.toList());
	    }
	
	    private TreeViewNodeRest buildResponse«lib.name.fuEscapeJava»(de.ls5.dywa.generated.entity.«lib.name.escapeJava».«lib.name.fuEscapeJava» entity, java.util.Map<de.ls5.dywa.generated.util.Identifiable,TreeViewNodeRest> cache) {
	        if(cache.containsKey(entity)) {
	        	return cache.get(entity);
	        }
	        
	        List<TreeViewNodeRest> restChildren = new LinkedList<>();
	        TreeViewNodeRest rest = TreeViewNodeRest.fromDywaEntity(
	                entity
	                ,objectCache,
	                entity.getDywaName(),
	                null,
	                "«lib.name.lowEscapeJava».«lib.name.fuEscapeJava»",
	                false,
	                false,
	                false,
	                restChildren
	
	        );
	        cache.put(entity,rest);
	        «FOR cl:lib.EClassifiers.filter(EClass).filter[isReferenceable]»
	        restChildren.addAll(
	                entity.get«cl.name.lowEscapeJava»s_«cl.name.fuEscapeJava»().stream().map(n->buildResponse«cl.name.fuEscapeJava»(n,cache)).collect(Collectors.toList())
	        );
	        «ENDFOR»
	        return rest;
	    }
			«FOR cl:lib.EClassifiers.filter(EClass)»
		    private TreeViewNodeRest buildResponse«cl.name.fuEscapeJava»(de.ls5.dywa.generated.entity.«lib.name.escapeJava».«cl.name.fuEscapeJava» entity, java.util.Map<de.ls5.dywa.generated.util.Identifiable,TreeViewNodeRest> cache) {
	
				if(cache.containsKey(entity)) {
		        	return cache.get(entity);
		        }
		        List<TreeViewNodeRest> restChildren = new LinkedList<>();
    	        
		        TreeViewNodeRest rest = TreeViewNodeRest.fromDywaEntity(
		                entity
		                ,objectCache,
		                entity.getDywaName(),
		                null,
		                "«lib.name.lowEscapeJava».«cl.name.fuEscapeJava»",
		                false,
		                false,
		                true,
		                restChildren
		
		        );
		        cache.put(entity,rest);
		        «FOR er:cl.EReferences.filter[isReferenceable]»
		        	«IF er.list»
		    	        restChildren.addAll(
		    	                entity.get«er.name.lowEscapeJava»_«er.EType.name.fuEscapeJava»().stream().map(n->buildResponse«er.EType.name.fuEscapeJava»(n,cache)).collect(Collectors.toList())
		    	        );
    	        	«ELSE»
    	        		if(entity.get«er.name.lowEscapeJava»()!=null) {
	    	        		restChildren.add(
	    	        			buildResponse«er.EType.name.fuEscapeJava»(entity.get«er.name.lowEscapeJava»(),cache)
			    	        );
		    	        }
    	        	«ENDIF»
    	        «ENDFOR»
		        return rest;
		    }
		    «ENDFOR»
		«ENDFOR»
	}
	
	'''
	
	rc
	}
	
	def isReferenceable(EClass c) {
		val r = gc.graphMopdels.map[annotations].flatten.filter[name.equals("pyroEcoreRootType")]
		r.exists[value.contains(c.name)] || r.empty
	}
	
	def isReferenceable(EReference c) {
		val r = gc.graphMopdels.map[annotations].flatten.filter[name.equals("pyroEcoreExcludeType")]
		!r.exists[value.contains(c.EType.name)]
	}
	
	
	
}