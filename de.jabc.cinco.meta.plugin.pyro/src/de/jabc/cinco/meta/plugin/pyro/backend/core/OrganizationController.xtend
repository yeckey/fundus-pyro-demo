package de.jabc.cinco.meta.plugin.pyro.backend.core

import de.jabc.cinco.meta.plugin.pyro.util.Generatable
import de.jabc.cinco.meta.plugin.pyro.util.GeneratorCompound

class OrganizationController extends Generatable {

	new(GeneratorCompound gc) {
		super(gc)
		
	}

	def filename() '''OrganizationController.java'''

	def content() '''	
		package info.scce.pyro.core;
		
		import com.fasterxml.jackson.core.JsonProcessingException;
		import com.fasterxml.jackson.databind.DeserializationFeature;
		import com.fasterxml.jackson.databind.MapperFeature;
		import com.fasterxml.jackson.databind.ObjectMapper;
		import com.fasterxml.jackson.databind.SerializationFeature;
		import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
		import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroProjectController;
		import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroUserController;
		import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroOrganizationController;
		import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroOrganizationAccessRightVectorController;
		import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroGraphModelPermissionVectorController;
		import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroStyleController;
		import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroSettingsController;
		import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroEditorGridController;
		import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroEditorGridItemController;
		import de.ls5.dywa.generated.util.Identifiable;
		import info.scce.pyro.core.rest.types.*;
		import info.scce.pyro.rest.PyroSelectiveRestFilter;
		import info.scce.pyro.sync.ProjectWebSocket;
		import info.scce.pyro.sync.UserWebSocket;
		import info.scce.pyro.sync.WebSocketMessage;
		import info.scce.pyro.util.DefaultColors;
		import info.scce.pyro.IPyroController;
		
		import javax.ws.rs.core.Response;
		import java.util.*;
		import java.util.stream.Collectors;
		import java.util.stream.Stream;
			
		@javax.transaction.Transactional
		@javax.ws.rs.Path("/organization")
		public class OrganizationController {
			
			@javax.inject.Inject
			private PyroUserController subjectController;
			
			@javax.inject.Inject
		    private PyroOrganizationController organizationController;
		    
		    @javax.inject.Inject
		    private info.scce.pyro.core.ProjectController projectController;
			
			@javax.inject.Inject
		    private PyroOrganizationAccessRightVectorController orgArvController;
			
			@javax.inject.Inject
		    private PyroGraphModelPermissionVectorController permissionController;
			
			@javax.inject.Inject
		    private PyroStyleController styleController;
			
			@javax.inject.Inject
			private PyroSettingsController settingsController;
			
			@javax.inject.Inject
			private PyroEditorGridController editorGridController;
			
			@javax.inject.Inject
			private PyroEditorGridItemController editorGridItemController;
			
			@javax.inject.Inject
			private ProjectService projectService;
			
			@javax.inject.Inject
			private de.ls5.dywa.generated.util.DomainFileController domainFileController;
			
			@javax.inject.Inject
			private info.scce.pyro.rest.ObjectCache objectCache;
			
			«FOR g:gc.graphMopdels»
		    @javax.inject.Inject
		    private info.scce.pyro.core.«g.name.fuEscapeJava»Controller «g.name.lowEscapeJava»Controller;
		    «ENDFOR»
			
			@javax.ws.rs.GET
			@javax.ws.rs.Path("/")
			@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
			@javax.ws.rs.Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
			@org.jboss.resteasy.annotations.GZIP
			public javax.ws.rs.core.Response getAll() {
				final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser subject = subjectController.read((Long)org.apache.shiro.SecurityUtils.getSubject().getPrincipal());
						
				if (subject != null) {
					final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganization searchObject = organizationController.createSearchObject(null);
					final java.util.List<de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganization> result = organizationController.findByProperties(searchObject);
					
					final java.util.List<PyroOrganization> orgs = new java.util.LinkedList<>();
					if (isOrgManager(subject)) {
						for (de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganization org : result) {
							orgs.add(PyroOrganization.fromDywaEntity(org, objectCache));
						}
					} else {
						for (de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganization org : result) {
							if (org.getmembers_PyroUser().contains(subject) || org.getowners_PyroUser().contains(subject)) {
								orgs.add(PyroOrganization.fromDywaEntity(org, objectCache));
							}
						}
					}
					
					return javax.ws.rs.core.Response.ok(orgs).build();
				}
				
		        return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.FORBIDDEN).build();
			}
			
			@javax.ws.rs.GET
			@javax.ws.rs.Path("/{orgId}")
			@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
			@javax.ws.rs.Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
			@org.jboss.resteasy.annotations.GZIP
			public javax.ws.rs.core.Response get(@javax.ws.rs.PathParam("orgId") final long orgId) {
				final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser subject = subjectController.read((Long)org.apache.shiro.SecurityUtils.getSubject().getPrincipal());
						
				if (subject != null) {
					final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganization org = organizationController.read(orgId);
					if (org == null) {
						return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.NOT_FOUND).build();
					}
					
					if (isMemberOf(subject, org) || isOwnerOf(subject, org) || isOrgManager(subject)) {
						return javax.ws.rs.core.Response.ok(PyroOrganization.fromDywaEntity(org, objectCache)).build();
					}
				}
				
		        return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.FORBIDDEN).build();
			}
			
			@javax.ws.rs.GET
			@javax.ws.rs.Path("/{orgId}/graphModelPermissions/my")
			@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
			@javax.ws.rs.Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
			@org.jboss.resteasy.annotations.GZIP
			public javax.ws.rs.core.Response getGraphModelPermissions(@javax.ws.rs.PathParam("orgId") final long orgId) {
				final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser subject = subjectController.read((Long)org.apache.shiro.SecurityUtils.getSubject().getPrincipal());
						
				if (subject != null) {
					final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganization org = organizationController.read(orgId);
					if (org == null) return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.NOT_FOUND).build();
					
					if (isMemberOf(subject, org) || isOwnerOf(subject, org)) {
						final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroGraphModelPermissionVector searchObject = permissionController.createSearchObject(null);
						searchObject.setuser(subject);
						final java.util.List<de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroGraphModelPermissionVector> result = permissionController.findByProperties(searchObject);
						final java.util.List<PyroGraphModelPermissionVector> permissions = new ArrayList<>();
						for (final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroGraphModelPermissionVector p: result) {
							if (p.getproject().getorganization().equals(org)) {
								permissions.add(PyroGraphModelPermissionVector.fromDywaEntity(p, objectCache));
							}
						}
						return javax.ws.rs.core.Response.ok(permissions).build();
					}
				}
				
		        return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.FORBIDDEN).build();
			}
			
			@javax.ws.rs.POST
			@javax.ws.rs.Path("/{orgId}/leave")
			@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
			@javax.ws.rs.Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
			@org.jboss.resteasy.annotations.GZIP
			public javax.ws.rs.core.Response leave(@javax.ws.rs.PathParam("orgId") final long orgId) {
				final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser subject = subjectController.read((Long)org.apache.shiro.SecurityUtils.getSubject().getPrincipal());
				
				final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganization org = organizationController.read(orgId);
				if (subject != null) {
					if (isMemberOf(subject, org)) {
						org.getmembers_PyroUser().remove(subject);
					} else if (isOwnerOf(subject, org) && org.getowners_PyroUser().size() > 1) {
						org.getowners_PyroUser().remove(subject);
					} else {
						return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.BAD_REQUEST).build();
					}
					
					deleteAccessRightVector(subject, org);
					deleteGraphModelPermissionVectors(subject, org);
					deleteEditorGrids(subject, org);
								
					return javax.ws.rs.core.Response.ok().build();
				}
				
				return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.FORBIDDEN).build();
			}
				
			@javax.ws.rs.POST
			@javax.ws.rs.Path("/{orgId}/addMember")
			@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
			@javax.ws.rs.Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
			@org.jboss.resteasy.annotations.GZIP
			public javax.ws.rs.core.Response addMember(@javax.ws.rs.PathParam("orgId") final long orgId, PyroUser user) {
				final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser subject = subjectController.read((Long)org.apache.shiro.SecurityUtils.getSubject().getPrincipal());
				
				final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganization org = organizationController.read(orgId);	
				
				if (subject != null && (isOrgManager(subject) || isOwnerOf(subject, org))) {
					final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser member = subjectController.read(user.getDywaId());
					
					if (org == null || member == null) {
						return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.NOT_FOUND).build();
					}
					
					if (org.getowners_PyroUser().contains(member)) {
						if (org.getowners_PyroUser().size() == 1) { // do not make the ownly owner a member of the org
							return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.BAD_REQUEST).build();
						}
						org.getowners_PyroUser().remove(member);
					}
					if(!org.getmembers_PyroUser().contains(member)) {
						addOrganizationMember(member,org);
					}
					
					return javax.ws.rs.core.Response.ok(PyroOrganization.fromDywaEntity(org, objectCache)).build();
				}
				
		        return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.FORBIDDEN).build();
			}
			
			public de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganization getOrganization(String name) {
				return organizationController.fetchByName("Organization_" + name).stream().findFirst().orElse(null);
			}
			
			public void addOrganizationMember(de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser user,de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganization org)
			{
			
				if (!accessRightVectorExists(user, org)) {
					createDefaultAccessRightVector(user, org);
					projectService.createDefaultEditorGrid(user, org);
					projectService.createDefaultGraphModelPermissionVectors(user, org);
				}
			
				org.getmembers_PyroUser().add(user);
			}
			
			@javax.ws.rs.POST
			@javax.ws.rs.Path("/{orgId}/addOwner")
			@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
			@javax.ws.rs.Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
			@org.jboss.resteasy.annotations.GZIP
			public javax.ws.rs.core.Response addOwner(@javax.ws.rs.PathParam("orgId") final long orgId, PyroUser user) {
				final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser subject = subjectController.read((Long)org.apache.shiro.SecurityUtils.getSubject().getPrincipal());
				
				final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganization org = organizationController.read(orgId);	
				
				if (subject != null && (isOrgManager(subject) || isOwnerOf(subject, org))) {
					final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser owner = subjectController.read(user.getDywaId());
					
					if (org == null || owner == null) {
						return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.NOT_FOUND).build();
					}
					
					if (org.getmembers_PyroUser().contains(owner)) {
						org.getmembers_PyroUser().remove(owner);
					}
					
					if(!org.getowners_PyroUser().contains(owner)) {
						addOrganizationOwner(owner,org);
					}
					return javax.ws.rs.core.Response.ok(PyroOrganization.fromDywaEntity(org, objectCache)).build();
				}
				
		        return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.FORBIDDEN).build();
			}
			
			public void addOrganizationOwner(de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser user,de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganization org) {
				if (!accessRightVectorExists(user, org)) {
					final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganizationAccessRightVector arv = createDefaultAccessRightVector(user, org);
					arv.getaccessRights_PyroOrganizationAccessRight().add(de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganizationAccessRight.CREATE_PROJECTS);
					arv.getaccessRights_PyroOrganizationAccessRight().add(de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganizationAccessRight.EDIT_PROJECTS);
					arv.getaccessRights_PyroOrganizationAccessRight().add(de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganizationAccessRight.DELETE_PROJECTS);
					projectService.createDefaultEditorGrid(user, org);
					projectService.createDefaultGraphModelPermissionVectors(user, org);
				}
				
				org.getowners_PyroUser().add(user);
			}
			
			@javax.ws.rs.POST
			@javax.ws.rs.Path("/{orgId}/removeUser")
			@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
			@javax.ws.rs.Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
			@org.jboss.resteasy.annotations.GZIP
			public javax.ws.rs.core.Response removeUser(@javax.ws.rs.PathParam("orgId") final long orgId, PyroUser user) {
				final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser subject = subjectController.read((Long)org.apache.shiro.SecurityUtils.getSubject().getPrincipal());
				
				final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganization org = organizationController.read(orgId);	
				
				if (subject != null && (isOrgManager(subject) || isOwnerOf(subject, org))) {
					final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser userToRemove = subjectController.read(user.getDywaId());
					
					if (org == null || userToRemove == null) {
						return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.NOT_FOUND).build();
					}
					
					if (org.getowners_PyroUser().contains(userToRemove) && org.getowners_PyroUser().size() > 1) { // don't delete single owner
						org.getowners_PyroUser().remove(userToRemove);
					} else if (org.getmembers_PyroUser().contains(userToRemove)) {
						org.getmembers_PyroUser().remove(userToRemove);
					} else {
						return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.BAD_REQUEST).build();
					}
					
					deleteAccessRightVector(userToRemove, org);
					deleteGraphModelPermissionVectors(userToRemove, org);
					deleteEditorGrids(subject, org);
					
					// assign projects of removed user to oneself for now
					for (de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject project: org.getprojects_PyroProject()) {
						project.setowner(subject);
					}
											
					return javax.ws.rs.core.Response.ok(PyroOrganization.fromDywaEntity(org, objectCache)).build();
				}
				
		        return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.FORBIDDEN).build();
			}
				
			@javax.ws.rs.POST
			@javax.ws.rs.Path("/")
			@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
			@javax.ws.rs.Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
			@org.jboss.resteasy.annotations.GZIP
			public javax.ws.rs.core.Response create(PyroOrganization newOrg) {
				final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser subject = subjectController.read((Long)org.apache.shiro.SecurityUtils.getSubject().getPrincipal());
				
				if (mayCreateOrganization(subject)) {
					final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganization org = this.createOrganization(newOrg.getname(),newOrg.getdescription(),subject);
					
					return javax.ws.rs.core.Response.ok(PyroOrganization.fromDywaEntity(org, objectCache)).build();
				}
				
				return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.FORBIDDEN).build();
			}
			
			public de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganization createOrganization(String name, String description, de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser subject) {
				final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganization org = organizationController.create("Organization_" + name);
				org.setname(name);
				org.setdescription(description);
		
				final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroStyle style = styleController.create("Style_" + name);
				style.setnavBgColor(DefaultColors.NAV_BG_COLOR);
				style.setnavTextColor(DefaultColors.NAV_TEXT_COLOR);
				style.setbodyBgColor(DefaultColors.BODY_BG_COLOR);
				style.setbodyTextColor(DefaultColors.BODY_TEXT_COLOR);
				style.setprimaryBgColor(DefaultColors.PRIMARY_BG_COLOR);
				style.setprimaryTextColor(DefaultColors.PRIMARY_TEXT_COLOR);
				org.setstyle(style);
				
				java.util.Set<IPyroController> bundles = new java.util.HashSet<>();
		        «FOR g:gc.graphMopdels»
		        bundles.add(«g.name.lowEscapeJava»Controller);
		        «ENDFOR»
		        
		        «FOR a:gc.organizationPostCreate.indexed»
		        «a.value» hook«a.key» = new «a.value»();
		        hook«a.key».init(bundles,settingsController,styleController);
		        hook«a.key».execute(org);
		        «ENDFOR»
		        if(subject != null) {
		        	this.addOrganizationOwner(subject,org);
		        «IF gc.projectPerUser !== null»
		        	«IF gc.projectPerUser.isEmpty»
		        		projectController.createProject(
		        			subject.getusername()+"'s Project",
		        			"",
		        			subject,
		        			org
		        		);
		        	«ELSE»
		        		«FOR a:gc.projectPerUser»
		        		if(name.equals("«a»")) {
		        			projectController.createProject(
		        				subject.getusername()+"'s Project",
		        				"",
		        				subject,
		        				org
		        			);
		        		}
		        		«ENDFOR»
		        	«ENDIF»
		        «ENDIF»
	        	}
		        return org;
			}
			
			@javax.ws.rs.PUT
			@javax.ws.rs.Path("/{orgId}")
			@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
			@javax.ws.rs.Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
			@org.jboss.resteasy.annotations.GZIP
			public javax.ws.rs.core.Response update(@javax.ws.rs.PathParam("orgId") final long orgId, PyroOrganization organization) {
				final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser subject = subjectController.read((Long)org.apache.shiro.SecurityUtils.getSubject().getPrincipal());
						
				if (subject != null) {
					final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganization orgInDB = organizationController.read(orgId);
					if (orgInDB == null) {
						return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.NOT_FOUND).build();
					}
					
					if ((orgInDB.getDywaId() != organization.getDywaId()) || organization.getname().trim().equals("")) {
						return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.BAD_REQUEST).build();
					}
					
					if (isOwnerOf(subject, orgInDB)) {
						orgInDB.setname(organization.getname());
						orgInDB.setdescription(organization.getdescription());	
						
						final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroStyle style = orgInDB.getstyle();			
						style.setnavBgColor(organization.getstyle().getnavBgColor());
						style.setnavTextColor(organization.getstyle().getnavTextColor());
						style.setbodyBgColor(organization.getstyle().getbodyBgColor());
						style.setbodyTextColor(organization.getstyle().getbodyTextColor());
						style.setprimaryBgColor(organization.getstyle().getprimaryBgColor());
						style.setprimaryTextColor(organization.getstyle().getprimaryTextColor());
						
						if (organization.getstyle().getlogo() != null) {
							final de.ls5.dywa.generated.util.FileReference logo = domainFileController.getFileReference(organization.getstyle().getlogo().getDywaId());
							style.setlogo(logo);
						} else {
							style.setlogo(null);
						}
						
						return javax.ws.rs.core.Response.ok(PyroOrganization.fromDywaEntity(orgInDB, objectCache)).build();
					}
				}
				
		        return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.FORBIDDEN).build();
			}
			
			@javax.ws.rs.DELETE
			@javax.ws.rs.Path("/{orgId}")
			@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
			@javax.ws.rs.Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
			@org.jboss.resteasy.annotations.GZIP
			public javax.ws.rs.core.Response delete(@javax.ws.rs.PathParam("orgId") final long orgId) {
				final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser subject = subjectController.read((Long)org.apache.shiro.SecurityUtils.getSubject().getPrincipal());
						
				if (subject != null) {
					final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganization orgInDB = organizationController.read(orgId);
					if (orgInDB == null) {
						return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.NOT_FOUND).build();
					}
								
					if (isOrgManager(subject) || isOwnerOf(subject, orgInDB)) {				
						deleteAccessRightVectors(orgInDB);				
						for (de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject project: orgInDB.getprojects_PyroProject()) {
							projectService.deleteById(subject, project.getDywaId());
						}
						organizationController.delete(orgInDB);
						return javax.ws.rs.core.Response.ok(PyroOrganization.fromDywaEntity(orgInDB, objectCache)).build();
					}
				}
				
		        return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.FORBIDDEN).build();
			}
			
			private de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganizationAccessRightVector findAccessRightVector(
					de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser user,
					de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganization org) {
				
				final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganizationAccessRightVector searchObject = orgArvController.createSearchObject(null);
				searchObject.setuser(user);
				searchObject.setorganization(org);
				
				final java.util.List<de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganizationAccessRightVector> result = orgArvController.findByProperties(searchObject);
				return result.size() == 1 ? result.get(0) : null;
			}
			
			private boolean mayCreateOrganization(de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser user) {
				final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroSettings searchObject = settingsController.createSearchObject(null);
				final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroSettings settings = settingsController.findByProperties(searchObject).get(0);
				
				return user != null && (isOrgManager(user) || settings.getgloballyCreateOrganizations());
			}
			
			private boolean accessRightVectorExists(
					de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser user,
					de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganization org) {
				return findAccessRightVector(user, org) != null;
			}
			
			private de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganizationAccessRightVector createDefaultAccessRightVector(
					de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser user,
					de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganization org) {
				
				final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganizationAccessRightVector arv = orgArvController.create("AccessRightVector_");
				arv.setuser(user);
				arv.setorganization(org);
				
				return arv;
			}
					
			private void deleteAccessRightVector(
					de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser user,
					de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganization org) {
				
				final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganizationAccessRightVector arv = findAccessRightVector(user, org);
				orgArvController.delete(arv);
			}
			
			private void deleteAccessRightVectors(de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganization org) {
				final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganizationAccessRightVector searchObject = orgArvController.createSearchObject(null);
				searchObject.setorganization(org);
				
				final java.util.List<de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganizationAccessRightVector> result = orgArvController.findByProperties(searchObject);
				for (de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganizationAccessRightVector arv: result) {
					orgArvController.delete(arv);
				}
			}
				
			private void deleteGraphModelPermissionVectors(
					de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser user,
					de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganization org) {
				for (final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject project: org.getprojects_PyroProject()) {
					final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroGraphModelPermissionVector searchObject = permissionController.createSearchObject(null);
					searchObject.setuser(user);
					searchObject.setproject(project);
					final java.util.List<de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroGraphModelPermissionVector> result = permissionController.findByProperties(searchObject);
					for (final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroGraphModelPermissionVector vector: result) {
						permissionController.delete(vector);
					}
				}
			}
			
			private void deleteEditorGrids(
					de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser user,
					de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganization org) {
				for (final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject project: org.getprojects_PyroProject()) {
					final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorGrid searchObject = editorGridController.createSearchObject(null);
					searchObject.setuser(user);
					searchObject.setproject(project);
					final java.util.List<de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorGrid> result = editorGridController.findByProperties(searchObject);
					for (final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorGrid grid: result) {
						editorGridController.delete(grid);
					}
				}
			}
						
			private boolean isOrgManager(de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser user) {
				return user.getsystemRoles_PyroSystemRole().contains(de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroSystemRole.ORGANIZATION_MANAGER);
			}
			
			private boolean isMemberOf(de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser user, de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganization org) {
				return org.getmembers_PyroUser().contains(user);
			}
			
			private boolean isOwnerOf(de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser user, de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganization org) {
				return org.getowners_PyroUser().contains(user);
			}
		}	

		
	'''
}
