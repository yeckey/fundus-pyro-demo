package de.jabc.cinco.meta.plugin.pyro.frontend

import de.jabc.cinco.meta.core.utils.CincoUtil
import de.jabc.cinco.meta.plugin.pyro.frontend.deserializer.Deserializer
import de.jabc.cinco.meta.plugin.pyro.frontend.model.Model
import de.jabc.cinco.meta.plugin.pyro.frontend.pages.editor.EditorComponent
import de.jabc.cinco.meta.plugin.pyro.frontend.pages.editor.canvas.CanvasComponent
import de.jabc.cinco.meta.plugin.pyro.frontend.pages.editor.canvas.graphs.graphmodel.GraphmodelComponent
import de.jabc.cinco.meta.plugin.pyro.frontend.pages.editor.check.CheckComponent
import de.jabc.cinco.meta.plugin.pyro.frontend.pages.editor.explorer.graphentry.FileEntryComponent
import de.jabc.cinco.meta.plugin.pyro.frontend.pages.editor.explorer.graphentry.create.CreateFileComponent
import de.jabc.cinco.meta.plugin.pyro.frontend.pages.editor.map.MapComponent
import de.jabc.cinco.meta.plugin.pyro.frontend.pages.editor.palette.PaletteComponent
import de.jabc.cinco.meta.plugin.pyro.frontend.pages.editor.palette.graphs.graphmodel.PaletteBuilder
import de.jabc.cinco.meta.plugin.pyro.frontend.pages.editor.palette.list.ListComponent
import de.jabc.cinco.meta.plugin.pyro.frontend.pages.editor.properties.graphs.graphmodel.GraphmodelTree
import de.jabc.cinco.meta.plugin.pyro.frontend.pages.editor.properties.graphs.graphmodel.IdentifiableElementPropertyComponent
import de.jabc.cinco.meta.plugin.pyro.frontend.pages.editor.properties.graphs.graphmodel.PropertyComponent
import de.jabc.cinco.meta.plugin.pyro.frontend.pages.editor.properties.tree.TreeComponet
import de.jabc.cinco.meta.plugin.pyro.frontend.service.GraphService
import de.jabc.cinco.meta.plugin.pyro.util.FileGenerator
import de.jabc.cinco.meta.plugin.pyro.util.FileHandler
import de.jabc.cinco.meta.plugin.pyro.util.GeneratorCompound
import de.jabc.cinco.meta.plugin.pyro.util.MGLExtension
import mgl.UserDefinedType
import org.eclipse.core.resources.IProject
import org.eclipse.core.runtime.IPath
import de.jabc.cinco.meta.plugin.pyro.frontend.model.Core
import de.jabc.cinco.meta.plugin.pyro.util.Escaper
import de.jabc.cinco.meta.plugin.pyro.frontend.pages.login.LoginComponent
import de.jabc.cinco.meta.plugin.pyro.frontend.pages.main.MainComponent
import de.jabc.cinco.meta.plugin.pyro.frontend.pages.editor.menu.MenuComponent
import de.jabc.cinco.meta.plugin.pyro.frontend.pages.welcome.WelcomeComponent
import de.jabc.cinco.meta.plugin.pyro.frontend.pages.admin.user_management.users.UsersComponent
import de.jabc.cinco.meta.plugin.pyro.frontend.pages.logout.LogoutComponent
import de.jabc.cinco.meta.plugin.pyro.frontend.pages.shared.navigation.NavigationComponent

class Generator extends FileGenerator{
	
	extension MGLExtension mglExtension
	extension Escaper = new Escaper
	
	new(IPath base) {
		super(base)
	}
	
	def generate(GeneratorCompound gc,IProject iProject)
	{
		mglExtension = gc.mglExtension
		
		val graphModels = gc.graphMopdels
		
		{
			//web
			val path = "web"
			val gen = new Main(gc)
			generateFile(path,
				gen.fileNameMain,
				gen.contentMain
			)
		}
		
		{
			//web
			val path = "web"
			val gen = new Index(gc)
			generateFile(path,
				gen.fileNameIndex,
				gen.contentIndex
			)
		}
		
		{
			//lib.deserialzer
			val path = "lib/src/deserializer"
			val gen = new Deserializer(gc)
			graphModels.forEach[g|{
				generateFile(path,
					gen.fileNameGraphmodelPropertyDeserializer(g.name),
					gen.contentGraphmodelPropertyDeserializer(g)
				)
			}]
			generateFile(path,
				gen.fileNamePropertyDeserializer(),
				gen.contentPropertyDeserializer()
			)
		}
		
		{
			//lib.model
			val path = "lib/src/model"
			val gen = new Model(gc)
			generateFile(path,
				gen.fileNameDispatcher,
				gen.contentDispatcher
			)
			graphModels.forEach[g|{
				val styles = CincoUtil.getStyles(g, iProject)
				generateFile(path,
					gen.fileNameGraphModel(g.name),
					gen.contentGraphmodel(g,styles)
				)
			}]
			gc.ecores.forEach[g|{
				generateFile(path,
					gen.fileNameEcore(g.name),
					gen.contentEcore(g)
				)
			}]
		}
		
		{
			val path = "lib/src/model"
			val gen = new Core(gc)
			generateFile(path,
				gen.fileNameDispatcher,
				gen.contentDispatcher
			)
		}
		
		{
			//lib.editor
			val path = "lib/src/pages/editor"
			val gen = new EditorComponent(gc)
			
			//view plugins
			generateFile(path,
				gen.fileNameEditorComponent,
				gen.contentEditorComponent()
			)
			generateFile(path,
				gen.fileNameEditorTemplate,
				gen.contentEditorTemplate()
			)		
		}
		
		{
			//lib.editor.menu
			val path = "lib/src/pages/editor/menu"
			val gen = new MenuComponent(gc)
			
			//view plugins
			generateFile(path,
				gen.fileNameMenuComponent,
				gen.contentMenuComponent
			)
			generateFile(path,
				gen.fileNameMenuTemplate,
				gen.contentMenuTemplate
			)		
		}
		
		
		{
			//lib.pages.editor.canvas.graphs.graphmodel
			graphModels.forEach[g|{
				val styles = CincoUtil.getStyles(g, iProject)
				val path = "lib/src/pages/editor/canvas/graphs/"+g.name.lowEscapeDart
				clearDirectory(path)
				val gen = new GraphmodelComponent(gc)
				generateFile(path,
					gen.fileNameGraphModelCommandGraph(g.name),
					gen.contentGraphModelCommandGraph(g,styles)
				)
				generateFile(path,
					gen.fileNameGraphModelComponent(g.name),
					gen.contentGraphModelComponent(g,styles)
				)
				generateFile(path,
					gen.fileNameGraphModelComponentTemplate(g.name),
					gen.contentGraphModelComponentTemplate(g)
				)
				
			}]
		}
		
		{
			//lib.editor.canvas
			val path = "lib/src/pages/editor/canvas"
			val gen = new CanvasComponent(gc)
			generateFile(path,
				gen.fileNameCanvasComponent,
				gen.contentCanvasComponent
			)
			generateFile(path,
				gen.fileNameCanvasComponentTemplate,
				gen.contentCanvasComponentTemplate
			)	
		}
		
		{
			//lib.editor.check
			val path = "lib/src/pages/editor/check"
			val gen = new CheckComponent(gc)
			generateFile(path,
				gen.fileNameCheckComponent,
				gen.contentCheckComponent
			)
			generateFile(path,
				gen.fileNameCheckComponentTemplate,
				gen.contentCheckComponentTemplate
			)
		}
		
		{
			//lib.pages.login
			val path = "lib/src/pages/login"
			val gen = new LoginComponent(gc)
			generateFile(path,
				gen.fileNameLoginComponent,
				gen.contentLoginComponent
			)
			generateFile(path,
				gen.fileNameLoginTemplate,
				gen.contentLoginTemplate
			)
		}
		
		{
			//lib.pages.shared.navigation
			val path = "lib/src/pages/shared/navigation"
			val gen = new NavigationComponent(gc)
			generateFile(path,
				gen.fileNameTemplate,
				gen.contentTemplate
			)
			
		}
		
		{
			//lib.pages.logout
			val path = "lib/src/pages/logout"
			val gen = new LogoutComponent(gc)
			generateFile(path,
				gen.fileNameTemplate,
				gen.contentTemplate
			)
		}
		
		{
			//lib.pages.welcome
			val path = "lib/src/pages/welcome"
			val gen = new WelcomeComponent(gc)
			generateFile(path,
				gen.fileNameTemplate,
				gen.contentTemplate
			)
		}
		
		{
			//lib.pages.admin.user_management.users
			val path = "lib/src/pages/admin/user-management/users"
			val gen = new UsersComponent(gc)
			generateFile(path,
				gen.fileNameTemplate,
				gen.contentTemplate
			)
			generateFile(path,
				gen.fileNameComponent,
				gen.contentComponent
			)
		}
		
		{
			//lib.pages.main
			val path = "lib/src/pages/main"
			val gen = new MainComponent(gc)
			generateFile(path,
				gen.fileNameTemplate,
				gen.contentTemplate
			)
		}
		
		{
			//lib.editor.explorer.graph_entry
			val path = "lib/src/pages/editor/explorer/graph_entry"
			val gen = new FileEntryComponent(gc)
			generateFile(path,
				gen.fileNameFileEntryComponent,
				gen.contentFileEntryComponent
			)		
		}
		
		{
			//lib.editor.explorer.graph_entry.create
			val path = "lib/src/pages/editor/explorer/graph_entry/create"
			val gen = new CreateFileComponent(gc)
			generateFile(path,
				gen.fileNameCreateFileComponent,
				gen.contentCreateFileComponent
			)
			generateFile(path,
				gen.fileNameCreateFileComponentTemplate,
				gen.contentCreateFileComponentTemplate
			)		
		}
		
		{
			//lib.editor.map
			val path = "lib/src/pages/editor/map"
			val gen = new MapComponent(gc)
			generateFile(path,
				gen.fileNameMapComponent,
				gen.contentMapComponent
			)
			generateFile(path,
				gen.fileNameMapComponentTemplate,
				gen.contentMapComponentTemplate
			)		
		}
		
		{
			//lib.editor.palette
			val path = "lib/src/pages/editor/palette"
			val gen = new PaletteComponent(gc)
			generateFile(path,
				gen.fileNamePaletteComponent,
				gen.contentPaletteComponent
			)
		}
		
		{
			//lib.editor.palette.graphs.graphmodel
			graphModels.forEach[g|{
				val path = "lib/src/pages/editor/palette/graphs/"+g.name.lowEscapeDart
				val gen = new PaletteBuilder(gc)
				generateFile(path,
					gen.fileNamePaletteBuilder,
					gen.contentPaletteBuilder(g)
				)
			}]
		}
		
		{
			//lib.editor.palette.list
			val path = "lib/src/pages/editor/palette/list"
			val gen = new ListComponent(gc)
			generateFile(path,
				gen.fileNameListComponent,
				gen.contentListComponent
			)
		}
		
		{
			//lib.editor.properties.graphs.graphmodel
			graphModels.forEach[g|{
				val path = "lib/src/pages/editor/properties/graphs/"+g.name.lowEscapeDart
				val treeGen = new GraphmodelTree(gc)
				generateFile(path,
					treeGen.fileNameGraphmodelTree(g.name),
					treeGen.contentGraphmodelTree(g)
				)
				val propGen = new PropertyComponent(gc)
				generateFile(path,
					propGen.fileNamePropertyComponent,
					propGen.contentPropertyComponent(g)
				)
				generateFile(path,
					propGen.fileNamePropertyComponentTemplate,
					propGen.contentPropertyComponentTemplate(g)
				)
				val proertyTypes = g.elements + g.types.filter(UserDefinedType) + #[g]
				proertyTypes.forEach[t|{
					val gen = new IdentifiableElementPropertyComponent(gc)
					generateFile(path,
						gen.fileNameIdentifiableElementPropertyComponent(t.name),
						gen.contentIdentifiableElementPropertyComponent(g,t)
					)
					generateFile(path,
						gen.fileNameIdentifiableElementPropertyComponentTemplate(t.name),
						gen.contentIdentifiableElementPropertyComponentTemplate(g,t)
					)
				}]
				
			}]
		}
		{
			//lib.editor.properties.property
			val path = "lib/src/pages/editor/properties/property"
			val gen = new de.jabc.cinco.meta.plugin.pyro.frontend.pages.editor.properties.property.PropertyComponent(gc)
			generateFile(path,
				gen.fileNamePropertyComponent,
				gen.contentPropertyComponent
			)
			generateFile(path,
				gen.fileNamePropertyComponentTemplate,
				gen.contentPropertyComponentTemplate
			)
		}
		{
			//lib.editor.properties.tree
			val path = "lib/src/pages/editor/properties/tree"
			val gen = new TreeComponet(gc)
			generateFile(path,
				gen.fileNameTreeComponent,
				gen.contentTreeComponent
			)
		}
//		{
//			//lib.editor.properties.tree.node
//			val path = "lib/src/pages/editor/properties/tree/node"
//			val gen = new TreeNodeComponent(gc)
//			generateFile(path,
//				gen.fileNameTreeNodeComponent,
//				gen.contentTreeNodeComponent
//			)
//		}
		
		
		{
			//lib.service
			val path = "lib/src/service"
			val gen = new GraphService(gc)
			generateFile(path,
				gen.fileNameGraphServcie,
				gen.contentGraphService
			)
		}
		{
			//.
			val path = ""
			val gen = new Pubspec(gc)
			generateFile(path,
				gen.fileNamePubspec,
				gen.contentPubspec
			)
		}
		
		//copy icons
		gc.graphMopdels.forEach[g|{
			if(!g.iconPath.nullOrEmpty){
				FileHandler.copyFile(g,g.iconPath,basePath+"/web/"+g.iconPath(g.name,false).toString.toLowerCase,true)
			}
			g.elements.filter[hasIcon].forEach[e|FileHandler.copyFile(e,e.eclipseIconPath,basePath+"/web/"+e.iconPath(g.name,false).toString.toLowerCase,true)]			
		}]
		
		if(!gc.cpd.image128.nullOrEmpty) {
			FileHandler.copyFile(gc.cpd,gc.cpd.image128.toString,basePath+"/web/"+gc.cpd.image128.cpdImagePath,true)
		}
		if(!gc.cpd.image64.nullOrEmpty) {
			FileHandler.copyFile(gc.cpd,gc.cpd.image64.toString,basePath+"/web/"+gc.cpd.image64.cpdImagePath,true)
		}
		if(!gc.cpd.image32.nullOrEmpty) {
			FileHandler.copyFile(gc.cpd,gc.cpd.image32.toString,basePath+"/web/"+gc.cpd.image32.cpdImagePath,true)
		}
		if(!gc.cpd.image16.nullOrEmpty) {
			FileHandler.copyFile(gc.cpd,gc.cpd.image16.toString,basePath+"/web/"+gc.cpd.image16.cpdImagePath,true)
		}

		
	}
}