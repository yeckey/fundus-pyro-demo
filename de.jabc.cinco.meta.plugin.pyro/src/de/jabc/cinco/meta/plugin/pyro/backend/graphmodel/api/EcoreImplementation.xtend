  package de.jabc.cinco.meta.plugin.pyro.backend.graphmodel.api

import de.jabc.cinco.meta.plugin.pyro.util.Generatable
import de.jabc.cinco.meta.plugin.pyro.util.GeneratorCompound
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.EEnum
import org.eclipse.emf.ecore.ENamedElement
import org.eclipse.emf.ecore.EPackage

class EcoreImplementation extends Generatable {
	
	protected extension GraphModelElementHook = new GraphModelElementHook
	
	new(GeneratorCompound gc) {
		super(gc)
	}
		
	def filename(ENamedElement me)'''«me.name.fuEscapeJava»Impl.java'''
	
	def content(EClass me,EPackage g)
	{
	'''
	package «g.apiImplFQN»;
	
	import info.scce.pyro.util.PyroList;
	
	public class «me.name.fuEscapeJava»Impl implements «g.apiFQN».«me.name.fuEscapeJava» {
		
		private final de.ls5.dywa.generated.entity.«g.name.escapeJava».«me.name.fuEscapeJava» delegate;
		
	    public «me.name.fuEscapeJava»Impl(
	    		de.ls5.dywa.generated.entity.«g.name.escapeJava».«me.name.fuEscapeJava» delegate
	        ) {
	        this.delegate = delegate;
	    }
	    
	    @Override
		public boolean equals(Object obj) {
			return obj!=null && obj instanceof «g.apiFQN».«me.name.fuEscapeJava» && ((«g.apiFQN».«me.name.fuEscapeJava») obj).getDelegate().getDywaId() == this.getDelegate().getDywaId();
		}
		
		@Override
		public int hashCode() {
			return java.lang.Math.toIntExact(delegate.getDywaId());
		}
	    
		@Override
		public String getId() {
			return Long.toString(this.delegate.getDywaId());
		}
		
		@Override
		public de.ls5.dywa.generated.entity.«g.name.escapeJava».«me.name.fuEscapeJava» getDelegate() {
			return this.delegate;
		}
		
	    @Override
	    public org.eclipse.emf.ecore.EObject eContainer() {
	        return null;
	    }
		«FOR attr:me.referencesExtended»
		@Override
		public «IF attr.isList»java.util.List<«ENDIF»«g.apiFQN».«attr.ecoreType(g)»«IF attr.isList»>«ENDIF» get«attr.name.fuEscapeJava»() {
			«IF attr.list»
			return new PyroList<>(
                (add)->this.delegate.get«attr.name.escapeJava»_«attr.type.name.fuEscapeJava»().add((de.ls5.dywa.generated.entity.«g.name.escapeJava».«attr.ecoreType(g)»)add),
                (rem)->this.delegate.get«attr.name.escapeJava»_«attr.type.name.fuEscapeJava»().remove(rem),
                this.delegate.get«attr.name.escapeJava»_«attr.type.name.fuEscapeJava»().stream().map(«attr.ecoreType(g)»Impl::new).collect(java.util.stream.Collectors.toList()));
			«ELSE»
			if(this.delegate.get«attr.name.escapeJava»() == null) {
				return null;
			}
			return new «attr.ecoreType(g)»Impl(this.delegate.get«attr.name.escapeJava»());
			«ENDIF»
		}
		@Override
		public void set«attr.name.fuEscapeJava»(«IF attr.isList»java.util.List<«ENDIF»«g.apiFQN».«attr.ecoreType(g)»«IF attr.isList»>«ENDIF» value) {
			«IF attr.list»
			this.delegate.get«attr.name.escapeJava»_«attr.type.name.fuEscapeJava»().clear();
			value.forEach(n->this.delegate.get«attr.name.escapeJava»_«attr.type.name.fuEscapeJava»().add(n.getDelegate()));
			«ELSE»
			this.delegate.set«attr.name.escapeJava»(value.getDelegate());
			«ENDIF»
		}
		«ENDFOR»
		
		«FOR attr:me.attributesExtended»
		@Override
		public «IF attr.isList»java.util.List<«ENDIF»«attr.ecoreType(g)»«IF attr.isList»>«ENDIF» «IF attr.type.equals("EBoolean")»is«ELSE»get«ENDIF»«attr.name.fuEscapeJava»() {
			«IF attr.isList»
				«IF attr.type instanceof EEnum»
				return this.delegate.get«attr.name.escapeJava»().stream().map(n->{
					switch (this.delegate.get«attr.name.escapeJava»()){
						«attr.type.name.getEnum(g).ELiterals.map[
							'''case «it.name.toUnderScoreCase.escapeJava»: return «g.apiFQN».«it.eContainer.name.fuEscapeJava».«it.name.toUnderScoreCase.fuEscapeJava»;'''
						].join("\n")»
					}
				}).collect(java.util.stream.Collectors.toList());
				«ELSE»
				return this.delegate.get«attr.name.escapeJava»();
				«ENDIF»
			«ELSE»
				«IF attr.type instanceof EEnum»
					switch (this.delegate.get«attr.name.escapeJava»()){
						«attr.type.name.getEnum(g).ELiterals.map[
							'''case «it.name.toUnderScoreCase.escapeJava»: return «g.apiFQN».«it.eContainer.name.fuEscapeJava».«it.name.toUnderScoreCase.fuEscapeJava»;'''
						].join("\n")»
					}
				«ELSE»
					return this.delegate.get«attr.name.escapeJava»();
				«ENDIF»
			«ENDIF»
		}
		
		@Override
		public void set«attr.name.fuEscapeJava»(«IF attr.isList»java.util.List<«ENDIF»«attr.ecoreType(g)»«IF attr.isList»>«ENDIF» value) {
			«IF attr.isList»
				«IF attr.type instanceof EEnum»
				value.forEach(n->{
					switch (n){
						«attr.type.name.getEnum(g).ELiterals.map[
							'''case «it.name.toUnderScoreCase.escapeJava»: this.delegate.set«attr.name.escapeJava»(«g.apiFQN».«it.eContainer.name.fuEscapeJava».«it.name.toUnderScoreCase.fuEscapeJava»);break;'''
						].join("\n")»
					}
				});
				«ELSE»
				this.delegate.set«attr.name.escapeJava»(value);
				«ENDIF»
			«ELSE»
				«IF attr.type instanceof EEnum»
					switch (value){
						«attr.type.name.getEnum(g).ELiterals.map[
							'''case «it.name.toUnderScoreCase.escapeJava»: this.delegate.set«attr.name.escapeJava»(«g.apiFQN».«it.eContainer.name.fuEscapeJava».«it.name.toUnderScoreCase.fuEscapeJava»);break;'''
						].join("\n")»
					}
				«ELSE»
				this.delegate.set«attr.name.escapeJava»(value);
				«ENDIF»
			«ENDIF»
			
		}
		«ENDFOR»
	}
	'''
	}
	
	
	
	
}