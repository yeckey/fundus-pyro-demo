package de.jabc.cinco.meta.plugin.pyro.util

class PluginComponent {
	public String fetchURL = null
	public String clickURL = null
	public String dbClickURL = null
	// The text that is displayed in the tab
	public String tab = null
	// The key to assign key -> component
	public String key = null
}