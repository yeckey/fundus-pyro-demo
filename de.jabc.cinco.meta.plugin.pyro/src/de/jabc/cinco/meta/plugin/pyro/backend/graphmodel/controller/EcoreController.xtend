package de.jabc.cinco.meta.plugin.pyro.backend.graphmodel.controller

import de.jabc.cinco.meta.plugin.pyro.util.Generatable
import de.jabc.cinco.meta.plugin.pyro.util.GeneratorCompound
import org.eclipse.emf.ecore.EPackage

class EcoreController extends Generatable {
	
	new(GeneratorCompound gc) {
		super(gc)
	}
	
	def filename(EPackage p)'''«p.name.fuEscapeJava»Controller.java'''
	
	def content(EPackage p)
	'''
	package info.scce.pyro.core;
	
	import info.scce.pyro.«p.name.lowEscapeJava».rest.«p.name.fuEscapeJava»List;
	import info.scce.pyro.«p.name.lowEscapeJava».rest.«p.name.fuEscapeJava»;
	import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroUserController;
	import de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroFolder;
	import de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject;
	import info.scce.pyro.core.rest.types.CreateEcore;
	import info.scce.pyro.sync.GraphModelWebSocket;
	import info.scce.pyro.sync.ProjectWebSocket;
	import info.scce.pyro.sync.WebSocketMessage;
	
	import javax.ws.rs.core.Response;
	import java.io.IOException;
	
	@javax.transaction.Transactional
	@javax.ws.rs.Path("/«p.name.lowEscapeJava»")
	public class «p.name.fuEscapeJava»Controller implements info.scce.pyro.IPyroController {
	
		@javax.inject.Inject
		private info.scce.pyro.rest.ObjectCache objectCache;
		
		«FOR e:p.elementsAndEnums + #[p]»
		@javax.inject.Inject
		private «p.dywaControllerFQN».«e.name.fuEscapeJava»Controller «e.name.escapeJava»Controller;
		
		public «p.dywaControllerFQN».«e.name.fuEscapeJava»Controller get«e.name.escapeJava»Controller() { return «e.name.escapeJava»Controller; }
		«ENDFOR»
		@javax.inject.Inject
		private de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroFolderController folderController;
			
		@javax.inject.Inject
		private de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroProjectController projectController;
				
		@javax.inject.Inject
		private PyroUserController subjectController;
		
		@javax.inject.Inject
		private ProjectWebSocket projectWebSocket;
	
		@javax.inject.Inject
		private GraphModelWebSocket graphModelWebSocket;
	
		@javax.inject.Inject
		private GraphModelController graphModelController;
		
		@javax.ws.rs.GET
		@javax.ws.rs.Path("read/{id}/private")
		@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
		@org.jboss.resteasy.annotations.GZIP
		public Response load(@javax.ws.rs.PathParam("id") final long id) {
			
			final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject project = projectController.read(id);
			
			if(project==null){
			    return Response.status(Response.Status.BAD_REQUEST).build();
			}
						
			final java.util.Set<«p.dywaFQN».«p.name.fuEscapeJava»> list = collectProjectFiles(project);
			if (list == null) {
				return Response.status(Response.Status.BAD_REQUEST).build();
			}
			return Response.ok(«p.name.fuEscapeJava»List.fromDywaEntity(list, objectCache))
					.build();
	
		}
		
		public java.util.Set<«p.dywaFQN».«p.name.fuEscapeJava»> collectProjectFiles(de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroFolder folder)
		{
			java.util.Set<«p.dywaFQN».«p.name.fuEscapeJava»> found = new java.util.HashSet<>();
			found.addAll(
				folder.getfiles_PyroFile()
					.stream()
					.filter(n->n instanceof «p.dywaFQN».«p.name.fuEscapeJava»)
					.map(n->(«p.dywaFQN».«p.name.fuEscapeJava»)n)
					.collect(java.util.stream.Collectors.toSet())
			);
			folder.getinnerFolders_PyroFolder().forEach(f->found.addAll(collectProjectFiles(f)));
			return found;
		}
		
		@javax.ws.rs.POST
		@javax.ws.rs.Path("create/private")
		@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
		@javax.ws.rs.Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
		@org.jboss.resteasy.annotations.GZIP
		public Response createEcore(CreateEcore ecore) {
	
			final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser subject = subjectController
							.read((Long) org.apache.shiro.SecurityUtils.getSubject()
									.getPrincipal());
	
	        final PyroFolder folder = folderController.read(ecore.getparentId());
	        if(folder==null||subject==null){
	            return Response.status(Response.Status.BAD_REQUEST).build();
	        }
	
		    final de.ls5.dywa.generated.entity.«p.name.escapeJava».«p.name.fuEscapeJava» newEcore =  «p.name.escapeJava»Controller.create("«p.name.fuEscapeJava»_"+ecore.getfilename());
		    newEcore.setfilename(ecore.getfilename());
		    newEcore.setextension("ecore");
		    
	        folder.getfiles_PyroFile().add(newEcore);
	        
	        PyroProject pp = graphModelController.getProject(folder);
	        projectWebSocket.send(pp.getDywaId(), WebSocketMessage.fromDywaEntity(subject.getDywaId(), info.scce.pyro.core.rest.types.PyroProjectStructure.fromDywaEntity(pp,objectCache)));
	        
			return Response.ok(«p.name.fuEscapeJava».fromDywaEntity(newEcore,new info.scce.pyro.rest.ObjectCache())).build();
		}
		
		@javax.ws.rs.GET
		@javax.ws.rs.Path("remove/{id}/{parentId}/private")
		@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
		@org.jboss.resteasy.annotations.GZIP
		public Response removeGraphModel(@javax.ws.rs.PathParam("id") final long id,@javax.ws.rs.PathParam("parentId") final long parentId) {
			final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser subject = subjectController
					.read((Long) org.apache.shiro.SecurityUtils.getSubject()
							.getPrincipal());
			//find parent
			final de.ls5.dywa.generated.entity.«p.name.escapeJava».«p.name.fuEscapeJava» gm = «p.name.escapeJava»Controller.read(id);
			final PyroFolder parent = folderController.read(parentId);
			if(gm==null||parent==null){
				return Response.status(Response.Status.NOT_FOUND).build();
			}
			//cascade remove
			if(parent.getfiles_PyroFile().contains(gm)){
				parent.getfiles_PyroFile().remove(gm);
				
				//TODO
				
				PyroProject pp = graphModelController.getProject(parent);
				projectWebSocket.send(pp.getDywaId(), WebSocketMessage.fromDywaEntity(subject.getDywaId(), info.scce.pyro.core.rest.types.PyroProjectStructure.fromDywaEntity(pp,objectCache)));
				
				return Response.ok("OK").build();
			}
			return Response.status(Response.Status.BAD_REQUEST).build();
	
		}
		
	
	}
	
	'''
}