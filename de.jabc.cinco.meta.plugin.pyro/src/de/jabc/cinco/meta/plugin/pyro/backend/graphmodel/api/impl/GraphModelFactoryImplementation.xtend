package de.jabc.cinco.meta.plugin.pyro.backend.graphmodel.api.impl

import de.jabc.cinco.meta.plugin.pyro.util.Generatable
import de.jabc.cinco.meta.plugin.pyro.util.GeneratorCompound
import mgl.GraphModel
import de.jabc.cinco.meta.plugin.pyro.backend.graphmodel.command.GraphModelCommandExecuter
import mgl.UserDefinedType

class GraphModelFactoryImplementation extends Generatable{
	
	new(GeneratorCompound gc) {
		super(gc)
	}
	
	def filename(GraphModel g)
	'''«g.name.toCamelCase.fuEscapeJava»FactoryImpl.java'''
	
	def content(GraphModel g)
	'''
	package «g.apiImplFQN»;
	
	import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroFolderController;
	import de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroFolder;
	import de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject;
	import «g.factoryFQN».«g.name.toCamelCase.fuEscapeJava»Factory;
	import de.ls5.dywa.generated.controller.info.scce.pyro.«g.name.escapeJava».«g.name.fuEscapeJava»Controller;
	import «g.apiFQN».«g.name.fuEscapeJava»;
	import info.scce.pyro.core.command.«g.name.fuEscapeJava»CommandExecuter;
	import info.scce.pyro.core.rest.types.PyroProjectStructure;
	import info.scce.pyro.core.MainControllerBundle;
	import info.scce.pyro.rest.ObjectCache;
	import info.scce.pyro.sync.ProjectWebSocket;
	import info.scce.pyro.sync.WebSocketMessage;
	
	import java.util.Optional;
	
	/**
	 * Author zweihoff
	 */
	@javax.enterprise.context.RequestScoped
	public class «g.name.toCamelCase.fuEscapeJava»FactoryImpl implements «g.name.toCamelCase.fuEscapeJava»Factory {
	
	    private PyroProject project;
	    private ProjectWebSocket projectWebSocket;
	    private PyroFolderController folderController;
	    private de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser subject;
	    private MainControllerBundle bundle;
	    private info.scce.pyro.core.command.«g.name.escapeJava»CommandExecuter executer;
	    
	    public void warmup(PyroProject project,
	    	         de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser subject,
	    	         MainControllerBundle bundle,
	    	         info.scce.pyro.core.command.«g.name.escapeJava»CommandExecuter executer
	    	    ) {
	    	        this.project = project;
	    	        this.subject = subject;
	    	        this.bundle = bundle;
	    	        this.projectWebSocket = bundle.get«g.name.fuEscapeJava»Controller().getProjectWebSocket();
	    	        this.folderController = bundle.get«g.name.fuEscapeJava»Controller().getPyroFolderController();
	    	        this.executer = executer;
	   }
	
	    public static «g.name.toCamelCase.fuEscapeJava»Factory init() {
	    	return new «g.name.toCamelCase.fuEscapeJava»FactoryImpl();
	    }
	
	    public «g.name.fuEscapeJava» create«g.name.fuEscapeJava»(String projectRelativePath, String filename)
	    {
	        String[] folders = projectRelativePath.split("/");
	        //create all not present folders
	        PyroFolder folder = createFolders(project,0,folders);
	        return create«g.name.fuEscapeJava»(folder, filename);
	    }
	    
	    public «g.name.fuEscapeJava» create«g.name.fuEscapeJava»(PyroFolder folder, String filename) {
	    	//create graphmodel
	        final de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.fuEscapeJava».«g.name.fuEscapeJava» newGraph =  bundle.get«g.name.fuEscapeJava»Controller().get«g.name.fuEscapeJava»Controller().create("«g.name.fuEscapeJava»_"+filename);
	        newGraph.setfilename(filename);
	        newGraph.setextension("«g.fileExtension»");
	        «new GraphModelCommandExecuter(gc).setDefault('''newGraph''',g,g,true)»
	        if(executer == null) {
	        	this.executer = bundle.get«g.name.fuEscapeJava»Controller().buildExecuter(newGraph,project);
			}	        	
	        folder.getfiles_PyroFile().add(newGraph);
	        «IF g.hasPostCreateHook»
	        	«g.apiFQN».«g.name.fuEscapeJava» ce = new «g.apiImplFQN».«g.name.fuEscapeJava»Impl(newGraph,executer);
	        	«g.postCreateHook» ca = new «g.postCreateHook»();
	        	ca.init(executer);
	        	ca.postCreate(ce);
	        «ENDIF»
	        sendProjectUpdate();
	        return new «g.name.fuEscapeJava»Impl(newGraph,executer);
	    }
	
	    private PyroFolder createFolders(PyroFolder parentFolder,int index,String[] folders) {
            if((folders.length==1&&folders[0].isEmpty())||index>=folders.length){
                return parentFolder;
            }
            String newFolderName = folders[index];
            if(newFolderName.isEmpty()) {
                return createFolders(parentFolder,index+1,folders);
            }
            Optional<PyroFolder> folder = parentFolder.getinnerFolders_PyroFolder().stream().filter(n -> n.getname().equals(newFolderName)).findAny();
            if(folder.isPresent()){
                //folder is already present
                //continue
                return createFolders(folder.get(),index+1,folders);
            } else {
                //create new folder
                final PyroFolder newPF = folderController.create("PyroFolder_"+newFolderName);
                newPF.setname(newFolderName);
                parentFolder.getinnerFolders_PyroFolder().add(newPF);
                //and continue
                return createFolders(newPF,index+1,folders);
            }
        }
	
	    private void sendProjectUpdate(){
	        projectWebSocket.send(project.getDywaId(), WebSocketMessage.fromDywaEntity(-1, PyroProjectStructure.fromDywaEntity(project,new ObjectCache())));
	    }
	    
	    «FOR udt:g.elementsAndTypes.filter(UserDefinedType).filter[!isAbstract]»
	    public «g.apiFQN».«udt.name.fuEscapeJava» create«udt.name.fuEscapeJava»() {
	        return new «g.apiImplFQN».«udt.name.fuEscapeJava»Impl(
	        	executer.getBundle().«udt.name.fuEscapeJava»Controller.create("«udt.name.fuEscapeJava»"),
	        	executer,
	        	null,
	        	null);
	    }
	    «ENDFOR»
	}
	
	'''
	
}