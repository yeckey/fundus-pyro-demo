package de.jabc.cinco.meta.plugin.pyro.frontend.model

import de.jabc.cinco.meta.plugin.pyro.util.Generatable
import de.jabc.cinco.meta.plugin.pyro.util.GeneratorCompound
import java.util.Collections
import mgl.ContainingElement
import mgl.Edge
import mgl.Enumeration
import mgl.GraphModel
import mgl.GraphicalModelElement
import mgl.ModelElement
import mgl.Node
import mgl.NodeContainer
import mgl.UserDefinedType
import org.eclipse.emf.ecore.EAttribute
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.EEnum
import org.eclipse.emf.ecore.EPackage
import org.eclipse.emf.ecore.EReference
import org.eclipse.emf.ecore.EStructuralFeature
import style.NodeStyle
import style.Styles
import org.eclipse.emf.ecore.ENamedElement

class Model extends Generatable {

	new(GeneratorCompound gc) {
		super(gc)
	}

	

	def fileNameDispatcher() '''dispatcher.dart'''
	
	

	def contentDispatcher() '''
		import 'core.dart' as core;
		
		«FOR g:gc.graphMopdels»
		import '«g.name.lowEscapeDart».dart' as imp_«g.name.lowEscapeDart»;
		«ENDFOR»
		«FOR p:gc.ecores»
		import '«p.name.lowEscapeDart».dart' as imp_«p.name.lowEscapeDart»;
		«ENDFOR»
		class GraphModelDispatcher {
			
			static core.PyroElement dispatchElement(Map<String, dynamic> jsog,Map cache) {
				«FOR g:gc.graphMopdels»
					«FOR e:g.elements.filter[!isIsAbstract] + #[g]»
						    if(jsog["dywaRuntimeType"]=='info.scce.pyro.«g.name.lowEscapeJava».rest.«e.name.fuEscapeJava»'){
						    	return «e.typeName(g)».fromJSOG(jsog,cache);
						    }
				  	«ENDFOR»
				«ENDFOR»
				return dispatchEcoreElement(cache,jsog);
			}
			
			static core.PyroElement dispatchEcoreElement(Map cache,dynamic jsog) {
				«FOR g:gc.ecores»
					«FOR e:g.elements.filter[!abstract] + #[g]»
						if(jsog["dywaRuntimeType"]=='info.scce.pyro.«g.name.lowEscapeJava».rest.«e.name.fuEscapeJava»'){
							return «e.typeName(g)».fromJSOG(jsog,cache);
						}
					«ENDFOR»
				«ENDFOR»
				throw new Exception("Unkown element ${jsog['dywaRuntimeType']}");
			}
		
		  static core.PyroModelFile dispatch(Map cache,dynamic jsog){
		  	«FOR g:gc.graphMopdels»
		    if(jsog["__type"]=='«g.name.fuEscapeJava»Impl'||jsog["__type"]=='«g.name.lowEscapeDart».«g.name.fuEscapeDart»'){
		      return «g.typeName(g)».fromJSOG(jsog,cache);
		    }
		    «ENDFOR»
		    return dispatchEcorePackage(cache,jsog);
		  }
		  
		  static core.PyroModelFile dispatchEcorePackage(Map cache,dynamic jsog) {
		  	«FOR g:gc.ecores»
		  	    if(jsog["__type"]=='«g.name.fuEscapeDart»Impl'||jsog["__type"]=='«g.name.lowEscapeDart».«g.name.fuEscapeDart»'){
		  	       return «g.typeName(g)».fromJSOG(jsog,cache);
		  	    }
		  	«ENDFOR»
		  	throw new Exception("Unkown dispatching type ${jsog["__type"]}");
		  }
		  
	  	«FOR g:gc.graphMopdels»
		static core.ModelElementContainer dispatch«g.name.escapeDart»ModelElementContainer(Map cache,dynamic jsog){
	  	  	«FOR e:g.elements.filter(NodeContainer).filter[!isIsAbstract] + #[g]»
			if(jsog["__type"]=='«g.name.lowEscapeDart».«e.name.escapeDart»'){
				return «e.typeName(g)».fromJSOG(jsog,cache);
			}
  		    «ENDFOR»
  		    throw new Exception("Unkown modelelement type ${jsog["__type"]}");
	  	  }
	  	
		  static core.ModelElement dispatch«g.name.escapeDart»ModelElement(Map cache,dynamic jsog){
		  	«FOR e:g.elements.filter[!isIsAbstract]»
		    if(jsog["__type"]=='«g.name.lowEscapeDart».«e.name.escapeDart»'){
  		      return «e.typeName(g)».fromJSOG(jsog,cache);
  		    }
  		    «ENDFOR»
  		    throw new Exception("Unkown modelelement type ${jsog["__type"]}");
  		  }
	    «ENDFOR»
		}
	'''

	def fileNameGraphModel(String graphModelName) '''«graphModelName.lowEscapeDart».dart'''
	def fileNameEcore(String packageName) '''«packageName.lowEscapeDart».dart'''

	def pyroElementConstr(ModelElement element, GraphModel g,Styles styles) '''
		if (cache == null) {
		   cache = new Map();
		}
			
		// default constructor
		if (jsog == null) {
		  this.dywaId = -1;
		  this.dywaVersion = -1;
		  this.dywaName = null;
		  «IF element instanceof Edge»
		  this.bendingPoints = new List();
		  «ENDIF»
		  «IF element instanceof ContainingElement»
		  this.modelElements = new List();
		  «ENDIF»
		  «IF element instanceof Node»
		  «{
		  	 val nodeStyle = styling(element,styles) as NodeStyle
		  	 val size = nodeStyle.mainShape.size
		  	 '''
		  	 «IF size!=null»
		  	 width = «size.width»;
		  	 height = «size.height»;
		  	 «ENDIF»
		  	 '''
		  }»
	  	  this.incoming = new List();
	  	  this.outgoing = new List();
	  	  this.x = 0;
	  	  this.y = 0;
  		  «ENDIF»
  		  «IF element instanceof GraphModel»
«««  		  this.commandGraph = new «g.name.fuEscapeDart»CommandGraph(this);
  		  this.width = 1000;
  		  this.height = 600;
  		  this.scale = 1.0;
  		  this.router = null;
  		  this.connector = 'normal';
  		  this.filename = '';
  		  this.extension = '«element.fileExtension»';
  		  «ENDIF»
		  // properties
			«FOR attr : element.attributesExtended»
				«attr.name.escapeDart» =
				«IF attr.list»
					new List();
				«ELSE»
					«attr.init(g,"")»;
				«ENDIF»
			«ENDFOR»
		}
		// from jsog
		else {
		      String jsogId = jsog['@id'];
		      cache[jsogId] = this;
		
		      this.dywaId = jsog['dywaId'];
		      this.dywaVersion = jsog['dywaVersion'];
		      this.dywaName = jsog['dywaName'];
		      «IF element instanceof GraphicalModelElement»
		      if(jsog.containsKey('a_container')){
		      	if(jsog['a_container']!=null){
			      	if(jsog['a_container'].containsKey('@ref')){
			      		this.container = cache[jsog['a_container']['@ref']];
			      	} else {
			      		this.container = GraphModelDispatcher.dispatch«g.name.escapeDart»ModelElementContainer(cache,jsog['a_container']);
			      	}
		      	}
		      }
		      «ENDIF»
		      «IF element instanceof Edge»
		      this.bendingPoints = new List();
		      if(jsog.containsKey('a_bendingPoints')){
		      	if(jsog['a_bendingPoints']!=null){
		      		this.bendingPoints = jsog['a_bendingPoints'].map((n)=>new core.BendingPoint(jsog:n)).toList().cast<core.BendingPoint>();
		      	}
		      }
		      if(jsog.containsKey('a_sourceElement')){
		      	if(jsog['a_sourceElement']!=null){
		      		Map<String, dynamic> j = jsog['a_sourceElement'];
	  		      	if(j.containsKey('@ref')){
	  		      		this.source = cache[j['@ref']];
	  		      	} else {
	  		      		this.source = GraphModelDispatcher.dispatch«g.name.escapeDart»ModelElement(cache,j);
	  		      	}
  		      	}
  		      }
  		      if(jsog.containsKey('a_targetElement')){
  		      	if(jsog['a_targetElement']!=null){
  		      		Map<String,dynamic> j = jsog['a_targetElement'];
			      	if(j.containsKey('@ref')){
			      		this.target = cache[j['@ref']];
			      	} else {
			      		this.target = GraphModelDispatcher.dispatch«g.name.escapeDart»ModelElement(cache,j);
			      	}
		        }	
		      }
	      «ENDIF»
	      «IF element instanceof GraphModel»
«««		      this.commandGraph = new «g.name.fuEscapeDart»CommandGraph(this,jsog:jsog['commandGraph']);
		  this.width = jsog['a_width'];
		  this.height = jsog['a_height'];
		  this.scale = jsog['a_scale'];
		  this.router = jsog['a_router'];
		  this.connector = jsog['a_connector'];
		  this.filename = jsog['filename'];
		  this.extension = jsog['extension'];
		  this.isPublic = jsog['isPublic'];
			this.modelElements = new List();
			if (jsog.containsKey("a_modelElements")) {
				if(jsog["a_modelElements"]!=null){
					for(var v in jsog["a_modelElements"]){
						if(v.containsKey("@ref")){
							this.modelElements.add(cache[v['@ref']]);
						} else {
						  this.modelElements.add(GraphModelDispatcher.dispatch«g.name.escapeDart»ModelElement(cache,v));
						}
					}
				}
			}
		  «ENDIF»
    		«IF element instanceof Node»
			width = jsog["a_width"];
			height = jsog["a_height"];
			x = jsog["a_x"];
			y = jsog["a_y"];
			angle = jsog["a_angle"];
			
			this.incoming = new List();
			if (jsog.containsKey("a_incoming")) {
				if(jsog["a_incoming"]!=null){
					for(var v in jsog["a_incoming"]){
						if(v.containsKey("@ref")){
							this.incoming.add(cache[v['@ref']]);
						} else {
						  this.incoming.add(GraphModelDispatcher.dispatch«g.name.escapeDart»ModelElement(cache,v));
						}
					}
				}
			}
			«IF element instanceof ContainingElement»
			this.modelElements = new List();
			if (jsog.containsKey("a_modelElements")) {
				if(jsog["a_modelElements"]!=null){
					for(var v in jsog["a_modelElements"]){
						if(v.containsKey("@ref")){
							this.modelElements.add(cache[v['@ref']]);
						} else {
						  this.modelElements.add(GraphModelDispatcher.dispatch«g.name.escapeDart»ModelElement(cache,v));
						}
					}
				}
			}
			«ENDIF»
			this.outgoing = new List();
			if (jsog.containsKey("a_outgoing")) {
				if(jsog["a_outgoing"]!=null) {
					for(var v in jsog["a_outgoing"]){
						if(v.containsKey("@ref")){
							this.outgoing.add(cache[v['@ref']]);
						} else {
						  this.outgoing.add(GraphModelDispatcher.dispatch«g.name.escapeDart»ModelElement(cache,v));
						}
					}
				}
			}
				«IF element.isPrime»
				if(jsog.containsKey("b_«element.primeReference.name.escapeDart»")){
					if(jsog["b_«element.primeReference.name.escapeDart»"]!=null){
						if(jsog["b_«element.primeReference.name.escapeDart»"].containsKey("@ref")){
							this.«element.primeReference.name.escapeDart» = cache[jsog["b_«element.primeReference.name.escapeDart»"]["@ref"]];
						} else {
							this.«element.primeReference.name.escapeDart» =
							«IF element.primeReference.type instanceof GraphModel»
							GraphModelDispatcher.dispatch«element.primeReference.type.name.escapeDart»ModelElementContainer(cache,jsog["b_«element.primeReference.name.escapeDart»"]);
							«ELSEIF element.primeReference.type.graphModel instanceof GraphModel»
							GraphModelDispatcher.dispatch«element.primeReference.type.graphModel.name.escapeDart»ModelElement(cache,jsog["b_«element.primeReference.name.escapeDart»"]);
							«ELSE»
							GraphModelDispatcher.dispatchEcoreElement(cache,jsog["b_«element.primeReference.name.escapeDart»"]);
							«ENDIF»
						}
					}
				}
				«ENDIF»
	  		«ENDIF»  
			
			
			
		      // properties
		      «FOR attr : element.attributesExtended.sortBy[name]»
		    «IF attr.list»
		    this.«attr.name.escapeDart» = new List();
		    «ENDIF»
			if (jsog.containsKey("c_«attr.name.escapeJava»")) {
				«IF attr.list»
				for(dynamic jsogObj in jsog["c_«attr.name.escapeJava»"]) {
				«ELSE»
				dynamic jsogObj = jsog["c_«attr.name.escapeJava»"];
				«ENDIF»
				 if (jsogObj != null) {
			«IF attr.isPrimitive(g)»
					«attr.primitiveDartType(g)» value«attr.name.escapeDart»;
					if (jsogObj != null) {
						«IF attr.type.getEnum(g)!==null»
						value«attr.name.escapeDart» = «attr.type.fuEscapeDart»Parser.fromJSOG(jsogObj);
						«ELSE»
					    value«attr.name.escapeDart» = jsogObj«attr.deserialize(g)»;
					    «ENDIF»
					}
					«IF attr.list»
					this.«attr.name.escapeDart».add(value«attr.name.escapeDart»);
					«ELSE»
					this.«attr.name.escapeDart» = value«attr.name.escapeDart»;
					«ENDIF»
			«ELSE»
				«attr.complexDartType.fuEscapeDart» value«attr.name.escapeDart»;
				String jsogId;
				if (jsogObj.containsKey('@ref')) {
				  jsogId = jsogObj['@ref'];
				}
				else {
				  jsogId = jsogObj['@id'];
				}
				if (cache.containsKey(jsogId)) {
				  value«attr.name.escapeDart» = cache[jsogId];
				}
				else {
				  if (jsogObj != null) {
				  «FOR subType:attr.type.subTypesAndType(g).filter(ModelElement).filter[!isAbstract]»
				  	if (jsogObj['__type'] ==
				  	"«g.name.lowEscapeDart».«subType.name.fuEscapeDart»") {
				  	    value«attr.name.escapeDart» =
				  	    new «subType.name.fuEscapeDart»(cache: cache, jsog: jsogObj);
				  	}
				  «ENDFOR»
				  }
				}
				«IF attr.list»
				this.«attr.name.escapeDart».add(value«attr.name.escapeDart»);
				«ELSE»
				this.«attr.name.escapeDart» = value«attr.name.escapeDart»;
				«ENDIF»
			«ENDIF»
			}
			}
			«IF attr.list»}«ENDIF»
			«ENDFOR»
		}
		
	'''

	def pyroElementFromJSOG(ModelElement element, GraphModel g) '''
		Map toJSOG(Map cache) {
		    Map map = new Map();
		    if(cache.containsKey(dywaId)){
		      map['@ref']=cache[dywaId];
		      return map;
		    }
		    else{
		   	  cache[dywaId]=(cache.length+1).toString();
		      map['@id']=cache[dywaId];
		      map['dywaRuntimeType']="info.scce.pyro.«g.name.lowEscapeJava».rest.«element.name.fuEscapeJava»";
		      map['dywaId']=dywaId;
		      map['dywaVersion']=dywaVersion;
		      map['dywaName']=dywaName;
		      «IF element instanceof GraphModel»
      		 map['width'] = this.width;
      		 map['height'] = this.height;
      		 map['scale'] = this.scale;
      		 map['router'] = this.router;
      		 map['connector'] = this.connector;
      		 map['filename'] = this.filename;
      		 map['extension'] = this.extension;
      		  «ENDIF»
		      cache[dywaId]=map;
		      «FOR attr : element.attributesExtended»
		      	map['«attr.name.escapeJava»']=«attr.name.escapeDart»==null?null:
		      	 «IF attr.isList»
		      	 	«attr.name.escapeDart».map((n)=>«attr.serialize(g,'''n''')»).toList()
		      	 «ELSE»
		      	 	«attr.serialize(g,'''«attr.name.escapeDart»''')»
		      	 «ENDIF»;
		      «ENDFOR»
		    }
		
		    return map;
		}
		
		@override
		String $type()=>"«g.name.lowEscapeDart».«element.name.fuEscapeDart»";
		
		«IF element instanceof GraphModel»
		@override
		String $lower_type()=>"«g.name.lowEscapeDart»";
		«ENDIF»
		
		static «element.name» fromJSOG(dynamic jsog, Map cache)
		{
			return new «element.name»(jsog: jsog,cache: cache);
		}
		
		@override
		«element.name» propertyCopy({bool root:true}) {
			var elem = new «element.name»();
			elem.dywaId = this.dywaId;
			elem.dywaVersion = this.dywaVersion;
			elem.dywaName = this.dywaName;
			if(!root) {
				return elem;
			}
			«FOR attr:element.attributesExtended»
				«IF !attr.isPrimitive(g)»if(this.«attr.name.escapeDart»!=null) {«ENDIF»
				«IF attr.list»
					elem.«attr.name.escapeDart» = this.«attr.name.escapeDart».map((n){
						«FOR sub:attr.type.subTypesAndType(g).filter(ModelElement).filter[!isAbstract] SEPARATOR " else "»
						if(n is «sub.name.fuEscapeDart») {
							return (n as «sub.name.fuEscapeDart»).propertyCopy(root:false);
						}
						«ENDFOR»
						return null;
					});
				«ELSE»
					«FOR sub:attr.type.subTypesAndType(g).filter(ModelElement).filter[!isAbstract] SEPARATOR " else "»
					if(this.«attr.name.escapeDart» is «sub.name.escapeDart») {
						elem.«attr.name.escapeDart» = (this.«attr.name.escapeDart» as «sub.name.escapeDart»).propertyCopy(root:false);
					}
					«ENDFOR»
				«ENDIF»
				
				
				«IF !attr.isPrimitive(g)»}«ENDIF»
			«ENDFOR»
			return elem;
		}
		
		@override
		void merge(covariant «element.name» elem,{bool structureOnly:false,covariant Map<int,core.PyroElement> cache}) {
			if(cache==null){
			   cache = new Map();
			}
			cache[this.dywaId]=this;
			dywaVersion = elem.dywaVersion;
			«IF element instanceof GraphModel»
			if(!structureOnly) {
			  filename = elem.filename;
			  extension = elem.extension;
			  scale = elem.scale;
			  connector = elem.connector;
			  router = elem.router;
			  height = elem.height;
			  width = elem.width;
			}
			«ENDIF»
			«IF element instanceof ContainingElement»
			  if(!structureOnly) {
			       //remove missing
			       modelElements.removeWhere((m)=>elem.modelElements.where((e)=>m.dywaId==e.dywaId).isEmpty);
			      //merge known
			       for(var m in modelElements){
			       	 if(!cache.containsKey(m.dywaId)){
			       	    m.merge(elem.modelElements.firstWhere((e)=>m.dywaId==e.dywaId),cache:cache);
			       	 }
			       }
			       //add new
			       modelElements.addAll(elem.modelElements.where((e)=>modelElements.where((m)=>m.dywaId==e.dywaId).isEmpty).toList());
			   	}
			«ENDIF»
			«IF element instanceof GraphicalModelElement»
			if(!structureOnly){
				if(container != null){
			      if(cache.containsKey(container.dywaId)){
			        container = cache[container.dywaId];
			      } else {
			        container.merge(elem.container,cache: cache,structureOnly: structureOnly);
			      }
			     } else {
			     	container = elem.container;
			     }
			}
			«ENDIF»
			«IF element instanceof Node»
				
				if(!structureOnly) {
					height = elem.height;
					width = elem.width;
					x = elem.x;
					y = elem.y;
					angle = elem.angle;
					//remove missing
				 	    incoming.removeWhere((m)=>elem.incoming.where((e)=>m.dywaId==e.dywaId).isEmpty);
				 	    //merge known
				 	    for(var m in incoming){
				 	    	if(!cache.containsKey(m.dywaId)){
				 	    		m.merge(elem.incoming.firstWhere((e)=>m.dywaId==e.dywaId),cache:cache);
				 	    	}
				 	    }
				 	    //add new
				 	    incoming.addAll(elem.incoming.where((e)=>incoming.where((m)=>m.dywaId==e.dywaId).isEmpty).toList());
				 	    //remove missing
				 	    outgoing.removeWhere((m)=>elem.outgoing.where((e)=>m.dywaId==e.dywaId).isEmpty);
				 	    //merge known
				 	    for(var m in outgoing){
				 	    	if(!cache.containsKey(m.dywaId)){
				 	    		m.merge(elem.outgoing.firstWhere((e)=>m.dywaId==e.dywaId),cache:cache);
				 	    	}
				 	    }
				 	    //add new
				 	    outgoing.addAll(elem.outgoing.where((e)=>outgoing.where((m)=>m.dywaId==e.dywaId).isEmpty).toList());
				}
				«IF element.prime»
				if(!structureOnly){
					if(«element.primeReference.name.escapeDart» != null){
						if(cache.containsKey(«element.primeReference.name.escapeDart».dywaId)){
						    «element.primeReference.name.escapeDart» = cache[«element.primeReference.name.escapeDart».dywaId];
						} else {
						    «element.primeReference.name.escapeDart».merge(elem.«element.primeReference.name.escapeDart»,cache: cache,structureOnly: structureOnly);
						}
					} else {
						«element.primeReference.name.escapeDart» = elem.«element.primeReference.name.escapeDart»;
					}
				}
				«ENDIF»
			«ENDIF»
			«IF element instanceof Edge»
			if(!structureOnly) {
				bendingPoints = elem.bendingPoints;
				if(target!=null){
					if(!cache.containsKey(target.dywaId)){
						target.merge(elem.target,cache:cache);
					}
				} else {
					target = elem.target;
				}
				if(source != null){
					if(!cache.containsKey(source.dywaId)){
						source.merge(elem.source,cache:cache);
					}
				} else {
					source = elem.source;
				}
			}
			«ENDIF»
			«FOR attr : element.attributesExtended»
				«IF !attr.list && g.elementsAndTypes.map[name].exists[equals(attr.type)]»
				if(«attr.name.escapeDart»!=null&&elem.«attr.name.escapeDart»!=null){
					if(!cache.containsKey(«attr.name.escapeDart».dywaId)){
						«attr.name.escapeDart».merge(elem.«attr.name.escapeDart»,cache:cache,structureOnly:structureOnly);
					} else{
						«attr.name.escapeDart» = cache[«attr.name.escapeDart».dywaId];
					}
				} else {
					«attr.name.escapeDart» = elem.«attr.name.escapeDart»;
				}
				«ELSEIF !attr.list && g.elementsAndTypes.map[name].exists[equals(attr.type)]»
				//remove missing
				«attr.name.escapeDart».removeWhere((m)=>elem.«attr.name.escapeDart».where((e)=>m.dywaId==e.dywaId).isEmpty);
			    //merge known
			    for(var m in «attr.name.escapeDart»){
			    	if(!cache.containsKey(m.dywaId)){
			    		m.merge(elem.«attr.name.escapeDart».firstWhere((e)=>m.dywaId==e.dywaId),cache:cache);
			    	}
			    }
			    //add new
			    elem.«attr.name.escapeDart».where((e)=>«attr.name.escapeDart».where((m)=>m.dywaId==e.dywaId).isEmpty).forEach((e)=>«attr.name.escapeDart».add(e));
				«ELSEIF attr.list && g.elementsAndTypes.map[name].exists[equals(attr.type)]»
				elem.«attr.name.escapeDart».asMap().forEach((idx,b){
					if(!«attr.name.escapeDart».any((a)=>a.dywaId==b.dywaId)) {
						if(«attr.name.escapeDart».length>idx && «attr.name.escapeDart»[idx].dywaId==-1) {
							«attr.name.escapeDart»[idx].dywaId = b.dywaId;
							«attr.name.escapeDart»[idx].merge(b,cache:cache,structureOnly:structureOnly);
						} else {
							if(idx>=«attr.name.escapeDart».length) {
								«attr.name.escapeDart».add(b);
							} else {
								«attr.name.escapeDart».insert(idx,b);								
							}
						}
					}
				});
				«attr.name.escapeDart».removeWhere((a) => !elem.«attr.name.escapeDart».any((b)=>b.dywaId==a.dywaId));
				«attr.name.escapeDart».forEach((a) =>
					a.merge(
						elem.«attr.name.escapeDart».where((b)=>b.dywaId==a.dywaId).first,
						structureOnly:false,
						cache:cache)
				);
				«ELSE»
				«attr.name.escapeDart» = elem.«attr.name.escapeDart»;
				«ENDIF»
			«ENDFOR»
		}
		
		«IF (element instanceof Node || element instanceof Edge) && !element.isIsAbstract»
		@override
		js.JsArray styleArgs() {
			try {
		    	return new js.JsArray.from([«element.styleArgs.map[n|'''"«n»"'''].join(",")»]);
		    } catch(e) {
		    	return new js.JsArray.from([«element.styleArgs.map[n|'''"invalid"'''].join(",")»]);
		    }
		}
		«ENDIF»
		
	'''
	
	def getStyleArgs(ModelElement element){
		val values = element.annotations.findFirst[name.equals("style")].value
		if(values.size>1){
			return values.subList(1,values.size).map[replaceEscapeDart];
		}
		return Collections.EMPTY_LIST
	}

	def pyroElementAttributeDeclaration(ModelElement element, GraphModel g) '''
		 «IF element instanceof GraphModel»
			«g.name.fuEscapeDart»CommandGraph commandGraph;
			int width;
			int height;
			double scale;
			String router;
			String connector;
	    «ENDIF»
	    «IF element instanceof Node»
	    	«IF element.prime»
	    	«{
	    		val refElem = element.primeReference.type
	    		val refGraph = refElem.graphModel
	    		'''«IF !refGraph.equals(g)»imp_«refGraph.name.lowEscapeDart».«ENDIF»«refElem.name.fuEscapeDart» «element.primeReference.name.escapeDart»;'''
	    	}»
	    	«ENDIF»
	    «ENDIF»
		«FOR attr : element.attributesExtended.filter[isPrimitive(g)]»
			«IF attr.list»List<«ENDIF»«attr.primitiveDartType(g)»«IF attr.list»>«ENDIF» «attr.name.escapeDart»;
		«ENDFOR»
		«FOR attr : element.attributesExtended.filter[!isPrimitive(g)]»
			«IF attr.list»List<«ENDIF»«attr.complexDartType.fuEscapeDart»«IF attr.list»>«ENDIF» «attr.name.escapeDart»;
		«ENDFOR»
	'''

	def pyroElementClass(ModelElement element, GraphModel g,Styles styles) '''
		«IF element.isIsAbstract»abstract «ENDIF»class «element.name.fuEscapeDart» extends «element.extending("core.")»«IF element.hasToExtendContainer» implements core.Container«ENDIF»
		{
		  «element.pyroElementAttributeDeclaration(g)»
		  «IF !element.isIsAbstract»
			  «element.name.fuEscapeDart»({Map cache, dynamic jsog}) {
			    «element.pyroElementConstr(g,styles)»
			  }
			  
			  «element.pyroElementFromJSOG(g)»
		  «ENDIF»
		  @override
		  String $displayName() =>"«element.displayName»";
		  «IF element instanceof GraphicalModelElement»
		  	@override
		  	String $information() {
				«IF element.information»
				return "${«element.informationAttribute.name.escapeDart»}";
				«ELSE»
				return null;
				«ENDIF»
			}
			
			@override
			String $label() {
			«IF element instanceof Node && (element as Node).directlyEditable»
				return "${«(element as Node).directlyEditableAttribute.name.escapeDart»}";
			«ELSE»
				return null;
			«ENDIF»
			}
			
			
			
		  @override
		  core.ModelElementContainer get container => super.container as «element.getBestContainerSuperTypeNameDart»;
		  «ENDIF»
		«IF !(element instanceof UserDefinedType)»
			«IF element.canContain»
				@override
				List<core.IdentifiableElement> allElements() {
				  List<core.IdentifiableElement> list = new List();
				  list.add(this);
				  list.addAll(modelElements.expand((n) => n.allElements()));
				  return list;
				}
			«ELSE»
				@override
				List<core.IdentifiableElement> allElements() {
					return [this];
				}
			«ENDIF»
			«IF element.hasToExtendContainer»
			    List<core.ModelElement> modelElements;
			«ENDIF»
		«ENDIF»
		}
	'''
	
	def pyroElementClass(EClass element, EPackage g) '''
		«IF element.abstract»abstract «ENDIF»class «element.name.fuEscapeDart» extends «element.extending()»
		{
			//attributes
			«FOR attr : element.eContents.filter(EAttribute)»
		 		«IF attr.list»List<«ENDIF»«attr.primitiveDartType(g)»«IF attr.list»>«ENDIF» «attr.name.escapeDart»«IF attr.list» = new List()«ENDIF»;
		 	«ENDFOR»
		 	«FOR attr : element.eContents.filter(EReference)»
		 		«IF attr.list»List<«ENDIF»«attr.EType.scopedTypeName(g)»«IF attr.list»>«ENDIF» «attr.name.escapeDart»«IF attr.list» = new List()«ENDIF»;
		 	«ENDFOR»
		 	
		  «IF !element.abstract»
		  «element.name.fuEscapeDart»({Map cache, dynamic jsog}) {
		    if (cache == null) {
		    	cache = new Map();
		    }
		    // default constructor
		    if (jsog == null) {
		    	this.dywaId = -1;
		    	this.dywaVersion = -1;
		    	this.dywaName = null;
		    	«FOR attr : element.eContents.filter(EStructuralFeature)»
		    		«attr.name.escapeDart» =
		    		«IF attr.list»
		    			new List();
		    		«ELSE»
		    			«attr.init»;
		    		«ENDIF»
		    	«ENDFOR»
		    }
		    // from jsog
		    else {
		    	String jsogId = jsog['@id'];
		    	cache[jsogId] = this;
		    		
		    	this.dywaId = jsog['dywaId'];
		    	this.dywaVersion = jsog['dywaVersion'];
		    	this.dywaName = jsog['dywaName'];
		    	// properties
		    	«FOR attr : element.eContents.filter(EStructuralFeature).sortBy[name]»
		    		if (jsog.containsKey("«attr.name.escapeDart»")) {
		    			«IF attr.list»
		    				«{
		    					'''
								«IF attr.isPrimitive»
								for(var value«attr.name.escapeDart» in jsog["«attr.name.escapeDart»"]) {
										«IF attr.EType.name.getEnum(g)!==null»
											this.«attr.name.escapeDart».add(«attr.EType.scopedTypeName(g)»Parser.fromJSOG(value«attr.name.escapeDart»));
										«ELSE»
											this.«attr.name.escapeDart».add(value«attr.name.escapeDart»«attr.deserialize(g)»);
										«ENDIF»
								}
								«ELSE»
								for(var value«attr.name.escapeDart» in jsog["«attr.name.escapeDart»"]) {
										if(value«attr.name.escapeDart».containsKey("@ref")) {
											this.«attr.name.escapeDart».add(cache[value«attr.name.escapeDart»["@ref"]]);
										} else {
										«FOR subType:attr.EType.name.subTypesAndType(g).filter(EClass)»
											if (value«attr.name.escapeDart»['__type'] == "«subType.EPackage.name.lowEscapeDart».«subType.name.fuEscapeDart»") {
												this.«attr.name.escapeDart».add(new «subType.scopedTypeName(subType.EPackage)»(cache: cache, jsog: value«attr.name.escapeDart»));
											}
										«ENDFOR»
										}
								}
								«ENDIF»
		    					'''
		    				}»
		    			«ELSE»
						«{
		    				'''
								if(jsog["«attr.name.escapeDart»"] != null) {
									«IF attr.isPrimitive»
										«IF attr.EType.name.getEnum(g)!==null»
											this.«attr.name.escapeDart» = «attr.EType.scopedTypeName(g)»Parser.fromJSOG(jsog["«attr.name.escapeDart»"]);
										«ELSE»
											this.«attr.name.escapeDart» = jsog["«attr.name.escapeDart»"]«attr.deserialize(g)»;
										«ENDIF»
										
									«ELSE»
									if(jsog["«attr.name.escapeDart»"].containsKey("@ref")) {
										this.«attr.name.escapeDart» = cache[jsog["«attr.name.escapeDart»"]["@ref"]];
									} else {
										«FOR subType:attr.EType.name.subTypesAndType(g).filter(EClass)»
										if (jsog["«attr.name.escapeDart»"]['__type'] == "«subType.EPackage.name.lowEscapeDart».«subType.name.fuEscapeDart»") {
											this.«attr.name.escapeDart» = new «subType.scopedTypeName(subType.EPackage)»(cache: cache, jsog: jsog["«attr.name.escapeDart»"]);
										}
										«ENDFOR»
									}
									«ENDIF»
								}
		    				'''
		    			}»
		    			«ENDIF»
		    		}
		    	«ENDFOR»
		   	}
		  }
		  
		  
		  
		  Map toJSOG(Map cache) {
		 	Map map = new Map();
		 	if(cache.containsKey(dywaId)){
		 		map['@ref']=cache[dywaId];
		 		return map;
		 	}
		 	else{
		 		cache[dywaId]=(cache.length+1).toString();
		 		map['@id']=cache[dywaId];
		 		map['dywaRuntimeType']="info.scce.pyro.«g.name.lowEscapeJava».rest.«element.name.fuEscapeJava»";
		 		map['dywaId']=dywaId;
		 		map['dywaVersion']=dywaVersion;
		 		map['dywaName']=dywaName;
		 		cache[dywaId]=map;
		 		«FOR attr : element.eContents.filter(EStructuralFeature)»
		 		map['«attr.name.escapeDart»']=«attr.name.escapeDart»==null?null:
		 		   «IF attr.isList»
		 		     «attr.name.escapeDart».map((n)=>«attr.serialize(g,'''n''')»).toList()
		 		   «ELSE»
		 		     «attr.serialize(g,'''«attr.name.escapeDart»''')»
		 		   «ENDIF»;
		 		 «ENDFOR»
		 	}
		 	return map;
		 }
		 		
		 @override
		 String $type()=>"«g.name.lowEscapeDart».«element.name.fuEscapeDart»";
		 
		 @override
		 String $displayName() =>"«element.name.fuEscapeDart»";
		 		
		 static «element.name» fromJSOG(dynamic jsog,Map cache) => new «element.name»(jsog: jsog,cache: cache);
		 		
		 @override
		 void merge(core.PyroElement ie,{bool structureOnly:false,Map cache}) {
		 	var elem = ie as «element.name»;
		 	if(cache==null){
		 		cache = new Map();
		 	}
		 	cache[this.dywaId]=this;
		 	dywaVersion = elem.dywaVersion;
			«FOR attr : element.eContents.filter(EStructuralFeature)»
				«attr.name.escapeDart» = elem.«attr.name.escapeDart»;
			«ENDFOR»
		  }
		  «ENDIF»
		 
		}
	'''

	def contentGraphmodel(GraphModel g,Styles styles) '''
		import 'core.dart' as core;
		import 'dispatcher.dart';
		import 'dart:js' as js;
		
		import 'package:«gc.projectName.escapeDart»/src/pages/editor/canvas/graphs/«g.name.lowEscapeDart»/«g.name.lowEscapeDart»_command_graph.dart';
		«FOR pr:g.primeReferencedGraphModels.filter[!equals(g)]»
		//prime referenced graphmodel «pr.name»
		import '«pr.name.lowEscapeDart».dart' as imp_«pr.name.lowEscapeDart»;
		«ENDFOR»
		«FOR pr:gc.ecores»
		//prime referenced ecore «pr.name»
		import '«pr.name.lowEscapeDart».dart' as imp_«pr.name.lowEscapeDart»;
		«ENDFOR»
		
		//Graphmodel
		«g.pyroElementClass(g,styles)»
		
		//Nodes, Edges, Container and UserDefinedTypes
		«g.elementsAndTypes.map[pyroElementClass(g,styles)].join("\n")»
		
		//Enumerations
		«FOR enu : g.types.filter(Enumeration)»
			enum «enu.name.fuEscapeDart» {
			  «FOR lit:enu.literals SEPARATOR ","»«lit.escapeDart»«ENDFOR»
			}
			
			class «enu.name.fuEscapeDart»Parser {
			  static «enu.name.fuEscapeDart» fromJSOG(dynamic s){
			    switch(s['dywaName']) {
			  «FOR lit:enu.literals»
			  	case '«lit.toUnderScoreCase.escapeJava»':return «enu.name.fuEscapeDart».«lit.escapeDart»;
			  «ENDFOR»
			  }
			  return «enu.name.fuEscapeDart».«enu.literals.get(0).escapeDart»;
			  }
			
			  static Map toJSOG(«enu.name.fuEscapeDart» e)
			  {
			    Map map = new Map();
			    switch(e) {
			    «FOR lit:enu.literals»
			       case «enu.name.fuEscapeDart».«lit.escapeDart»:map['dywaName']='«lit.toUnderScoreCase.escapeJava»';break;
			    «ENDFOR»
			    }
			    return map;
			  }
			}
		«ENDFOR»
	'''
	
	def contentEcore(EPackage g) '''
		import 'core.dart' as core;
		import 'dispatcher.dart';
		
		//Ecore package class
		
		class «g.name.fuEscapeDart» extends core.PyroModelFile implements core.PyroElement {
			«FOR c:g.EClassifiers.filter(EClass)»
			List<«c.scopedTypeName(g)»> «c.name.lowEscapeDart»s = new List();
			«ENDFOR»
			
			static «g.name.fuEscapeDart» fromJSOG(dynamic jsog, Map cache) {
				var entity = new «g.name.fuEscapeDart»();
				if (cache == null) {
				   	cache = new Map();
				}
				// default constructor
				if (jsog == null) {
					entity.dywaId = -1;
					entity.dywaVersion = -1;
					entity.dywaName = null;
					entity.filename = "";
					entity.extension = "ecore";
				}
				// from jsog
				else {
				  	String jsogId = jsog['@id'];
				   	cache[jsogId] = entity;
					entity.dywaId = jsog['dywaId'];
					entity.dywaVersion = jsog['dywaVersion'];
					entity.dywaName = jsog['dywaName'];
					entity.filename = jsog['filename'];
					entity.extension = jsog['extension'];
					// properties
					«FOR c:g.EClassifiers.filter(EClass).sortBy[name]»
					if(jsog.containsKey('«c.name.lowEscapeJava»')) {
						for(var entry in jsog['«c.name.lowEscapeJava»']) {
							entity.«c.name.lowEscapeDart»s.add(«c.scopedTypeName(g)».fromJSOG(entry,cache));
						}
					}
					«ENDFOR»
				}
				return entity;
			}
			
			@override
			String $type() => "«g.name.lowEscapeDart».«g.name.fuEscapeDart»";
			
			@override
			void merge(core.PyroElement ie, {bool structureOnly: false, Map cache}) { }
			
			@override
			void mergeStructure(core.PyroFile pf) {
			    this.filename = pf.filename;
				this.extension = pf.extension;
			}
			
			@override
			Map toJSOG(Map cache) {
			    return null;
			}
		}
		
		//Ecore classes of «g.name» package
		«g.EClassifiers.filter(EClass).map[pyroElementClass(g)].join("\n")»
		
		//Enumerations
		«FOR enu : g.EClassifiers.filter(EEnum)»
			enum «enu.name.fuEscapeDart» {
			  «FOR lit:enu.ELiterals SEPARATOR ","»«lit.name.escapeDart»«ENDFOR»
			}
			
			class «enu.name.fuEscapeDart»Parser {
			  static «enu.name.fuEscapeDart» fromJSOG(dynamic s){
			    switch(s['dywaName']) {
			  «FOR lit:enu.ELiterals»
			  	case '«lit.name.escapeJava»':return «enu.name.fuEscapeDart».«lit.name.escapeDart»;
			  «ENDFOR»
			  }
			  return «enu.name.fuEscapeDart».«enu.ELiterals.get(0).name.escapeDart»;
			  }
			
			  static Map toJSOG(«enu.name.fuEscapeDart» e)
			  {
			    Map map = new Map();
			    switch(e) {
			    «FOR lit:enu.ELiterals»
			       case «enu.name.fuEscapeDart».«lit.name.escapeDart»:map['dywaName']='«lit.name.escapeJava»';break;
			    «ENDFOR»
			    }
			    return map;
			  }
			}
		«ENDFOR»
	'''
}
