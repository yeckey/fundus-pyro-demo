package de.jabc.cinco.meta.plugin.pyro

import de.jabc.cinco.meta.util.xapi.WorkspaceExtension
import java.io.IOException
import java.net.URISyntaxException
import java.util.Set
import mgl.GraphModel
import org.eclipse.core.resources.IFolder
import org.eclipse.core.resources.IProject
import productDefinition.CincoProduct

class CreatePyroPlugin {
	protected extension WorkspaceExtension _we = new WorkspaceExtension()
	public static final String PYRO = "pyro"
	public static final String PRIME = "primeviewer"
	public static final String PRIME_LABEL = "pvLabel"
	package IFolder pyroFolder

	def void execute(Set<GraphModel> graphModels, IProject project,CincoProduct cp) throws IOException, URISyntaxException {
		try {
			pyroFolder = _we.createFolder(project, "pyro")			
		} catch(Exception e) {
			
		}
		val absolutebasPath = pyroFolder.getLocation()
		
		//generate
		val gen = new Generator
		gen.generate(graphModels,cp,absolutebasPath,project)
		
	}
}
