package de.jabc.cinco.meta.plugin.pyro.backend.graphmodel.api

import de.jabc.cinco.meta.plugin.pyro.util.Generatable
import de.jabc.cinco.meta.plugin.pyro.util.GeneratorCompound
import mgl.ContainingElement
import mgl.Enumeration
import mgl.GraphModel
import mgl.GraphicalModelElement
import mgl.ModelElement
import mgl.Node
import mgl.Type

class GraphModelElementInterface extends Generatable {
	
	new(GeneratorCompound gc) {
		super(gc)
	}
	
	def filename(Type me)'''«me.name.fuEscapeJava».java'''
	
	def contentEnum(Enumeration me,GraphModel g)
	'''
	package «g.apiFQN»;
	
	public enum «me.name.fuEscapeJava» {
		«me.literals.map['''«it.toUnderScoreCase.escapeJava»'''].join(",")»
	}
	'''
	
	def content(ModelElement me,GraphModel g,boolean isTransient)
	'''
	package «g.apiFQN»;
	
	public interface «me.name.fuEscapeJava» extends «me.javaExtending»«IF me.hasToExtendContainer», graphmodel.Container«ENDIF» {
		«IF isTransient»
		de.ls5.dywa.generated.entity.info.scce.pyro.core.«me.baseTypeName» getDelegate();
		«ELSE»
		de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«me.name.fuEscapeJava» getDelegate();
		«ENDIF»
		«IF me instanceof GraphicalModelElement»
		«g.name.fuEscapeJava» getRootElement();
		«ENDIF»
		«IF me instanceof GraphModel»
			«me.embeddedEdges»
			void save();
		«ENDIF»
		«IF me instanceof GraphicalModelElement»
			«me.getBestContainerSuperTypeNameAPI(g.apiFQN).escapeJava» getContainer();
		«ENDIF»
		«IF me instanceof Node»
			«IF me.isPrime»
			«me.primeReference.type.graphModel.apiFQN».«me.primeReference.type.name.fuEscapeJava» get«me.primeReference.name.fuEscapeJava»();
			«ENDIF»
		«connectedNodeMethods(me,g)»
		«ENDIF»
		«IF me instanceof ContainingElement»
		«embeddedNodeMethods(me,g)»
		«ENDIF»
		«FOR attr:me.attributes»
			«attr.name.getSet('''«IF attr.isList»java.util.List<«ENDIF»«attr.javaType(g)»«IF attr.list»>«ENDIF»''',true)»
			«IF attr.isPrimitive(g) && attr.annotations.exists[name.equals("file")]»
				de.ls5.dywa.generated.util.FileReference get«attr.name.fuEscapeJava»FileReference();
				java.io.File get«attr.name.fuEscapeJava»File();
			«ENDIF»
		«ENDFOR»
	}
	'''
	
	def embeddedEdges(GraphModel g)
	'''
	«FOR edge:g.edgesTopologically»
	java.util.List<«edge.name.fuEscapeJava»> get«edge.name.fuEscapeJava»s();
	«ENDFOR»
	'''
	
	def connectedNodeMethods(Node node,GraphModel g)
	'''
	«FOR incoming:node.possibleIncoming(g)»
	«'''incoming«incoming.name.fuEscapeJava»s'''.toString.getMethod('''java.util.List<«incoming.name.fuEscapeJava»>''',false)»
	«ENDFOR»
	«FOR source:node.possibleIncoming(g).map[possibleSources(g)].flatten.toSet»
		«'''«source.name.fuEscapeJava»Predecessors'''.toString.getMethod('''java.util.List<«source.name.fuEscapeJava»>''',false)»
	«ENDFOR»
	«FOR incoming:node.possibleOutgoing(g)»
	«'''outgoing«incoming.name.fuEscapeJava»s'''.toString.getMethod('''java.util.List<«incoming.name.fuEscapeJava»>''',false)»
		«IF !incoming.isIsAbstract»
			«FOR target:incoming.possibleTargets(g)»
				«incoming.name.fuEscapeJava» new«incoming.name.fuEscapeJava»(«target.name» target);
			«ENDFOR»
		«ENDIF»
	«ENDFOR»
	«FOR target:node.possibleOutgoing(g).map[possibleTargets(g)].flatten.toSet»
		«'''«target.name.fuEscapeJava»Successors'''.toString.getMethod('''java.util.List<«target.name.fuEscapeJava»>''',false)»
	«ENDFOR»
	'''
	
	def embeddedNodeMethods(ContainingElement ce,GraphModel g)
	'''
	«FOR em:ce.possibleEmbeddingTypes(g)»
		«IF !em.isIsAbstract»
			«IF em.isPrime»
			«em.name.fuEscapeJava» new«em.name.fuEscapeJava»(
				«(em as Node).primeReference.type.graphModel.apiFQN».«(em as Node).primeReference.type.name.fuEscapeJava» object,
				int x,
				int y
			);
			«em.name.fuEscapeJava» new«em.name.fuEscapeJava»(
				«(em as Node).primeReference.type.graphModel.apiFQN».«(em as Node).primeReference.type.name.fuEscapeJava» object,
				int x,
				int y,
				int width,
				int height
			);
			«ELSE»
			«em.name.fuEscapeJava» new«em.name.fuEscapeJava»(int x, int y, int width, int height);
			«em.name.fuEscapeJava» new«em.name.fuEscapeJava»(int x, int y);
			«ENDIF»
		«ENDIF»
		java.util.List<«em.name.fuEscapeJava»> get«em.name.fuEscapeJava»s();
	«ENDFOR»
	'''
	
	def getSet(String name,String type,boolean replacePrefix)
	'''
	«name.getMethod(type,replacePrefix)»
	«name.setMethod(type)»
	'''
	
	def getMethod(String name,String type,boolean replacePrefix)
	'''«type» «IF replacePrefix && type.toLowerCase.contains("boolean")»is«ELSE»get«ENDIF»«name.fuEscapeJava»();'''
	
	def setMethod(String name,String type)
	'''void set«name.fuEscapeJava»(«type» «name.lowEscapeJava»);'''
	
	def voidMethod(String name,String args)
	'''void «name»(«args»);'''
	
}