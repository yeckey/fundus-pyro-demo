package de.jabc.cinco.meta.plugin.pyro.frontend.pages.editor.properties.graphs.graphmodel

import de.jabc.cinco.meta.plugin.pyro.util.Generatable
import de.jabc.cinco.meta.plugin.pyro.util.GeneratorCompound
import mgl.GraphModel

class PropertyComponent extends Generatable {
	
	new(GeneratorCompound gc) {
		super(gc)
	}
	
	def fileNamePropertyComponent()'''property_component.dart'''
	
	def contentPropertyComponent(GraphModel g)
	'''
	import 'package:angular/angular.dart';
	import 'dart:async';
	
	import 'package:«gc.projectName.escapeDart»/src/model/core.dart';
	
	import 'package:«gc.projectName.escapeDart»/src/model/«g.name.lowEscapeDart».dart' as «g.name.lowEscapeDart»;
	
	// the «g.name.lowEscapeDart» itself
	import 'package:«gc.projectName.escapeDart»/src/pages/editor/properties/graphs/«g.name.lowEscapeDart»/«g.name.lowEscapeDart»_property_component.dart';
	// all elements of the «g.name.lowEscapeDart»
	«FOR elem:g.elementsAndTypes»
	import 'package:«gc.projectName.escapeDart»/src/pages/editor/properties/graphs/«g.name.lowEscapeDart»/«elem.name.lowEscapeDart»_property_component.dart';
	«ENDFOR»
	
	@Component(
	    selector: '«g.name.lowEscapeDart»',
	    templateUrl: 'property_component.html',
	    directives: const [
	      «g.name.fuEscapeDart»PropertyComponent,
	      coreDirectives
	      «FOR elem:g.elementsAndTypes BEFORE "," SEPARATOR ","»
	      «elem.name.fuEscapeDart»PropertyComponent
	      «ENDFOR»
	      ]
	)
	class PropertyComponent {
	
	  @Input()
	  PyroElement currentElement;
	  
	  @Input()
	  GraphModel currentGraphModel;
	
	  final hasChangedSC = new StreamController();
	  @Output() Stream get hasChanged => hasChangedSC.stream;
	  
	
	  /// checks if the given element is a «g.name.fuEscapeDart»
	  /// instance.
	  bool check«g.name.fuEscapeDart»(PyroElement element)
	  {
	    return element is «g.name.lowEscapeDart».«g.name.fuEscapeDart»;
	  }
	«FOR elem:g.elementsAndTypes.filter[!isIsAbstract]»
	  /// checks if the given element is a «elem.name.fuEscapeDart»
	  /// instance.
	  bool check«elem.name.fuEscapeDart»(PyroElement element)
	  {
	    return element.$type()=='«g.name.lowEscapeDart».«elem.name.fuEscapeDart»';
	  }
	«ENDFOR»
	}
	
	'''
	
	def fileNamePropertyComponentTemplate()'''property_component.html'''
	
	def contentPropertyComponentTemplate(GraphModel g)
	'''
	<«g.name.lowEscapeDart»-property
	    *ngIf="check«g.name.fuEscapeDart»(currentElement)"
	    [currentElement]="currentElement"
	    [currentGraphModel]="currentGraphModel"
	    (hasChanged)="hasChangedSC.add($event)"
	>
	</«g.name.lowEscapeDart»-property>
	«FOR elem:g.elementsAndTypes.filter[!isIsAbstract]»
	<«elem.name.lowEscapeDart»-property
	    *ngIf="check«elem.name.fuEscapeDart»(currentElement)"
	    [currentElement]="currentElement"
	    [currentGraphModel]="currentGraphModel"
	    (hasChanged)="hasChangedSC.add($event)"
	></«elem.name.lowEscapeDart»-property>
	«ENDFOR»
	'''
}
