  package de.jabc.cinco.meta.plugin.pyro.backend.graphmodel.api.impl

import de.jabc.cinco.meta.plugin.pyro.util.Generatable
import de.jabc.cinco.meta.plugin.pyro.util.GeneratorCompound
import de.jabc.cinco.meta.plugin.pyro.util.MGLExtension
import mgl.Attribute
import mgl.ContainingElement
import mgl.Edge
import mgl.GraphModel
import mgl.GraphicalModelElement
import mgl.ModelElement
import mgl.Node
import mgl.NodeContainer
import mgl.UserDefinedType
import style.NodeStyle
import style.Styles
import mgl.ComplexAttribute
import de.jabc.cinco.meta.plugin.pyro.backend.graphmodel.api.GraphModelElementHook

class GraphModelElementImplementation extends Generatable {
	
	protected extension GraphModelElementHook = new GraphModelElementHook
	
	new(GeneratorCompound gc) {
		super(gc)
	}
		
	def filename(ModelElement me)'''«me.name.fuEscapeJava»Impl.java'''
	
	def content(ModelElement me,GraphModel g,Styles styles)
	{
	'''
	package «g.apiImplFQN»;
	
	public class «me.name.fuEscapeJava»Impl implements «g.apiFQN».«me.name.fuEscapeJava» {
		
		private final de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«me.name.fuEscapeJava» delegate;
	    private final info.scce.pyro.core.command.«g.name.escapeJava»CommandExecuter cmdExecuter;
		«IF me instanceof UserDefinedType»
		private final graphmodel.IdentifiableElement parent;
		private final info.scce.pyro.core.graphmodel.IdentifiableElement prev;
		«ENDIF»
		
	    public «me.name.fuEscapeJava»Impl(
	    	de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«me.name.fuEscapeJava» delegate,
	        info.scce.pyro.core.command.«g.name.escapeJava»CommandExecuter cmdExecuter
	                         «IF me instanceof UserDefinedType»
	                         ,graphmodel.IdentifiableElement parent
	                         ,info.scce.pyro.core.graphmodel.IdentifiableElement prev
	                         «ENDIF»
	                         ) {
	        this.delegate = delegate;
	        this.cmdExecuter = cmdExecuter;
	        «IF me instanceof UserDefinedType»
	        this.parent = parent;
	        this.prev = prev;
	        «ENDIF»
	    }
	    
	    @Override
		public boolean equals(Object obj) {
			return obj!=null && obj instanceof «g.apiFQN».«me.name.fuEscapeJava» && ((«g.apiFQN».«me.name.fuEscapeJava») obj).getId().equals(getId());
		}
		
		@Override
		public int hashCode() {
			return java.lang.Math.toIntExact(delegate.getDywaId());
		}
	    
		@Override
		public String getId() {
			return Long.toString(this.delegate.getDywaId());
		}
		
		@Override
		public de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«me.name.fuEscapeJava» getDelegate() {
			return this.delegate;
		}
		«IF me instanceof GraphModel»
		«me.embeddedEdges»
		@Override
		public void save() {}
		
		@Override
		public void deleteModelElement(graphmodel.ModelElement cme) {
			«FOR e:g.elements»
			if(cme instanceof «g.apiFQN».«e.name.fuEscapeJava») {
				((«g.apiFQN».«e.name.fuEscapeJava»)cme).delete();
			}
			«ENDFOR»
		}
		«ENDIF»
		«IF me instanceof GraphicalModelElement»
		
	    @Override
	    public «g.apiFQN».«g.name.fuEscapeJava» getRootElement() {
	    	if(this.getContainer() instanceof «g.apiFQN».«g.name.fuEscapeJava»){
	    		return («g.apiFQN».«g.name.fuEscapeJava») this.getContainer();
	    	}
	    	return («g.apiFQN».«g.name.fuEscapeJava») ((graphmodel.Container)this.getContainer()).getRootElement();
	    }
	    
	    @Override
	    public «me.getBestContainerSuperTypeNameAPI(g.apiFQN).escapeJava» getContainer() {
	        «FOR container:me.getPossibleContainmentTypes.filter(ModelElement).filter[!isAbstract]»
	        if(this.delegate.getcontainer() instanceof de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«container.name.fuEscapeJava») {
	        	return new «container.name.fuEscapeJava»Impl((de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«container.name.fuEscapeJava»)this.delegate.getcontainer(),cmdExecuter);
	        }
	        «ENDFOR»
	        «IF me instanceof Edge»
	        if(this.delegate.getcontainer() instanceof de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«g.name.fuEscapeJava») {
	        	 return new «g.name.fuEscapeJava»Impl((de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«g.name.fuEscapeJava»)this.delegate.getcontainer(),cmdExecuter);
	        }
	        «ENDIF»
	        return null;
	    }
		«ENDIF»
		«IF me instanceof Edge»
		@Override
		public void delete() {
			«'''cmdExecuter.remove«me.name.fuEscapeJava»(this.delegate);'''.deleteHooks(me)»
		}
		
		@Override
		public graphmodel.Node getSourceElement() {
			«FOR source:me.possibleSources(g).filter[!isIsAbstract]»
			if(this.delegate.getsourceElement() instanceof de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«source.name.fuEscapeJava») {
				return new «source.name.fuEscapeJava»Impl((de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«source.name.fuEscapeJava»)this.delegate.getsourceElement(),cmdExecuter);
			}
			«ENDFOR»
			return null;
		}
		
		@Override
		public graphmodel.Node getTargetElement() {
			«FOR target:me.possibleTargets(g).filter[!isIsAbstract]»
			if(this.delegate.gettargetElement() instanceof de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«target.name.fuEscapeJava») {
				return new «target.name.fuEscapeJava»Impl((de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«target.name.fuEscapeJava»)this.delegate.gettargetElement(),cmdExecuter);
			}
			«ENDFOR»
			return null;
		}
		
		@Override
		public void reconnectSource(graphmodel.Node node) {
			cmdExecuter.reconnect«me.name.fuEscapeJava»(this.delegate,(de.ls5.dywa.generated.entity.info.scce.pyro.core.Node)node.getDelegate(),this.delegate.gettargetElement());
		}
		
		@Override
		public void reconnectTarget(graphmodel.Node node) {
			cmdExecuter.reconnect«me.name.fuEscapeJava»(this.delegate,this.delegate.getsourceElement(),(de.ls5.dywa.generated.entity.info.scce.pyro.core.Node)node.getDelegate());
			
		}
		
		@Override
		public void addBendingPoint(int x, int y) {
			cmdExecuter.addBendpoint«me.name.fuEscapeJava»(this.getDelegate(),x,y);
		}
		«ENDIF»
		«IF me instanceof Node»
			@Override
			public void delete() {
				«'''
				«IF me instanceof NodeContainer»
				getModelElements().stream().filter(n->n instanceof graphmodel.Node).forEach(n->((graphmodel.Node)n).delete());
				«ENDIF»
				java.util.Set<graphmodel.Edge> edges = new java.util.HashSet<>();
				edges.addAll(getIncoming());
				edges.addAll(getOutgoing());
				edges.forEach(graphmodel.Edge::delete);
				«IF me.modelPrime»
					cmdExecuter.remove«me.name.fuEscapeJava»(this.delegate,this.get«me.primeReference.name.fuEscapeJava»().getDelegate());
				«ELSEIF me.primeReference !=null»
					cmdExecuter.remove«me.name.fuEscapeJava»(this.delegate,this.get«me.primeReference.name.fuEscapeJava»().getDelegate());
				«ELSE»
					cmdExecuter.remove«me.name.fuEscapeJava»(this.delegate);
				«ENDIF»
				'''.deleteHooks(me)»
			}
			
			@Override
		    public int getX() {
		        return java.lang.Math.toIntExact(this.delegate.getx());
		    }
		
		    @Override
		    public int getY() {
		        return java.lang.Math.toIntExact(this.delegate.gety());
		    }
		
		    @Override
		    public int getWidth() {
		        return java.lang.Math.toIntExact(this.delegate.getwidth());
		    }
		
		    @Override
		    public int getHeight() {
		        return java.lang.Math.toIntExact(this.delegate.getheight());
		    }
		
		    @Override
		    public java.util.List<graphmodel.Edge> getIncoming() {
		    	«IF me.possibleIncoming(g).empty»
		    	return java.util.Collections.EMPTY_LIST;
		    	«ELSE»
		    	return this.delegate.getincoming_Edge().stream().map((n)->{
		        	«FOR incoming:me.possibleIncoming(g).filter[!isIsAbstract]»
					if(n instanceof de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«incoming.name.fuEscapeJava»){
    	                return new «incoming.name.fuEscapeJava»Impl((de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«incoming.name.fuEscapeJava»)n,cmdExecuter);
                    }
                    «ENDFOR»
                    throw new IllegalStateException("Unknown incoming edge type");
                }).collect(java.util.stream.Collectors.toList());
                «ENDIF»
		    }
		
		    @Override
		    public <T extends graphmodel.Edge> java.util.List<T> getIncoming(Class<T> clazz) {
		        return getIncoming().stream().filter(n->clazz.isInstance(n)).map(n->clazz.cast(n)).collect(java.util.stream.Collectors.toList());
		    }
		
		    @Override
		    public java.util.List<graphmodel.Node> getPredecessors() {
		        return getIncoming().stream().map(n->n.getSourceElement()).collect(java.util.stream.Collectors.toList());
		    }
		
		    @Override
		    public <T extends graphmodel.Node> java.util.List<T> getPredecessors(Class<T> clazz) {
		       return getPredecessors().stream().filter(n->clazz.isInstance(n)).map(n->clazz.cast(n)).collect(java.util.stream.Collectors.toList());
		    }
		
		    @Override
		    public java.util.List<graphmodel.Edge> getOutgoing() {
		    	«IF me.possibleOutgoing(g).empty»
		    	return java.util.Collections.EMPTY_LIST;
		    	«ELSE»
		        return this.delegate.getoutgoing_Edge().stream().map((n)->{
		        «FOR outgoing:me.possibleOutgoing(g).filter[!isIsAbstract]»
		            if(n instanceof de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«outgoing.name.fuEscapeJava»){
	                	return new «outgoing.name.fuEscapeJava»Impl((de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«outgoing.name.fuEscapeJava»)n,cmdExecuter);
		            }
		        «ENDFOR»
		        	throw new IllegalStateException("Unknown outgoing edge type");
                }).collect(java.util.stream.Collectors.toList());
                «ENDIF»
		    }
		
		    @Override
		    public <T extends graphmodel.Edge> java.util.List<T> getOutgoing(Class<T> clazz) {
		       return getOutgoing().stream().filter(n->clazz.isInstance(n)).map(n->clazz.cast(n)).collect(java.util.stream.Collectors.toList());
		    }
		
		    @Override
		    public java.util.List<graphmodel.Node> getSuccessors() {
		        return getOutgoing().stream().map(n->n.getTargetElement()).collect(java.util.stream.Collectors.toList());
		    }
		
		    @Override
		    public <T extends graphmodel.Node> java.util.List<T> getSuccessors(Class<T> clazz) {
		        return getSuccessors().stream().filter(n->clazz.isInstance(n)).map(n->clazz.cast(n)).collect(java.util.stream.Collectors.toList());
		    }
		
		    @Override
		    public void move(int x, int y) {
				this.moveTo(this.getContainer(),x,y);
		    }
		
		    @Override
		    public void moveTo(graphmodel.ModelElementContainer container,int x, int y) {
		    	«IF me.hasPostMove»
		    	graphmodel.ModelElementContainer preContainer = this.getContainer();
		    	int preX = this.getX();
		    	int preY = this.getY();
		    	«ENDIF»
		    	«FOR container:g.nodesTopologically.filter(NodeContainer).filter[!isIsAbstract]+#[g]»
		    	if(container instanceof «g.apiFQN».«container.name.fuEscapeJava») {
		    		this.cmdExecuter.move«me.name.fuEscapeJava»(this.delegate,x,y,((«g.apiFQN».«container.name.fuEscapeJava»)container).getDelegate());
		    	}
		    	«ENDFOR»
		    	«IF me.hasPostMove»
		    	//post move
		    	«me.postMoveHook» hook = new «me.postMoveHook»();
		    	hook.init(cmdExecuter);
		    	hook.postMove(this,preContainer,container,x,y,x-preX,y-preY);
		    	«ENDIF»
		    }
		
		    @Override
		    public void resize(int width, int height) {
				this.cmdExecuter.resize«me.name.fuEscapeJava»(this.delegate,width,height);
				«IF me.hasPostResize»
				//post resize
				«me.postResizeHook» hook = new «me.postResizeHook»();
				hook.init(cmdExecuter);
				hook.postResize(this,width,height);
				«ENDIF»
		    }
		
			«IF me.isPrime»
			@Override
			public «me.primeReference.type.graphModel.apiFQN».«me.primeReference.type.name.fuEscapeJava» get«me.primeReference.name.fuEscapeJava»()
			{
				if(delegate.get«me.primeReference.name.escapeJava»()==null) {
					return null;
				}
				«IF me.primeReference.type.graphModel instanceof GraphModel»
				return new «me.primeReference.type.graphModel.apiFQN».impl.«me.primeReference.type.name.fuEscapeJava»Impl(delegate.get«me.primeReference.name.escapeJava»(),cmdExecuter«IF !me.primeReference.type.graphModel.equals(g)».getBundle().primeGraph«(me.primeReference.type.graphModel as GraphModel).name.fuEscapeJava»Controller.buildExecuter(delegate.get«me.primeReference.name.escapeJava»(),cmdExecuter.pyroProject)«ENDIF»);
				«ELSE»
				return new «me.primeReference.type.graphModel.apiFQN».impl.«me.primeReference.type.name.fuEscapeJava»Impl(delegate.get«me.primeReference.name.escapeJava»());
				«ENDIF»
			}
			«ENDIF»
		«connectedNodeMethods(me,g)»
		«ENDIF»
		«IF me instanceof ContainingElement»
		
		@Override
		public java.util.List<graphmodel.ModelElement> getModelElements() {
			«IF g.elements.empty»
			return new java.util.LinkedList<>();
			«ELSE»
			return this.delegate.getmodelElements_ModelElement().stream().map((n)->{
				«FOR e:g.elements.filter[!isIsAbstract]»
				if(n instanceof de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«e.name.fuEscapeJava») {
					return new «e.name.fuEscapeJava»Impl((de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«e.name.fuEscapeJava»)n,this.cmdExecuter);
				}
				«ENDFOR»
				throw new IllegalStateException("Element type unkown");
			}).collect(java.util.stream.Collectors.toList());
			«ENDIF»
		}
			
		@Override
		public <T extends graphmodel.ModelElement> java.util.List<T> getModelElements(Class<T> clazz) {
			return this.getModelElements().stream().filter(n->clazz.isInstance(n)).map(n->clazz.cast(n)).collect(java.util.stream.Collectors.toList());
		}
				
		private java.util.List<graphmodel.ModelElement> getAllModelElements(graphmodel.ModelElementContainer cmc) {
			java.util.List<graphmodel.ModelElement> cm = new java.util.LinkedList<>(cmc.getModelElements());
			cm.addAll(cmc.getModelElements().stream().filter(n->n instanceof graphmodel.ModelElementContainer).flatMap(n->getAllModelElements((graphmodel.ModelElementContainer)n).stream()).collect(java.util.stream.Collectors.toList()));
			return cm;
		}
		
		@Override
		public <T extends graphmodel.Edge> java.util.List<T> getEdges(Class<T> clazz) {
			return getModelElements(clazz);
		}
	
		@Override
		public <T extends graphmodel.Node> java.util.List<T> getNodes(Class<T> clazz) {
			return getModelElements(clazz);
		}
	
		@Override
		public java.util.List<graphmodel.Node> getNodes() {
			return getModelElements(graphmodel.Node.class);
		}
			
		@Override
		public java.util.List<graphmodel.Node> getAllNodes() {
			return getAllModelElements(this).stream()
			.filter(n->n instanceof graphmodel.Node)
			.map(n->(graphmodel.Node)n)
			.collect(java.util.stream.Collectors.toList());
		}
			
		@Override
		public java.util.List<graphmodel.Edge> getAllEdges() {
			return getAllModelElements(this).stream()
				.filter(n->n instanceof graphmodel.Edge)
				.map(n->(graphmodel.Edge)n)
				.collect(java.util.stream.Collectors.toList());
		}
		
		@Override
		public java.util.List<graphmodel.Container> getAllContainers() {
			return getAllModelElements(this).stream()
				.filter(n->n instanceof graphmodel.Container)
				.map(n->(graphmodel.Container)n)
				.collect(java.util.stream.Collectors.toList());
		}
		«embeddedNodeMethods(me,g,styles)»
		«ENDIF»
		«FOR attr:me.attributesExtended»
		@Override
		public «IF attr.isList»java.util.List<«ENDIF»«IF !attr.isPrimitive(g)»«g.apiFQN».«ENDIF»«attr.javaType(g)»«IF attr.isList»>«ENDIF» «IF attr.type.equals("EBoolean")»is«ELSE»get«ENDIF»«attr.name.fuEscapeJava»() {
			«IF attr.isPrimitive(g)»
				«IF attr.type.getEnum(g)!==null»
					«IF attr.list»
					return this.delegate.get«attr.name.escapeJava»_«attr.type.fuEscapeJava»().stream().map((n)->{
						«{  
							val e = attr.type.getEnum(g) '''
							switch (n){
								«e.literals.map['''case «it.toUnderScoreCase.escapeJava»: return «g.apiFQN».«e.name.fuEscapeJava».«it.toUnderScoreCase.fuEscapeJava»;'''].join("\n")»
							}
							return null;
						'''}»
					}).collect(java.util.stream.Collectors.toList());
					«ELSE»
					«{  
						val e = attr.type.getEnum(g) '''
						switch (this.delegate.get«attr.name.escapeJava»()){
							«e.literals.map['''case «it.toUnderScoreCase.escapeJava»: return «g.apiFQN».«e.name.fuEscapeJava».«it.toUnderScoreCase.fuEscapeJava»;'''].join("\n")»
						}
						return null;
					'''}»
					«ENDIF»
				«ELSE»
					return «attr.primitiveGETConverter('''this.delegate.get«attr.name.escapeJava»()''')»;
				«ENDIF»
			«ELSE»
				«IF !(me instanceof UserDefinedType)»
				info.scce.pyro.«g.name.lowEscapeJava».rest.«me.name.escapeJava» prev = info.scce.pyro.«g.name.lowEscapeJava».rest.«me.name.escapeJava».fromDywaEntityProperties(this.delegate,null);
				«ENDIF»
				«IF attr.isList»
				return this.delegate.get«attr.name.escapeJava»_«attr.type.escapeJava»().stream().map(n->{
					«FOR sub:attr.type.subTypesAndType(g).filter(ModelElement).filter[!isAbstract] SEPARATOR " else "»
					if(n instanceof de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«sub.name.fuEscapeJava») {
						return new «sub.name.fuEscapeJava»Impl((de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«sub.name.fuEscapeJava»)n,this.cmdExecuter
						«IF me instanceof UserDefinedType»
							«IF !attr.isModelElement(g)»,parent,prev«ENDIF»
						«ELSE»
							«IF !attr.isModelElement(g)»,this,prev«ENDIF»
						«ENDIF»);
					}
					«ENDFOR»
					return null;
				}).collect(java.util.stream.Collectors.toList());
				«ELSE»
				if(this.delegate.get«attr.name.escapeJava»()!=null) {
					«FOR sub:attr.type.subTypesAndType(g).filter(ModelElement).filter[!isAbstract] SEPARATOR " else "»
					if(this.delegate.get«attr.name.escapeJava»() instanceof de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«sub.name.fuEscapeJava») {
						return new «sub.name.fuEscapeJava»Impl((de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«sub.name.fuEscapeJava»)this.delegate.get«attr.name.escapeJava»(),this.cmdExecuter
							«IF me instanceof UserDefinedType»
								«IF !attr.isModelElement(g)»,parent,prev«ENDIF»
							«ELSE»
								«IF !attr.isModelElement(g)»,this,prev«ENDIF»
							«ENDIF»);
					}
					«ENDFOR»
				} 
				return null;
				«ENDIF»
			«ENDIF»
		}
		
		«IF attr.isPrimitive(g) && !attr.isList && attr.annotations.exists[name.equals("file")]»
		@Override
		public de.ls5.dywa.generated.util.FileReference get«attr.name.fuEscapeJava»FileReference() {
			try {
				java.net.URI uri = new java.net.URI(this.get«attr.name.fuEscapeJava»());
				String[] segments = uri.getPath().split("/");
				String idStr = segments[segments.length-2];
				long id = Long.parseLong(idStr);
				return this.cmdExecuter.getBundle().getDomainFileController().getFileReference(id);
			} catch (java.net.URISyntaxException e) {
				e.printStackTrace();
				return null;
			}
		}
		@Override
		public java.io.File get«attr.name.fuEscapeJava»File() {
			de.ls5.dywa.generated.util.FileReference fr = this.get«attr.name.fuEscapeJava»FileReference();
			if(fr == null) {
				return null;
			}
			final java.io.InputStream stream = this.cmdExecuter.getBundle().getDomainFileController()
					.loadFile(fr);
			try {
				java.io.File tempFile = java.io.File.createTempFile(org.apache.commons.io.FilenameUtils.getBaseName(fr.getFileName()), "."+org.apache.commons.io.FilenameUtils.getExtension(fr.getFileName()));
				org.apache.commons.io.FileUtils.copyInputStreamToFile(stream, tempFile);
				return tempFile;
			}catch( java.io.IOException e) {
				e.printStackTrace();
			}
			return null;
		}
		«ENDIF»
		
		@Override
		public void set«attr.name.fuEscapeJava»(«IF attr.isList»java.util.List<«ENDIF»«IF !attr.isPrimitive(g)»«g.apiFQN».«ENDIF»«attr.javaType(g)»«IF attr.isList»>«ENDIF» attr) {
			«IF !(me instanceof UserDefinedType)»
			info.scce.pyro.«g.name.lowEscapeJava».rest.«me.name.escapeJava» prev = info.scce.pyro.«g.name.lowEscapeJava».rest.«me.name.escapeJava».fromDywaEntityProperties(this.delegate,null);
			«ENDIF»
			«IF attr.isPrimitive(g)»
				«IF attr.type.getEnum(g)!==null»
					«IF attr.list»
						this.delegate.set«attr.name.escapeJava»_«attr.type.fuEscapeJava»(attr.stream().map((n)->{
						«{  
							val e = attr.type.getEnum(g) '''
							switch (n){
								«e.literals.map['''case «it.toUnderScoreCase.fuEscapeJava»: return de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«e.name.fuEscapeJava».«it.toUnderScoreCase.escapeJava»;'''].join("\n")»
							}
							return null;
						'''}»
						}).collect(java.util.stream.Collectors.toList()));
					«ELSE»
						«{  
							val e = attr.type.getEnum(g) '''
							switch (attr){
								«e.literals.map['''case «it.toUnderScoreCase.escapeJava»: this.delegate.set«attr.name.escapeJava»(de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«e.name.fuEscapeJava».«it.toUnderScoreCase.escapeJava»);break;'''].join("\n")»
							}
						'''}»
					«ENDIF»
				«ELSE»
				this.delegate.set«attr.name.escapeJava»(«attr.primitiveSETConverter('''attr''')»);
				«ENDIF»
			«ELSE»
				«IF attr.isList»
					this.delegate.set«attr.name.escapeJava»_«attr.type.escapeJava»(
						attr.stream().map(n->n.getDelegate()).collect(java.util.stream.Collectors.toList())
					);
				«ELSE»
					this.delegate.set«attr.name.escapeJava»(attr.getDelegate());
				«ENDIF»
			«ENDIF»
			«IF me instanceof UserDefinedType»if(this.parent != null) {«ENDIF»
			this.cmdExecuter.update«IF me instanceof UserDefinedType»IdentifiableElement«ELSE»«me.name.escapeJava»Properties«ENDIF»(this«IF me instanceof UserDefinedType».parent«ENDIF».getDelegate(),prev);
			«IF me instanceof UserDefinedType»}«ENDIF»
			«IF me.hasPostAttributeValueChange»
				//property change hook
				org.eclipse.emf.ecore.EStructuralFeature esf = new org.eclipse.emf.ecore.EStructuralFeature();
				esf.setName("«attr.name»");
				«me.postAttributeValueChange» hook = new «me.postAttributeValueChange»();
				hook.init(cmdExecuter);
				if(hook.canHandleChange(this,esf)) {
					hook.handleChange(this,esf);
				}
			«ENDIF»
		}
		«ENDFOR»
	}
	'''
	}
	
	
	
	
	def primitiveGETConverter(Attribute attribute, String string) {
		return switch(attribute.type) {
			case "EInt":'''«IF attribute.list»«string».stream().map(n->Math.toIntExact(n)).collect(java.util.stream.Collectors.toList())«ELSE»Math.toIntExact(«string»)«ENDIF»'''
			case "EBigInteger":'''«IF attribute.list»«string».stream().map(n->Math.toIntExact(n)).collect(java.util.stream.Collectors.toList())«ELSE»Math.toIntExact(«string»)«ENDIF»'''
			case "ELong":'''«IF attribute.list»«string».stream().map(n->Math.toIntExact(n)).collect(java.util.stream.Collectors.toList())«ELSE»Math.toIntExact(«string»)«ENDIF»'''
			case "EByte":'''«IF attribute.list»«string».stream().map(n->Math.toIntExact(n)).collect(java.util.stream.Collectors.toList())«ELSE»Math.toIntExact(«string»)«ENDIF»'''
			case "EShort":'''«IF attribute.list»«string».stream().map(n->Math.toIntExact(n)).collect(java.util.stream.Collectors.toList())«ELSE»Math.toIntExact(«string»)«ENDIF»'''
			default:string
		}
	}
	
	def primitiveSETConverter(Attribute attribute, String string) {
		return switch(attribute.type) {
			case "EInt":'''«IF attribute.list»«string».stream().map(n->Long.valueOf(n)).collect(java.util.stream.Collectors.toList())«ELSE»Long.valueOf(«string»)«ENDIF»'''
			case "EBigInteger":'''«IF attribute.list»«string».stream().map(n->Long.valueOf(n)).collect(java.util.stream.Collectors.toList())«ELSE»Long.valueOf(«string»)«ENDIF»'''
			case "ELong":'''«IF attribute.list»«string».stream().map(n->Long.valueOf(n)).collect(java.util.stream.Collectors.toList())«ELSE»Long.valueOf(«string»)«ENDIF»'''
			case "EByte":'''«IF attribute.list»«string».stream().map(n->Long.valueOf(n)).collect(java.util.stream.Collectors.toList())«ELSE»Long.valueOf(«string»)«ENDIF»'''
			case "EShort":'''«IF attribute.list»«string».stream().map(n->Long.valueOf(n)).collect(java.util.stream.Collectors.toList())«ELSE»Long.valueOf(«string»)«ENDIF»'''
			default:string
		}
	}
	

	
	def embeddedEdges(GraphModel g)
	'''
	«FOR edge:g.edgesTopologically»
	@Override
	public java.util.List<«g.apiFQN».«edge.name.fuEscapeJava»> get«edge.name.fuEscapeJava»s() {
		return this.getModelElements(«g.apiFQN».«edge.name.fuEscapeJava».class);
	}
	«ENDFOR»
	'''
	
	def connectedNodeMethods(Node node,GraphModel g)
	'''
	«FOR incoming:node.possibleIncoming(g)»
	@Override
	public java.util.List<«g.apiFQN».«incoming.name.fuEscapeJava»> getIncoming«incoming.name.fuEscapeJava»s() {
		return getIncoming(«g.apiFQN».«incoming.name.fuEscapeJava».class);
	}
	«ENDFOR»
	«FOR source:node.possibleIncoming(g).map[possibleSources(g)].flatten.toSet»
	@Override
	public java.util.List<«g.apiFQN».«source.name.fuEscapeJava»> get«source.name.fuEscapeJava»Predecessors() {
		return getPredecessors(«g.apiFQN».«source.name.fuEscapeJava».class);
	}
	«ENDFOR»
	«FOR outgoing:node.possibleOutgoing(g)»
	@Override
	public java.util.List<«g.apiFQN».«outgoing.name.fuEscapeJava»> getOutgoing«outgoing.name.fuEscapeJava»s() {
		return getOutgoing(«g.apiFQN».«outgoing.name.fuEscapeJava».class);
	}
		«IF !outgoing.isIsAbstract»
			«FOR target:outgoing.possibleTargets(g)»
				@Override
				public «g.apiFQN».«outgoing.name.fuEscapeJava» new«outgoing.name.fuEscapeJava»(«g.apiFQN».«target.name» target) {
					de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.fuEscapeJava».«outgoing.name.fuEscapeJava» edge = cmdExecuter.create«outgoing.name.fuEscapeJava»(this.delegate,target.getDelegate(),java.util.Collections.EMPTY_LIST,null);
					«g.apiFQN».«outgoing.name.fuEscapeJava» cn = new «outgoing.name.fuEscapeJava»Impl(edge,cmdExecuter);
					«outgoing.postCreate(g,"cn",gc,false)»
					return cn;
				}
			«ENDFOR»
		«ENDIF»
	«ENDFOR»
	«FOR target:node.possibleOutgoing(g).map[possibleTargets(g)].flatten.toSet»
		@Override
		public java.util.List<«g.apiFQN».«target.name.fuEscapeJava»> get«target.name.fuEscapeJava»Successors() {
			return getSuccessors(«g.apiFQN».«target.name.fuEscapeJava».class);
		}
	«ENDFOR»
	'''
	
	
	def embeddedNodeMethods(ContainingElement ce,GraphModel g,Styles styles)
	'''
	«FOR em:ce.possibleEmbeddingTypes(g)»
	«IF !em.isIsAbstract»
		«IF em.isPrime»
		@Override
		public «g.apiFQN».«em.name.fuEscapeJava» new«em.name.fuEscapeJava»(
			«(em as Node).primeReference.type.graphModel.apiFQN».«(em as Node).primeReference.type.name.fuEscapeJava» object,
			int x,
			int y
		) {
			de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«em.name.fuEscapeJava» node = cmdExecuter.create«em.name.fuEscapeJava»(x,y,
			«{
				val nodeStyle = styling(em as Node,styles) as NodeStyle
				val size = nodeStyle.mainShape.size
				 '''
					  	 «IF size!==null»
					  	 «size.width»,
					  	 «size.height»
					  	 «ELSE»
					  	 «MGLExtension.DEFAULT_WIDTH»,
					  	 «MGLExtension.DEFAULT_HEIGHT»
					  	 «ENDIF»
					  	 '''
			}»
			,this.delegate,null,object.getDelegate().getDywaId());
			«g.apiFQN».«em.name.fuEscapeJava» cn = new «em.name.fuEscapeJava»Impl(node,cmdExecuter);
			«em.postCreate(g,"cn",gc,false)»
			return cn;
		}
		
		@Override
		public «g.apiFQN».«em.name.fuEscapeJava» new«em.name.fuEscapeJava»(
			«(em as Node).primeReference.type.graphModel.apiFQN».«(em as Node).primeReference.type.name.fuEscapeJava» object,
			int x,
			int y,
			int width,
			int height
		) {
			de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«em.name.fuEscapeJava» node = cmdExecuter.create«em.name.fuEscapeJava»(x,y,new Long(width),new Long(height),this.delegate,null,object.getDelegate().getDywaId());
			«g.apiFQN».«em.name.fuEscapeJava» cn = new «em.name.fuEscapeJava»Impl(node,cmdExecuter);
			«em.postCreate(g,"cn",gc,false)»
			return cn;
		}
		«ELSE»
		@Override
		public «g.apiFQN».«em.name.fuEscapeJava» new«em.name.fuEscapeJava»(int x, int y, int width, int height) {
			de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«em.name.fuEscapeJava» node = cmdExecuter.create«em.name.fuEscapeJava»(x,y,new Long(width),new Long(height),this.delegate,null);
			«g.apiFQN».«em.name.fuEscapeJava» cn = new «em.name.fuEscapeJava»Impl(node,cmdExecuter);
			«em.postCreate(g,"cn",gc,false)»
			return cn;
		}
		@Override
		public «g.apiFQN».«em.name.fuEscapeJava» new«em.name.fuEscapeJava»(int x, int y) {
				return this.new«em.name.fuEscapeJava»(x,y,«{
					val nodeStyle = styling(em as Node,styles) as NodeStyle
					val size = nodeStyle.mainShape.size
					 '''
						  	 «IF size!==null»
						  	 «size.width»,
						  	 «size.height»
						  	 «ELSE»
						  	 «MGLExtension.DEFAULT_WIDTH»,
						  	 «MGLExtension.DEFAULT_HEIGHT»
						  	 «ENDIF»
						  	 '''
				}»);
		}
		«ENDIF»
	«ENDIF»
	@Override
	public java.util.List<«g.apiFQN».«em.name.fuEscapeJava»> get«em.name.fuEscapeJava»s() {
		return getModelElements(«g.apiFQN».«em.name.fuEscapeJava».class);
	}
	
	«ENDFOR»
	
	
	'''
	
	def deleteHooks(CharSequence inner, ModelElement me)'''
	«IF me.hasPreDeleteHook»
	//pre delete
	«me.preDeleteHook» prehook = new «me.preDeleteHook»();
	prehook.init(cmdExecuter);
	prehook.preDelete(this);
	«ENDIF»
	«IF me.hasPostDeleteHook»
	«me.postDeleteHook» posthook = new «me.postDeleteHook»();
	Runnable runnable = posthook.getPostDeleteFunction(this);
	«ENDIF»
	«inner»
	«IF me.hasPostDeleteHook»
	runnable.run();
	«ENDIF»
	'''
	
	
}