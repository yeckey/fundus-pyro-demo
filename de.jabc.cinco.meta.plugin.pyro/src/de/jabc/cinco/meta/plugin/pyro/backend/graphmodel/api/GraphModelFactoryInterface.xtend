package de.jabc.cinco.meta.plugin.pyro.backend.graphmodel.api

import de.jabc.cinco.meta.plugin.pyro.util.Generatable
import de.jabc.cinco.meta.plugin.pyro.util.GeneratorCompound
import mgl.GraphModel
import mgl.UserDefinedType

class GraphModelFactoryInterface extends Generatable{
	
	new(GeneratorCompound gc) {
		super(gc)
	}
	
	def filename(GraphModel g)
	'''«g.name.toCamelCase.fuEscapeJava»Factory.java'''
	
	def content(GraphModel g, boolean isTransient)
	'''
	package «g.factoryFQN»;
	«IF !isTransient»
	import de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject;
	import de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroFolder;
	import info.scce.pyro.core.command.«g.name.fuEscapeJava»CommandExecuter;
	import de.ls5.dywa.generated.controller.info.scce.pyro.«g.name.escapeJava».«g.name.fuEscapeJava»Controller;
	import info.scce.pyro.core.MainControllerBundle;
	«ENDIF»

	/**
	 * Author zweihoff
	 */
	public interface «g.name.toCamelCase.fuEscapeJava»Factory {

	    «g.name.toCamelCase.fuEscapeJava»Factory eINSTANCE = «g.apiImplFQN».«g.name.toCamelCase.fuEscapeJava»FactoryImpl.init();
		«g.apiFQN».«g.name.fuEscapeJava» create«g.name.fuEscapeJava»(String projectRelativePath, String filename);
		«IF !isTransient»
		«g.apiFQN».«g.name.fuEscapeJava» create«g.name.fuEscapeJava»(PyroFolder parentFolder, String filename);
		void warmup(PyroProject project,
			    	        de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser subject,
			    	        MainControllerBundle bundle,
			    	        info.scce.pyro.core.command.«g.name.escapeJava»CommandExecuter executer
			    	    );
		«ENDIF»
		
		«FOR udt:g.elementsAndTypes.filter(UserDefinedType).filter[!isAbstract]»
	    «g.apiFQN».«udt.name.fuEscapeJava» create«udt.name.fuEscapeJava»();
	    «ENDFOR»
	}
	
	'''
	
}