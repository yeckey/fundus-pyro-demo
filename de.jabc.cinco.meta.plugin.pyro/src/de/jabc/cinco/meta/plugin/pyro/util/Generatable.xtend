package de.jabc.cinco.meta.plugin.pyro.util

import mgl.GraphModel

class Generatable {
	protected final GeneratorCompound gc
	protected extension Escaper = new Escaper
	protected extension MGLExtension mglExtension
	protected extension DyWAExtension = new DyWAExtension
	
	
	new(GeneratorCompound gc){
		this.gc = gc
		this.mglExtension = gc.mglExtension
	}
	
	def getNodes(GraphModel g) {
		gc.mglExtension.nodes(g)
	}
}