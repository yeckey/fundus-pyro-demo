package de.jabc.cinco.meta.plugin.pyro.backend.graphmodel.api

import de.jabc.cinco.meta.plugin.pyro.util.Generatable
import de.jabc.cinco.meta.plugin.pyro.util.GeneratorCompound
import org.eclipse.emf.ecore.EAttribute
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.EEnum
import org.eclipse.emf.ecore.ENamedElement
import org.eclipse.emf.ecore.EPackage
import org.eclipse.emf.ecore.EReference

class EcoreInterface extends Generatable {
	
	new(GeneratorCompound gc) {
		super(gc)
	}
	
	def filename(ENamedElement me)'''«me.name.fuEscapeJava».java'''
	
	def contentEnum(EEnum me,EPackage g)
	'''
	package «g.apiFQN»;
	
	public enum «me.name.fuEscapeJava» {
		«me.ELiterals.map['''«it.name.toUnderScoreCase.escapeJava»'''].join(",")»
	}
	'''
	
	def content(EClass me,EPackage g)
	'''
	package «g.apiFQN»;
	
	public interface «me.name.fuEscapeJava» extends «IF me.ESuperTypes.empty»org.eclipse.emf.ecore.EObject«ELSE»«FOR sup:me.ESuperTypes SEPARATOR ","»«g.apiFQN».«sup.name.fuEscapeJava»«ENDFOR»«ENDIF» {
		public de.ls5.dywa.generated.entity.«g.name.escapeJava».«me.name.fuEscapeJava» getDelegate();
		«FOR attr:me.eContents.filter(EReference)»
			«attr.name.getSet('''«IF attr.isList»java.util.List<«ENDIF»«attr.ecoreType(g)»«IF attr.list»>«ENDIF»''',true)»
		«ENDFOR»
		«FOR attr:me.eContents.filter(EAttribute)»
			«attr.name.getSet('''«IF attr.isList»java.util.List<«ENDIF»«attr.ecoreType(g)»«IF attr.list»>«ENDIF»''',true)»
		«ENDFOR»
	}
	'''
	
	
	
	def getSet(String name,String type,boolean replacePrefix)
	'''
	«name.getMethod(type,replacePrefix)»
	«name.setMethod(type)»
	'''
	
	def getMethod(String name,String type,boolean replacePrefix)
	'''public «type» «IF replacePrefix && type.toLowerCase.contains("boolean")»is«ELSE»get«ENDIF»«name.fuEscapeJava»();'''
	
	def setMethod(String name,String type)
	'''public void set«name.fuEscapeJava»(«type» «name.lowEscapeJava»);'''
	
	def voidMethod(String name,String args)
	'''public void «name»(«args»);'''
	
}