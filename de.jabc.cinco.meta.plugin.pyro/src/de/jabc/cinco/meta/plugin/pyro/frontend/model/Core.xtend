package de.jabc.cinco.meta.plugin.pyro.frontend.model

import de.jabc.cinco.meta.plugin.pyro.util.Generatable
import de.jabc.cinco.meta.plugin.pyro.util.GeneratorCompound

class Core extends Generatable {
	
	new(GeneratorCompound gc) {
		super(gc)
	}
	
	def fileNameDispatcher() '''core.dart'''
	
	def contentDispatcher() '''
		import 'dart:convert';
		import 'dart:js' as js;
		import 'dart:html';
		import 'dispatcher.dart';
		
		import '../filesupport/fileuploader.dart';
		
		abstract class PyroElement {
		  int dywaId;
		  int dywaVersion;
		  String dywaName;
		
		  Map toJSOG(Map cache);
		
		  String $type();
		
		  void merge(PyroElement ie,{bool structureOnly:false,Map cache});
		
		  PyroElement({Map jsog,Map cache});
		}
		
		abstract class IdentifiableElement implements PyroElement{
		  int dywaId;
		  int dywaVersion;
		  String dywaName;
		  bool $isDirty;
		
		  GraphModel getRootElememt();
		
		  IdentifiableElement propertyCopy();
		  
		  String $displayName();
		 
		}
		
		abstract class ModelElement implements IdentifiableElement{
		  int dywaId;
		  int dywaVersion;
		  String dywaName;
		  ModelElementContainer container;
		
		  List<IdentifiableElement> allElements()
		  {
		    List<IdentifiableElement> list = new List();
		    list.add(this);
		    return list;
		  }
		  
		  js.JsArray styleArgs();
		
		  @override
		  GraphModel getRootElememt() => container.getRootElememt();
		  
		  String $information();
		  
		  String $label();
		  
		  
		}
		
		abstract class ModelElementContainer implements IdentifiableElement{
		  int dywaId;
		  int dywaVersion;
		  String dywaName;
		  List<ModelElement> modelElements;
		
		  List<IdentifiableElement> allElements()
		  {
		      List<IdentifiableElement> list = new List();
		      list.add(this);
		      list.addAll(modelElements.expand((n) => n.allElements()));
		      return list;
		  }
		
		}
		
		abstract class Node implements ModelElement {
		  int dywaId;
		  bool $isDirty;
		  int dywaVersion;
		  String dywaName;
		  ModelElementContainer container;
		  List<Edge> incoming;
		  List<Edge> outgoing;
		  int x;
		  int y;
		  int width;
		  int height;
		  int angle;
		
		  @override
		  GraphModel getRootElememt() => container.getRootElememt();
		  
		}
		
		abstract class Container implements Node, ModelElementContainer {
		  int dywaId;
		  bool $isDirty;
		  int dywaVersion;
		  String dywaName;
		  ModelElementContainer container;
		  List<Edge> incoming;
		  List<Edge> outgoing;
		  List<ModelElement> modelElements;
		  int x;
		  int y;
		  int width;
		  int height;
		  int angle;
		
		  @override
		  GraphModel getRootElememt() => container.getRootElememt();
		}
		
		abstract class Edge implements ModelElement {
		  int dywaId;
		  bool $isDirty;
		  int dywaVersion;
		  String dywaName;
		  ModelElementContainer container;
		  Node source;
		  Node target;
		  List<BendingPoint> bendingPoints;
		
		  @override
		  GraphModel getRootElememt() => container.getRootElememt();
		}
		
		abstract class GraphModel extends PyroModelFile implements ModelElementContainer {
		  int dywaId;
		  bool $isDirty;
		  int dywaVersion;
		  String dywaName;
		  int width;
		  int height;
		  bool isPublic = false;
		  double scale;
		  String router;
		  String connector;
		  List<ModelElement> modelElements;
		  GlobalGraphModelSettings globalGraphModelSettings;
		
		  void mergeStructure(PyroFile ie)
		  {
		  	var gm = ie as GraphModel;
		    dywaName = gm.dywaName;
		    dywaVersion = gm.dywaVersion;
		    filename = gm.filename;
		    width = gm.width;
		    height = gm.height;
		    scale = gm.scale;
		    router = gm.router;
		    connector = gm.connector;
		    isPublic = gm.isPublic;
		  }
		
		  @override
		  GraphModel getRootElememt() => this;
		  
		  String $lower_type();
		  
		  String $displayName();
		}
		
		class BendingPoint implements PyroElement{
		  int dywaId;
		  int dywaVersion;
		  String dywaName;
		  int x;
		  int y;
		  BendingPoint({Map cache,dynamic jsog})
		  {
		    if(jsog!=null)
		    {
		      dywaId = jsog["dywaId"];
		      dywaVersion = jsog["dywaVersion"];
		      dywaName = jsog["dywaName"];
		      x = jsog["x"];
		      y = jsog["y"];
		
		    }
		    else{
		      dywaId=-1;
		      dywaName="BendingPoint";
		      dywaVersion=0;
		      x=0;
		      y=0;
		    }
		  }
		  @override
		  Map toJSOG(Map cache)
		  {
		    Map map = new Map();
		    if(cache.containsKey(dywaId)){
		      map['@ref']=cache[dywaId];
		      return map;
		    }
		    cache[dywaId]=(cache.length+1).toString();
		    map['@id']=cache[dywaId];
		    map['dywaRuntimeType']="info.scce.pyro.core.graphmodel.BendingPoint";
		    map['dywaId'] = dywaId;
		    map['dywaVersion'] = dywaVersion;
		    map['dywaName'] = dywaName;
		    map['x'] = x;
		    map['y'] = y;
		    return map;
		  }
		
		
		  BendingPoint fromJSOG(jsog, {Map cache}) {
		    return new BendingPoint(cache: cache,jsog: jsog);
		  }
		  @override
		  String $type() {
		    return "core.BendingPoint";
		  }
		  @override
		  void merge(PyroElement ie, {bool structureOnly: false, Map cache}) {
		    // TODO: implement merge
		  }
		}
		
		class LocalGraphModelSettings {
		  int dywaId;
		  int dywaVersion;
		  String dywaName;
		  String router;
		  String connector;
		
		  List<PyroFile> openedFiles;
		  
		  LocalGraphModelSettings({Map cache,dynamic jsog})
		  {
		    openedFiles = new List<GraphModel>();
		  	if(jsog!=null){
		  		dywaId = jsog["dywaId"];
		  		  dywaVersion = jsog["dywaVersion"];
		  		  dywaName = jsog["dywaName"];
		      router = jsog["router"];
		      connector = jsog["connector"];
		  		for(var g in jsog["openedFiles"]){
		  			if(g.containsKey("@ref")){
		  				openedFiles.add(cache[g["@ref"]]);
		  			} else {
		  		openedFiles.add( GraphModelDispatcher.dispatch(cache,g));
		  	}
		  		}
		  	}
		  	else{
		  		dywaId = -1;
		  		   dywaVersion = 0;
		  		   dywaName = "localGraphmodelSettings";
		  		   router = null;
		  		   connector = "normal";
		  		   openedFiles = new List<PyroFile>();
		  	}
		  }
		  
		  	static LocalGraphModelSettings fromJSOG(dynamic jsog){
		  		return new LocalGraphModelSettings(cache:new Map(),jsog:jsog);
		  	}
		  	
		  	Map toJSOG(Map cache){
		  		Map jsog = new Map();
		  		if(cache.containsKey(dywaId)){
		  	jsog["@ref"]=cache[dywaId];
		  } else {
		  	cache[dywaId]=(cache.length+1).toString();
		  	jsog["@id"]=cache[dywaId];
		  	jsog["dywaId"]=dywaId;
		  	jsog["dywaVersion"]=dywaVersion;
		  	jsog["dywaName"]=dywaName;
		    jsog["connector"]=connector;
		    jsog["router"]=router;
		  	jsog["openedFiles"]=openedFiles.map((n)=>n.toJSOG(cache));
		  }
		  return jsog;
		  	}
		}
		
		class GlobalGraphModelSettings {
		  int dywaId;
		    int dywaVersion;
		    String dywaName;
		  GlobalGraphModelSettings({Map cache,dynamic jsog})
		  {
		    if(jsog!=null){
		    	dywaId = jsog["dywaId"];
		    	  dywaVersion = jsog["dywaVersion"];
		    	  dywaName = jsog["dywaName"];
		    } else {
		    	dywaId = -1;
		    	 dywaVersion = 0;
		    	 dywaName = 'globalGraphmodelSettnings';
		    }
		  }
		  Map toJSOG(Map cache){
		  	Map jsog = new Map();
		  if(cache.containsKey(dywaId)){
		  	jsog["@ref"]=cache[dywaId];
		  		} else {
		  			cache[dywaId]=(cache.length+1).toString();
		  jsog["@id"]=cache[dywaId];
		  jsog["dywaId"]=dywaId;
		  jsog["dywaVersion"]=dywaVersion;
		  jsog["dywaName"]=dywaName;
		  		}
		  		return jsog;
			}
		}
		
		class PyroGraphModelType {
			«FOR g: gc.graphMopdels»
			  static final String «g.name.toUnderScoreCase» = "«g.name.toUnderScoreCase»";
			«ENDFOR»
		}		
		
		class PyroCrudOperation {
		  static final String CREATE = "CREATE";
		  static final String READ = "READ";
		  static final String UPDATE = "UPDATE";
		  static final String DELETE = "DELETE";
		}
		
		class PyroGraphModelPermissionVector {
		  int dywaId;
		  int dywaVersion;
		  String dywaName;
		  
		  PyroUser user;
		  PyroProject project;
		  String graphModelType;
		  List<String> permissions;
		  
		  PyroGraphModelPermissionVector({Map cache,dynamic jsog}) {
		    permissions = new List<String>();
		    
		    if(jsog != null) {
		  	  cache[jsog["@id"]]=this;
		      dywaId = jsog["dywaId"];
		      dywaVersion = jsog["dywaVersion"];
		      dywaName = jsog["dywaName"];
		        	  
		      if(jsog["project"].containsKey("@ref")) {
		        project = cache[jsog["project"]["@ref"]];
		      } else {
		        project = new PyroProject(cache:cache, jsog:jsog["project"]);
		      }
		      
		      if(jsog["user"].containsKey("@ref")){              
		        user = cache[jsog["user"]["@ref"]];
		      } else {
		      	user = new PyroUser(cache:cache, jsog:jsog["user"]);
		      }
		      
		      for(var value in jsog["permissions"]){  
			  	if (value == PyroCrudOperation.CREATE) {
			  	  permissions.add(PyroCrudOperation.CREATE);
			  	} else if (value == PyroCrudOperation.READ) {
			  	  permissions.add(PyroCrudOperation.READ);
			  	} else if (value == PyroCrudOperation.UPDATE) {
			  	  permissions.add(PyroCrudOperation.UPDATE);
			  	} else if (value == PyroCrudOperation.DELETE) {
			  	  permissions.add(PyroCrudOperation.DELETE);
			  	}
			  }
			  
			  «FOR g: gc.graphMopdels»
			  	if(jsog["graphModelType"] == PyroGraphModelType.«g.name.toUnderScoreCase») {
			  	  graphModelType = PyroGraphModelType.«g.name.toUnderScoreCase»;
			  	}
		      «ENDFOR»
		  	} else {
		  	  dywaId=-1;
		      dywaName="PyroGraphModelPermissionVector";
		      dywaVersion=0;
		      permissions = new List<String>();
		  	}
		  }
		  
		  static PyroGraphModelPermissionVector fromJSON(String s) {
		    return fromJSOG(cache: new Map(), jsog: jsonDecode(s));
		  }
		
		  static PyroGraphModelPermissionVector fromJSOG({Map cache, dynamic jsog}) {
		    return new PyroGraphModelPermissionVector(cache: cache, jsog: jsog);
		  }
		
		  String toJSON() {
		    return jsonEncode(toJSOG(new Map()));
		  }
		  
		  Map toJSOG(Map cache) {
		    Map jsog = new Map();
		    if(cache.containsKey(dywaId)){
		  		jsog["@ref"]=cache[dywaId];
		    } else {
		    	cache[dywaId]=(cache.length+1).toString();
		    	jsog["@id"]=cache[dywaId];
		    	jsog["dywaId"]=dywaId;
		  		jsog["dywaVersion"]=dywaVersion;
		  		jsog["dywaName"]=dywaName;
		  		jsog["user"]=user.toJSOG(cache);
		  		jsog["project"]=project.toJSOG(cache);
		  		jsog["permissions"]=permissions;
		  		jsog["graphModelType"]=graphModelType;
		    }
		    return jsog;
		  }
		}
		
		
		
		class PyroEditorGrid {
			int dywaId;
			int dywaVersion;
			String dywaName;
			
			PyroUser user;
			PyroProject project;
			List<PyroEditorGridItem> items;
			List<PyroEditorWidget> availableWidgets;
			
			PyroEditorGrid({Map cache,dynamic jsog}) {
			  items = new List();
			  availableWidgets = new List();
				
			  if(jsog != null) {
  				cache[jsog["@id"]]=this;
  				dywaId = jsog["dywaId"];
  				dywaVersion = jsog["dywaVersion"];
  				dywaName = jsog["dywaName"];
  				
  				for(var value in jsog["availableWidgets"]){
    			  if(value.containsKey("@ref")){
    			    availableWidgets.add(cache[value["@ref"]]);
    			  } else {
    			    availableWidgets.add(new PyroEditorWidget(cache:cache,jsog:value));
    			  }
    			}
    			
    			for(var value in jsog["items"]){
    			  if(value.containsKey("@ref")){
    			    items.add(cache[value["@ref"]]);
    			  } else {
    			    items.add(new PyroEditorGridItem(cache:cache,jsog:value));
    			  }
    			}
  				
  				if(jsog["project"].containsKey("@ref")){              
  				  project = cache[jsog["project"]["@ref"]];
  				} else {
  				  project = new PyroProject(cache:cache, jsog:jsog["project"]);
  				}
  						      
		      	if(jsog["user"].containsKey("@ref")){              
  			      user = cache[jsog["user"]["@ref"]];
  			    } else {
  			      user = new PyroUser(cache:cache, jsog:jsog["user"]);
  			    }
  			    
  			    
    			
    			
  			  } else {
  				dywaId=-1;
  		      	dywaName="PyroEditorGrid";
  				dywaVersion=0;
  			  }
			}
			
			static PyroEditorGrid fromJSON(String s) {
		 	  return fromJSOG(cache: new Map(), jsog: jsonDecode(s));
			}
						
			static PyroEditorGrid fromJSOG({Map cache, dynamic jsog}) {
			  return new PyroEditorGrid(cache: cache, jsog: jsog);
			}
			
			String toJSON() {
			  return jsonEncode(toJSOG(new Map()));
			}
			
			Map toJSOG(Map cache) {
		      Map jsog = new Map();
		      if(cache.containsKey(dywaId)){
		  		jsog["@ref"]=cache[dywaId];
		      } else {
		    	cache[dywaId]=(cache.length+1).toString();
		    	jsog["@id"]=cache[dywaId];
		    	jsog["dywaId"]=dywaId;
		  		jsog["dywaVersion"]=dywaVersion;
		  		jsog["dywaName"]=dywaName;
		  		jsog["user"]=user.toJSOG(cache);
		  		jsog["project"]=project.toJSOG(cache);
		  		jsog["items"]=items.map((i) => i.toJSOG(cache)).toList();
		  		jsog["availableWidgets"]=availableWidgets.map((i) => i.toJSOG(cache)).toList();
		      }
		      return jsog;
		    }
		}
		
		class PyroEditorWidget {
			int dywaId;
			int dywaVersion;
			String dywaName;
			
			PyroEditorGrid grid;
			PyroEditorGridItem area;
			String tab;
			String key;
			int position;
			
			PyroEditorWidget({Map cache,dynamic jsog}) {
			  if(jsog != null) {
				cache[jsog["@id"]]=this;
				dywaId = jsog["dywaId"];
				dywaVersion = jsog["dywaVersion"];
				dywaName = jsog["dywaName"];
					
				tab = jsog["tab"];
				key = jsog["key"];
				position = jsog["position"];
					
				if (jsog["area"] != null) {
				  if(jsog["area"].containsKey("@ref")){              
  	  			    area = cache[jsog["area"]["@ref"]];
  	  			  } else {
  	  			    area = new PyroEditorGridItem(cache:cache, jsog:jsog["area"]);
  	  			  }
				}
					
	  			if(jsog["grid"].containsKey("@ref")){              
	  			  grid = cache[jsog["grid"]["@ref"]];
	  			} else {
	  			  grid = new PyroEditorGrid(cache:cache, jsog:jsog["grid"]);
	  			}			
			  } else {
				dywaId=-1;
			    dywaName="PyroEditorWidget";
				dywaVersion=0;
			  }
			}
			
			static PyroEditorWidget fromJSON(String s) {
			  return fromJSOG(cache: new Map(), jsog: jsonDecode(s));
			}
			
			static PyroEditorWidget fromJSOG({Map cache, dynamic jsog}) {
			  return new PyroEditorWidget(cache: cache, jsog: jsog);
			}
			
			String toJSON() {
			  return jsonEncode(toJSOG(new Map()));
			}
			
			Map toJSOG(Map cache) {
		      Map jsog = new Map();
		      if(cache.containsKey(dywaId)){
		  		jsog["@ref"]=cache[dywaId];
		      } else {
		    	cache[dywaId]=(cache.length+1).toString();
		    	jsog["@id"]=cache[dywaId];
		    	jsog["dywaId"]=dywaId;
		  		jsog["dywaVersion"]=dywaVersion;
		  		jsog["dywaName"]=dywaName;
		  		jsog["tab"]=tab;
		  		jsog["key"]=key;
		  		jsog["position"]=position;
		  		jsog["area"]=area != null ? area.toJSOG(cache) : null;
		  		jsog["grid"]=grid.toJSOG(cache);
		      }
		      return jsog;
		    }
		}
		
		class PyroEditorGridItem {
			int dywaId;
		    int dywaVersion;
			String dywaName;
					  
			int x;
			int y;
			int width;
			int height;
			List<PyroEditorWidget> widgets;
			
			PyroEditorGridItem({Map cache,dynamic jsog}) {
			  widgets = new List();
		      if(jsog != null) {
				cache[jsog["@id"]]=this;
				dywaId = jsog["dywaId"];
				dywaVersion = jsog["dywaVersion"];
				dywaName = jsog["dywaName"];
				
				x = jsog["x"];
				y = jsog["y"];
				width = jsog["width"];
				height = jsog["height"];
				
				for(var value in jsog["widgets"]){
    			  if(value.containsKey("@ref")){
    			    widgets.add(cache[value["@ref"]]);
    			  } else {
    			    widgets.add(new PyroEditorWidget(cache:cache,jsog:value));
    			  }
    			}   			
			  } else {
				dywaId=-1;
		      	dywaName="PyroEditorGridItem";
				dywaVersion=0;
			  }
			}
			
			List<PyroEditorWidget> get visibleWidgets => widgets.where((w) => w.area != null).toList();
			
			static PyroEditorGridItem fromJSON(String s) {
			  return fromJSOG(cache: new Map(), jsog: jsonDecode(s));
			}
			
			static PyroEditorGridItem fromJSOG({Map cache, dynamic jsog}) {
			  return new PyroEditorGridItem(cache: cache, jsog: jsog);
			}
			
			String toJSON() {
			  return jsonEncode(toJSOG(new Map()));
			}
			
			Map toJSOG(Map cache) {
		      Map jsog = new Map();
		      if(cache.containsKey(dywaId)){
		  		jsog["@ref"]=cache[dywaId];
		      } else {
		    	cache[dywaId]=(cache.length+1).toString();
		    	jsog["@id"]=cache[dywaId];
		    	jsog["dywaId"]=dywaId;
		  		jsog["dywaVersion"]=dywaVersion;
		  		jsog["dywaName"]=dywaName;
		  		jsog["x"]=x;
		  		jsog["y"]=y;
		  		jsog["width"]=width;
		  		jsog["height"]=height;
		  		jsog["widgets"]=widgets.map((w) => w.toJSOG(cache)).toList();
		      }
		      return jsog;
		    }
		}
		
		
		
		class PyroOrganizationAccessRight {
		  static final String CREATE_PROJECTS = "CREATE_PROJECTS";
		  static final String EDIT_PROJECTS = "EDIT_PROJECTS";
		  static final String DELETE_PROJECTS = "DELETE_PROJECTS";
		}
		
		class PyroOrganizationAccessRightVector {
		  int dywaId;
		  int dywaVersion;
		  String dywaName;
		  
		  PyroUser user;
		  PyroOrganization organization;
		  List<String> accessRights;
		  
		  PyroOrganizationAccessRightVector({Map cache,dynamic jsog}) {
		  	accessRights = new List<String>();
		  	
		  	if(jsog != null) {
		  	  cache[jsog["@id"]]=this;
		      dywaId = jsog["dywaId"];
		      dywaVersion = jsog["dywaVersion"];
		      dywaName = jsog["dywaName"];
		        	  
		      if(jsog["organization"].containsKey("@ref")) {
		        organization = cache[jsog["organization"]["@ref"]];
		      } else {
		        organization = new PyroOrganization(cache:cache, jsog:jsog["organization"]);
		      }
		      
		      if(jsog["user"].containsKey("@ref")){              
		        user = cache[jsog["user"]["@ref"]];
		      } else {
		      	user = new PyroUser(cache:cache, jsog:jsog["user"]);
		      }
		      
		      for(var value in jsog["accessRights"]){  
			  	if (value == PyroOrganizationAccessRight.CREATE_PROJECTS) {
			  	  accessRights.add(PyroOrganizationAccessRight.CREATE_PROJECTS);
			  	} else if (value == PyroOrganizationAccessRight.EDIT_PROJECTS) {
			  	  accessRights.add(PyroOrganizationAccessRight.EDIT_PROJECTS);
			  	} else if (value == PyroOrganizationAccessRight.DELETE_PROJECTS) {
			  	  accessRights.add(PyroOrganizationAccessRight.DELETE_PROJECTS);
			  	}
			  }
		  	} else {
		  	  dywaId=-1;
		      dywaName="PyroOrganizationAccessRightVector";
		      dywaVersion=0;
		      accessRights = new List<String>();
		  	}
		  }
		  
		  static PyroOrganizationAccessRightVector fromJSON(String s) {
		    return fromJSOG(cache: new Map(), jsog: jsonDecode(s));
		  }
		
		  static PyroOrganizationAccessRightVector fromJSOG({Map cache, dynamic jsog}) {
		    return new PyroOrganizationAccessRightVector(cache: cache, jsog: jsog);
		  }
		
		  String toJSON() {
		    return jsonEncode(toJSOG(new Map()));
		  }
		
		  Map toJSOG(Map cache)
		  {
		    Map jsog = new Map();
		    if(cache.containsKey(dywaId)){
		  		jsog["@ref"]=cache[dywaId];
		    } else {
		    	cache[dywaId]=(cache.length+1).toString();
		    	jsog["@id"]=cache[dywaId];
		    	jsog["dywaId"]=dywaId;
		  		jsog["dywaVersion"]=dywaVersion;
		  		jsog["dywaName"]=dywaName;
		  		jsog["user"]=user.toJSOG(cache);
		  		jsog["organization"]=organization.toJSOG(cache);
		  		jsog["accessRights"]=accessRights;
		    }
		    return jsog;
		  }
		  
		}
		
		
		
		class PyroSystemRole { 
			static final String ADMIN = "ADMIN"; 
			static final String ORGANIZATION_MANAGER = "ORGANIZATION_MANAGER"; 
		}
		
		class PyroUser {
		  int dywaId;
		  int dywaVersion;
		  String dywaName;
		  String username;
		  String email;
		  String emailHash;
		  FileReference profilePicture;
		  
		  List<PyroProject> ownedProjects;
		  List<String> systemRoles;
		
		  PyroUser({Map cache,dynamic jsog})
		  {
		    ownedProjects = new List<PyroProject>();
		    systemRoles = new List<String>();
		    
		    if(jsog!=null)
		    {
		      cache[jsog["@id"]]=this;
		      dywaId = jsog["dywaId"];
		      dywaVersion = jsog["dywaVersion"];
		      dywaName = jsog["dywaName"];
		      username = jsog["username"];
		      email = jsog["email"];
		      emailHash = jsog["emailHash"];
		      
		      for(var value in jsog["ownedProjects"]){
		      	if(value.containsKey("@ref")){
		      		ownedProjects.add(cache[value["@ref"]]);
		      	} else {
		      		ownedProjects.add(new PyroProject(cache:cache,jsog:value));
		      	}
		      }
		      
		      if (jsog.containsKey("profilePicture") && jsog["profilePicture"] != null) {
  		  	  	profilePicture = new FileReference(jsog:jsog["profilePicture"]);
  		  	  }
		    
			  
			  
			  for(var value in jsog["systemRoles"]){  
			  	if (value == PyroSystemRole.ADMIN) {
			  		systemRoles.add(PyroSystemRole.ADMIN);
			  	} else if (value == PyroSystemRole.ORGANIZATION_MANAGER) {
			  		systemRoles.add(PyroSystemRole.ORGANIZATION_MANAGER);
			  	}
			  }
		    }
		    else{
		      dywaId=-1;
		      dywaName="PyroUser";
		      dywaVersion=0;
		      ownedProjects = new List<PyroProject>();
		      systemRoles = new List<String>();
		    }
		  }
		
		  static PyroUser fromJSON(String s)
		  {
		    return fromJSOG(new Map(),jsonDecode(s));
		  }
		
		  static PyroUser fromJSOG(Map cache,dynamic jsog)
		  {
		    return new PyroUser(cache: cache,jsog: jsog);
		  }
		
		  String toJSON()
		  {
		    return jsonEncode(toJSOG(new Map()));
		  }
		
		  Map toJSOG(Map cache)
		  {
		    Map jsog = new Map();
		    if(cache.containsKey(dywaId)){
		  		jsog["@ref"]=cache[dywaId];
		    } else {
		    	cache[dywaId]=(cache.length+1).toString();
		    	jsog["@id"]=cache[dywaId];
		    	jsog["dywaId"]=dywaId;
		  		jsog["dywaVersion"]=dywaVersion;
		  		jsog["dywaName"]=dywaName;
		  		jsog["username"]=username;
		  		jsog["email"]=email;
		  		jsog["emailHash"]=emailHash;
		  		jsog["ownedProjects"]=ownedProjects.map((n)=>n.toJSOG(cache)).toList();
		  		if (profilePicture != null) {
			    	jsog["profilePicture"]=profilePicture.toJSOG(cache);
		  		}
		    }
		    return jsog;
		  }
		}
		
		
		class PyroStyle {
		  int dywaId;
		  int dywaVersion;
		  String dywaName;
		  
		  String navBgColor;
		  String navTextColor;
		  String bodyBgColor;
		  String bodyTextColor;
		  String primaryBgColor;
		  String primaryTextColor;
		  FileReference logo;
		  
		  PyroStyle({Map cache, dynamic jsog}) {
		    if (jsog != null) {
		  	  cache[jsog["@id"]]=this;
		  	  dywaId = jsog["dywaId"];
		  	  dywaVersion = jsog["dywaVersion"];
		  	  dywaName = jsog["dywaName"];
		  	  
		  	  navBgColor = jsog["navBgColor"];
		  	  navTextColor = jsog["navTextColor"];
		  	  bodyBgColor = jsog["bodyBgColor"];
		  	  bodyTextColor = jsog["bodyTextColor"];
		  	  primaryBgColor = jsog["primaryBgColor"];
		  	  primaryTextColor = jsog["primaryTextColor"];
		  	  
		  	  if (jsog.containsKey("logo") && jsog["logo"] != null) {
		  	  	logo = new FileReference(jsog:jsog["logo"]);
		  	  }
		  	} else {
		  	  dywaId = -2;
			  dywaName = "PyroStyle";
			  dywaVersion = 0;
		  	}
		  }
		  
		  static PyroStyle fromJSON(String s) {
		    return PyroStyle.fromJSOG(cache: new Map(), jsog: jsonDecode(s));
		  }
		
		  static PyroStyle fromJSOG({Map cache, dynamic jsog}) {
		    return new PyroStyle(cache: cache, jsog: jsog);
		  }
		  
		  Map toJSOG(Map cache) {
		    Map jsog = new Map();
			if(cache.containsKey(dywaId)){
			  jsog["@ref"]=cache[dywaId];
			} else {
			  cache[dywaId]=(cache.length+1).toString();
			  jsog['@id']=cache[dywaId];
			  jsog['dywaId']=dywaId;
			  jsog['dywaVersion']=dywaVersion;
			  jsog['dywaName']=dywaName;
		
			  jsog['navBgColor']=navBgColor;
			  jsog['navTextColor']=navTextColor;
			  jsog['bodyBgColor']=bodyBgColor;
			  jsog['bodyTextColor']=bodyTextColor;
			  jsog['primaryBgColor']=primaryBgColor;
			  jsog['primaryTextColor']=primaryTextColor;
			  if (logo != null) {
			    jsog['logo']=logo.toJSOG(cache);
			  }
			}
			return jsog;
		  }
		}
		
		
		
		class PyroSettings {
		  int dywaId;
		  int dywaVersion;
		  String dywaName;
		  PyroStyle style;
		  bool globallyCreateOrganizations;
		  
		  PyroSettings({Map cache, dynamic jsog}) {
		  	if (jsog != null) {
		  	  cache[jsog["@id"]]=this;
		  	  dywaId = jsog["dywaId"];
		  	  dywaVersion = jsog["dywaVersion"];
		  	  dywaName = jsog["dywaName"];
		  	  globallyCreateOrganizations = jsog["globallyCreateOrganizations"];
		  	    	  
		  	  if (jsog.containsKey("style")) {
		  	  	style = new PyroStyle(cache:cache, jsog:jsog["style"]);
		  	  }
		  	} else {
		  	  dywaId = -1;
			  dywaName = "PyroSettings";
			  dywaVersion = 0;
			  style = new PyroStyle();
			  globallyCreateOrganizations = false;
		  	}
		  }
		  
		  static PyroSettings fromJSON(String s) {
		    return PyroSettings.fromJSOG(cache: new Map(), jsog: jsonDecode(s));
		  }
		
		  static PyroSettings fromJSOG({Map cache, dynamic jsog}) {
		    return new PyroSettings(cache: cache, jsog: jsog);
		  }
		
		  Map toJSOG(Map cache) {
			Map jsog = new Map();
			if(cache.containsKey(dywaId)){
				jsog["@ref"]=cache[dywaId];
			} else {
				cache[dywaId]=(cache.length+1).toString();
				jsog['@id']=cache[dywaId];
				jsog['dywaId']=dywaId;
				jsog['dywaVersion']=dywaVersion;
				jsog['dywaName']=dywaName;
				jsog['style']=style.toJSOG(cache);
				jsog['globallyCreateOrganizations'] = globallyCreateOrganizations;
			}
			return jsog;
		  }
		}
		
		
		
		class PyroOrganization {
		  int dywaId;
		  int dywaVersion;
		  String dywaName;
		  String name;
		  String description;
		  PyroStyle style;
		  List<PyroUser> owners;
		  List<PyroUser> members;
		  List<PyroProject> projects;
		  
		  PyroOrganization({Map cache, dynamic jsog}) {
		  	owners = new List<PyroUser>();
		  	members = new List<PyroUser>();
		  	projects = new List<PyroProject>();
		  	
		  	if (jsog != null) {
		  	  cache[jsog["@id"]]=this;
		  	  dywaId = jsog["dywaId"];
		  	  dywaVersion = jsog["dywaVersion"];
		  	  dywaName = jsog["dywaName"];
		  	  name = jsog["name"];
		  	  description = jsog["description"];
		  	    	  
		  	  
		  	  
		  	  if (jsog.containsKey("members")) {
		  	  	for(var value in jsog["members"]){
		  	  		if(value.containsKey("@ref")){
		  	  		  members.add(cache[value["@ref"]]);
		  	  		} else {
		  	  		  members.add(new PyroUser(cache:cache, jsog:value));
		  	  		}
		  	  	}
		  	  }
		  	  
		  	  if (jsog.containsKey("owners")) {
		  	  	for(var value in jsog["owners"]){
		          if(value.containsKey("@ref")){
		            owners.add(cache[value["@ref"]]);
		          } else {
		            owners.add(new PyroUser(cache:cache, jsog:value));
		          }
		        }
		  	  }
		  	  
		  	  if (jsog.containsKey("projects")) {
		  	  	for(var value in jsog["projects"]){
		          if(value.containsKey("@ref")){
		            projects.add(cache[value["@ref"]]);
		          } else {
		            projects.add(new PyroProject(cache:cache, jsog:value));
		          }
		        }
		  	  }
		  	  
		  	  if (jsog.containsKey("style")) {
		  	  	style = new PyroStyle(cache:cache, jsog:jsog["style"]);
		  	  }
		  	  
		  	} else {
		  	  dywaId = -1;
			  dywaName = "PyroOrganization";
			  dywaVersion = 0;
			  style = new PyroStyle();
		  	}
		  }
		  
		  void merge(PyroOrganization other) {
		    dywaId = other.dywaId;
		    dywaName = other.dywaName;
		    dywaVersion = other.dywaVersion;
		    name = other.name;
		  
		    projects.removeWhere((n) => other.projects.where((g) => n.dywaId==g.dywaId).isEmpty);
		    members.removeWhere((n) => other.members.where((g) => n.dywaId==g.dywaId).isEmpty);
		    owners.removeWhere((n) => other.owners.where((g) => n.dywaId==g.dywaId).isEmpty);
		  
		    //update files
		    projects.forEach((n){
		        n.merge(other.projects.where((g) => g.dywaId==n.dywaId).first);
		    });
		  
		    projects.addAll(other.projects.where((n) => projects.where((g) => n.dywaId==g.dywaId).isEmpty));
		    members.addAll(other.members.where((n) => members.where((g) => n.dywaId==g.dywaId).isEmpty));
		    owners.addAll(other.owners.where((n) => owners.where((g) => n.dywaId==g.dywaId).isEmpty));
		  
		  }
		  
		  static PyroOrganization fromJSON(String s) {
		    return PyroOrganization.fromJSOG(cache: new Map(), jsog: jsonDecode(s));
		  }
		
		  static PyroOrganization fromJSOG({Map cache, dynamic jsog}) {
		    return new PyroOrganization(cache: cache, jsog: jsog);
		  }
		
		  Map toJSOG(Map cache) {
			Map jsog = new Map();
			if(cache.containsKey(dywaId)){
				jsog["@ref"]=cache[dywaId];
			} else {
				cache[dywaId]=(cache.length+1).toString();
				jsog['@id']=cache[dywaId];
				jsog['dywaId']=dywaId;
				jsog['dywaVersion']=dywaVersion;
				jsog['dywaName']=dywaName;
				jsog['name']=name;
				jsog['description']=description;
				jsog['owners']=owners.map((n)=>n.toJSOG(cache)).toList();
				jsog['members']=members.map((n)=>n.toJSOG(cache)).toList();
				jsog['projects']=projects.map((n)=>n.toJSOG(cache)).toList();
				jsog['style']=style.toJSOG(cache);
			}
			return jsog;
		  }
		  
		  List<PyroUser> get users => new List.from(owners)..addAll(members);
		}
		
		class PyroProject extends PyroFolder{
		  int dywaId;
		  int dywaVersion;
		  String dywaName;
		  PyroUser owner;
		  String name;
		  String description;
		  PyroOrganization organization;
		  List<PyroFolder> innerFolders;
		  List<PyroFile> files;
		
		  PyroProject({Map cache,dynamic jsog})
		  {
		    innerFolders = new List<PyroFolder>();
		    files = new List<PyroFile>();
		    if(jsog!=null)
		    {
		      cache[jsog["@id"]]=this;
		  	  dywaId = jsog["dywaId"];
		  	  dywaVersion = jsog["dywaVersion"];
		  	  dywaName = jsog["dywaName"];
		  	  description = jsog["description"];
		  	  name = jsog["name"];
		  	  
		  	  if(jsog["organization"].containsKey("@ref")){
		  	   	organization = cache[jsog["organization"]["@ref"]];
		  	   } else {
		  	   	organization = new PyroOrganization(cache:cache,jsog:jsog["organization"]);
		  	   }
		  	  
		  	  if(jsog["owner"].containsKey("@ref")){
		  	   	owner = cache[jsog["owner"]["@ref"]];
		  	   } else {
		  	   	owner = new PyroUser(cache:cache,jsog:jsog["owner"]);
		  	   }
		  	     	  
			    if(jsog.containsKey("innerFolders")){
			      for(var value in jsog["innerFolders"]){
			        if(value.containsKey("@ref")){
			          innerFolders.add(cache[value["@ref"]]);
			        } else {
			          innerFolders.add(new PyroFolder(cache:cache,jsog:value));
			        }
			      }
			    }
			    
			    if(jsog.containsKey("files")){
			      for(var value in jsog["files"]){
			        if(value.containsKey("@ref")){
			          files.add(cache[value["@ref"]]);
			        } else {
			          if(value["__type"]=='PyroBinaryFileImpl') {
			    			files.add(new PyroBinaryFile(cache:cache,jsog:value));
			    		} else if(value["__type"]=='PyroTextualFileImpl') {
			    			files.add(new PyroTextualFile(cache:cache,jsog:value));
			    		} else if(value["__type"]=='PyroURLFileImpl') {
			    			files.add(new PyroURLFile(cache:cache,jsog:value));
			    		} else {
				    		files.add(GraphModelDispatcher.dispatch(cache,value));
			    		}
			        }
			      }
			    }
			  }
			   else{
			   	 dywaId=-1;
			   	 dywaName="PyroUser";
			   	 dywaVersion=0;
			   	 innerFolders = new List<PyroFolder>();
			   	 files = new List<PyroFile>();
			   }
			 }
		
		  static PyroProject fromJSON(String s)
		  {
		    return PyroProject.fromJSOG(cache: new Map(),jsog: jsonDecode(s));
		  }
		
		  static PyroProject fromJSOG({Map cache,dynamic jsog})
		  {
		    return new PyroProject(cache: cache,jsog: jsog);
		  }
		
			Map toJSOG(Map cache) {
				Map jsog = new Map();
				if(cache.containsKey(dywaId)){
					jsog["@ref"]=cache[dywaId];
				} else {
					cache[dywaId]=(cache.length+1).toString();
					jsog['@id']=cache[dywaId];
					jsog['dywaId']=dywaId;
					jsog['dywaVersion']=dywaVersion;
					jsog['dywaName']=dywaName;
					jsog['name']=name;
					if(owner!=null) {
						jsog['owner']=owner.toJSOG(cache);
					}
					if(organization!=null) {
						jsog['organization']=organization.toJSOG(cache);
					}
					jsog['description']=description;
					jsog['innerFolders']=innerFolders.map((n)=>n.toJSOG(cache)).toList();
					jsog['files']=files.map((n)=>n.toJSOG(cache)).toList();
				}
				return jsog;
			}
		
		  @override
		  String fullPath(PyroFile pf) {
		
		    if(files.where((f)=>f.dywaId==pf.dywaId).isNotEmpty) {
		      return "";
		    }
		
		    if(innerFolders.isEmpty) {
		      return null;
		    }
		
		    List<String> paths = innerFolders.map((f)=>f.fullPath(pf)).where((f)=>f!=null).toList();
		    if(paths.isEmpty) {
		      return null;
		    }
		    return paths[0]+"/";
		  }
		
		}
		
		class PyroFolder {
		  int dywaId;
		  int dywaVersion;
		  String dywaName;
		  String name;
		  List<PyroFolder> innerFolders;
		  List<PyroFile> files;
		
		  PyroFolder({Map cache,dynamic jsog})
		  {
		    innerFolders = new List<PyroFolder>();
		    files = new List<PyroFile>();
		    if(jsog!=null)
		    {
		      cache[jsog["@id"]]=this;
		      dywaId = jsog["dywaId"];
		      dywaVersion = jsog["dywaVersion"];
		      dywaName = jsog["dywaName"];
		      name = jsog["name"];
		      for(var value in jsog["innerFolders"]){
		      	if(value.containsKey("@ref")){
		      			innerFolders.add(cache[value["@ref"]]);
		      		} else {
		      			innerFolders.add(new PyroFolder(cache:cache,jsog:value));
		      		}
		      	}
		      for(var value in jsog["files"]){
		    if(value.containsKey("@ref")){
		    		files.add(cache[value["@ref"]]);
		    	} else {
		    		if(value["__type"]=='PyroBinaryFileImpl') {
		    			files.add(new PyroBinaryFile(cache:cache,jsog:value));
		    		} else if(value["__type"]=='PyroTextualFileImpl') {
		    			files.add(new PyroTextualFile(cache:cache,jsog:value));
		    		} else if(value["__type"]=='PyroURLFileImpl') {
		    			files.add(new PyroURLFile(cache:cache,jsog:value));
		    		} else {
			    		files.add(GraphModelDispatcher.dispatch(cache,value));
		    		}
		    	}
		    }
		    }
		    else{
		      dywaId=-1;
		      dywaName="PyroUser";
		      dywaVersion=0;
		      name = "";
		      innerFolders = new List<PyroFolder>();
		      files = new List<PyroFile>();
		    }
		  }
		  
		  Map toJSOG(Map cache) {
			Map jsog = new Map();
			if(cache.containsKey(dywaId)){
				jsog["@ref"]=cache[dywaId];
			} else {
				cache[dywaId]=(cache.length+1).toString();
				jsog["@id"]=cache[dywaId];
				jsog['dywaId']=dywaId;
				jsog['dywaVersion']=dywaVersion;
				jsog['dywaName']=dywaName;
				jsog['name']=name;
				jsog['innerFolders']=innerFolders.map((n)=>n.toJSOG(cache)).toList();
				jsog['files']=files.map((n)=>n.toJSOG(cache)).toList();
			}
			return jsog;
		}
		
		  static PyroFolder fromJSON(String s)
		  {
		    return PyroFolder.fromJSOG(new Map(),jsonDecode(s));
		  }
		
		  static PyroFolder fromJSOG(Map cache,dynamic jsog)
		  {
		    return new PyroFolder(cache: cache,jsog: jsog);
		  }
		
		  List<GraphModel> allGraphModels()
		  {
		     List<GraphModel> gs = new List();
		     gs.addAll(this.files.whereType<GraphModel>().toList());
		     gs.addAll(this.innerFolders.expand((n) => n.allGraphModels()).toList());
		     return gs;
		  }
		
		  List<PyroFile> allFiles()
		  {
		     List<PyroFile> gs = new List();
		     gs.addAll(this.files.whereType<PyroFile>().toList());
		     gs.addAll(this.innerFolders.expand((n) => n.allFiles()).toList());
		     return gs;
		  }
		
		  void merge(PyroFolder pp)
		  {
		    dywaId = pp.dywaId;
		    dywaName = pp.dywaName;
		    dywaVersion = pp.dywaVersion;
		    name = pp.name;
		
		    //remove missing files
		    files.removeWhere((n) => pp.files.where((g) => n.dywaId==g.dywaId).isEmpty);
		    //remove missing folders
		    innerFolders.removeWhere((n) => pp.innerFolders.where((g) => n.dywaId==g.dywaId).isEmpty);
		
		    //update files
		    files.forEach((n){
		      n.mergeStructure(pp.files.where((g) => g.dywaId==n.dywaId).first);
		
		    });
		    //update folders
		    innerFolders.forEach((n){
		      n.merge(pp.innerFolders.where((g) => g.dywaId==n.dywaId).first);
		
		    });
		
		    //add new files
		    files.addAll(pp.files.where((n) => files.where((g) => n.dywaId==g.dywaId).isEmpty));
		    //add new folder
		    innerFolders.addAll(pp.innerFolders.where((n) => innerFolders.where((g) => n.dywaId==g.dywaId).isEmpty));
		  }
		  
		  String fullPath(PyroFile pf) {
		    
		    if(files.where((f)=>f.dywaId==pf.dywaId).isNotEmpty) {
		      return this.name;
		    }
		    
		    if(innerFolders.isEmpty) {
		      return null;
		    }
		    
		    List<String> paths = innerFolders.map((f)=>f.fullPath(pf)).where((f)=>f!=null).toList();
		    if(paths.isEmpty) {
		      return null;
		    }
		    return this.name+"/"+paths[0];
		  }
		 
		
		}
		
		abstract class PyroFile {
		  int dywaId;
		  int dywaVersion;
		  String dywaName;
		  String filename;
		  String extension;
		  
		  Map toJSOG(Map cache);
		
		  void mergeStructure(PyroFile pf);
		
		  String $type() => "core.PyroFile";
		 
		}
		
		abstract class PyroModelFile extends PyroFile {
		 
		}
		
		class PyroTextualFile extends PyroFile {
		  String content;
		
		  PyroTextualFile({Map cache,dynamic jsog})
		  {
		    if(jsog!=null)
		    {
		      cache[jsog["@id"]]=this;
		      dywaId = jsog["dywaId"];
		      dywaVersion = jsog["dywaVersion"];
		      dywaName = jsog["dywaName"];
		      filename = jsog["filename"];
		      extension = jsog["extension"];
		      content = jsog["content"];
		      
		    }
		    else{
		      dywaId=-1;
		      dywaName="PyroUser";
		      dywaVersion=0;
		      filename = "";
		      extension = "";
		      content = "";
		    }
		  }
		  
		  Map toJSOG(Map cache) {
			Map jsog = new Map();
			if(cache.containsKey(dywaId)){
				jsog["@ref"]=cache[dywaId];
			} else {
				cache[dywaId]=(cache.length+1).toString();
				jsog["@id"]=cache[dywaId];
				jsog['dywaId']=dywaId;
				jsog['dywaVersion']=dywaVersion;
				jsog['dywaName']=dywaName;
				jsog['filename']=filename;
				jsog['extension']=extension;
				jsog['content']=content;
			}
			return jsog;
		}
		
		  static PyroTextualFile fromJSON(String s)
		  {
		    return PyroTextualFile.fromJSOG(new Map(),jsonDecode(s));
		  }
		
		  static PyroTextualFile fromJSOG(Map jsog,dynamic cache)
		  {
		    return new PyroTextualFile(cache: cache,jsog: jsog);
		  }
		
		  void merge(PyroFile ie)
		  {
		    var pp = ie as PyroTextualFile;
		    dywaId = pp.dywaId;
		    dywaName = pp.dywaName;
		    dywaVersion = pp.dywaVersion;
		    filename = pp.filename;
			content = pp.content;
		  }
		 
		
		  @override
		  void mergeStructure(PyroFile ie) {
		    
		    this.merge(ie);
		  }
		}
		
		class PyroURLFile extends PyroFile {
		  String url;
		
		  PyroURLFile({Map cache,dynamic jsog})
		  {
		    if(jsog!=null)
		    {
		      cache[jsog["@id"]]=this;
		      dywaId = jsog["dywaId"];
		      dywaVersion = jsog["dywaVersion"];
		      dywaName = jsog["dywaName"];
		      filename = jsog["filename"];
		      extension = jsog["extension"];
		      url = jsog["url"];
		      
		    }
		    else{
		      dywaId=-1;
		      dywaName="PyroUser";
		      dywaVersion=0;
		      filename = "";
		      extension = "";
		      url = "";
		    }
		  }
		  
		  Map toJSOG(Map cache) {
			Map jsog = new Map();
			if(cache.containsKey(dywaId)){
				jsog["@ref"]=cache[dywaId];
			} else {
				cache[dywaId]=(cache.length+1).toString();
				jsog["@id"]=cache[dywaId];
				jsog['dywaId']=dywaId;
				jsog['dywaVersion']=dywaVersion;
				jsog['dywaName']=dywaName;
				jsog['filename']=filename;
				jsog['extension']=extension;
				jsog['url']=url;
			}
			return jsog;
		}
		
		  static PyroURLFile fromJSON(String s)
		  {
		    return PyroURLFile.fromJSOG(new Map(),jsonDecode(s));
		  }
		
		  static PyroURLFile fromJSOG(Map jsog,dynamic cache)
		  {
		    return new PyroURLFile(cache: cache,jsog: jsog);
		  }
		
		  void merge(PyroFile ie)
		  {
		    var pp = ie as PyroURLFile;
		    dywaId = pp.dywaId;
		    dywaName = pp.dywaName;
		    dywaVersion = pp.dywaVersion;
		    filename = pp.filename;
			url = pp.url;
		  }
		 
		
		  @override
		  void mergeStructure(PyroFile ie) {
		    this.merge(ie);
		  }
		}
		
		class PyroBinaryFile extends PyroFile {
		  FileReference file;
		
		  PyroBinaryFile({Map cache,dynamic jsog})
		  {
		    if(jsog!=null)
		    {
		      cache[jsog["@id"]]=this;
		      dywaId = jsog["dywaId"];
		      dywaVersion = jsog["dywaVersion"];
		      dywaName = jsog["dywaName"];
		      filename = jsog["filename"];
		      extension = jsog["extension"];
		      file = new FileReference(jsog:jsog["file"]);
		      
		    }
		    else{
		      dywaId=-1;
		      dywaName="PyroUser";
		      dywaVersion=0;
		      filename = "";
		      extension = "";
		      file = null;
		    }
		  }
		  
		  Map toJSOG(Map cache) {
			Map jsog = new Map();
			if(cache.containsKey(dywaId)){
				jsog["@ref"]=cache[dywaId];
			} else {
				cache[dywaId]=(cache.length+1).toString();
				jsog["@id"]=cache[dywaId];
				jsog['dywaId']=dywaId;
				jsog['dywaVersion']=dywaVersion;
				jsog['dywaName']=dywaName;
				jsog['filename']=filename;
				jsog['extension']=extension;
				jsog['file']=file.toJSOG(cache);
			}
			return jsog;
		}
		
		  static PyroBinaryFile fromJSON(String s)
		  {
		    return PyroBinaryFile.fromJSOG(new Map(),jsonDecode(s));
		  }
		
		  static PyroBinaryFile fromJSOG(Map jsog,dynamic cache)
		  {
		    return new PyroBinaryFile(cache: cache,jsog: jsog);
		  }
		
		  void merge(PyroFile ie)
		  {
		    var pp = ie as PyroBinaryFile;
		    dywaId = pp.dywaId;
		    dywaName = pp.dywaName;
		    dywaVersion = pp.dywaVersion;
		    filename = pp.filename;
			file = pp.file;
		  }
		
		  @override
		  void mergeStructure(PyroFile ie) {
		    this.merge(ie);
		  }
		
		}
	'''
}
