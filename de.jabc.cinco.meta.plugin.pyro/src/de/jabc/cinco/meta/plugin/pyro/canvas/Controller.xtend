package de.jabc.cinco.meta.plugin.pyro.canvas

import de.jabc.cinco.meta.plugin.pyro.util.Generatable
import de.jabc.cinco.meta.plugin.pyro.util.GeneratorCompound
import java.util.HashMap
import java.util.LinkedHashMap
import java.util.LinkedList
import java.util.List
import java.util.Map
import java.util.regex.Matcher
import java.util.regex.Pattern
import mgl.ContainingElement
import mgl.Edge
import mgl.GraphModel
import mgl.IncomingEdgeElementConnection
import mgl.Node
import mgl.NodeContainer
import mgl.OutgoingEdgeElementConnection
import org.eclipse.emf.ecore.EObject
import style.AbstractShape
import style.ContainerShape
import style.EdgeStyle
import style.MultiText
import style.NodeStyle
import style.Styles
import style.Text
import java.util.Set

class Controller extends Generatable{
	
	new(GeneratorCompound gc) {
		super(gc)
	}
	
	def fileNameController() '''controller.js'''

	def contentController(GraphModel g,Styles styles) {
		val nodes = g.nodesTopologically.filter[!isIsAbstract]
		val edges = g.edgesTopologically.filter[!isIsAbstract]
		

	'''
	var $graph_«g.name.lowEscapeDart» = null;
	var $paper_«g.name.lowEscapeDart» = null;
	var $map_paper_«g.name.lowEscapeDart» = null;
	var $router_«g.name.lowEscapeDart» = 'manhattan';
	var $connector_«g.name.lowEscapeDart» = 'rounded';
	var $graphmodel_id_«g.name.lowEscapeDart» = -1;
	var $_disable_events_«g.name.lowEscapeDart» = true;
	var $checkResults_«g.name.lowEscapeDart» = [];
	var $cursor_manager_«g.name.lowEscapeDart» = {};
	var $«g.name.lowEscapeDart»_lib = null;
	
	
	var $cb_functions_«g.name.lowEscapeDart» = {};

	
	function load_«g.name.lowEscapeDart»(
	    w,
	    h,
	    scale,
	    graphmodelid,
	    router,
	    connector,
	    //callback afert initialization
	    initialized,
	    //message callbacks
	    cb_element_selected,
	    cb_graphmodel_selected,
	    cb_update_bendpoint,
	    cb_can_move_node,
	    cb_can_reconnect_edge,
	    cb_get_valid_targets,
	    cb_is_valid_connection,
	    cb_get_valid_containers,
	    cb_is_valid_container,
	    cb_get_custom_actions,
	    cb_fire_dbc_actions,
	    cb_delete_selected,
	    cb_cursor_moved,
	    cb_property_update,
	    «FOR elem:g.elements.filter[!isIsAbstract] SEPARATOR ","»
	    	«IF elem instanceof Node»
	    cb_create_node_«elem.name.lowEscapeDart»,
	    cb_remove_node_«elem.name.lowEscapeDart»,
	    cb_move_node_«elem.name.lowEscapeDart»,
	    cb_resize_node_«elem.name.lowEscapeDart»,
	    cb_rotate_node_«elem.name.lowEscapeDart»
	    	«ENDIF»
	    	«IF elem instanceof Edge»
		cb_create_edge_«elem.name.lowEscapeDart»,
		cb_remove_edge_«elem.name.lowEscapeDart»,
		cb_reconnect_edge_«elem.name.lowEscapeDart»
	    	«ENDIF»
	    «ENDFOR»
	    
	) {
	    $router_«g.name.lowEscapeDart» = router;
	    $graphmodel_id_«g.name.lowEscapeDart» = graphmodelid;
	    $connector_«g.name.lowEscapeDart» = connector;
	    $checkResults_«g.name.lowEscapeDart» = [];
	    $property_persist_fun = cb_property_update;
	
	    $cb_functions_«g.name.lowEscapeDart» = {
	    	cb_element_selected:cb_element_selected,
		    cb_update_bendpoint:cb_update_bendpoint,
		    cb_can_move_node:cb_can_move_node,
		    cb_can_reconnect_edge:cb_can_reconnect_edge,
		    cb_get_valid_targets:cb_get_valid_targets,
		    cb_is_valid_connection:cb_is_valid_connection,
		    cb_get_valid_containers:cb_get_valid_containers,
		    cb_is_valid_container:cb_is_valid_container,
		    cb_cursor_moved:cb_cursor_moved,
	    	«FOR elem:g.elements.filter[!isIsAbstract] SEPARATOR ","»
		    	«IF elem instanceof Node»
		    cb_create_node_«elem.name.lowEscapeDart»:cb_create_node_«elem.name.lowEscapeDart»,
		    cb_remove_node_«elem.name.lowEscapeDart»:cb_remove_node_«elem.name.lowEscapeDart»,
		    cb_move_node_«elem.name.lowEscapeDart»:cb_move_node_«elem.name.lowEscapeDart»,
		    cb_resize_node_«elem.name.lowEscapeDart»:cb_resize_node_«elem.name.lowEscapeDart»,
		    cb_rotate_node_«elem.name.lowEscapeDart»:cb_rotate_node_«elem.name.lowEscapeDart»
		    	«ENDIF»
		    	«IF elem instanceof Edge»
			cb_create_edge_«elem.name.lowEscapeDart»:cb_create_edge_«elem.name.lowEscapeDart»,
			cb_remove_edge_«elem.name.lowEscapeDart»:cb_remove_edge_«elem.name.lowEscapeDart»,
			cb_reconnect_edge_«elem.name.lowEscapeDart»:cb_reconnect_edge_«elem.name.lowEscapeDart»
		    	«ENDIF»
		    «ENDFOR»
	    };
	
		
	    $graph_«g.name.lowEscapeDart» = new joint.dia.Graph;
	    $paper_«g.name.lowEscapeDart» = new joint.dia.Paper({
	
	        el: document.getElementById('paper_«g.name.lowEscapeDart»'),
	        width: w,
	        height: h,
	        gridSize: 5,
	        drawGrid: true,
	        model: $graph_«g.name.lowEscapeDart»,
	        snapLinks: false,
	        linkPinning: false,
	        elementView: constraint_element_view($graph_«g.name.lowEscapeDart»,highlight_valid_targets_«g.name.lowEscapeDart»,highlight_valid_containers_«g.name.lowEscapeDart»),
	        embeddingMode: true,
	
	        highlighting: {
	            'default': {
	                name: 'stroke',
	                options: {
	                    padding: 10
	                }
	            },
	            'elementAvailability': {
	                name: 'stroke',
	                options: {
	                    'color':'green',
	                    padding: 10,
	                    attrs: {
	                        'stroke':'green',
	                        'fill-opacity':0.1,
	                        'fill':'green',
	                    }
	                }
	            },
	            'embedding': {
	                name: 'stroke',
	                options: {
	                    'color':'green',
	                    padding: 10,
	                    attrs: {
	                        'stroke':'green',
	                        'fill-opacity':0.1,
	                        'fill':'green',
	                    }
	                }
	            }
	        },
	        validateEmbedding: function(childView, parentView) {
	        	if(childView == null) {
	        		return false;
	        	}
	        	var nodeId = childView.model.attributes.attrs.dywaId;
	        	if(parentView == null) {
	        		return $cb_functions_«g.name.lowEscapeDart».cb_is_valid_container(nodeId,graphmodelid);
	        	}
	        	var parentId = parentView.model.attributes.attrs.dywaId;
	            return $cb_functions_«g.name.lowEscapeDart».cb_is_valid_container(nodeId,parentId);
	        },
	
	        validateConnection: function(cellViewS, magnetS, cellViewT, magnetT, end, linkView) {
	        	if(cellViewS == null || cellViewT == null || linkView == null) {
	        		return false;
	        	}
	            var sourceId = cellViewS.model.attributes.attrs.dywaId;
	            var targetId = cellViewT.model.attributes.attrs.dywaId;
	            var edgeId = linkView.model.attributes.attrs.dywaId;
	            return $cb_functions_«g.name.lowEscapeDart».cb_is_valid_connection(edgeId,sourceId,targetId);
	        }
	    });
	    $paper_«g.name.lowEscapeDart».options.multiLinks = false;
	    $paper_«g.name.lowEscapeDart».options.markAvailable = true;
	    $paper_«g.name.lowEscapeDart».options.restrictTranslate=false;
	    $paper_«g.name.lowEscapeDart».options.drawGrid= { name: 'mesh', args: { color: 'black' }};
		$paper_«g.name.lowEscapeDart».scale(scale);
		$paper_«g.name.lowEscapeDart».options.defaultConnectionPoint = {
		    name: 'boundary',
		    args: {
		        sticky: true
		    }
		};
		
	    /*
	    Register event listener triggering the callbacks to the NG-App
	     */
	     
 	     $«g.name.lowEscapeDart»_lib = pyroGlueLines(joint,$graph_«g.name.lowEscapeDart», $paper_«g.name.lowEscapeDart», {
 	       shapeType: 'pyro.GlueLine',
 	       verticalLineColor: 'red',
 	       horizontalLineColor: 'blue',
 	       middleLineColor: 'green',
 	       offset: 10
 	     });
 	     
 	     /**
  	       * Element has been added
 	       * change selection
 	       * show properties
 	       */
 	    $graph_«g.name.lowEscapeDart».on('change:position', _.debounce($«g.name.lowEscapeDart»_lib.handleChangePosition, 10));
	
	    /**
	     * Graphmodel (Canvas) has been clicked
	     */
	    $paper_«g.name.lowEscapeDart».on('blank:pointerup', function(evt, x, y) {
	    	remove_edge_creation_menu();
	    	remove_context_menu();
	        deselect_all_elements(null,$paper_«g.name.lowEscapeDart»,$graph_«g.name.lowEscapeDart»);
	        cb_graphmodel_selected();
	        console.log("graphmodel clicked");
	    });
	    $paper_«g.name.lowEscapeDart».on('blank:pointerdown', function(evt, x, y) {
	        evt.data = { action: 'paper_drag', paper_drag_x: x, paper_drag_y: y };
	    });
	    
	    $paper_«g.name.lowEscapeDart».on('blank:pointermove', function(evt, x, y) {
	        var trans = $paper_«g.name.lowEscapeDart».translate();
	        if(x - evt.data.x != 0 || y - evt.data.y != 0) {
	        	var sc = $paper_«g.name.lowEscapeDart».scale();
	            $paper_«g.name.lowEscapeDart».translate(
	               (trans.tx) + Math.round((x - evt.data.paper_drag_x) * sc.sx),
	               (trans.ty) + Math.round((y - evt.data.paper_drag_y) * sc.sy)
	            );
	        }
	    });
	    
	    $paper_«g.name.lowEscapeDart».on('blank:mousewheel', function(evt, x, y,delta) {
			zoom_paper($paper_«g.name.lowEscapeDart»,evt,x,y,delta);
		});
		$paper_«g.name.lowEscapeDart».on('cell:mousewheel', function(cv,evt, x, y,delta) {
			zoom_paper($paper_«g.name.lowEscapeDart»,evt,x,y,delta);
		});
		
		(function() {
			/**
			 * emit the cursor position of the user on the paper
			 * fire mousemove event every n seconds
			 */
		
		    var UPDATE_DELAY = 2000; // ms
		
			function handleMousemove(evt) {
				if ($paper_«g.name.lowEscapeDart» == null || $cursor_manager_resizestart) return; 
				var pointOnPaper = $paper_«g.name.lowEscapeDart».clientToLocalPoint(evt.clientX, evt.clientY);
				$cb_functions_«g.name.lowEscapeDart».cb_cursor_moved(Math.round(pointOnPaper.x),Math.round(pointOnPaper.y));
			}
			
			var handleMousemoveThrottled = _.throttle(handleMousemove, UPDATE_DELAY);
		
			$(document.getElementById('paper_«g.name.lowEscapeDart»')).on('mousemove', handleMousemoveThrottled);
		}());
		
		$cursor_manager_«g.name.lowEscapeDart» = (function(){
		
		  // after how many seconds of inactivity the cursor is removed
		  var REMOVE_TIMEOUT = 4000;
		  
		  // how many chars of the username should be displayed
		  var MAX_USERNAME_LENGTH = 8;
		  
		  var ANIMTATION_TRANSITION = {
		  	delay: 0,
		  	duration: 400,
		  	valueFunction: joint.util.interpolate.number,
		    timingFunction: joint.util.timing.linear
		  };
		  
		  // userId -> color
		  var color_cache = {};
		  
		  // userId -> (cell, timer)
		  var cursors = {};
		  
		  function handle_cursor_timeout(userId) {
		    cursors[userId].timer = window.setTimeout(function() {
			  cursors[userId].cell.remove();
			  delete cursors[userId];
			}, REMOVE_TIMEOUT);
		  }
		  
		  function add_cursor(userId, username, x, y) {      
		      var color = generate_cursor_color(userId);
		      var cursor = new joint.shapes.pyro.PyroCursor(); 
		      cursor.attr('.pyro-cursor/fill', color);
		      cursor.attr('.pyro-cursor-tooltip/fill', color);
		      $graph_«g.name.lowEscapeDart».addCell(cursor);
		      
		      // update label to username
		      // update width of tooltip accordingly
		      // querySelectorAll because of the same id is also used in the mini map
		      var cursorNodes = document.querySelectorAll('g[model-id="' + cursor.id + '"]');
		      cursorNodes.forEach(function(cursorNode){
		        var tooltip = cursorNode.querySelector('.pyro-cursor-tooltip'); 
		        var text = cursorNode.querySelector('text');
		        text.textContent = username.length > MAX_USERNAME_LENGTH ? username.substring(0, 8) + '...' : username;
		        tooltip.setAttribute('width', text.getBBox().width + 8);
		      });
		          
		      cursors[userId] = {
		        cell: cursor,
		        timer: null
		      };
		      
		      handle_cursor_timeout(userId);
		    }
					  
		  function generate_random_number(min, max) {
		    return Math.random() * (+max - +min) + +min
		  }
		  
		  function generate_cursor_color(userId) {
		    if (color_cache[userId] != null) {
		      return color_cache[userId];
		    }
		  
		    var h = generate_random_number(0, 359);
		    var s = generate_random_number(35, 100);	// not so super grayish colors
		    var l = generate_random_number(15, 25);		// low lighness so that white text color is readable
		  
		    var color = 'hsl(' + h + ', ' + s + '%, ' + l + '%)';
		    color_cache[userId] = color;
		    return color;
		  }
		  
		  return {
			update_cursor: function(userId, username, x, y) {
		  	  if (cursors[userId] == null) {
		  	    add_cursor(userId, username, x, y);
		      } else {
		        window.clearTimeout(cursors[userId].timer);
		        
		  	    var cursor = cursors[userId].cell;
		  	    cursor.transition('position/x', x, ANIMTATION_TRANSITION);
		  	    cursor.transition('position/y', y, ANIMTATION_TRANSITION);
		  	    
		  	    handle_cursor_timeout(userId);
		      }
		    }
		  }
		}());
	
«««	    /**
«««	     * Element has been selected
«««	     * change selection
«««	     * show properties
«««	     */
«««	    $paper_«g.name.lowEscapeDart».on('cell:pointerclick', function(cellView,evt, x, y) {
«««	    	remove_edge_creation_menu();
«««	    	remove_context_menu();
«««	        console.log("element clicked");
«««	    });
	    
	    /**
		* Link has been selected
		* change selection
		* show properties
		*/
		$paper_«g.name.lowEscapeDart».on('link:options', function(cellView,evt, x, y) {
			remove_edge_creation_menu();
			remove_context_menu();
			update_selection(cellView,$paper_«g.name.lowEscapeDart»,$graph_«g.name.lowEscapeDart»);
			cb_element_selected(cellView.model.attributes.attrs.dywaId);
			console.log("link clicked");
		});
	    
	    /**
		 * Canvas has been right clicked
		 * Show context menu for the graphmodel
		 * including registered custome actions
		 */
		$paper_«g.name.lowEscapeDart».on('blank:contextmenu', function(evt,x,y){
			//fetch ca for graphmodel
			var pos = getPaperToScreenPosition(x,y,$paper_«g.name.lowEscapeDart»);
			cb_get_custom_actions($graphmodel_id_«g.name.lowEscapeDart»,Math.round(pos.x),Math.round(pos.y+$(document).scrollTop()),x,y);
			console.log("graphmodel context menu clicked");
		});
		
		/**
		* Graphmodel has been double clicked
		* Activate double click action
		*/
		$paper_«g.name.lowEscapeDart».on('blank:pointerdblclick', function(evt,x,y){
			//fetch ca for element
			cb_fire_dbc_actions($graphmodel_id_«g.name.lowEscapeDart»);
			console.log("graphmodel double clicked");
		});
	    
	    /**
	     * Element has been right clicked
	     * Show context menu for the element
	     * including registered custome actions
	     */
	    $paper_«g.name.lowEscapeDart».on('cell:contextmenu', function(cellView,evt,x,y){
	    	//fetch ca for element
	    	var pos = getPaperToScreenPosition(x,y,$paper_«g.name.lowEscapeDart»);
	    	cb_get_custom_actions(cellView.model.attributes.attrs.dywaId,Math.round(pos.x),Math.round(pos.y+$(document).scrollTop()),x,y);
	    });
	    
	    /**
	     * Element has been double clicked
	     * Activate double click action
	     */
	    $paper_«g.name.lowEscapeDart».on('cell:pointerdblclick', function(cellView,evt,x,y){
	    	//fetch ca for element
	    	cb_fire_dbc_actions(cellView.model.attributes.attrs.dywaId);
	    });
	    
	    
	    /**
	     * Element has been selected
	     * change selection
	     * show properties
	     */
	     $paper_«g.name.lowEscapeDart».on('cell:pointerup', function(cellView,evt) {
	     	if(cellView.model.attributes.attrs.isDeleted!==true) {
	     		
	     		//check for select disabled and try for container
	     		while(cellView.model.attributes.attrs.disableSelect===true) {
	     			// get container
	     			if(cellView.model.attributes.parent == null){
	     				cb_graphmodel_selected();
	     				return;
     				}
	     			cellView = $paper_«g.name.lowEscapeDart».findViewByModel($graph_«g.name.lowEscapeDart».getCell(cellView.model.attributes.parent));
	     		}
	     		
		        update_selection(cellView,$paper_«g.name.lowEscapeDart»,$graph_«g.name.lowEscapeDart»);
	 	        cb_element_selected(cellView.model.attributes.attrs.dywaId);
	 	        «FOR node:nodes»
	 	        if(cellView.model.attributes.type=='«g.name.lowEscapeDart».«node.name.escapeDart»'){
	 	        	//check if container has changed
	 	        	move_node_«node.name.lowEscapeDart»_«g.name.lowEscapeDart»_hook(cellView);
	 	        	if(!cellView.model.attributes.attrs.disableResize) {
		 	        	$cb_functions_«g.name.lowEscapeDart».cb_resize_node_«node.name.lowEscapeDart»(Math.round(cellView.model.attributes.size.width),Math.round(cellView.model.attributes.size.height),$node_resize_last_direction,cellView.model.attributes.attrs.dywaId);	 	        		
	 	        	}
	 	        }
	 	        «ENDFOR»
	 	        «FOR edge:edges»
	 	        if(cellView.model.attributes.type=='«g.name.lowEscapeDart».«edge.name.escapeDart»'){
	 	        	var source = $graph_«g.name.lowEscapeDart».getCell(cellView.model.attributes.source.id);
	 	        	var target = $graph_«g.name.lowEscapeDart».getCell(cellView.model.attributes.target.id);
	 	        	reconnect_edge_«edge.name.lowEscapeDart»_«g.name.lowEscapeDart»_hook(cellView);
«««	 	        	$cb_functions_«g.name.lowEscapeDart».cb_reconnect_edge_«edge.name.lowEscapeDart»(
«««	 	        		source.attributes.attrs.dywaId,
«««	 	        		target.attributes.attrs.dywaId,
«««	 	        		cellView.model.attributes.attrs.dywaId
«««	 	        	);
	 	        	$cb_functions_«g.name.lowEscapeDart».cb_update_bendpoint(
	 	        		cellView.model.attributes.vertices,
	 	        		cellView.model.attributes.attrs.dywaId
	 	        	);
	 	        }
	 	        «ENDFOR»
		         console.log(cellView);
		         console.log("element clicked");
	        }
	     });
	
	    /**
	     * Element has been added
	     * change selection
	     * show properties
	     */
	    $graph_«g.name.lowEscapeDart».on('add', function(cellView) {
	    	if(!$_disable_events_«g.name.lowEscapeDart» && cellView.attributes.type!=='pyro.PyroLink' && cellView.attributes.type!=='pyro.GlueLine' ){
	    		update_selection($paper_«g.name.lowEscapeDart».findViewByModel(cellView),$paper_«g.name.lowEscapeDart»,$graph_«g.name.lowEscapeDart»);
«««		        //for each node
«««		        «FOR node:nodes»
«««		        if(cellView.attributes.type==='«g.name.lowEscapeDart».«node.name.escapeDart»') {
«««		        	var node = $graph_«g.name.lowEscapeDart».getCell(cellView.attributes.id);
«««		            var parentId = $graphmodel_id_«g.name.lowEscapeDart»;
«««		            if(node.parent != null){
«««		                parentId = node.parent.model.attrs.dywaId;
«««		            }
«««		            $cb_functions_«g.name.lowEscapeDart».cb_create_node_«node.name.lowEscapeDart»(cellView.attributes.position.x, cellView.attributes.position.y, cellView.attributes.id,parentId);
«««		        }
«««		        «ENDFOR»
		        //for each edge
		        «FOR edge:edges»
		        if(cellView.attributes.type==='«g.name.lowEscapeDart».«edge.name.escapeDart»') {
		            var link = $graph_«g.name.lowEscapeDart».getCell(cellView.attributes.id);
		            var source = link.getSourceElement();
		            var target = link.getTargetElement();
		            if(source.id===target.id){
		            	var p1 = {
		            		x:source.attributes.position.x,
		            		y:source.attributes.position.y-source.attributes.size.height
		            	};
		                var p2 = {
		                    x:source.attributes.position.x-source.attributes.size.width,
		                    y:source.attributes.position.y
		                };
		                link.set('vertices', [p1,p2]);
		            }
		            refresh_routing_«g.name.lowEscapeDart»();
		            $cb_functions_«g.name.lowEscapeDart».cb_create_edge_«edge.name.lowEscapeDart»(
		              source.attributes.attrs.dywaId,
		              target.attributes.attrs.dywaId,
		              cellView.attributes.id,
		              cellView.attributes.vertices
		            );
		        }
		        «ENDFOR»
		        console.log(cellView);
		        console.log("element added");
	        }
	    });
	
		function remove_cascade_node_«g.name.lowEscapeDart»(cellView) {
			if(!$_disable_events_«g.name.lowEscapeDart»){
				 deselect_all_elements(null,$paper_«g.name.lowEscapeDart»,$graph_«g.name.lowEscapeDart»);
				 //trigger callback
				 cb_graphmodel_selected();
				 //foreach node
				 «FOR node:nodes»
				        if(cellView.attributes.type==='«g.name.lowEscapeDart».«node.name.escapeDart»'){
				            $cb_functions_«g.name.lowEscapeDart».cb_remove_node_«node.name.lowEscapeDart»(cellView.attributes.attrs.dywaId);
				        }
				 «ENDFOR»
				 fitContent($paper_«g.name.lowEscapeDart»,$map_paper_«g.name.lowEscapeDart»);
			}
		
		}
	
	    /**
	     * Element has been removed
	     * change selection to the graphmodel
	     * show properties
	     */
	    $graph_«g.name.lowEscapeDart».on('remove', function(cellView) {
	    	if(cellView.attributes.type == 'pyro.GlueLine') {
	    		return;
	    	}
	    	if(!$_disable_events_«g.name.lowEscapeDart»){
		        deselect_all_elements(null,$paper_«g.name.lowEscapeDart»,$graph_«g.name.lowEscapeDart»);
		        //trigger callback
		        cb_graphmodel_selected();
		        //foreach node
«««		        «FOR node:nodes»
«««		        if(cellView.attributes.type==='«g.name.lowEscapeDart».«node.name.escapeDart»'){
«««		            $cb_functions_«g.name.lowEscapeDart».cb_remove_node_«node.name.lowEscapeDart»(cellView.attributes.attrs.dywaId);
«««		        }
«««		        «ENDFOR»
		        //foreach edge
		        «FOR edge:edges»
		        if(cellView.attributes.type==='«g.name.lowEscapeDart».«edge.name.escapeDart»'){
		        	cellView.attributes.attrs.isDeleted = true;
		            $cb_functions_«g.name.lowEscapeDart».cb_remove_edge_«edge.name.lowEscapeDart»(cellView.attributes.attrs.dywaId);
		        }
		        «ENDFOR»
		        console.log(cellView);
		        console.log("element removed");
	        }
	    });
	    $("html").off('dragend');
        $("html").on("dragend", function(event) {
            event.preventDefault();  
            event.stopPropagation();
            unhighlight_all_element_valid_target($paper_«g.name.lowEscapeDart»,$graph_«g.name.lowEscapeDart»);
        });
	    $(document).off('mouseup');
	    $(document).mouseup(function (evt) {
	        $mouse_clicked_menu=false;
	        if($temp_link!==null && !$edge_menu_shown)
	        {
	           unhighlight_all_element_valid_target($paper_«g.name.lowEscapeDart»,$graph_«g.name.lowEscapeDart»);
	           var rp = getRelativeScreenPosition(evt.clientX,evt.clientY,$paper_«g.name.lowEscapeDart»);
	           var views = $paper_«g.name.lowEscapeDart».findViewsFromPoint(rp);
	           if(views.length > 0)
	           {
	             «edgecreation(g)»
	           }
	           var pyroLink = $graph_«g.name.lowEscapeDart».getCell($temp_link.id);
	           $graph_«g.name.lowEscapeDart».removeCells([pyroLink]);
	           $temp_link=null;
	        }
	    });
	    
	    var disableRemove = [
	    	«FOR e:g.elements.filter[!removable] SEPARATOR ","»
	    	'«g.name.lowEscapeDart».«e.name.fuEscapeDart»'
		    «ENDFOR»
	    ];
		var disableResize = [
			«FOR e:g.elements.filter[!resizable] SEPARATOR ","»
	    	'«g.name.lowEscapeDart».«e.name.fuEscapeDart»'
		    «ENDFOR»
		];
		var disableEdge = [
			«FOR e:g.nodes.filter[!isAbstract].filter[!connectable(g)] SEPARATOR ","»
	    	'«g.name.lowEscapeDart».«e.name.fuEscapeDart»'
		    «ENDFOR»
		];
	
	    init_event_system($paper_«g.name.lowEscapeDart»,$graph_«g.name.lowEscapeDart»,remove_cascade_node_«g.name.lowEscapeDart»,disableRemove,disableResize,disableEdge,highlight_valid_containers_«g.name.lowEscapeDart»);
	    
	    create_«g.name.lowEscapeDart»_map();
	    
	    //key bindings
	    $(window).off('keyup');
	    $(window).keyup(function(evt){
	    	// remove key
	    	if(evt.which == 46) {
	    		evt.preventDefault();
	    		//delete selected element
	    		cb_delete_selected();
	    		deselect_all_elements(null,$paper_«g.name.lowEscapeDart»,$graph_«g.name.lowEscapeDart»);
	    	}
	    });
	
	    //callback after initialization
	    initialized();
	    
	}
	
	
	function start_propagation_«g.name.lowEscapeDart»() {
	    block_user_interaction($paper_«g.name.lowEscapeDart»);
	    $_disable_events_«g.name.lowEscapeDart» = true;
	}
	function end_propagation_«g.name.lowEscapeDart»() {
	    unblock_user_interaction($paper_«g.name.lowEscapeDart»);
	    $_disable_events_«g.name.lowEscapeDart» = false;
	}
	
	function destroy_«g.name.lowEscapeDart»() {
	    block_user_interaction($paper_«g.name.lowEscapeDart»);
	    deselect_all_elements(null,$paper_«g.name.lowEscapeDart»,$graph_«g.name.lowEscapeDart»);
	    $paper_«g.name.lowEscapeDart» = null;
	    $map_paper_«g.name.lowEscapeDart» = null;
	    $graph_«g.name.lowEscapeDart» = null;
	    $('#paper_«g.name.lowEscapeDart»').empty();
	    $('#paper_map_«g.name.lowEscapeDart»').empty();
	}
	
	function highlight_valid_targets_«g.name.lowEscapeDart»(cell) {
		var validTargets = $cb_functions_«g.name.lowEscapeDart».cb_get_valid_targets(cell.model.attributes.attrs.dywaId);
		validTargets.forEach(function(vt){
			var elem = findElementByDywaId(vt,$graph_«g.name.lowEscapeDart»);
			var cellView = $paper_«g.name.lowEscapeDart».findViewByModel(elem);
			highlight_cell_valid_target(cellView);
		});
	}
	
	function highlight_valid_containers_«g.name.lowEscapeDart»(dywaId,type) {
		var validContainers = $cb_functions_«g.name.lowEscapeDart».cb_get_valid_containers(dywaId,type);
		validContainers.forEach(function(vt){
			var elem = findElementByDywaId(vt,$graph_«g.name.lowEscapeDart»);
			var cellView = $paper_«g.name.lowEscapeDart».findViewByModel(elem);
			highlight_cell_valid_target(cellView);
		});
	}
	
	function refresh_checks_«g.name.lowEscapeDart»(checkResults) {
		$checkResults_«g.name.lowEscapeDart» = checkResults;
		unhighlight_all_elements_check($paper_«g.name.lowEscapeDart»,$graph_«g.name.lowEscapeDart»);
		checkResults.forEach(function(e) {
	        var elem = findElementByDywaId(e['id'],$graph_«g.name.lowEscapeDart»);
	        if(elem == null) {
	         return;
	        }
	        var cell = $paper_«g.name.lowEscapeDart».findViewByModel(elem);
			highlight_cell_check(cell,e['level'],e['errors'],$graph_«g.name.lowEscapeDart»);
		});
	}
	
	function refresh_gluelines_«g.name.lowEscapeDart»(status) {
		if(status) {
			$«g.name.lowEscapeDart»_lib.enable();
		} else {
			$«g.name.lowEscapeDart»_lib.disable();
		}
	}
	
	
	function create_«g.name.lowEscapeDart»_map() {
		var map = $('#paper_map_«g.name.lowEscapeDart»');
		if(map.length && $graph_«g.name.lowEscapeDart») {
			//create map
			$map_paper_«g.name.lowEscapeDart» = new joint.dia.Paper({
			    el: map,
			    width: 300,
			    height: 300,
			    model: $graph_«g.name.lowEscapeDart»,
			    gridSize: 1,
			    interactive:false
			});
			$graph_«g.name.lowEscapeDart».resetCells($graph_«g.name.lowEscapeDart».getCells());
			$map_paper_«g.name.lowEscapeDart».scaleContentToFit();
			$rebuild_map_fun = function rebuild_«g.name.lowEscapeDart»_map_rect() {
«««			    if(!$('#«g.name.lowEscapeDart»_map_rect').length) {
«««			        $('body').append('<div id="«g.name.lowEscapeDart»_map_rect" style="position:absolute;z-index:0;opacity:0.5;background-color:#f7b233;"></div>');
«««			    }
«««			    var sourcePos = $paper_«g.name.lowEscapeDart».translate();
«««	            var sourceScale = $paper_«g.name.lowEscapeDart».scale();
«««			    var targetPos = $map_paper_«g.name.lowEscapeDart».localToClientPoint($map_paper_«g.name.lowEscapeDart».paperToLocalPoint(
«««			            Math.round(sourcePos.tx/sourceScale.sx),
«««			            Math.round(sourcePos.ty/sourceScale.sy)
«««		        ));
«««	            console.log(sourcePos);
«««	            console.log(targetPos);
«««		        
«««			    $('#flowgraph_map_rect').css('top',targetPos.y);
«««			    $('#flowgraph_map_rect').css('left',targetPos.x);
«««			
«««			    var w = Math.round(($('#paper_«g.name.lowEscapeDart»').parent().width() / $paper_«g.name.lowEscapeDart».scale().sx) * $map_paper_«g.name.lowEscapeDart».scale().sx);
«««			    var h = Math.round(($('#paper_«g.name.lowEscapeDart»').parent().height() / $paper_«g.name.lowEscapeDart».scale().sy) * $map_paper_«g.name.lowEscapeDart».scale().sy);
«««			
«««	            var max_w = $('#paper_map_«g.name.lowEscapeDart»').parent().width();
«««	            var max_h = $('#paper_map_«g.name.lowEscapeDart»').parent().height();
«««	
«««			    $('#«g.name.lowEscapeDart»_map_rect').css('width',Math.min(w,max_w));
«««			    $('#«g.name.lowEscapeDart»_map_rect').css('height',Math.min(h,max_h));
			      
			};
		}
	}
	
	/*
	 Settings handling methods to be called from the NG-App
	 */
	
	function update_routing_«g.name.lowEscapeDart»(routing,connector) {
		$router_«g.name.lowEscapeDart» = routing;
		$connector_«g.name.lowEscapeDart» = connector;
	    refresh_routing_«g.name.lowEscapeDart»();
	}
	
	function «g.name.lowEscapeDart»_«g.name.lowEscapeDart»_jump(dywaId) {
		jump_to_element(dywaId,$graph_«g.name.lowEscapeDart»,$paper_«g.name.lowEscapeDart»,$cb_functions_«g.name.lowEscapeDart»);
	}
	
	function refresh_routing_«g.name.lowEscapeDart»() {
		update_edeg_routing($router_«g.name.lowEscapeDart»,$connector_«g.name.lowEscapeDart»,$graph_«g.name.lowEscapeDart»);
	}
	
	function update_scale_«g.name.lowEscapeDart»(scale) {
	    $paper_«g.name.lowEscapeDart».scale(scale);
	    fitContent($paper_«g.name.lowEscapeDart»,$map_paper_«g.name.lowEscapeDart»);
	}
	
	/*
	Element handling methods to be called from the NG-App
	 */
	
	/**
	 *
	 * @param cellId
	 * @param dywaId
	 * @param dywaVersion
	 * @param dywaName
	 * @param styleArgs
	 */
	function update_element_«g.name.lowEscapeDart»(cellId,dywaId,dywaVersion,dywaName,styleArgs,information,label,graph,paper) {
		graph = (typeof graph !== 'undefined') ?  graph : $graph_«g.name.lowEscapeDart»;
		if(styleArgs!==null) {
			var elem = findElementByDywaId(dywaId,graph);
			if(cellId!=null&&elem==null){
			   elem =  graph.getCell(cellId);
			}
		    if(elem == null) {
		     return;
		    }
		    paper = (typeof paper !== 'undefined') ?  paper : $paper_«g.name.lowEscapeDart»;
			var cell = paper.findViewByModel(elem);
			«FOR node:nodes»
			«{
				val styleForNode = node.styling(styles) as NodeStyle
				'''
				if(cell.model.attributes.type==='«g.name.lowEscapeDart».«node.name.fuEscapeDart»') {
					«styleForNode.updateStyleArgs(g)»
				}
				'''
			}»
			«ENDFOR»
			«FOR edge:edges»
			«{
				val styleForEdge = edge.styling(styles) as EdgeStyle
				'''
				if(cell.model.attributes.type==='«g.name.lowEscapeDart».«edge.name.fuEscapeDart»') {
					«styleForEdge.updateStyleArgs»
				}
				'''
			}»
			«ENDFOR»
		}
	    update_element_internal(cellId,dywaId,dywaName,dywaVersion,styleArgs,information,label,graph);
	}
	
	function update_element_highlight_«g.name.lowEscapeDart»(dywaId,
			background_r,background_g,background_b,
			foreground_r,foreground_g,foreground_b
	) {
		var elem = findElementByDywaId(dywaId,$graph_«g.name.lowEscapeDart»);
		var cell = $paper_«g.name.lowEscapeDart».findViewByModel(elem);
		«FOR node:nodes.filter[styling(styles)!=null]»
		«{
			val styleForNode = node.styling(styles) as NodeStyle
			'''
			if(cell.model.attributes.type==='«g.name.lowEscapeDart».«node.name.fuEscapeDart»') {
				«styleForNode.updateHighlight(g)»
			}
			'''
		}»
		«ENDFOR»
		«FOR edge:edges.filter[styling(styles)!=null]»
		«{
			'''
			if(cell.model.attributes.type==='«g.name.lowEscapeDart».«edge.name.fuEscapeDart»') {
					return update_node_highlight_internal(
				    	cell,'.connection',
						background_r,background_g,background_b,
						foreground_r,foreground_g,foreground_b
					);
			}
			'''
		}»
		«ENDFOR»
	}

	function update_element_appearance_«g.name.lowEscapeDart»(dywaId,shapeId,
		background_r,background_g,background_b,
		foreground_r,foreground_g,foreground_b,
		lineInVisible,
		lineStyle,
		transparency,
		lineWidth,
		filled,
		angle,
		fontName,
		fontSize,
		fontBold,
		fontItalic,
		imagePath
	) {
		var elem = findElementByDywaId(dywaId,$graph_«g.name.lowEscapeDart»);
		var cell = $paper_«g.name.lowEscapeDart».findViewByModel(elem);
		«FOR node:nodes.filter[!styling(styles).appearanceProvider.nullOrEmpty]»
		«{
			val styleForNode = node.styling(styles) as NodeStyle
			'''
			if(cell.model.attributes.type==='«g.name.lowEscapeDart».«node.name.fuEscapeDart»') {
				«styleForNode.updateAppearance(g)»
			}
			'''
		}»
		«ENDFOR»
		«FOR edge:edges.filter[!styling(styles).appearanceProvider.nullOrEmpty]»
		«{
			val styleForEdge = edge.styling(styles) as EdgeStyle
			'''
			if(cell.model.attributes.type==='«g.name.lowEscapeDart».«edge.name.fuEscapeDart»') {
				«styleForEdge.updateAppearance»
			}
			'''
		}»
		«ENDFOR»
		
	}
	
	«FOR node:nodes»
		/**
		 * Build the WYSIWYG Palette for node type «node.name.escapeDart»
		 */
		function build_palette_«g.name.lowEscapeDart»_«node.name.escapeDart»() {
			var graphP = new joint.dia.Graph();
			
			var paperP = new joint.dia.Paper({
			    el: $('#wysiwig«g.name.lowEscapeDart»_«node.name.escapeDart»'),
			    width: '100%',
			    height: 50,
			    model: graphP,
			    gridSize: 1,
			    interactive:false,
			    elementView:constraint_element_view_palette()
			});
			
			var elem = new joint.shapes.«g.name.lowEscapeDart».«node.name.escapeDart»({
				position: {
			 	   x: 0,
			 	   y: 0
				},
			});
			graphP.addCell(elem);
			update_element_«g.name.lowEscapeDart»(elem.attributes.id,-1,-1,"dywaName",[«node.styleDefaults.map['''"«it»"'''].join(",")»],"","",graphP,paperP);
			paperP.scaleContentToFit({padding:15});
			
			enable_wysiwyg_palette(
				paperP,
				graphP,
				$paper_«g.name.lowEscapeDart»,
				$graph_«g.name.lowEscapeDart»,
				'«g.name.lowEscapeDart».«node.name.escapeDart»',
				create_node_«g.name.lowEscapeDart»_after_drop
			);
		}
		
		
		/**
		 * creates a «node.name.escapeDart» node in position
		 * this method is called by th NG-App
		 * @param x
		 * @param y
		 * @param dywaId
		 * @param containerId
		 * @param dywaName
		 * @param dywaVersion
		 * @param styleArgs
		 * @returns {*}
		 */
		function create_node_«node.name.lowEscapeDart»_«g.name.lowEscapeDart»(x,y,width,height,dywaId,containerId,dywaName,dywaVersion,styleArgs,information,label«IF node.isPrime»,primeId«ENDIF») {
		    var elem = null;
		    if(width != null && height != null) {
		    	elem = new joint.shapes.«g.name.lowEscapeDart».«node.name.escapeDart»({
		    		position: {
		    		    x: x,
		    		    y: y
		    		},
		    		size: {
		    		  	width:width,
		    		   	height:height
		    		},
		    		attrs:{
		    		    dywaId:dywaId,
		    		    dywaName:dywaName,
		    		    dywaVersion:dywaVersion,
		    		    disableMove:«IF node.movable»false«ELSE»true«ENDIF»,
		    		    disableResize:«IF node.resizable»false«ELSE»true«ENDIF»
		    		}
		    	});
		    } else {
			    elem = new joint.shapes.«g.name.lowEscapeDart».«node.name.escapeDart»({
			        position: {
			            x: x,
			            y: y
			        },
			        attrs:{
			            dywaId:dywaId,
			            dywaName:dywaName,
			            dywaVersion:dywaVersion,
			            disableMove:«IF node.movable»false«ELSE»true«ENDIF»,
			            disableResize:«IF node.resizable»false«ELSE»true«ENDIF»
			        }
			    });
		    }
		    add_node_internal(elem,$graph_«g.name.lowEscapeDart»,$paper_«g.name.lowEscapeDart»,$map_paper_«g.name.lowEscapeDart»);
		    var pos = {x:x,y:y};
		    if(containerId>-1&&containerId!=$graphmodel_id_«g.name.lowEscapeDart»){
		    	var parent = findElementByDywaId(containerId,$graph_«g.name.lowEscapeDart»);
		    	parent.embed(elem);
		    	pos.x -= parent.position().x;
		    	pos.y -= parent.position().y;
			}
			update_element_«g.name.lowEscapeDart»(elem.attributes.id,dywaId,dywaVersion,dywaName,styleArgs,information,label);
		    if(!$_disable_events_«g.name.lowEscapeDart»){
		    	$cb_functions_«g.name.lowEscapeDart».cb_create_node_«node.name.lowEscapeDart»(Math.round(pos.x), Math.round(pos.y),Math.round(elem.attributes.size.width),Math.round(elem.attributes.size.height), elem.attributes.id,containerId«IF node.isPrime»,parseInt(primeId)«ENDIF»);
		    }
		    return 'ready';
		}
		
		function move_node_«node.name.lowEscapeDart»_«g.name.lowEscapeDart»_hook(elem) {
			if(!$_disable_events_«g.name.lowEscapeDart»){
				var parentId = $graphmodel_id_«g.name.lowEscapeDart»;
				var pos = {x:elem.model.attributes.position.x,y:elem.model.attributes.position.y};
			    if(elem.model.attributes.parent != null){
			    	var parent = $graph_«g.name.lowEscapeDart».getCell(elem.model.attributes.parent);
			         parentId = parent.attributes.attrs.dywaId;
			         pos.x -= parent.position().x;
			         pos.y -= parent.position().y;
			    }
			    //check if the container change was allowed
			    var valid = $cb_functions_«g.name.lowEscapeDart».cb_can_move_node(elem.model.attributes.attrs.dywaId,parentId);
			    if(valid===true) {
			    	//movement has been valid
				    $cb_functions_«g.name.lowEscapeDart».cb_move_node_«node.name.lowEscapeDart»(Math.round(pos.x),Math.round(pos.y),elem.model.attributes.attrs.dywaId,parentId);
				    fitContent($paper_«g.name.lowEscapeDart»,$map_paper_«g.name.lowEscapeDart»);
			    } else {
			    	//movement is not valid and has to be reseted
			    	var preX = valid['x'];
			    	var preY = valid['y'];
			    	var diffX = preX - elem.model.attributes.position.x;
			    	var diffY = preY - elem.model.attributes.position.y;
			    	var preContainerDywaId = valid['containerId'];
			    	//remove the containement
			    	if(elem.model.attributes.parent != null) {
				    	$graph_«g.name.lowEscapeDart».getCell(elem.model.attributes.parent).unembed($graph_«g.name.lowEscapeDart».getCell(elem.model.id));
				    }
			    	//check if the pre container was not the graphmodel
			    	if(preContainerDywaId!==$graphmodel_id_«g.name.lowEscapeDart»)
			    	{
			    		//embed the node in the precontainer
				    	var parentCell = findElementByDywaId(preContainerDywaId,$graph_«g.name.lowEscapeDart»);
				    	parentCell.embed(elem.model);
				    	//move back
				    	elem.model.position(preX,preY,{ parentRealtive: true });
			    	}
			    	else {
				    	//move back
			    		elem.model.position(preX,preY);
			    	}
			    	«IF node instanceof NodeContainer»
			    	//move all children
			    	
    				elem.model.getEmbeddedCells({deep:true}).forEach(function(child){
    					var childCell = $graph_«g.name.lowEscapeDart».getCell(child);
    					if(!childCell.isLink()) {
    						var childPos = childCell.position();
    						childCell.position(childPos.x+diffX,childPos.y+diffY);
    					}
    	
    				});
    				«ENDIF»
			    	
			    }
		        console.log("node «g.name.lowEscapeDart» change position");
		    }
		}
		
		
		/**
		 * moves the «node.name.escapeDart» node to another position, relative to its parent container
		 * if the container id is provided (containerId != -1). the node is
		 * embedded in the given container
		 * this method is called by th NG-App
		 * 
		 * @param x
		 * @param y
		 * @param dywaId
		 * @param containerId
		 */
		function move_node_«node.name.lowEscapeDart»_«g.name.lowEscapeDart»(x,y,dywaId,containerId) {
		    if(containerId==$graphmodel_id_«g.name.lowEscapeDart»){
		        move_node_internal(x,y,dywaId,-1,$graph_«g.name.lowEscapeDart»);
		    } else {
		        move_node_internal(x,y,dywaId,containerId,$graph_«g.name.lowEscapeDart»);
		    }
		    fitContent($paper_«g.name.lowEscapeDart»,$map_paper_«g.name.lowEscapeDart»);
		    return 'ready';
		}
		
		/**
		 * removes the «node.name.escapeDart» node by id
		 * this method is called by th NG-App
		 * @param dywaId
		 */
		function remove_node_«node.name.lowEscapeDart»_«g.name.lowEscapeDart»(dywaId) {
		    remove_node_internal(dywaId,$graph_«g.name.lowEscapeDart»,$paper_«g.name.lowEscapeDart»);
		    fitContent($paper_«g.name.lowEscapeDart»,$map_paper_«g.name.lowEscapeDart»);
		    return 'ready';
		}
		
		/**
		 * resizes the «node.name.escapeDart» node by id depended on the 
		 * given absolute width and height
		 * this method is called by th NG-App
		 * @param width
		 * @param height
		 * @param dywaId
		 */
		function resize_node_«node.name.lowEscapeDart»_«g.name.lowEscapeDart»(width,height,direction,dywaId) {
		    resize_node_internal(width,height,direction,dywaId,$graph_«g.name.lowEscapeDart»,$paper_«g.name.lowEscapeDart»);
		    return 'ready';
		}
		
		/**
		 * rotates the «node.name.escapeDart» node by id depended on the 
		 * given on the absolute angle
		 * this method is called by th NG-App
		 * @param angle
		 * @param dywaId
		 */
		function rotate_node_«node.name.lowEscapeDart»_«g.name.lowEscapeDart»(angle,dywaId) {
		    rotate_node_internal(angle,dywaId,$graph_«g.name.lowEscapeDart»);
		    return 'ready';
		}
	«ENDFOR»
	
	
		/**
		* removes a edge with the given dywaId from the canvas
		* this method is called by th NG-App
		* @param dywaId
		*/
		function remove_edge__«g.name.lowEscapeDart»(dywaId) {
			remove_edge_internal(dywaId,$graph_«g.name.lowEscapeDart»);
			return 'ready';
		}
	
	«FOR edge:edges»
		/**
		 * creates a «edge.name.escapeDart» edge connecting
		 * the nodes specified by the source and target dywaId
		 * registers the listener for reconnnection and bendpoints
		 * this method is called by th NG-App
		 * @param sourceId
		 * @param targetId
		 * @param dywaId
		 * @param dywaName
		 * @param dywaVersion
		 * @param styleArgs
		 */
		function create_edge_«edge.name.lowEscapeDart»_«g.name.lowEscapeDart»(sourceId,targetId,dywaId,dywaName,dywaVersion,positions,styleArgs,information,label) {
		    var sourceN = findElementByDywaId(sourceId,$graph_«g.name.lowEscapeDart»);
		    var targetN = findElementByDywaId(targetId,$graph_«g.name.lowEscapeDart»);
		
		    var link = new joint.shapes.«g.name.lowEscapeDart».«edge.name.escapeDart»({
		        attrs:{
		            dywaId:dywaId,
		            dywaName:dywaName,
		            dywaVersion:dywaVersion,
		            disableMove:«IF edge.movable»false«ELSE»true«ENDIF»,
		            disableResize:«IF edge.resizable»false«ELSE»true«ENDIF»,
		            styleArgs:styleArgs,
		            information:information,
		            label:label
		        },
		        source: { id: sourceN.id },
		        target: { id: targetN.id }
		    });
		    if(positions!==null){
			    link.set('vertices', positions.map(function (n) {
    		    	return {x:n.x,y:n.y};
			    }));
		    }
		    add_edge_internal(link,$graph_«g.name.lowEscapeDart»,$router_«g.name.lowEscapeDart»,$connector_«g.name.lowEscapeDart»);
		    update_element_«g.name.lowEscapeDart»(link.attributes.id,dywaId,dywaVersion,dywaName,styleArgs,information,label);
«««		    «"link".addLinkListeners(edge,g)»
		    return 'ready';
		}
		
		/**
		 * removes the «edge.name.escapeDart» edge with the given dywaId from the canvas
		 * this method is called by th NG-App
		 * @param dywaId
		 */
		function remove_edge_«edge.name.lowEscapeDart»_«g.name.lowEscapeDart»(dywaId) {
		    remove_edge_internal(dywaId,$graph_«g.name.lowEscapeDart»);
		    return 'ready';
		}
		
		/**
		 * reconnets the «edge.name.escapeDart» edge to a new target and source node
		 * specified by their dywaId
		 * this method is called by th NG-App
		 * @param sourceId
		 * @param targetId
		 * @param dywaId
		 */
		function reconnect_edge_«edge.name.lowEscapeDart»_«g.name.lowEscapeDart»(sourceId,targetId,dywaId) {
		    reconnect_edge_internal(sourceId,targetId,dywaId,$graph_«g.name.lowEscapeDart»);
		    return 'ready';
		}
		
		function reconnect_edge_«edge.name.lowEscapeDart»_«g.name.lowEscapeDart»_hook(elem) {
			if(!$_disable_events_«g.name.lowEscapeDart»){
				var edgeId = elem.model.attributes.attrs.dywaId;
				var source = elem.model.attributes.source.id;
				var sourceDywaId = $graph_«g.name.lowEscapeDart».getCell(source).attributes.attrs.dywaId;
				var target = elem.model.attributes.target.id;
				var targetDywaId = $graph_«g.name.lowEscapeDart».getCell(target).attributes.attrs.dywaId;
			    //check if the container change was allowed
			    var valid = $cb_functions_«g.name.lowEscapeDart».cb_can_reconnect_edge(edgeId,sourceDywaId,targetDywaId);
			    if(valid===true) {
			    	//reconnection has been valid
				    $cb_functions_«g.name.lowEscapeDart».cb_reconnect_edge_«edge.name.lowEscapeDart»(sourceDywaId,targetDywaId,edgeId);
			    } else {
			    	//movement is not valid and has to be reseted
			    	var preSource = valid.source;
			    	var preTarget = valid.target;
			    	reconnect_edge_internal(preSource,preTarget,edgeId,$graph_«g.name.lowEscapeDart»);
			    	
			    }
		    }
		}
		
		
	«ENDFOR»
	
	/**
	 * updates the edge verticles
	 * specified by the edge dywaId and all verticle positions
	 * this method is called by th NG-App
	 * @param point {x,y}
	 * @param dywaId
	 */
	function update_bendpoint_«g.name.lowEscapeDart»(points,dywaId) {
	    update_bendpoint_internal(points, dywaId,$graph_«g.name.lowEscapeDart»);
	    return 'ready';
	}
	
	
	/*
	Creation of nodes by drag and dropping from the palette
	 */
	
	function create_prime_node_menu_«g.name.lowEscapeDart»(possibleNodes,x,y,absX,absY,containerDywaId,elementId) {
	    var btn_group = $('<div id="pyro_node_menu" class="btn-group-vertical btn-group-sm" style="position: absolute;z-index: 99999;top: '+absY+'px;left: '+absX+'px;"></div>');
	    $('body').append(btn_group);
	    for(var node in possibleNodes) {
	            var button = $('<button type="button" class="btn">'+possibleNodes[node]+'</button>');
	
	            btn_group.append(button);
	
	            $(button).on('click',function () {
	                switch(this.innerText){
	                	«FOR node:nodes.filter[isPrime]» 
			            case '«node.name.escapeDart»':{
			            	create_node_«node.name.lowEscapeDart»_«g.name.lowEscapeDart»(x,y,null,null,-1,containerDywaId,"undefined",-1,null,null,null,elementId);
				            break;
			            }
			            «ENDFOR»
	                }
	                $('#pyro_node_menu').remove();
	            });
	    }
	}
	
	/**
	 *
	 * @param ev
	 */
	function drop_on_canvas_«g.name.lowEscapeDart»(ev) {
		unhighlight_all_element_valid_target($paper_«g.name.lowEscapeDart»,$graph_«g.name.lowEscapeDart»);
	    ev.preventDefault();
	    var rp = getRelativeScreenPosition(ev.clientX,ev.clientY,$paper_«g.name.lowEscapeDart»);
	    var x = rp.x;
	    var y = rp.y;
	    var containerDywaId = get_container_id_«g.name.lowEscapeDart»(rp);
	    var content = JSON.parse(ev.dataTransfer.getData("text"));
	    var typeName = content.typename;
	    var elementId = content.elementid;
	    //check prime node
	    if(typeof elementId !== 'undefined' && typeName != ''){
    		var possibleNodes = [];
    		//for all prime nodes
    		//check prime referenced type and super types
    		«FOR pr:nodes.filter[prime].filter[primeCreatabel]»
    		«{

    			val graph = pr.primeReference.type.graphModel
    			val subTypes = pr.primeReference.type.name.subTypesAndType(graph)
    			'''if(«FOR sub:subTypes SEPARATOR "||"»typeName == '«graph.name.lowEscapeDart».«sub.name.fuEscapeDart»'«ENDFOR»)'''
    		}»
    		{
    			if(is_containement_allowed_«g.name.lowEscapeDart»(rp,'«g.name.lowEscapeDart».«pr.name.fuEscapeDart»')) {
    				possibleNodes[possibleNodes.length] = '«pr.name.fuEscapeDart»'; 
    			}
    		}
    		«ENDFOR»
    		if(possibleNodes.length==1){
    			//one node possible
    			switch (possibleNodes[0]) {
		            //foreach node
		            «FOR node:nodes.filter[isPrime]» 
		            case '«node.name.escapeDart»':{
		            	create_node_«node.name.lowEscapeDart»_«g.name.lowEscapeDart»(x,y,null,null,-1,containerDywaId,"undefined",-1,null,null,null,elementId);
			            break;
		            }
		            «ENDFOR»
		        }
    		}
    		else{
    			//multiple nodes possible
    			//show selection
    			 create_prime_node_menu_«g.name.lowEscapeDart»(possibleNodes,x,y,ev.clientX,ev.clientY,containerDywaId,elementId);
    		}
    		return;
    	}
	    if(typeName != ''){
	        // create node
	        create_node_«g.name.lowEscapeDart»_after_drop(x,y,typeName);
	    }
	    fitContent($paper_«g.name.lowEscapeDart»,$map_paper_«g.name.lowEscapeDart»);
	
	}
	
	function create_node_«g.name.lowEscapeDart»_after_drop(x,y,typeName) {
		if(is_containement_allowed_«g.name.lowEscapeDart»({x:x,y:y},typeName)) {
			var containerDywaId = get_container_id_«g.name.lowEscapeDart»({x:x,y:y});
	        switch (typeName) {
	            //foreach node
	            «FOR node:nodes» 
	            case '«g.name.lowEscapeDart».«node.name.escapeDart»': {
	            	create_node_«node.name.lowEscapeDart»_«g.name.lowEscapeDart»(x,y,null,null,-1,containerDywaId,"undefined",-1,null);
		            break;
	            }
	            «ENDFOR»
	        }
        }
	}
	
	function get_container_id_«g.name.lowEscapeDart»(rp) {
		var views = $paper_«g.name.lowEscapeDart».findViewsFromPoint(rp);
		if(views.length<=0){
			return $graphmodel_id_«g.name.lowEscapeDart»;
		}
		return views[views.length-1].model.attributes.attrs.dywaId;
	}
	
	function is_containement_allowed_«g.name.lowEscapeDart»(rp,creatableTypeName) {
	    var views = $paper_«g.name.lowEscapeDart».findViewsFromPoint(rp);
	    if(views.length<=0){
	    	var targetNode = null;
	        //target is graphmodel
	        «g.containmentCheck(g)»
	    }
	    «IF !nodes.filter(NodeContainer).empty»
	    else {
	        var targetNode = views[views.length-1];
	        var targetType = targetNode.model.attributes.type;
			//foreach container
			«FOR container:nodes.filter(NodeContainer)»
	        if(targetType==='«g.name.lowEscapeDart».«container.name.fuEscapeDart»')
	        {
	        	«container.containmentCheck(g)»
	        }
	        «ENDFOR»
	    }
	    «ENDIF»
	    return false;
	}
	
	function confirm_drop_«g.name.lowEscapeDart»(ev) {
	    ev.preventDefault();
	    ev.stopPropagation();
	    var rp = getRelativeScreenPosition(ev.clientX,ev.clientY,$paper_«g.name.lowEscapeDart»);
	    var x = rp.x;
	    var y = rp.y;
	    var typeName = ev.dataTransfer.getData("typename");
	    if(typeName != ''){
	    	if(!is_containement_allowed_«g.name.lowEscapeDart»(rp,typeName)) {
	       	   		ev.dataTransfer.effectAllowed= 'none';
	       	        ev.dataTransfer.dropEffect= 'none';
	       	 }
	    }
	
	}
	
	'''
	
	}
	
	def getStyleDefaults(Node node) {
		val ann = node.annotations.findFirst[name.equals("styleDefaults")]
		if(ann !== null) {
			return ann.value
		}
		#[]
	}
	
	def containmentCheck(ContainingElement ce,GraphModel g){
		val containableElements = ce.allContainableElement(g)
		'''
			«IF containableElements.empty»
			return true;
			«ELSE»
				«FOR group:containableElements.get(0).containingElement.containableElements.indexed»
				//check if type can be contained in group
				if(
					«FOR containableType:group.value.getGroupContainables(g).toSet SEPARATOR "||"»
					creatableTypeName === '«g.name.lowEscapeDart».«containableType.name.fuEscapeDart»'
					«ENDFOR»
				) {
					«IF group.value.upperBound>-1»
					var group«group.key»Size = 0;
					«FOR containableType:group.value.getGroupContainables(g).toSet»
					group«group.key»Size += getContainedByType(targetNode,'«g.name.lowEscapeDart».«containableType.name.fuEscapeDart»',$graph_«g.name.lowEscapeDart»).length;
					«ENDFOR»
					if(«IF group.value.upperBound>-1»group«group.key»Size<«group.value.upperBound»«ELSE»true«ENDIF»){
						return true;
					}
					«ELSE»
					return true;
					«ENDIF»
				}
				«ENDFOR»
				return false;
			«ENDIF»
	
		'''
	}
	
	def edgecreation(GraphModel g)
	{
		val nodes = g.nodesTopologically
	
	'''
	var sourceNode = $graph_«g.name.lowEscapeDart».getCell($temp_link.attributes.source.id);
	var sourceType = sourceNode.attributes.type;
	var targetNode = $graph_«g.name.lowEscapeDart».getCell(views[views.length-1].model.id);
	var targetType = targetNode.attributes.type;
	var outgoing = getOutgoing(sourceNode,$graph_«g.name.lowEscapeDart»);
	var incoming = getIncoming(targetNode,$graph_«g.name.lowEscapeDart»);
	//create the correct link
	var possibleEdges = {};
	//get possible edges
	//depends on cardinallity
	«FOR source:nodes.filter[!isAbstract]»
	if(sourceType == '«g.name.lowEscapeDart».«source.name.fuEscapeDart»')
	{
		«FOR group:source.name.parentTypes(g).filter(Node).map[outgoingEdgeConnections].flatten.indexed»
		//check bound group condition
		var groupSize«group.key» = 0;
			«FOR outgoingEdge:group.value.connectingEdges.map[subTypesAndType(it.name,g)].flatten»
			groupSize«group.key» += filterEdgesByType(outgoing,'«g.name.lowEscapeDart».«outgoingEdge.name.fuEscapeDart»').length;
			«ENDFOR»
			«IF group.value.connectingEdges.nullOrEmpty»
		groupSize«group.key» += outgoing.length;
			«ENDIF»
		//check cardinality
		if(«IF group.value.upperBound < 0»true«ELSE»groupSize«group.key»<«group.value.upperBound»«ENDIF»)
		{
		   «g.targetCheck(group.value)»
		}
		«ENDFOR»
	}
	«ENDFOR»
	var possibleEdgeSize = Object.keys(possibleEdges).length;
	if(possibleEdgeSize==1)
	{
		//only one edge can be created
		//so, create it
		$temp_link_multi = $temp_link;
		create_edge(targetNode,possibleEdges[Object.keys(possibleEdges)[0]].type,$paper_«g.name.lowEscapeDart»,$graph_«g.name.lowEscapeDart»,$map_paper_«g.name.lowEscapeDart»);
	}
	else if(possibleEdgeSize>1)
	{
		//multiple edge types possible
		//show menu
		create_edge_menu(targetNode,possibleEdges,evt.clientX,evt.clientY+$(document).scrollTop(),$paper_«g.name.lowEscapeDart»,$graph_«g.name.lowEscapeDart»);
	}
	'''
	
	}
	
	def reachable(Iterable<Node> sources, GraphModel g, OutgoingEdgeElementConnection connection){
		val edges = g.edgesTopologically
		val result = new HashMap<Node,Set<IncomingEdgeElementConnection>>();
		for(node:sources) {
			val possibleEdgeConnections = new LinkedList
			
			val possibleOutgoingEdges = new LinkedList
			if(connection.connectingEdges.empty) {
				possibleOutgoingEdges += edges		
			}
			possibleOutgoingEdges+=connection.connectingEdges.map[name.subTypesAndType(g).filter(Edge)].flatten
			
			for(incomingEdge:possibleOutgoingEdges.toSet){
				possibleEdgeConnections += node.incomingEdgeConnections.filter[connectingEdges.map[name.subTypesAndType(g).filter(Edge)].flatten.toSet.contains(incomingEdge)]
				possibleEdgeConnections	+= node.incomingEdgeConnections.filter[connectingEdges.map[name.subTypesAndType(g).filter(Edge)].flatten.toSet.empty]
			}
			if(!possibleEdgeConnections.empty){
				result.put(node,possibleEdgeConnections.toSet)
			}
		}
		result
	}
	
	def addLinkListeners(String link,Edge edge,GraphModel g)
	'''
	«link».on('change:source', function() {
		if(!$_disable_events_«g.name.lowEscapeDart»){
			var source = «link».getSourceElement();
			var target = «link».getTargetElement();
			$cb_functions_«g.name.lowEscapeDart».cb_reconnect_edge_«edge.name.lowEscapeDart»(source.attrs.dywaId,target.attrs.dywaId,«link».attrs.dywaId);
			console.log("change link source");
		}
	});
    «link».on('change:target', function() {
    	if(!$_disable_events_«g.name.lowEscapeDart»){
	        var source = «link».getSourceElement();
	        var target = «link».getTargetElement();
	        $cb_functions_«g.name.lowEscapeDart».cb_reconnect_edge_«edge.name.lowEscapeDart»(source.attrs.dywaId,target.attrs.dywaId,«link».attrs.dywaId);
	        console.log("change link target");
        }
    });
    «link».on('change:vertices', function() {
		if(!$_disable_events_«g.name.lowEscapeDart»){
	        $cb_functions_«g.name.lowEscapeDart».cb_update_bendpoint_«edge.name.lowEscapeDart»(«link».get('verticles'),«link».attrs.dywaId);
		        console.log("change link target");
        }
    });
	'''
	
	def targetCheck(GraphModel g, OutgoingEdgeElementConnection group)
	'''
	«FOR possibleTarget:g.nodesTopologically.reachable(g,group).entrySet»
	if(«FOR sub:possibleTarget.key.name.subTypes(g) + #[possibleTarget.key]SEPARATOR " || "»targetType == '«g.name.lowEscapeDart».«sub.name.fuEscapeDart»'«ENDFOR»)
	{
		«FOR incomingGroup:possibleTarget.value.indexed»
		
			var incommingGroupSize«incomingGroup.key» = 0;
			«FOR incomingEdge:incomingGroup.value.connectingEdges.map[subTypesAndType(it.name,g)].flatten»
			incommingGroupSize«incomingGroup.key» += filterEdgesByType(incoming,'«g.name.lowEscapeDart».«incomingEdge.name.fuEscapeDart»').length;
			«ENDFOR»
			«IF incomingGroup.value.connectingEdges.nullOrEmpty»
			incommingGroupSize«incomingGroup.key» += incoming.length;
    		«ENDIF»
			if(«IF incomingGroup.value.upperBound < 0»true«ELSE»incommingGroupSize«incomingGroup.key»<«incomingGroup.value.upperBound»«ENDIF»)
			{
				«g.possibleEdges(group,incomingGroup.value).filter[creatabel].flatMap[name.subTypes(g)+#[it]].indexed.map[n|'''
					var link«n.key» = new joint.shapes.«g.name.lowEscapeDart».«n.value.name.fuEscapeDart»({
					    source: { id: sourceNode.attributes.id }, target: { id: targetNode.attributes.id }
					});
					possibleEdges['«n.value.name.fuEscapeDart»'] = {
						name: '«n.value.name.fuEscapeDart»',
						type: link«n.key»
					};
				'''].join»
			}
		«ENDFOR»
	}
	«ENDFOR»
	'''
	
	def Map<Integer,Edge> indexed(List<Edge> edges) {
		val result = new HashMap
		edges.forEach[e,i|result.put(i,e)]
		result
	}
	
	def Iterable<Edge> possibleEdges(GraphModel g,OutgoingEdgeElementConnection outgoing, IncomingEdgeElementConnection incoming) {
		if(outgoing.connectingEdges.empty && incoming.connectingEdges.empty) {
			return g.edgesTopologically
		}
		if(outgoing.connectingEdges.empty && !incoming.connectingEdges.empty) {
			return incoming.connectingEdges.map[name.subTypesAndType(g).filter(Edge)].flatten
		}
		if(!outgoing.connectingEdges.empty && incoming.connectingEdges.empty) {
			return outgoing.connectingEdges.map[name.subTypesAndType(g).filter(Edge)].flatten
		}
		return incoming.connectingEdges.map[name.subTypesAndType(g).filter(Edge)].flatten.filter[e|outgoing.connectingEdges.map[name.subTypesAndType(g).filter(Edge)].flatten.toSet.contains(e)]
	}
	
	def updateStyleArgs(NodeStyle ns,GraphModel g)
	'''
		«FOR textShape:new Shapes(gc,g).collectSelectorTags(ns.mainShape,"x",0).entrySet.filter[n|new Shapes(gc,g).getIsTextual(n.key)]»
	    cell.model.attr('«textShape.value»/text',"«textShape.key.value.parsePlaceholder»");
	    «ENDFOR»
	'''
	
	def updateStyleArgs(EdgeStyle es)
	'''
		cell.model.attributes.labels.forEach(function (label,idx) {
	    «FOR decorator:es.decorator.filter[n|n.decoratorShape instanceof Text ||n.decoratorShape instanceof MultiText].indexed»
			if(label.attrs.hasOwnProperty('text.pyro«decorator.key»link')){
				cell.model.prop(['labels',idx,'attrs','text.pyro«decorator.key»link','text'],"«decorator.value.decoratorShape.value.parsePlaceholder»");
«««				label.attrs['text.pyro«decorator.key»link'].text = "«decorator.value.decoratorShape.value.parsePlaceholder»";
			}
	    «ENDFOR»
		});
«««		cell.renderLabels();
	'''
	
	def updateAppearance(NodeStyle ns,GraphModel g)
	'''
		«FOR shape:new Shapes(gc,g).collectSelectorTags(ns.mainShape,"x",0).entrySet»
		if('«shape.value»'.endsWith(shapeId)) {
			update_node_apperance_internal(cell,'«shape.value»',
				background_r,background_g,background_b,
				foreground_r,foreground_g,foreground_b,
				lineInVisible,
				lineStyle,
				transparency,
				lineWidth,
				filled,
				angle,
				fontName,
				fontSize,
				fontBold,
				fontItalic,
				imagePath
			);
		}
	    «ENDFOR»
	'''
	
	def updateHighlight(NodeStyle ns,GraphModel g) {
		val s = new Shapes(gc,g)
		val shape = '''«s.selector(ns.mainShape,"x",0)»'''
		'''
		return update_node_highlight_internal(cell,'«shape»',
			background_r,background_g,background_b,
			foreground_r,foreground_g,foreground_b
		);
		'''
	}
	
	
	
	def updateAppearance(EdgeStyle es)
	{
		val l = new LinkedHashMap
		val target = es.decorator.filter[it.location==1.0]
		val source = es.decorator.filter[it.location==0.0]
		es.decorator.forEach[n,idx|l.put(n,'''pyrox«idx»tag''')]	
	'''
		//update textual edge decorators
		cell.model.attributes.labels.forEach(function (label,idx) {
	    «FOR decorator:es.decorator.filter[n|n.decoratorShape instanceof Text ||n.decoratorShape instanceof MultiText].indexed»
			if(shapeId == '«l.get(decorator)»' && label.attrs.hasOwnProperty('text.pyro«decorator.key»link')) {
				update_edge_text_apperance_internal(
					idx,cell,'text.pyro«decorator.key»link',
					background_r,background_g,background_b,
					foreground_r,foreground_g,foreground_b,
					lineInVisible,
					lineStyle,
					transparency,
					lineWidth,
					filled,
					angle,
					fontName,
					fontSize,
					fontBold,
					fontItalic,
					imagePath
					);
			}
	    «ENDFOR»
		});
	    //update edge
	    if(shapeId == 'root') {
		    update_node_apperance_internal(
		    	cell,'.connection',
				background_r,background_g,background_b,
				foreground_r,foreground_g,foreground_b,
				lineInVisible,
				lineStyle,
				transparency,
				lineWidth,
				filled,
				angle,
				fontName,
				fontSize,
				fontBold,
				fontItalic,
				imagePath
		    );
	    }
	    «IF !target.isEmpty»
		//update target marker
		if(shapeId == '«l.get(target.get(0))»') {
		    update_node_apperance_internal(
		    	cell,'.marker-target',
				background_r,background_g,background_b,
				foreground_r,foreground_g,foreground_b,
				lineInVisible,
				lineStyle,
				transparency,
				lineWidth,
				filled,
				angle,
				fontName,
				fontSize,
				fontBold,
				fontItalic,
				imagePath
		    );
		}
	    «ENDIF»
	    «IF !source.isEmpty»
	    //update source marker
		if(shapeId == '«l.get(source.get(0))»') {
		    update_node_apperance_internal(
		    	cell,'.marker-source',
				background_r,background_g,background_b,
				foreground_r,foreground_g,foreground_b,
				lineInVisible,
				lineStyle,
				transparency,
				lineWidth,
				filled,
				angle,
				fontName,
				fontSize,
				fontBold,
				fontItalic,
				imagePath
		    );
	    }
	    «ENDIF»
	'''
	}
	
	def getValue(EObject shape){
		if(shape instanceof Text){
			return shape.value
		}
		if(shape instanceof MultiText){
			return shape.value
		}
		return ""
	}
	
	
	def parsePlaceholder(String s){
		s.parseIterativePlaceholder.parseIndexedPlaceholder
	}
	
	def parseIterativePlaceholder(String s) {
		var result = ""
		var m = Pattern.compile("%s").matcher(s);
		var parameterIdx = 0;
		var preIdx = 0;
		//var postIdx = 0;
		while (m.find()) {
		    var repString = m.group();
		    //add in between
    		result += s.substring(preIdx,m.start)
    		//set post index
    		preIdx = m.end
    		//replace
    		result += '''"+styleArgs[«parameterIdx»]+"'''
    		parameterIdx++
		}
		//suffix
		result += s.substring(preIdx)
	}
	
	def parseIndexedPlaceholder(String s) {
		var result = ""
		//%1$s
		var m = Pattern.compile("%\\d+\\$s").matcher(s);
		var parameterIdx = 0;
		var preIdx = 0;
		//var postIdx = 0;
		while (m.find()) {
		    var repString = m.group();
		    var start = m.start
		    //add in between
    		result += s.substring(preIdx,start)
    		//set post index
    		preIdx = m.end
    		//replace
    		result += '''"+styleArgs[«(repString.number-1)»]+"'''
    		parameterIdx++
		}
		//suffix
		result += s.substring(preIdx)
	}
	
	def getNumber(String input) {
		var Pattern lastIntPattern = Pattern.compile("\\d");
		var Matcher matcher = lastIntPattern.matcher(input);
		if (matcher.find()) {
		    var String someNumberStr = matcher.group();
		    return Integer.parseInt(someNumberStr);
		}
		return 0
	}

	
}
