package de.jabc.cinco.meta.plugin.pyro.backend.core

import de.jabc.cinco.meta.plugin.pyro.util.Generatable
import de.jabc.cinco.meta.plugin.pyro.util.GeneratorCompound

class InitializeSettingsBean extends Generatable {

	new(GeneratorCompound gc) {
		super(gc)
		
	}

	def filename() '''InitializeSettingsBean.java'''

	def content() '''	
		package info.scce.pyro.core;
		
		import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroSettingsController;
		import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroStyleController;
		import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroUserController;
		import info.scce.pyro.util.DefaultColors;
		import info.scce.pyro.IPyroController;
		import javax.annotation.PostConstruct;
		import javax.ejb.Singleton;
		import javax.ejb.Startup;
		
		@Singleton
		@Startup
		public class InitializeSettingsBean {
		
			@javax.inject.Inject
			private PyroSettingsController settingsController;
			
			@javax.inject.Inject
			private PyroStyleController styleController;
			@javax.inject.Inject
			private OrganizationController organizationController;
			
			@javax.inject.Inject
			private PyroUserController userController;
			
			«FOR g:gc.graphMopdels»
		    @javax.inject.Inject
		    private info.scce.pyro.core.«g.name.fuEscapeJava»Controller «g.name.lowEscapeJava»Controller;
		    «ENDFOR»
		
			@PostConstruct
			@javax.transaction.Transactional
			public void initializeSettings() {
				try {
					final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroSettings searchObject = settingsController.createSearchObject(null);
					final java.util.List<de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroSettings> result = settingsController.findByProperties(searchObject);
					
					if (result.size() == 0) {
						
						«IF gc.cpd.hasClosedRegistration»
						«FOR u:gc.cpd.adminUsers»
						{
						
							de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser user = userController.create("«u»");
							user.setisActivated(false);
							user.setusername("«u»");
						}
						«ENDFOR»
						«ENDIF»
						
						final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroStyle style = styleController.create("Style_");
						style.setnavBgColor(DefaultColors.NAV_BG_COLOR);
						style.setnavTextColor(DefaultColors.NAV_TEXT_COLOR);
						style.setbodyBgColor(DefaultColors.BODY_BG_COLOR);
						style.setbodyTextColor(DefaultColors.BODY_TEXT_COLOR);
						style.setprimaryBgColor(DefaultColors.PRIMARY_BG_COLOR);
						style.setprimaryTextColor(DefaultColors.PRIMARY_TEXT_COLOR);
						
						final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroSettings settings = settingsController.create("Settings_");
						settings.setstyle(style);
						settings.setgloballyCreateOrganizations(false);
						
						java.util.Set<IPyroController> bundles = new java.util.HashSet<>();
				        «FOR g:gc.graphMopdels»
				        bundles.add(«g.name.lowEscapeJava»Controller);
				        «ENDFOR»
				        
				        «FOR a:gc.rootPostCreate.indexed»
				        «a.value» hook«a.key» = new «a.value»();
				        hook«a.key».init(bundles,settingsController,styleController);
				        hook«a.key».execute(settings);
				        «ENDFOR»
				        
				        «FOR a:gc.initialOrganizations»
				        this.organizationController.createOrganization("«a»","",null);
				        «ENDFOR»
					}
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
		}
	'''
}
