package de.jabc.cinco.meta.plugin.pyro.backend.core

import de.jabc.cinco.meta.plugin.pyro.util.Generatable
import de.jabc.cinco.meta.plugin.pyro.util.GeneratorCompound
import de.jabc.cinco.meta.plugin.pyro.util.EditorViewPlugin
import java.util.List
import de.jabc.cinco.meta.plugin.pyro.util.EditorViewPluginRegistry

class EditorLayoutService extends Generatable {
	List<EditorViewPlugin> eps

	new(GeneratorCompound gc) {
		super(gc)
		eps = new EditorViewPluginRegistry().getPlugins(gc);
	}

	def fileNameDispatcher() '''EditorLayoutService.java'''

	def contentDispatcher() '''
		package info.scce.pyro.core;
				
		import com.fasterxml.jackson.core.JsonProcessingException;
		import com.fasterxml.jackson.databind.DeserializationFeature;
		import com.fasterxml.jackson.databind.MapperFeature;
		import com.fasterxml.jackson.databind.ObjectMapper;
		import com.fasterxml.jackson.databind.SerializationFeature;
		import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
		import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroProjectController;
		import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroUserController;
		import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroOrganizationController;
		import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroGraphModelPermissionVectorController;
		import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroEditorGridController;
		import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroEditorGridItemController;
		import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroEditorWidgetController;
		import de.ls5.dywa.generated.entity.info.scce.pyro.core.*;
		import de.ls5.dywa.generated.util.Identifiable;
		import info.scce.pyro.core.rest.types.*;
		import info.scce.pyro.core.rest.types.PyroProject;
		import info.scce.pyro.core.rest.types.PyroUser;
		import info.scce.pyro.core.rest.types.PyroOrganization;
		import info.scce.pyro.rest.PyroSelectiveRestFilter;
		import info.scce.pyro.sync.ProjectWebSocket;
		import info.scce.pyro.sync.WebSocketMessage;
		
		import javax.ejb.Singleton;
		import java.util.*;
		import java.util.stream.Collectors;
		import java.util.stream.Stream;
		
		@Singleton
		@javax.transaction.Transactional
		public class EditorLayoutService {
			
			@javax.inject.Inject
			private PyroEditorGridController editorGridController;
			
			@javax.inject.Inject
			private PyroEditorGridItemController editorGridItemController;
			
			@javax.inject.Inject
			private PyroEditorWidgetController editorWidgetController;
			
			public de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorGridItem createGridArea(
				final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorGrid grid, 
				Long x, 
				Long y, 
				Long width, 
				Long height
			) {
				final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorGridItem item = editorGridItemController.create("EditorGridItem_");
				item.setx(x);
				item.sety(y);
				item.setwidth(width);
				item.setheight(height);
				grid.getitems_PyroEditorGridItem().add(item);
				return item;
			}
			
			public de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorWidget createWidget(
				final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorGrid grid,
				final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorGridItem area,
				final String tab,
				final String key
			) {
				final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorWidget widget = editorWidgetController.create("EditorWidget_");
				widget.setarea(area);
				widget.settab(tab);
				widget.setkey(key);
				widget.setposition(0L);	
				widget.setgrid(grid);
				return widget;
			}
			
			private void link(
					final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorGrid grid,
					final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorGridItem area,
					final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorWidget widget) {
						
				if(widget == null) {
					return;
				}
				widget.setarea(area);
				area.getwidgets_PyroEditorWidget().add(widget);
				grid.getavailableWidgets_PyroEditorWidget().remove(widget);
			}
			
			public void setLayout(final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorGrid grid, EditorGridLayout layout) {		
				// remove all existing areas, make widgets invisible
				for (de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorGridItem area: grid.getitems_PyroEditorGridItem()) {
					for (de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorWidget w: area.getwidgets_PyroEditorWidget()) {
						w.setarea(null);
						grid.getavailableWidgets_PyroEditorWidget().add(w);
					}
					area.getwidgets_PyroEditorWidget().clear();
					editorGridItemController.delete(area);
				}
				grid.getitems_PyroEditorGridItem().clear();
				
				// key -> widget
				final Map<String, de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorWidget> widgetMap = new HashMap<>();
				for (de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorWidget w: grid.getavailableWidgets_PyroEditorWidget()) {
					widgetMap.put(w.getkey(), w);
				}
				
				// place widgets
				switch(layout) {
					case DEFAULT:
						link(grid, createGridArea(grid, 0L, 0L, 3L, 3L), widgetMap.get("explorer"));
						link(grid, createGridArea(grid, 0L, 3L, 3L, 3L), widgetMap.get("map"));
						link(grid, createGridArea(grid, 3L, 0L, 6L, 6L), widgetMap.get("canvas"));
						link(grid, createGridArea(grid, 3L, 6L, 6L, 3L), widgetMap.get("properties"));
						link(grid, createGridArea(grid, 9L, 0L, 3L, 3L), widgetMap.get("palette"));
						link(grid, createGridArea(grid, 9L, 3L, 3L, 3L), widgetMap.get("checks"));
						break;
					case MINIMAL:
						link(grid, createGridArea(grid, 0L, 0L, 3L, 3L), widgetMap.get("explorer"));
						link(grid, createGridArea(grid, 3L, 0L, 6L, 6L), widgetMap.get("canvas"));
						link(grid, createGridArea(grid, 9L, 0L, 3L, 3L), widgetMap.get("palette"));
						link(grid, createGridArea(grid, 9L, 3L, 3L, 3L), widgetMap.get("properties"));
						break;
					case MAXIMUM_CANVAS:
						link(grid, createGridArea(grid, 0L, 0L, 3L, 3L), widgetMap.get("explorer"));
						link(grid, createGridArea(grid, 0L, 3L, 3L, 3L), widgetMap.get("palette"));
						link(grid, createGridArea(grid, 0L, 6L, 3L, 3L), widgetMap.get("properties"));
						link(grid, createGridArea(grid, 3L, 0L, 9L, 9L), widgetMap.get("canvas"));
						break;
					case COMPLETE:
						link(grid, createGridArea(grid, 0L, 0L, 3L, 3L), widgetMap.get("explorer"));
						link(grid, createGridArea(grid, 0L, 3L, 3L, 3L), widgetMap.get("map"));
						link(grid, createGridArea(grid, 3L, 0L, 6L, 6L), widgetMap.get("canvas"));
						link(grid, createGridArea(grid, 3L, 6L, 6L, 3L), widgetMap.get("properties"));
						link(grid, createGridArea(grid, 9L, 0L, 3L, 6L), widgetMap.get("palette"));
						link(grid, createGridArea(grid, 9L, 6L, 3L, 3L), widgetMap.get("checks"));
						link(grid, createGridArea(grid, 0L, 6L, 3L, 3L), widgetMap.get("command_history"));
						«var i = 0»
						«FOR pc:eps.filter[pluginComponent.fetchURL!==null].map[pluginComponent]»
						link(grid, createGridArea(grid, «(i % 4) * 3»L, «9 + Math.floorDiv(i, 4) * 3»L, 3L, 3L), widgetMap.get("«pc.key»"));
						«{i = i + 1; ""}»
						«ENDFOR»
						break;
					default:
						break;
				}
			}
		}
	'''
}