package de.jabc.cinco.meta.plugin.pyro.backend.graphmodel.api.impl

import de.jabc.cinco.meta.plugin.pyro.util.Generatable
import de.jabc.cinco.meta.plugin.pyro.util.GeneratorCompound
import mgl.GraphModel
import de.jabc.cinco.meta.plugin.pyro.backend.graphmodel.command.GraphModelCommandExecuter
import mgl.UserDefinedType

class GraphModelFactoryTransientImplementation extends Generatable{
	
	new(GeneratorCompound gc) {
		super(gc)
	}
	
	def filename(GraphModel g)
	'''«g.name.toCamelCase.fuEscapeJava»FactoryImpl.java'''
	
	def content(GraphModel g)
	'''
	package «g.apiImplFQN»;
	import «g.factoryFQN».«g.name.toCamelCase.fuEscapeJava»Factory;
	
	/**
	 * Author zweihoff
	 */
	@javax.enterprise.context.RequestScoped
	public class «g.name.toCamelCase.fuEscapeJava»FactoryImpl implements «g.name.toCamelCase.fuEscapeJava»Factory {
	
	
	    public static «g.name.toCamelCase.fuEscapeJava»Factory init() {
	    	return new «g.name.toCamelCase.fuEscapeJava»FactoryImpl();
	    }
	
	    public «g.apiFQN».«g.name.fuEscapeJava» create«g.name.fuEscapeJava»(String projectRelativePath, String filename)
	    {
	        return create«g.name.fuEscapeJava»();
	    }
	    
	    public «g.apiFQN».«g.name.fuEscapeJava» create«g.name.fuEscapeJava»() {
	    	«g.apiFQN».«g.name.fuEscapeJava» ce = new «g.apiImplFQN».«g.name.fuEscapeJava»Impl();
	        «IF g.hasPostCreateHook»
	        	«g.postCreateHook» ca = new «g.postCreateHook»();
	        	ca.init(executer);
	        	ca.postCreate(ce);
	        «ENDIF»
	        return ce;
	    }
	    «FOR udt:g.elementsAndTypes.filter(UserDefinedType).filter[!isAbstract]»
	    public «g.apiFQN».«udt.name.fuEscapeJava» create«udt.name.fuEscapeJava»() {
	        return new «g.apiImplFQN».«udt.name.fuEscapeJava»Impl();
	    }
	    «ENDFOR»
	}
	
	'''
	
}