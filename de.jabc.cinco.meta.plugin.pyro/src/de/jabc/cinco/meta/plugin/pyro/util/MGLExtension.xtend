package de.jabc.cinco.meta.plugin.pyro.util

import java.util.Collections
import java.util.HashMap
import java.util.LinkedList
import java.util.Map
import java.util.Set
import mgl.Annotation
import mgl.Attribute
import mgl.ComplexAttribute
import mgl.ContainingElement
import mgl.Edge
import mgl.Enumeration
import mgl.GraphModel
import mgl.GraphicalElementContainment
import mgl.GraphicalModelElement
import mgl.ModelElement
import mgl.Node
import mgl.NodeContainer
import mgl.PrimitiveAttribute
import mgl.ReferencedEClass
import mgl.ReferencedModelElement
import mgl.ReferencedType
import mgl.Type
import mgl.UserDefinedType
import org.eclipse.emf.ecore.EAttribute
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.EClassifier
import org.eclipse.emf.ecore.EDataType
import org.eclipse.emf.ecore.EEnum
import org.eclipse.emf.ecore.ENamedElement
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EPackage
import org.eclipse.emf.ecore.EStructuralFeature
import style.ContainerShape
import style.EdgeStyle
import style.Ellipse
import style.Image
import style.NodeStyle
import style.Styles
import de.jabc.cinco.meta.core.utils.InheritanceUtil
import de.jabc.cinco.meta.core.mgl.generator.MGLAlternateGenerator
import org.eclipse.emf.ecore.EReference
import productDefinition.CincoProduct

class MGLExtension {
	
	public static int DEFAULT_WIDTH = 40
	public static int DEFAULT_HEIGHT = 40
	
	Map<GraphModel,Iterable<GraphicalModelElement>> elementMap;
	Map<GraphModel,Iterable<Node>> nodeMap;
	Map<GraphModel,Iterable<Edge>> edgeMap;

	protected extension Escaper = new Escaper
	
	static def instance() {
		new MGLExtension()
	}
	
	private new() {
		elementMap = new HashMap
		nodeMap = new HashMap
		edgeMap = new HashMap
	}
	
	static def String[] primitiveETypes() {
		return #["EString","EBoolean","EInt","EDouble", "EShort", "ELong","EBigInteger","EFloat","EBigDecimal","EByte","EChar"]
	} 
	
	def nodesTopologically(GraphModel g) {
		this.nodes(g)
	}
	
	def hasToExtendContainer(ModelElement me) {
		me instanceof NodeContainer && (me as NodeContainer).extends !== null && !((me as NodeContainer).extends instanceof NodeContainer)
	}
	
	def nodes(GraphModel g){
		if(nodeMap.containsKey(g)) {
			return nodeMap.get(g)
		}
		val elements = g.getNodes().filter(GraphicalModelElement).sortTopologically.filter(Node)
		nodeMap.put(g,elements)
		elements
	}
	
	def edgesTopologically(GraphModel g) {
		this.edges(g)
	}
	
	def edges(GraphModel g){
		if(edgeMap.containsKey(g)) {
			return edgeMap.get(g)
		}
		val elements = g.getEdges().filter(GraphicalModelElement).sortTopologically.filter(Edge)
		edgeMap.put(g,elements)
		elements
	}
	
	def elements(GraphModel g){
		if(elementMap.containsKey(g)) {
			return elementMap.get(g)
		}
		val elements = (this.nodes(g) + this.edges(g)).sortTopologically
		elementMap.put(g,elements)
		elements
	}
	
	private def sortTopologically(Iterable<GraphicalModelElement> elements) {
		elements.sortBy[inheritanceScore]
	}
	
	private def dispatch int inheritanceScore(Node element) {
		var i = 0
		if(element.extends!==null) {
			i--
			i += element.extends.inheritanceScore
		}
		i*(-1) 
	}
	
	private def dispatch int inheritanceScore(Edge element) {
		var i = 0
		if(element.extends!==null) {
			i++
			i += element.extends.inheritanceScore
		}
		i *(-1) 
	}
	
	private def dispatch int inheritanceScore(NodeContainer element) {
		var i = 0
		if(element.extends!==null) {
			i++
			i += element.extends.inheritanceScore
		}
		i *(-1) 
	}
	
	def elements(EPackage g){
		return g.EClassifiers.filter(EClass)
	}
	
	def elementsAndEnums(EPackage g){
		return g.EClassifiers.filter(EClass) + g.EClassifiers.filter(EEnum)
	}
	
	def elementsAndGraphmodel(GraphModel g){
		return g.elements + #[g]
	}
	
	def boolean isModelElement(Attribute attribute, GraphModel model){
		model.elementsAndGraphmodel.exists[name.equals(attribute.type)]
	}
	
	def isFile(Attribute attr) {
		if(attr instanceof PrimitiveAttribute){
			return attr.annotations.exists[name.equals("file")]
		}
		false
	}
	
	def getFile(Attribute attr) {
		if(attr instanceof PrimitiveAttribute){
			return attr.annotations.findFirst[name.equals("file")]
		}
		null
	}
	
	def type(Attribute attr) {
		if(attr instanceof PrimitiveAttribute){
			return attr.type.getName
		}
		if(attr instanceof ComplexAttribute) {
			return attr.type.name
		}
		throw new IllegalStateException("Exhaustive if");
	}
	
	def type(EAttribute attr) {
		attr.getEAttributeType
	}
	
	def type(EReference attr) {
		attr.getEReferenceType
	}
	
	def htmlType(Attribute attr){
		switch(attr.type){
			case "EBoolean": return '''checkbox'''
			case "EInt": return '''number'''
			case "ELong": return '''number'''
			case "EBigInteger": return '''number'''
			case "EByte": return '''number'''
			case "EShort": return '''number'''
			case "EFloat": return '''number'''
			case "EBigDecimal": return '''number'''
			case "EDouble": return '''number'''
			default: return '''text'''
		}
	}
	
	def javaType(Attribute attr,GraphModel g){
		if(attr.type.getEnum(g)!==null){
			return g.apiFQN+"."+attr.type.fuEscapeJava
		}
		if(!attr.isPrimitive(g)) {
			return '''«attr.type.fuEscapeJava»'''
		}
		switch(attr.type){
			case "EBoolean": {
				if(attr.list){
					return '''Boolean'''
				}
				return '''boolean'''
			}
			case "EInt":{
				if(attr.list){
					return '''Integer'''
				}
				return '''int'''
			} 
			case "EDouble":{
				if(attr.list) {
					return '''Double'''
				}
				return '''double'''
			}
			case "ELong": {
				if(attr.list){
					return '''Integer'''
				}
				return '''int'''
			} 
			case "EBigInteger":{
				if(attr.list){
					return '''Integer'''
				}
				return '''int'''
			} 
			case "EByte": {
				if(attr.list){
					return '''Integer'''
				}
				return '''int'''
			} 
			case "EShort": {
				if(attr.list){
					return '''Integer'''
				}
				return '''int'''
			} 
			case "EFloat":{
				if(attr.list) {
					return '''Double'''
				}
				return '''double'''
			}
			case "EBigDecimal": {
				if(attr.list) {
					return '''Double'''
				}
				return '''double'''
			}
			default: return '''String'''
		}
	}
	
	def ecoreType(EAttribute attr,EPackage g){
		if(attr.type instanceof EEnum){
			return g.apiFQN+"."+attr.type.name.fuEscapeJava
		}
		if(!attr.isPrimitive(g)) {
			return '''«attr.type.name.fuEscapeJava»'''
		}
		switch(attr.type.name){
			case "EBoolean": {
				if(attr.list){
					return '''Boolean'''
				}
				return '''boolean'''
			}
			case "EInt":{
				if(attr.list){
					return '''Integer'''
				}
				return '''int'''
			} 
			case "EDouble":{
				if(attr.list) {
					return '''Double'''
				}
				return '''double'''
			}
			case "ELong": {
				if(attr.list){
					return '''Integer'''
				}
				return '''int'''
			} 
			case "EBigInteger":{
				if(attr.list){
					return '''Integer'''
				}
				return '''int'''
			} 
			case "EByte": {
				if(attr.list){
					return '''Integer'''
				}
				return '''int'''
			} 
			case "EShort": {
				if(attr.list){
					return '''Integer'''
				}
				return '''int'''
			} 
			case "EFloat":{
				if(attr.list) {
					return '''Double'''
				}
				return '''double'''
			}
			case "EBigDecimal": {
				if(attr.list) {
					return '''Double'''
				}
				return '''double'''
			}
			default: return '''String'''
		}
	}
	
	def ecoreType(EReference attr,EPackage g){
		if(attr.type instanceof EEnum){
			return g.apiFQN+"."+attr.type.name.fuEscapeJava
		}
		return '''«attr.type.name.fuEscapeJava»'''
	}
	
	
	def getPrimeReferencedGraphModels(GraphModel g) {
		g.primeRefs.map[referencedElement.graphModel].toSet
	}
	
	def getPrimeReferencingElements(GraphModel g,GraphModel refG) {
		g.primeRefs.filter[referencedElement.graphModel.equals(refG)].toSet
	}
	
	def getAllPrimeRefs(GraphModel g){
		g.primeRefs + g.ecorePrimeRefs
	}
	
	def getPrimeRefs(GraphModel g){
		this.nodes(g).filter[primeReference!==null].map[primeReference].filter(ReferencedModelElement)
	}
	
	def getEcorePrimeRefs(GraphModel g){
		this.nodes(g).filter[primeReference!==null].map[primeReference].filter(ReferencedType).toSet
	}
	
	def GraphModel getGraphModel(ModelElement me){
		if(me instanceof GraphModel){
			return me
		}
		if(me.eContainer === null){
			return null
		}
		if(me.eContainer instanceof ModelElement){
			return (me.eContainer as ModelElement).graphModel
		}
		return null
	}
	
	def getGraphModel(EObject me){
		if(me instanceof ModelElement){
			return me.graphModel
		}
		if(me instanceof EClassifier){
			return me.EPackage
		}
		return null
	}
	
	def getReferencedElement(ReferencedModelElement rt){
		rt.type
	}
	
	def getReferenceName(ReferencedModelElement rt){
		rt.name
	}
	
	def getReferencedElementAttributeName(ReferencedModelElement rt){
		val a = rt.annotations.findFirst[name.equals("pvLabel")]
		if(a!==null){
			return a.value.get(0)
		}
		return null
	}
	
	def hasClassAnnotation(Annotation a) {
		val ca = #["postDelete",
		"postCreate",
		"preDelete",
		"postMove",
		"postResize",
		"postSelect",
		"postAttributeChange",
		"generatable",
		"contextMenuAction",
		"doubleClickAction",
		"mcam_checkmodule",
		"preSave",
		"pyroEditorButton",
		"pyroInterpreter"
		]
		ca.contains(a.name)
	}
	
	def hasIncludeResourcesAnnotation(GraphModel g) {
		g.annotations.exists[name.equals("pyroGeneratorResource")]
	}
	
	
	def hasIncludeJARAnnotation(GraphModel g) {
		g.annotations.exists[name.equals("pyroAdditionalJAR")]
	}
	
	def defaultRights(GraphModel g) {
		if(g.annotations.exists[name.equals("pyroUserRights")]) {
			return g.annotations.findFirst[name.equals("pyroUserRights")].value.filter[r|r.equals("create")||r.equals("update")||r.equals("delete")]
		}
		#[]
	}
	
	def dispatch boolean hasAppearanceProvider(GraphModel g,Styles styles) {
		g.elements.filter[!isIsAbstract].exists[it.hasAppearanceProvider(styles)]
	}
	def hasChecks(GraphModel g) {
		g.annotations.exists[name.equals("mcam")]
	}
	
	def isGenerating(GraphModel g) {
		g.annotations.exists[name.equals("generatable")]
	}
	
	def isInterpreting(GraphModel g) {
		g.annotations.exists[name.equals("pyroInterpreter")]
	}
	
	def interperters(GraphModel g) {
		g.annotations.filter[name.equals("pyroInterpreter")]
	}
	
	def generators(GraphModel g) {
		g.annotations.filter[name.equals("generatable")]
	}
	
	def isHidden(Attribute attr){
		(attr.annotations.exists[name.equals("propertiesViewHidden")])
	}
	
	def isReadOnly(Attribute attr){
		(attr.annotations.exists[name.equals("readOnly")])
	}
	
	def hasPostCreateHook(ModelElement me){
		me.hasHook("postCreate")
	}
	
	def hasJumpToAnnotation(ModelElement me) {
		me.hasAnnotation("jumpToPrime",true)
	}
	
	def getPostCreateHook(ModelElement me){
		me.getHookFQN("postCreate")
	}
	
	def hasPostDeleteHook(ModelElement me){
		me.hasHook("postDelete")
	}
	
	def getPostDeleteHook(ModelElement me){
		me.getHookFQN("postDelete")
	}
	
	def hasPreDeleteHook(ModelElement me){
		me.hasHook("preDelete")
	}
	
	def getPreDeleteHook(ModelElement me){
		me.getHookFQN("preDelete")
	}
	
	def getEditorButtons(GraphModel g) {
		g.annotations.filter[name.equals("pyroEditorButton")]
	}
	
	
	def hasPostMove(ModelElement me){
		me.hasHook("postMove")
	}
	
	def getPostMoveHook(ModelElement me){
		me.getHookFQN("postMove")
	}
	
	def hasPostResize(ModelElement me){
		me.hasHook("postResize")
	}
	
	def getPostResizeHook(ModelElement me){
		me.getHookFQN("postResize")
	}
	
	def hasPostSelect(ModelElement me){
		me.hasHook("postSelect")
	}
	 
	def getPostSelectHook(ModelElement me){
		me.getHookFQN("postSelect")
	}
	
	def hasPostAttributeValueChange(ModelElement me){
		me.hasHook("postAttributeChange")
	}
	
	def getPostAttributeValueChange(ModelElement me){
		me.getHookFQN("postAttributeChange")
	}
	
	def hasPreSave(ModelElement me){
		me.hasHook("preSave")
	}
	
	def getPreChange(ModelElement me){
		me.getHookFQN("preSave")
	}
	
	def hasAnnotation(ModelElement me,String annotation){
		me.hasAnnotation(annotation,false)
	}
	
	def hasAnnotation(ModelElement me,String annotation,boolean includeAbstract){
		(me.annotations.exists[name.equals(annotation)]) && (!me.isIsAbstract || includeAbstract)
	}
	
	def hasHook(ModelElement me,String hook){
		(me.annotations.exists[name.equals(hook)&&!value.nullOrEmpty]) && (!me.isIsAbstract)
	}
	
	def getHookFQN(ModelElement me,String hook){
		me.annotations.findFirst[name.equals(hook)&&!value.nullOrEmpty].value.get(0)
	}
	
	def primeCreatabel(GraphicalModelElement gme){
		(!gme.annotations.exists[name.equals("disable")&&value.contains("create")]) && !gme.isIsAbstract && gme.isPrime
	}
	
	def creatabel(GraphicalModelElement gme){
		(!gme.annotations.exists[name.equals("disable")&&value.contains("create")]) && !gme.isIsAbstract && !gme.isPrime
	}
	
	def creatabelPrimeRef(GraphicalModelElement gme){
		(!gme.annotations.exists[name.equals("disable")&&value.contains("create")]) && !gme.isIsAbstract
	}
	
	def importedPrimeNodes(Iterable<Node> nodes,GraphModel g){
		nodes.filter[prime].map[primeReference].filter[n|!g.elementsAndGraphmodel.toList.contains(n.type)]
	}
	
	def importetPrimeTypes(GraphModel g) {
		g.nodesTopologically.importedPrimeNodes(g).groupBy[type].entrySet.map[value.get(0)]
	}
	
	def isPrime(GraphicalModelElement gme) {
		if(gme instanceof Node){
			if(gme.primeReference!==null) {
				return true
			}
		}
		return false
	}
	
	def dispatch getType(ReferencedEClass type) {
		type.type
	}
	
	def dispatch getType(ReferencedModelElement type) {
		type.type
	}
	
	def isEcorePrime(GraphicalModelElement gme) {
		if(gme instanceof Node){
			if(gme.primeReference!==null) {
				return gme.primeReference instanceof ReferencedEClass
			}
		}
		return false
	}
	
	def isModelPrime(GraphicalModelElement gme) {
		if(gme instanceof Node){
			if(gme.primeReference!==null) {
				return gme.primeReference instanceof ReferencedModelElement
			}
		}
		return false
	}
	
	def removable(GraphicalModelElement gme){
		(!gme.annotations.exists[name.equals("disable")&&value.contains("delete")])
	}
	
	def resizable(GraphicalModelElement gme){
		(!gme.annotations.exists[name.equals("disable")&&value.contains("resize")])
	}
	
	def information(GraphicalModelElement gme){
		gme.attributesExtended.exists[annotations.exists[name.equals("pyroInformation")]]
	}
	
	def multiline(Attribute it){
		annotations.exists[name.equals("multiLine")]
	}
	
	def String displayName(ModelElement g) {
		if(g.annotations.exists[name.equals("displayName")]) {
			return g.annotations.findFirst[name.equals("displayName")].value.get(0)
		}
		return g.name.fuEscapeDart
	}
	
	
	
	def directlyEditable(Node node) {
		node.attributesExtended.filter(PrimitiveAttribute).exists[annotations.exists[name.equals("pyroDirectEdit")]]
	}
	
	def directlyEditableAttribute(Node node) {
		node.attributesExtended.filter(PrimitiveAttribute).findFirst[annotations.exists[name.equals("pyroDirectEdit")]]
	}
	
	def informationAttribute(GraphicalModelElement gme){
		gme.attributesExtended.findFirst[annotations.exists[name.equals("pyroInformation")]]
	}
	
	def boolean connectable(Node node,GraphModel g){
		node.name.parentTypes(g).filter(Node).exists[!outgoingEdgeConnections.empty]
	}
	
	def movable(GraphicalModelElement gme){
		(!gme.annotations.exists[name.equals("disable")&&value.contains("move")])
	}
	
	def selectbale(GraphicalModelElement gme){
		(!gme.annotations.exists[name.equals("disable")&&value.contains("select")])
	}
	
	def hasIcon(GraphicalModelElement gme){
		(gme.annotations.exists[name.equals("icon")&&!value.empty]) && (gme instanceof Node)
	}
	
	def eclipseIconPath(GraphicalModelElement gme){
		gme.annotations.findFirst[name.equals("icon")].value.get(0)
	}
	
	def boolean getHasClosedRegistration(CincoProduct product) {
		product.annotations.exists[name.equals("pyroClosedRegistration")]
	}
	
	def getAdminUsers(CincoProduct product) {
		product.annotations.filter[name.equals("pyroClosedRegistration")].map[value].flatten
	}
	
	def hasCustomAction(ModelElement gme){
		(gme.annotations.exists[name.equals("contextMenuAction")&&!value.empty])
	}
	
	def getCustomAction(ModelElement gme){
		(gme.annotations.filter[name.equals("contextMenuAction")&&!value.empty])
	}
	
	def hasDoubleClickAction(ModelElement gme){
		(gme.annotations.exists[name.equals("doubleClickAction")&&!value.empty])
	}
	
	def getDoubleClickAction(ModelElement gme){
		(gme.annotations.filter[name.equals("doubleClickAction")&&!value.empty])
	}
	
	def isCreatable(GraphModel it) {
		return !annotations.exists[name.equals("disable")&&value.contains("create")]
	}
	
	def isDeletable(GraphModel it) {
		return !annotations.exists[name.equals("disable")&&value.contains("delete")]
	}
	
	def isEditable(GraphModel it) {
		return !annotations.exists[name.equals("disable")&&value.contains("edit")]
	}
	
	def isReadable(GraphModel it) {
		return !annotations.exists[name.equals("disable")&&value.contains("read")]
	}
	
	def creatableGraphmodels(GeneratorCompound gc) {
		gc.graphMopdels.filter[isCreatable]
	}
	
	def iconPath(GraphicalModelElement gme,String g){
		return gme.iconPath(g,true)
	}
	
	def iconPath(GraphModel gme,String g){
		return gme.iconPath(g,true)
	}
	
	def iconPath(GraphicalModelElement gme,String g,boolean includeFile){
		val path = gme.eclipseIconPath
		'''img/«g»«IF includeFile»«path.substring(path.lastIndexOf("/"),path.length)»«ENDIF»'''
	}
	
	def cpdImagePath(String path) {
		"cpd/"+path.substring(0,path.lastIndexOf("/"))
	}
	
	def iconPath(Image gme,String g,boolean includeFile){
		val path = gme.path
		'''img/«g»«IF includeFile»«path.substring(path.lastIndexOf("/"),path.length)»«ENDIF»'''
	}
	
	def iconPath(GraphModel gme,String g,boolean includeFile){
		val path = gme.iconPath
		'''img/«g»«IF includeFile»«path.substring(path.lastIndexOf("/"),path.length)»«ENDIF»'''
	}
	
	def paletteGroup(GraphicalModelElement gme){
		val groupId = gme.annotations.findFirst[name.equals("palette")]
		if(groupId === null){
			switch(gme){
				NodeContainer:return "Container"
			}
			return "Node"
		} else{
			if(groupId.value.empty)return "Node"
			return groupId.value.get(0)
		}
		
	}
	
	def elementsAndTypes(GraphModel g){
		return g.elements + g.types.filter(UserDefinedType)
	}
	
	def elementsAndTypesAndGraphModel(GraphModel g){
		return g.elementsAndTypes + #[g]
	}
	
	def elementsAndTypesAndEnums(GraphModel g){
		return g.elements + g.types.filter(UserDefinedType) +g.enumerations
	}
	
	def boolean getIsType(ModelElement element){
		element instanceof UserDefinedType
	}
	
	def enumerations(GraphModel g){
		return g.types.filter(Enumeration)
	}
	
	def isPrimitive(Attribute attr,GraphModel g){
		primitiveETypes.contains(attr.type) || attr.type.getEnum(g)!==null
	}
	
	def isPrimitive(EStructuralFeature attr){
		attr instanceof EAttribute
	}
	
	def isPrimitive(EStructuralFeature attr,EPackage g){
		if(attr.EType instanceof EDataType) {
			return primitiveETypes.contains((attr.EType as EDataType).name)		
		}
		false
	}
	
	def getEnum(String type,GraphModel g) {
		g.enumerations.findFirst[name.equals(type)]
	}
	
	def getEnum(String type,EPackage g) {
		g.EClassifiers.filter(EEnum).findFirst[name.equals(type)]
	}
	
	def init(Attribute it,GraphModel g,String prefix){
		if(isPrimitive(g)){
			if(type.getEnum(g)!==null){
				return '''«prefix»«type».«type.getEnum(g).literals.get(0).escapeDart»'''
			}
			if(!defaultValue.nullOrEmpty) {
				return '''«primitiveBolster»«defaultValue»«primitiveBolster»'''
			}
			return '''«primitiveBolster»«initValue»«primitiveBolster»'''
		}
		'''null'''
	}
	
	def init(EStructuralFeature it){
		if(isPrimitive()){
			if(it.EType instanceof EEnum){
				return '''«it.EType.name.fuEscapeDart».«it.defaultValue»'''
			}
			if(it instanceof EAttribute) {
				if(defaultValue!==null) {
					return '''«primitiveBolster»«defaultValue»«primitiveBolster»'''
				}
				return '''«primitiveBolster»«initValue»«primitiveBolster»'''
				
			}
		}
		'''null'''
	}
	
	def initValue(Attribute attr){
		switch(attr.type){
			case "EBoolean": return '''false'''
			case "EInt": return '''0'''
			case "ELong": return '''0'''
			case "EBigInteger": return '''0'''
			case "EByte": return '''0'''
			case "EShort": return '''0'''
			case "EFloat": return '''0.0'''
			case "EBigDecimal": return '''0.0'''
			case "EDouble": return '''0.0'''
			default: return ''''''
			
		}
	}
	
	def initValue(EAttribute attr){
		switch(attr.EType.name){
			case "EBoolean": return '''false'''
			case "EInt": return '''0'''
			case "ELong": return '''0'''
			case "EBigInteger": return '''0'''
			case "EByte": return '''0'''
			case "EShort": return '''0'''
			case "EFloat": return '''0.0'''
			case "EBigDecimal": return '''0.0'''
			case "EDouble": return '''0.0'''
			default: return ''''''
			
		}
	}
	
	def canContain(ModelElement element){
		element instanceof GraphModel || element instanceof NodeContainer
	}
	
	def isExtending(ModelElement element){
		switch element {
			NodeContainer: {
				return element.extends !== null
			}
			Node: {
				return element.extends !== null
			}
			Edge: {
				return element.extends !== null
			}
			UserDefinedType: {
				return element.extends !== null
			}
		}
		return false
	}
	
	def isExtending(EClass element){
		!element.ESuperTypes.empty
	}
	
	def extendingWithouTypes(ModelElement element){
		return element.extendingModelElement(false,"");
	}
	
	def extendingWithouTypes(ModelElement element,String prefix){
		return element.extendingModelElement(false,prefix);
	}
	
	def extending(ModelElement element){
		return element.extendingModelElement(true,"");
	}
	
	def extending(ModelElement element,String prefix){
		return element.extendingModelElement(true,prefix);
	}
	
	def extending(EClass element){
		if(element instanceof EPackage)return "core.PyroModelFile";
		if(element.ESuperTypes.empty)return "core.PyroElement"
		return element.ESuperTypes.map[scopedTypeName(element.EPackage)].join(", ");
	}
	
	def typeName(ModelElement e,GraphModel g) '''imp_«g.name.lowEscapeDart».«e.name.fuEscapeDart»'''
	def typeName(ENamedElement e,EPackage g){
		return '''imp_«g.name.lowEscapeDart».«e.name.fuEscapeDart»'''
	}
	
	def scopedTypeName(EClass t,EPackage g){
		if(t.EPackage.equals(g)) {
			return t.name.fuEscapeDart
		} else {
			return t.typeName(t.EPackage)
		}
	}
	def scopedTypeName(EClassifier t,EPackage g){
		if(t.EPackage.equals(g)) {
			return t.name.fuEscapeDart
		} else {
			return t.typeName(t.EPackage)
		}
	} 
	
	
	def extendingModelElement(ModelElement element,boolean extendType,String prefix){
		switch element {
			GraphModel: return prefix+"GraphModel"
			NodeContainer: {
				if (element.extends === null) return prefix+"Container"
				return element.extends.name
			}
			Node: {
				if (element.extends === null) return prefix+"Node"
				return element.extends.name
			}
			Edge: {
				if (element.extends === null) return prefix+"Edge"
				return element.extends.name
			}
			UserDefinedType: {
				if (element.extends === null || !extendType) return prefix+"PyroElement"
				return element.extends.name
			}
			Enumeration: {
				return ""
			}
		}
		return ""
	}
	
	def GraphicalModelElement extendingModelType(ModelElement element){
		switch element {
			NodeContainer: {
				if (element.extends === null) return null
				return element.extends
			}
			Node: {
				if (element.extends === null) return null
				return element.extends
			}
			Edge: {
				if (element.extends === null) return null
				return element.extends
			}
		}
		return null
	}
	
	def javaExtending(ModelElement element){
		switch element {
			GraphModel: return "graphmodel.GraphModel"
			NodeContainer: {
				if (element.extends === null) return "graphmodel.Container"
				return element.extends.name
			}
			Node: {
				if (element.extends === null) return "graphmodel.Node"
				return element.extends.name
			}
			Edge: {
				if (element.extends === null) return "graphmodel.Edge"
				return element.extends.name
			}
			UserDefinedType: {
				if (element.extends === null) return "graphmodel.PyroElement"
				return element.extends.name
			}
			Enumeration: {
				return ""
			}
		}
		return ""
	}
	
	def primitiveDartType(Attribute attr,GraphModel g){
		if(attr.type.getEnum(g)!==null){
			return attr.type.getEnum(g).name.fuEscapeDart
		}
		switch(attr.type){
			case "EBoolean": return '''bool'''
			case "EInt": return '''int'''
			case "ELong": return '''int'''
			case "EBigInteger": return '''int'''
			case "EByte": return '''int'''
			case "EShort": return '''int'''
			case "EFloat": return '''double'''
			case "EBigDecimal": return '''double'''
			case "EDouble": return '''double'''
			default: return '''String'''
			
		}
	}
	
	def primitiveDartType(EStructuralFeature attr,EPackage g){
		if(attr.EType instanceof EEnum){
			return attr.EType.name
		}
		switch(attr.EType.name){
			case "EBoolean": return '''bool'''
			case "EInt": return '''int'''
			case "ELong": return '''int'''
			case "EBigInteger": return '''int'''
			case "EByte": return '''int'''
			case "EShort": return '''int'''
			case "EFloat": return '''double'''
			case "EBigDecimal": return '''double'''
			case "EDouble": return '''double'''
			default: return '''String'''
			
		}
	}
	
	def primitiveJavaType(EStructuralFeature attr,EPackage g){
		if(attr.EType instanceof EEnum){
			if(attr.list){
				return '''java.util.List<«attr.EType.name.fuEscapeJava»>'''
			}
			return attr.EType.name.fuEscapeJava
		}
		if(attr.list){
			switch(attr.EType.name){
			case "EBoolean": return '''java.util.List<Boolean>'''
			case "EInt": return '''java.util.List<Integer>'''
			case "EDouble": return '''java.util.List<Double>'''
			case "ELong": return '''java.util.List<Integer>'''
			case "EBigInteger": return '''java.util.List<Integer>'''
			case "EByte": return '''java.util.List<Integer>'''
			case "EShort": return '''java.util.List<Integer>'''
			case "EFloat": return '''java.util.List<Double>'''
			case "EBigDecimal": return '''java.util.List<Double>'''
			default: return '''java.util.List<String>'''
			
		}
		}
		switch(attr.EType.name){
			case "EBoolean": return '''boolean'''
			case "EInt": return '''int'''
			case "ELong": return '''int'''
			case "EBigInteger": return '''int'''
			case "EByte": return '''int'''
			case "EShort": return '''int'''
			case "EFloat": return '''double'''
			case "EBigDecimal": return '''double'''
			case "EDouble": return '''double'''
			default: return '''String'''
			
		}
	}
	
	def complexJavaType(EStructuralFeature attr,EPackage g){
		if(attr.list){
			 return '''java.util.List<info.scce.pyro.«g.name.lowEscapeJava».rest.«attr.EType.name.fuEscapeJava»>'''
		}
		return '''info.scce.pyro.«g.name.lowEscapeJava».rest.«attr.EType.name.fuEscapeJava»'''
		
	}
	
	def serialize(Attribute it,GraphModel g,String s){
		if(isPrimitive(g)){
			if(type.getEnum(g)!==null){
				return '''«type.fuEscapeDart»Parser.toJSOG(«s»)'''
			}
			switch(type){
				case "EBoolean": return '''«s»?"true":"false"'''
				case "EInt": return '''«s»'''
				case "ELong": return '''«s»'''
				case "EBigInteger": return '''«s»'''
				case "EByte": return '''«s»'''
				case "EShort": return '''«s»'''
				case "EFloat": return '''«s»'''
				case "EBigDecimal": return '''«s»'''
				case "EDouble": return '''«s»'''
				default: return '''«s»'''
				
			}
		}
		return '''«s».toJSOG(cache)'''
	}
	
	def serialize(EStructuralFeature it,EPackage g,String s
	){
		if(isPrimitive){
			if(EType.name.getEnum(g)!==null){
				return '''«EType.name.fuEscapeDart»Parser.toJSOG(«s»)'''
			}
			switch(EType.name){
				case "EBoolean": return '''«s»?"true":"false"'''
				case "EInt": return '''«s»'''
				case "ELong": return '''«s»'''
				case "EBigInteger": return '''«s»'''
				case "EByte": return '''«s»'''
				case "EShort": return '''«s»'''
				case "EDouble": return '''«s»'''
				case "EFloat": return '''«s»'''
				case "EBigDecimal": return '''«s»'''
				default: return '''«s»'''
				
			}
		}
		return '''«s».toJSOG(cache)'''
	}
	
	def deserialize(Attribute it,GraphModel g) {
		if(isPrimitive(g)){
			if(type.getEnum(g)!==null)return ""
			switch(type){
				case "EBoolean": return '''=="true"||jsogObj==true'''
				case "EInt": return ''''''
				case "ELong": return ''''''
				case "EBigInteger": return ''''''
				case "EByte": return ''''''
				case "EShort": return ''''''
				case "EDouble": return ''''''
				case "EFloat": return ''''''
				case "EBigDecimal": return ''''''
				default: return '''.toString()'''
				
			}
		}
		return ".toString()"
	}
	
	def deserialize(EStructuralFeature it,EPackage g) {
		if(isPrimitive) {
			if(EType.name.getEnum(g)!==null)return ""
			switch(EType.name){
				case "EBoolean": return '''=="true"||jsogObj==true'''
				case "EInt": return ''''''
				case "ELong": return ''''''
				case "EBigInteger": return ''''''
				case "EByte": return ''''''
				case "EShort": return ''''''
				case "EDouble": return ''''''
				case "EFloat": return ''''''
				case "EBigDecimal": return ''''''
				default: return '''.toString()'''
				
			}
		}
		return ".toString()"
	}
	
	def complexDartType(Attribute attr){
		attr.type
	}
	
	def complexDartType(EStructuralFeature attr){
		attr.EType.name
	}
	
//	def List<String> selfAndSubTypeNames(String typeName,GraphModel g){
//		val l = new LinkedList
//		l.add(typeName)
//		l.addAll(typeName.subTypes(g).map[name])
//		l
//	}


	def dispatch Iterable<EClass> subTypes(String typeName,EPackage g){
		 g.EClassifiers.filter(EClass).filter[ESuperTypes.map[name].contains(typeName)]
	}
	
	def Iterable<ModelElement> parentTypes(String typeName,GraphModel g){
		if(typeName === null){
			return Collections.EMPTY_LIST
		}
		val all = g.elementsAndGraphmodel
		val subType = all.findFirst[name.equals(typeName)]
		if(subType !== null) {
			if(subType instanceof Node){
				val l = new LinkedList
				l.add(subType)
				if(subType.extends!==null){
					l.addAll(subType.extends.name.parentTypes(g))					
				}
				return l.filter(ModelElement)
			}
			if(subType instanceof Edge){
				val l = new LinkedList
				l.add(subType)
				if(subType.extends !==null){
					l.addAll(subType.extends.name.parentTypes(g))					
				}
				return l.filter(ModelElement)
			}
			if(subType instanceof GraphModel){
				return #[subType]
			}
		}
		return Collections.EMPTY_LIST
	}
	
	def dispatch Iterable<ModelElement> subTypesAndType(String typeName,GraphModel g){
		return g.elementsAndTypesAndGraphModel.filter[name.equals(typeName)] + typeName.subTypes(g).filter(ModelElement)
	}
	
	def dispatch Iterable<EClass> subTypesAndType(String typeName,EPackage g){
		
		return g.EClassifiers.filter(EClass).filter[name.equals(typeName)] +typeName.subTypes(g).filter(EClass)
	}
	
	def dispatch Iterable<ModelElement> subTypes(String typeName,GraphModel g){
		
		val directSubTypes = g.elementsAndTypes.filter[isExtending].filter[n|extending(n).equals(typeName)]
		return directSubTypes + (directSubTypes.map[n|n.name.subTypes(g).filter(ModelElement)].flatten)
	}
	
	def primitiveBolster(Attribute attr){
		switch(attr.type){
			case "EString": return '''"'''
			case "EChar": return '''"'''
			default: return ""
			
		}
	}
	
	def primitiveBolster(EAttribute attr){
		switch(attr.EType.name){
			case "EString": return '''"'''
			case "EChar": return '''"'''
			default: return ""
			
		}
	}
	
	def dispatch styling(Node node, Styles styles){
		styles.styles.findFirst[name.equals(node.styleAnnotation(styles).value.get(0))]
	}
	
	def dispatch styling(Edge edge, Styles styles){
		styles.styles.findFirst[name.equals(edge.styleAnnotation(styles).value.get(0))]
	}
	
	def styleAnnotation(GraphicalModelElement gme, Styles styles){
		gme.annotations.findFirst[name.equals("style")]
	}
	
	def dispatch Iterable<Image> getImages(NodeStyle ns) {
		ns.mainShape.collectImages
	}
	def dispatch Iterable<Image> getImages(EdgeStyle ns) {
		ns.decorator.map[decoratorShape].map[collectImages].flatten
	}
	
	def Iterable<Image> collectImages(EObject abs) {
		if(abs instanceof Image) {
			return #[abs]
		}
		if(abs instanceof ContainerShape) {
			return abs.children.map[collectImages(it)].flatten.filter[it!==null]
		}
		#[]
	}
	
	def getImage(NodeStyle ns) {
		ns.mainShape
	}
	
	def dispatch boolean hasAppearanceProvider(Node n,Styles styles) {
		val styleForNode = n.styleFor(styles)
		return !styleForNode.appearanceProvider.nullOrEmpty
	}
	
	def dispatch boolean hasAppearanceProvider(Edge n,Styles styles) {
		val styleForEdge = n.styleFor(styles)
		return !styleForEdge.appearanceProvider.nullOrEmpty
	}
	
	def dispatch styleFor(Node n,Styles styles) {
		n.styling(styles) as NodeStyle
	}
	
	def dispatch styleFor(Edge n,Styles styles) {
		n.styling(styles) as EdgeStyle
	}
	
	
	
	def isList(Attribute attr){
		attr.upperBound>1 || attr.upperBound<0
	}
	
	def isList(EStructuralFeature attr){
		attr.upperBound>1 || attr.upperBound<0
	}
	
	def getGroupContainables(GraphicalElementContainment group,GraphModel g) {
		if(group.types.empty) {
			return g.nodes.filter[!isIsAbstract]
		}
		(group.types.filter[!isIsAbstract] + group.types.map[name].map[subTypes(g)].flatten).filter(ModelElement).filter[!isIsAbstract].toSet
	}
	
	
	def Set<GraphicalModelElement> possibleEmbeddingTypes(ContainingElement ce,GraphModel g){
		val directContainable = ce.containableElements.map[types].flatten
		val subTypesOfDirectContainable = directContainable.map[n|n.name.subTypes(g)].flatten.filter(GraphicalModelElement)
		if(ce instanceof NodeContainer){
			if(ce.extends !== null){
				if(ce.extends instanceof NodeContainer){
					return (directContainable + subTypesOfDirectContainable + (ce.extends as NodeContainer).possibleEmbeddingTypes(g)).toSet				
				}
			}
		}
		if(directContainable.empty){
			return this.nodes(g).filter(GraphicalModelElement).toSet
		}
		
		return (directContainable + subTypesOfDirectContainable).toSet
	}
	
	def Iterable<GraphicalElementContainment> allContainableElement(ContainingElement ce,GraphModel g){
		val containable = ce.containableElements
		if(ce instanceof NodeContainer){
			if(ce.extends !== null){
				if(ce.extends instanceof NodeContainer){
					 containable += (ce.extends as NodeContainer).allContainableElement(g)				
				}
			}
		}
		
		return containable
	}
	
	def Iterable<Edge> parentEgdes(Edge e){
		if(e.extends!==null){
			return #[e] + parentEgdes(e.extends)
		}
		return #[e]
	}
	
	def Set<Edge> possibleOutgoing(Node node,GraphModel g){
		val directOutgoing = node.outgoingEdgeConnections.map[connectingEdges].flatten
		if(node.outgoingEdgeConnections.exists[connectingEdges.empty]){
			return this.edges(g).toSet
		}
		val subTypesOfDorectOutgoing = directOutgoing.map[n|n.name.subTypes(g)].flatten.filter(Edge)
		if(node.extends !== null){
			return (directOutgoing + subTypesOfDorectOutgoing +node.extends.possibleOutgoing(g) ).toSet
		}
		return (directOutgoing + subTypesOfDorectOutgoing).toSet
	}
	
	def Set<Edge> possibleIncoming(Node node,GraphModel g){
		val directIncoming = node.incomingEdgeConnections.map[connectingEdges].flatten
		if(node.incomingEdgeConnections.exists[connectingEdges.empty]){
			return this.edges(g).toSet
		}
		val subTypesOfDorectIncoming = directIncoming.map[n|n.name.subTypes(g)].flatten.filter(Edge)
		if(node.extends !== null){
			return (directIncoming + subTypesOfDorectIncoming + node.extends.possibleIncoming(g) ).toSet
		}
		return (directIncoming + subTypesOfDorectIncoming).toSet
	}
	
	def Set<Node> possibleSources(Edge edge,GraphModel g){
		val possibleDirectPredecessors  = this.nodes(g).filter[possibleOutgoing(g).contains(edge)]
		return (possibleDirectPredecessors + possibleDirectPredecessors.map[name.subTypes(g)].flatten.filter(Node)).toSet
	}
	
	def Set<Node> possibleTargets(Edge edge,GraphModel g){
		val possibleDirectSuccessors  = this.nodes(g).filter[possibleIncoming(g).contains(edge)].toSet
		return (possibleDirectSuccessors + possibleDirectSuccessors.map[name.subTypes(g)].flatten.filter(Node)).toSet
	}
	
	def Iterable<Attribute> attributesExtended(ModelElement me) {
		val attrs = new LinkedList
		attrs += me.attributes
		if(me.isExtending){
			switch(me){
				Node:attrs+=me.extends.attributesExtended
				GraphModel:attrs+=me.extends.attributesExtended
				Edge:attrs+=me.extends.attributesExtended
				UserDefinedType:attrs+=me.extends.attributesExtended
			}
		}
		attrs
	}
	
	def Iterable<EAttribute> attributesExtended(EClass me) {
		val attrs = new LinkedList
		attrs += me.eContents.filter(EAttribute)
		attrs += me.ESuperTypes.map[attributesExtended]
		attrs
	}
	
	def Iterable<EReference> referencesExtended(EClass me) {
		val attrs = new LinkedList
		attrs += me.eContents.filter(EReference)
		attrs += me.ESuperTypes.map[referencesExtended]
		attrs
	}
	
	def getPossibleContainmentTypes(GraphicalModelElement node) {
		new MGLAlternateGenerator().allContainingElements(node)
	}
	
	def ModelElement getBestContainerSuperType(GraphicalModelElement node) {
		if(node instanceof Edge) {
			return node.graphModel
		}
		val util = new InheritanceUtil
		
		val containers = node.getPossibleContainmentTypes
		if(!containers.nullOrEmpty && containers.filter(GraphModel).size == 0){
			val lmsn  = util.getLowestMutualSuperNode(containers.filter(Node))
			println(containers)
			println(lmsn)
			if(lmsn !== null){
				return lmsn;
			}
		}else if(containers.size == 1 && containers.head instanceof GraphModel){
			return node.graphModel
		}
		null		
	}
	
	def String getBestContainerSuperTypeNameAPI(GraphicalModelElement node,CharSequence fqn) {
		
		val type = node.bestContainerSuperType
		if(type === null  ) {
			if(node.isExtending) {
				//check for super type
				return node.extendingModelType.getBestContainerSuperTypeNameAPI(fqn)
			} else {
				return "graphmodel.ModelElementContainer"
			}
		}
		'''«fqn».«type.name»'''.toString
	}
	def getBestContainerSuperTypeNameDart(GraphicalModelElement node) {
		val type = node.bestContainerSuperType
		if(type === null) return "core.ModelElementContainer"
		type.name.escapeDart
	}
	
	def boolean isElliptic(ModelElement element, Styles styles) {
		if(element instanceof Node) {
			val style = element.styling(styles) as NodeStyle
			return style.mainShape instanceof Ellipse
		}
		false
	}
	def String baseTypeName(ModelElement me) {
		switch(me) {
			NodeContainer: return "Container"
			Node: return "Node"
			Edge: return "Edge"
			GraphModel: return "GraphModel"
		}
	}
	def String getName(EObject object) {
		if(object instanceof Type){
			return object.name
		}
		if(object instanceof ENamedElement) {
			return object.name
		}
		throw new IllegalStateException(object.toString)
	}
	
	def dispatch boolean isAbstract(GraphModel ce) {
		false
	}
	def dispatch boolean isAbstract(UserDefinedType ce) {
		ce.isIsAbstract
	}
	def dispatch boolean isAbstract(NodeContainer ce) {
		ce.isIsAbstract
	}
	
	def getPrimitiveDefault(String string,Attribute attr) {
		if(attr.defaultValue!==null) {
			switch(string){
			case "EInt": return '''«attr.defaultValue»L'''
			case "ELong": return '''«attr.defaultValue»L'''
			case "EBigInteger": return '''«attr.defaultValue»L'''
			case "EByte": return '''«attr.defaultValue»L'''
			case "EShort": return '''«attr.defaultValue»L'''
			case "EString": return '''"«attr.defaultValue»"'''
			default: return '''«attr.defaultValue»'''
		}
		}
		switch(string){
			case "EBoolean": return '''false'''
			case "ELong": return '''0L'''
			case "EBigInteger": return '''0L'''
			case "EByte": return '''0L'''
			case "EShort": return '''0L'''
			case "EFloat": return '''0.0'''
			case "EBigDecimal": return '''0.0'''
			case "EInt": return '''0L'''
			case "EDouble": return '''0.0'''
			default: return '''null'''
		}
	}
	
	
	def packagePath(GraphModel g) '''«g.package.lowEscapeJava.replaceAll("\\.","/")»'''
	
	def dispatch apiFQN(GraphModel g) '''«g.package.lowEscapeJava».«g.name.lowEscapeJava»'''
	def dispatch apiFQN(EPackage g) '''«g.name.lowEscapeJava»'''
	def dispatch apiPath(GraphModel g) '''«g.packagePath»/«g.name.lowEscapeJava»'''
	def factoryPath(GraphModel g) '''«g.packagePath»/factory'''
	def factoryFQN(GraphModel g) '''«g.package.lowEscapeJava».factory'''
	def dispatch apiImplFQN(GraphModel g) '''«g.apiFQN».impl'''
	def dispatch apiImplPath(GraphModel g) '''«g.apiPath»/impl'''
	
	def dispatch dywaFQN(GraphModel g) '''de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava»'''
	def dispatch dywaFQN(EPackage g) '''de.ls5.dywa.generated.entity.«g.name.escapeJava»'''
	
	def dispatch dywaControllerFQN(GraphModel g) '''de.ls5.dywa.generated.controller.info.scce.pyro.«g.name.escapeJava»'''
	def dispatch dywaControllerFQN(EPackage g) '''de.ls5.dywa.generated.controller.«g.name.escapeJava»'''
	
	
	def dispatch apiPath(EPackage g) '''«g.name.lowEscapeJava»'''
	def dispatch apiImplFQN(EPackage g) '''«g.apiFQN».impl'''
	def dispatch apiImplPath(EPackage g) '''«g.apiPath»/impl'''
	
	def jarFilename(String path) {
		if(path.contains("/")) {
			return path.subSequence(path.lastIndexOf("/")+1,path.lastIndexOf("."));
		}
		path.subSequence(0,path.lastIndexOf("\\."));
	}
}