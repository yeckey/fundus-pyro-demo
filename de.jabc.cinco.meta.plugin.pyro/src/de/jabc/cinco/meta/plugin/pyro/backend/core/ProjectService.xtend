package de.jabc.cinco.meta.plugin.pyro.backend.core

import de.jabc.cinco.meta.plugin.pyro.util.Generatable
import de.jabc.cinco.meta.plugin.pyro.util.GeneratorCompound
import de.jabc.cinco.meta.plugin.pyro.util.EditorViewPluginRegistry
import de.jabc.cinco.meta.plugin.pyro.util.EditorViewPlugin
import java.util.List

class ProjectService extends Generatable {
	List<EditorViewPlugin> eps

	new(GeneratorCompound gc) {
		super(gc)
		
		eps = new EditorViewPluginRegistry().getPlugins(gc);
	}

	def fileNameDispatcher() '''ProjectService.java'''

	def contentDispatcher() '''	
		package info.scce.pyro.core;
		
		import com.fasterxml.jackson.core.JsonProcessingException;
		import com.fasterxml.jackson.databind.DeserializationFeature;
		import com.fasterxml.jackson.databind.MapperFeature;
		import com.fasterxml.jackson.databind.ObjectMapper;
		import com.fasterxml.jackson.databind.SerializationFeature;
		import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
		import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroProjectController;
		import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroUserController;
		import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroOrganizationController;
		import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroGraphModelPermissionVectorController;
		import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroEditorGridController;
		import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroEditorGridItemController;
		import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroEditorWidgetController;
		import de.ls5.dywa.generated.entity.info.scce.pyro.core.*;
		import de.ls5.dywa.generated.util.Identifiable;
		import info.scce.pyro.core.rest.types.*;
		import info.scce.pyro.core.rest.types.PyroProject;
		import info.scce.pyro.core.rest.types.PyroUser;
		import info.scce.pyro.core.rest.types.PyroOrganization;
		import info.scce.pyro.rest.PyroSelectiveRestFilter;
		import info.scce.pyro.sync.ProjectWebSocket;
		import info.scce.pyro.sync.WebSocketMessage;
		
		import javax.ejb.Singleton;
		import java.util.*;
		import java.util.stream.Collectors;
		import java.util.stream.Stream;
		
		@Singleton
		@javax.transaction.Transactional
		public class ProjectService {
		
		    @javax.inject.Inject
		    private GraphModelController graphModelController;
		
			@javax.inject.Inject
			private PyroProjectController projectController;
			
			@javax.inject.Inject
			private PyroOrganizationController organizationController;
			
			@javax.inject.Inject
		    private PyroGraphModelPermissionVectorController permissionController;
		
			@javax.inject.Inject
			private PyroEditorGridController editorGridController;
			
			@javax.inject.Inject
			private PyroEditorGridItemController editorGridItemController;
			
			@javax.inject.Inject
			private PyroEditorWidgetController editorWidgetController;
		
		    @javax.inject.Inject
		    private ProjectWebSocket projectWebSocket;
		    
		    @javax.inject.Inject
		    private EditorLayoutService editorLayoutService;
				
		    public void deleteById(de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser user, final long id) {
		        final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject pp = projectController.read(id);
		
		        graphModelController.checkPermission(pp);
		        
		        if(pp.getowner().equals(user)){
		            pp.getowner().getownedProjects_PyroProject().remove(pp);
		            pp.setowner(null);
		            graphModelController.removeFolder(pp,null);
		            projectWebSocket.updateUserList(pp.getDywaId(), Collections.emptyList());
		        } else {
		            projectWebSocket.updateUserList(pp.getDywaId(), Stream.of(pp.getowner().getDywaId()).collect(Collectors.toList()));
		        }
		        
		        deletePermissionVectors(pp);
		        deleteEditorGrids(pp);
		        
		        // remove project from organization
		        final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganization org = organizationController.read(pp.getorganization().getDywaId());
		        org.getprojects_PyroProject().remove(pp);
		    }
		
		    private void deletePermissionVectors(de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject project) {
				final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroGraphModelPermissionVector searchObject = permissionController.createSearchObject(null);
				searchObject.setproject(project);
							
				final java.util.List<de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroGraphModelPermissionVector> result = permissionController.findByProperties(searchObject);
				for (final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroGraphModelPermissionVector vector: result) {
					permissionController.delete(vector);
				}
			}
			
			private void deleteEditorGrids(de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject project) {
				final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorGrid searchObject = editorGridController.createSearchObject(null);
				searchObject.setproject(project);
				
				final java.util.List<de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorGrid> result = editorGridController.findByProperties(searchObject);
				for (final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorGrid grid: result) {
					editorGridController.delete(grid);
				}
			}
			
			public void createDefaultEditorGrid(
				final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser user,
				final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganization org) {
				
				for (final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject project: org.getprojects_PyroProject()) {
					createDefaultEditorGrid(user, project);
				}
			}
			
			public void createDefaultEditorGrid(final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject project) {
				final List<de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser> users = new ArrayList<>();
				users.addAll(project.getorganization().getowners_PyroUser());
				users.addAll(project.getorganization().getmembers_PyroUser());
				
				for (final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser user: users) {
					createDefaultEditorGrid(user, project);
				}
			}
						
			private void createDefaultEditorGrid(
				final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser user,
				final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject project) {
				
				final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorGrid grid = editorGridController.create("EditorGrid_");
				grid.setuser(user);
				grid.setproject(project);
				
				«IF !gc.editorLayout.empty»
					«FOR e:gc.editorLayout»
						{
							«e» editorLayout = new «e»();
							editorLayout.init(editorLayoutService);
							editorLayout.execute(grid);
						}
					«ENDFOR»
				«ELSE»
					
					// explorer
					final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorGridItem explorerItem = editorLayoutService.createGridArea(grid, 0L, 0L, 3L, 3L);				
					final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorWidget explorerWidget = editorLayoutService.createWidget(grid, explorerItem, "Explorer", "explorer");
					explorerItem.getwidgets_PyroEditorWidget().add(explorerWidget);
					
					// map
					final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorGridItem mapItem = editorLayoutService.createGridArea(grid, 0L, 3L, 3L, 3L);				
					final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorWidget mapWidget = editorLayoutService.createWidget(grid, mapItem, "Map", "map");
					mapItem.getwidgets_PyroEditorWidget().add(mapWidget);
					
					// canvas
					final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorGridItem canvasItem = editorLayoutService.createGridArea(grid, 3L, 0L, 6L, 6L);				
					final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorWidget canvasWidget = editorLayoutService.createWidget(grid, canvasItem, "Canvas", "canvas");
					canvasItem.getwidgets_PyroEditorWidget().add(canvasWidget);
					
					// properties
					final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorGridItem propsItem = editorLayoutService.createGridArea(grid, 3L, 6L, 6L, 3L);				
					final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorWidget propsWidget = editorLayoutService.createWidget(grid, propsItem, "Properties", "properties");
					propsItem.getwidgets_PyroEditorWidget().add(propsWidget);
					
					// palette
					final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorGridItem paletteItem = editorLayoutService.createGridArea(grid, 9L, 0L, 3L, 3L);				
					final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorWidget paletteWidget = editorLayoutService.createWidget(grid, paletteItem, "Palette", "palette");
					paletteItem.getwidgets_PyroEditorWidget().add(paletteWidget);
					
					// checks
					final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorGridItem checksItem = editorLayoutService.createGridArea(grid, 9L, 3L, 3L, 3L);				
					final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorWidget checksWidget = editorLayoutService.createWidget(grid, checksItem, "Checks", "checks");
					checksItem.getwidgets_PyroEditorWidget().add(checksWidget);
					
					// command history, not visible by default
					final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorWidget cmdHistoryWidget = editorLayoutService.createWidget(grid, null, "Command History", "command_history");
					grid.getavailableWidgets_PyroEditorWidget().add(cmdHistoryWidget);
					
					// add widgets for registered plugins
					// plugins aren't assigned a position and are not visible by default
					«FOR pc:eps.filter[pluginComponent.fetchURL!==null].map[pluginComponent]»
					final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorWidget plugin_«pc.key»Widget = editorLayoutService.createWidget(grid, null, "«pc.tab»", "«pc.key»");
					grid.getavailableWidgets_PyroEditorWidget().add(plugin_«pc.key»Widget);
					«ENDFOR»
				«ENDIF»
			}
					    
		    /**
		     * Create default graphmodel permission vectors for all users of the organization.
		     * Should be called when a new project is created.
		     * 
		     * @param project
		     */
		    public void createDefaultGraphModelPermissionVectors(de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject project) {
				final List<de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser> users = new ArrayList<>();
				users.addAll(project.getorganization().getowners_PyroUser());
				users.addAll(project.getorganization().getmembers_PyroUser());
				
				«FOR g: gc.graphMopdels»
					for (final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser user: users) {
						final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroGraphModelPermissionVector vector = permissionController.create("GraphModelPermissionVector_");
						vector.setuser(user);
						vector.setproject(project);
						vector.setgraphModelType(de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroGraphModelType.«g.name.toUnderScoreCase»);
						vector.getpermissions_PyroCrudOperation().add(de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroCrudOperation.READ);
						if (user.equals(project.getowner())) {
							vector.getpermissions_PyroCrudOperation().add(de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroCrudOperation.CREATE);
							vector.getpermissions_PyroCrudOperation().add(de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroCrudOperation.UPDATE);
							vector.getpermissions_PyroCrudOperation().add(de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroCrudOperation.DELETE);
						} else {
							«FOR right:g.defaultRights»
							vector.getpermissions_PyroCrudOperation().add(de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroCrudOperation.«right.toUpperCase»);
							«ENDFOR»
						}
					}
				«ENDFOR»
			}
		    
		    /**
		     * Create default graphmodel permission vectors for when a new user is added to an organization
		     * 
		     * @param user
		     * @param organization
		     */
		    public void createDefaultGraphModelPermissionVectors(
		    		de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser user,
		    		de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganization organization) {
		    			
				«FOR g: gc.graphMopdels»
					for (final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject project: organization.getprojects_PyroProject()) {
						final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroGraphModelPermissionVector vector = permissionController.create("GraphModelPermissionVector_");
						vector.setuser(user);
						vector.setproject(project);
						vector.setgraphModelType(de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroGraphModelType.«g.name.toUnderScoreCase»);
						vector.getpermissions_PyroCrudOperation().add(de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroCrudOperation.READ);
						«FOR right:g.defaultRights»
						vector.getpermissions_PyroCrudOperation().add(de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroCrudOperation.«right.toUpperCase»);
						«ENDFOR»
					}
				«ENDFOR»
			}
		}
		
	'''
}
