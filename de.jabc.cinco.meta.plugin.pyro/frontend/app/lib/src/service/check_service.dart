import 'dart:async';
import 'dart:html';

import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import '../model/core.dart';
import '../model/check.dart';
import 'base_service.dart';

class CheckService extends BaseService {

  Map<int,StreamController> graphModelUpdate = new Map();

  Map<String,CheckResults> results = new Map();
  Map<int,StreamController<CheckResults>> checkListeners = new Map();
  
  CheckService(Router router) : super(router);

  Stream register(int dywaId) {
    if(graphModelUpdate.containsKey(dywaId)){
      graphModelUpdate[dywaId].close();
    }
    graphModelUpdate[dywaId] = new StreamController();
    return graphModelUpdate[dywaId].stream;
  }

  void recheck(int dywaId) {
    if(graphModelUpdate.containsKey(dywaId)) {
      graphModelUpdate[dywaId].add({});
    } else {
      print("NO UPDATE");
    }
  }

  Stream<CheckResults> listen(int dywaId) {
    checkListeners[dywaId] = new StreamController<CheckResults>();
    return checkListeners[dywaId].stream;
  }

  Future<CheckResults> read(String type,GraphModel gm) async {
    return HttpRequest.request("${getBaseUrl()}/rest/${type}/checks/${gm.dywaId.toString()}/private",method: "GET", withCredentials: true)
    	.then((response){
      		var cr = CheckResults.fromJSON(response.responseText);
          //refresh checks on canvas
          if(checkListeners.containsKey(gm.dywaId)) {
            checkListeners[gm.dywaId].add(cr);
          }
      		return cr;
    	})
    	.catchError((e) {
    		// ignore 404 responses
    		if (e.currentTarget.status != 404) {
    	  		super.handleProgressEvent(e);
    	  	}
    	}, test: (e) => e is ProgressEvent);
  }
}